<?php
class Apps_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    public function get_all_apps()
    {
        $this->db->select('*');
        $this->db->from('apps');
        $this->db->where(array('status!='=>''));
        $this->db->order_by('id desc');
        $query = $this->db->get();
        return $query->result_array();
    }
	public function increment_logos_count($column,$app_id){

        $this->db->where('id',$app_id)

            ->set($column,"$column+1", FALSE)

            ->update('logos');

    }
    public function increment_share_count($column,$id){
        
        $this->db->where('id',$id)

        ->set($column,"$column+1", FALSE)

        ->update('share_count');
    }

    public function get_latest_apps_category($category_id,$required_apps) {
        $this -> db -> select('apps.id,apps.category_id,apps_categories.name as category_name,apps.title');
        $this -> db -> from('apps');
        $this->db->join('apps_categories', 'apps_categories.id = apps.category_id');
        $this -> db -> where( array('apps.category_id'=>$category_id,'apps.status'=>"active","apps_categories.status"=>"active"));
        $this->db->order_by('apps.id','desc');
        $this -> db -> limit($required_apps);
        $query = $this -> db -> get();
        return $query->result();
    }
    public function get_latest_apps($page) {
        $this -> db -> select('apps.id,apps.category_id,apps_categories.name as category_name,apps.title');
        $this -> db -> from('apps');
        $this->db->join('apps_categories', 'apps_categories.id = apps.category_id');
        $this -> db -> where( array('apps.status'=>"active","apps_categories.status"=>"active"));
        $this->db->order_by('apps.id','desc');
        $this -> db -> limit(18,$page*20);
        $query = $this -> db -> get();
        return $query->result();
    }

    public function get_category_apps($category_id,$page) {
        $this -> db -> select('apps.id,apps.category_id,apps_categories.name as category_name,apps.title');
        $this -> db -> from('apps');
        $this->db->join('apps_categories', 'apps_categories.id = apps.category_id');
        $this -> db -> where( array('apps.category_id'=>$category_id,'apps.status'=>"active","apps_categories.status"=>"active"));
        $this->db->order_by('apps.id','desc');
        $this -> db -> limit(12,$page*10);
        $query = $this -> db -> get();
        return $query->result();
    }
    public function get_all_app_details($app_id) {
        $this -> db -> select('*');
        $this -> db -> from('apps');
        $this -> db -> where( array('id'=>$app_id,'status'=>"active"));
        $this -> db -> limit(1);
        $query = $this -> db -> get();
        return $query->result();
    }
    public function get_app_details($app_id) {
        $this -> db -> select('id,category_id,title,picture_dimensions,picture_position,users,shares,logic,app_description,autoshare');
        $this -> db -> from('apps');
        $this -> db -> where( array('id'=>$app_id,'status'=>"active"));
        $this -> db -> limit(1);
        $query = $this -> db -> get();
        return $query->result();
    }
    public function get_apps_count($category_id){
        $this -> db -> select('count(id) as count');
        $this -> db -> from('apps');
        $this -> db -> where( array('apps.category_id'=>$category_id));
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function is_user_exists($facebook_id){
        $this -> db -> select('id');
        $this -> db -> from('users');
        $this -> db -> where( array('facebook_id'=>$facebook_id));
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function insert_user($user_data){
        $this->db->insert('users', $user_data);
        return $this->db->insert_id();
    }
    public function add_polls_db($user_data){
        $this->db->insert('polls', $user_data);
        return $this->db->insert_id();
    }
    public function add_options_db($user_data){
        $this->db->insert('options', $user_data);
        return $this->db->insert_id();
    }
    public function upload_images_db($user_data){
        $this->db->insert('upload_images', $user_data);
        return $this->db->insert_id();
    }
    public function get_polls_by_category($category){
        $this -> db -> select('id,category_id,title,slug,comment_vote,comments,description,options,status');
        $this -> db -> from('polls');
        $this -> db -> where( array('category_id'=>$category,'status'=>'active'));
        $this->db->order_by('id','desc');
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_divisions(){
        $this -> db -> select('id,name,title,status');
        $this -> db -> from('divisions');
        $this -> db -> where( array('status'=>'active'));
        $this->db->order_by('id','desc');
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_latest_polls_index(){
        $this -> db -> select('*');
        $this -> db -> from('polls');
        $this -> db -> where( array('status'=>'active','is_latest'=>1));
        $this->db->order_by('id','desc');
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_polls_index_limit($limit,$start){
        $this -> db -> select('*');
        $this -> db -> from('polls');
        $this -> db -> where( array('status'=>'active'));
        $this->db->limit($limit, $start);
        $this->db->order_by('id','desc');
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_polls_index(){
        $this -> db -> select('*');
        $this -> db -> from('polls');
        $this -> db -> where( array('status'=>'active'));
        $this->db->order_by('id','desc');
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_polls_index_admin(){
        $this -> db -> select('*');
        $this -> db -> from('polls');
		$this -> db -> where( array('status'=>'active'));
        $this->db->order_by('id','desc');
        $query = $this -> db -> get();
        return $query ->result();
    }
     public function get_polls($poll_id){
        $this -> db -> select('*');
        $this -> db -> from('polls');
        $this -> db -> where( array('id'=>$poll_id,'status'=>'active'));
        $this->db->order_by('id','desc');
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_options_by_poll_id($poll_id){
        $this -> db -> select('id,poll_id,option,status');
        $this -> db -> from('options');
        $this -> db -> where( array('poll_id'=>$poll_id,'status'=>'active'));
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_trending_poll(){
        
        
        $query = $this -> db -> query("SELECT poll_id, count(poll_id) as attempts_count FROM poll_attempts group by poll_id ORDER by attempts_count");
        return $query ->result();
    }
    public function get_poll_result($poll_id){
        
        $query = $this->db->query("SELECT poll_data_with_options.option, poll_data_with_options.id as option_id, poll_data_with_options.poll_id, IFNULL(attempts_grouped_by_poll_id.attempts_count, 0) as attempts_count FROM (SELECT poll_id, option_id, count(option_id) as attempts_count FROM poll_attempts WHERE poll_id=$poll_id group by option_id) as attempts_grouped_by_poll_id RIGHT JOIN (SELECT * FROM options WHERE poll_id=$poll_id && status = 'active') as poll_data_with_options ON attempts_grouped_by_poll_id.option_id = poll_data_with_options.id");

        
        return $query ->result();
    }
    public function get_total_count_by_poll_id($poll_id){
        $this -> db -> select('id');
        $this -> db -> from('poll_attempts');
        $this -> db -> where( array('poll_id'=>$poll_id));
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    
    public function get_total_counts(){
        $this -> db -> select('id');
        $this -> db -> from('poll_attempts');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    public function insert_poll($user_data){
        $this->db->insert('poll_attempts', $user_data);
        return $this->db->insert_id();
    }
    public function get_languages(){
        $this -> db -> select('id,name,language,path,icon,status');
        $this -> db -> from('languages');
        $this -> db -> where( array('status'=>'active'));
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_category_name($category_id){
        $this -> db -> select('id,name,slug,status');
        $this -> db -> from('add_category');
        $this -> db -> where( array('status'=>'active','id'=>$category_id));
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_category_by_language($language){
        $this -> db -> select('id,name,slug,is_trending,status');
        $this -> db -> from('add_category');
        $this -> db -> where( array('language'=>$language));
        $this -> db -> where( array('is_trending'=>1));
        $this -> db -> where( array('status'=>'active'));
        $query = $this -> db -> get();
        
        return $query ->result();
    }
    public function view_category_by_language($language){
        $this -> db -> select('id,name,slug,is_trending,status');
        $this -> db -> from('add_category');
        $this -> db -> where( array('language'=>$language));
        $this -> db -> where( array('status'=>'active'));
        $query = $this -> db -> get();
        
        return $query ->result();
    }
    public function view_polls_by_category($category_id){
        $this -> db -> select('*');
        $this -> db -> from('polls');
        $this -> db -> where( array('category_id'=>$category_id));
        $this->db->order_by('id','desc');
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_trending_polls($categories){
        $this -> db -> select('*');
        $this -> db -> from('polls');
        $this -> db -> where_in('id',$categories);
        $this -> db -> where( array('status'=>'active'));
        $order = sprintf('FIELD(id, %s)', implode(', ', $categories));
        $this->db->order_by($order);
        //$this -> db -> where_in('id',$random_array);
        
        $query = $this -> db -> get();
        
        return $query ->result();
    }
     public function get_trending_images_by_language($categories){
        $this -> db -> select('id,category_id,path,status');
        $this -> db -> from('upload_images');
        $this -> db -> where_in('category_id',$categories);
        $this -> db -> where( array('status'=>'active'));
        $this->db->order_by('id','desc');
        //$this -> db -> where_in('id',$random_array);
        
        $query = $this -> db -> get();
        
        return $query ->result();
    }
    public function get_images_by_category($category){
        $this -> db -> select('id,category_id,path,status');
        $this -> db -> from('upload_images');
        $this -> db -> where( array('category_id'=>$category));
        $this -> db -> where( array('status'=>'active'));
        $this->db->order_by('id','desc');
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_image_data_by_id($image_id){
        $this -> db -> select('id,category_id,path');
        $this -> db -> from('upload_images');
        $this -> db -> where( array('id'=>$image_id));
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function edit_images_by_category($category,$id){
        $this -> db -> select('id,category_id,path,image_type,img_shape,img_width,img_height,img_x,img_y,text_x,text_y,text_size,text_color,font');
        $this -> db -> from('upload_images');
        $this -> db -> where( array('id'=>$id));
        $this -> db -> where( array('category_id'=>$category));
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function update_images_db($id,$category,$post_data){
        $this -> db -> select('id,category_id,path,image_type,img_shape,img_width,img_height,img_x,img_y,text_x,text_y,text_size,text_color');
        $this -> db -> where( array('id'=>$id));
        $this -> db -> where( array('category_id'=>$category));
        $this->db->update('upload_images', $post_data);
        
        return true;
    }
    public function update_polls($poll_id,$poll_data){
        $this -> db -> select('id,category_id,title,slug,comment_vote,comments,description,options,status');
        $this -> db -> where( array('id'=>$poll_id));
        $this->db->update('polls', $poll_data);
        
        return true;
    }
    public function update_trending_category($category_id,$is_trending){
       $this->db->where('id',$category_id)
            ->set('is_trending',$is_trending, FALSE)
            ->update('add_category');
    }
    public function update_image_downloads($insert_id){
        $this->db->where('id',$insert_id)
            ->set('downloads_count',"downloads_count+1", FALSE)
            ->set('is_downloaded',1, FALSE)
            ->update('statistics');
    }
    public function insert_statistics($user_data){
        $this->db->insert('statistics', $user_data);
        return $this->db->insert_id();
    }
    public function insert_profile_statistics($user_data){
        $this->db->insert('profile_pictures', $user_data);
        return $this->db->insert_id();
    }
    public function get_profile_pic($profile_pic_id){
        $this -> db -> select('id,path');
        $this -> db -> from('profile_pictures');
        $this -> db -> where( array('id'=>$profile_pic_id));
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_user($facebook_id){
        $this -> db -> select('id,updated_at,gender');
        $this -> db -> from('users');
        $this -> db -> where( array('facebook_id'=>$facebook_id));
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_statistics($insert_id){
        $this -> db -> select('id,user_name,image_path,category_id,image_id');
        $this -> db -> from('statistics');
        $this -> db -> where( array('id'=>$insert_id));
        $query = $this -> db -> get();
        return $query ->result();
    }
    public function get_adds_from_db($add_type,$add_num) {
        $this -> db -> select('adds.script');
        $this -> db -> from('adds');
        $this -> db -> where(array('adds.type'=>$add_type,'adds.status'=>"active"));
        $this -> db -> limit($add_num);
        $query = $this -> db -> get();
        return $query->result();
    }

    public function get_apps_as_adds($category_id,$required_apps) {
        $this -> db -> select('apps.id,apps.category_id,apps_categories.name as category_name,apps.title');
        $this -> db -> from('apps');
        $this->db->join('apps_categories', 'apps_categories.id = apps.category_id');
        $this -> db -> where( array('apps.category_id'=>$category_id,'apps.status'=>"active","apps_categories.status"=>"active"));
        $this->db->order_by('apps.id','desc');
        $this -> db -> limit($required_apps);
        $query = $this -> db -> get();
        return $query->result();
    }

    public function get_category_names_menu() {
        $this -> db -> select('apps_categories.id,apps_categories.name');
        $this -> db -> from('apps_categories');
        $this -> db -> where(array('apps_categories.status'=>"active"));
        $query = $this -> db -> get();
        return $query->result();
    }
    public function get_meta_tags($table,$id){
        $this -> db -> select('meta_title,meta_description,meta_keywords');
        $this -> db -> from($table);
        $this -> db -> where(array('id'=>$id));
        $query = $this -> db -> get();
        return $query->result();
    }
    public function increment_count($column,$app_id){
        $this->db->where('id',$app_id)
            ->set($column,"$column+1", FALSE)
            ->update('apps');
    }
    public function update_pycker_clicks($slot_id){
        $this->db->where('slot_id',$slot_id)
            ->set('clicks',"clicks+1", FALSE)
            ->update('pycker_clicks');
    }
    public function insert_app($data) {
        $last_insert_id=$this->db->insert('apps', $data);
        return $this->db->insert_id();
    }
    public function count_rows($tbl, $cond = "")
    {
        $this->db->select('COUNT(*) as num');

        $this->db->from($tbl);

        $this->db->where($cond);

        $query = $this->db->get();

        $row = $query->row();

        return $row->num;
    }
    public function myhash($password)
    {

        $salt = "omf@siriit";

        $hash = sha1($salt . $password);

        // make it take 1000 times longer

        for ($i = 0; $i < 2000; $i++) {

            $hash = sha1($hash);

        }

        return $hash;

    }
    public function FetchData($tbl, $sel = '*', $cond = '', $alterBy = '', $orderBy = 'ASC')
    {
        $this->db->select($sel);
        $this->db->from($tbl);
        $this->db->where($cond);
        if ($alterBy != "") {
            $this->db->order_by($alterBy, $orderBy);
        }
        $query = $this->db->get();

        return $query->result_array();
    }
    public function FetchSingle($tbl, $sel, $cond)
    {

        $this->db->select($sel);

        $this->db->from($tbl);

        $this->db->where($cond);

        $query = $this->db->get();

        $row = $query->row();

        if (isset($row->$sel)) {

            $dts = $row->$sel;

        } else {

            $dts = false;

        }

        return $dts;

    }
    public function UpdateData($tbl, $data, $where)
    {


        $str = $this->db->update($tbl, $data, $where);

        return $this->db->affected_rows();

    }
    public function InsertData($tbl, $data)
    {

        $str = $this->db->insert($tbl, $data);

        return $this->db->insert_id();

        //	echo $this->db->last_query();

    }
    public function InsertBatchData($tbl, $data)
    {

        $str = $this->db->insert_batch($tbl, $data);

        // $str = $this->db->insert($tbl, $data);

        return $this->db->insert_id();

    }
    public function DeleteData($tbl, $cond)
    {

        $this->db->where($cond);

        $this->db->delete($tbl);

        return $this->db->affected_rows();

    }
    public function meta_insert($tbl, $data, $where)
    {

        foreach ($where as $ky => $whe) {

            $con_field = $ky;

            $con_data = $whe;

        }

        $status = "";

        foreach ($data as $key => $val) {


            if ($this->count_rows($tbl, array("field_name" => $key, "$con_field" => $con_data)) > 0) {

                $status = $this->UpdateData($tbl, array('field_value' => $val), array("field_name" => $key, "$con_field" => $con_data));

            } else {

                $status = $this->InsertData($tbl, array("field_name" => $key, 'field_value' => $val, "$con_field" => $con_data));

            }

            //echo $this->db->last_query();

        }

        if ($status) {

            return true;

        } else {

            return false;

        }

    }
    public function userid()
    {

        $username = $this->native_session->userdata('username');

        return $uid = $this->FetchSingle('user', 'id', array('user_name' => $username));

    }
    public function do_upload($filename, $config)
    {


        $config['max_size'] = '30000';

        $config['max_width'] = '4096';

        $config['max_height'] = '4096';

        $this->load->library('upload', $config);


        if (!$this->upload->do_upload($filename)) {

            return $error = array('error' => $this->upload->display_errors());

        } else {

            return $data = array('upload_data' => $this->upload->data());

        }

    }
    public function UpdateDataString($tbl, $data, $where)
    {


        $str = $this->db->query($this->db->update_string($tbl, $data, $where));

        return $this->db->affected_rows();

    }
    public function FetchQuery($tbl, $sel = '*', $cond = '')
    {

        if ($cond != "") {

            $query = $this->db->query("SELECT $sel FROM {PRE}$tbl WHERE $cond");

        } else {

            $query = $this->db->query("SELECT $sel FROM {PRE}$tbl");

        }

        return $query->result_array();

    }
    public function increment_field($tbl, $fields, $cond)
    {
        if (!empty($fields)) {
            foreach ($fields as $ky => $field) {
                $this->db->set($ky, "`$ky`+$field", FALSE);
            }

        }

        $this->db->where($cond);
        $x = $this->db->update($tbl);

        return $x;
    }
    public function decrement_field($tbl, $fields, $cond)
    {
        if (!empty($fields)) {
            foreach ($fields as $ky => $field) {
                $this->db->set($ky, "`$ky`-$field", FALSE);
            }

        }

        $this->db->where($cond);
        $x = $this->db->update($tbl);

        return $x;
    }
    public function validateForm($arr, $arr1 = array())
    {
        $this->load->library('form_validation');
        if (!empty($arr)) {
            foreach ($arr as $ky => $ar) {
                $ky1 = $ky;
                //print_r($arr1);
                if (array_key_exists($ky, $arr1)) {
                    $ky1 = $arr1[$ky];
                }
                $this->form_validation->set_rules($ky, $ky1, $ar);
            }
        }

        if ($this->form_validation->run() == FALSE) {

            $errors = array();
            foreach ($arr as $kk => $field) {
                if (form_error($kk)) {

                    $errors[$kk] = form_error($kk, '&nbsp;', '&nbsp;');
                }
            }

            return $errors;
        } else {
            return true;
        }
    }
}