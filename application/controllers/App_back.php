<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Apps extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	function __construct() {

		parent::__construct();

		$this->load->model("Apps_model");

                //session_write_close( );

	}

	private $FB_APP_ID = "487651168108939";

	private $FB_SECRET_KEY = "a2aa032e417ac39047414760631a6f75";

	public function testing_generic_function($app){

		//$this->print_exit($app);

		$user_id = 37;//$this->session->userdata("user_id");

		$where_conditon = array("app_id"=>$app[0]->id,"status"=>"active");

		$image_objects = $this->Apps_model->FetchData('image_objects','*',$where_conditon,'','desc');

		$text_objects = $this->Apps_model->FetchData('text_objects','*',$where_conditon,'','desc');

		$source_img = "container/input-images/".$app[0]->id."/source.jpg";

		if($app[0]->type=='random'){

			$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");

			$source_img =  "container/input-images/" . $app[0]->id."/".rand(1, count($images) - 2).".jpg";

		}

		$this->merge_data($source_img,$image_objects,$text_objects,"container/output-images/".$app[0]->id."/".$user_id.".jpg",$user_id,$app[0]->type);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" .$user_id);

	}

	public function testing_redirect_to_logic($category_name,$app_id){

            

		$app = $this->get_app_details($app_id);

		$app[0]->category_name = $category_name;

		$logic = 'testing_generic_function';

		$this->{$logic}($app);

	}

	public function index($page=0){

		/*$apps = array();



		$key=0;

		for($key;$key<9;$key++)

		{

				$apps[$key]= $this->get_latest_apps_category($key + 1,6);

		}

		$temp= $apps[0];

		$apps[0] = $apps[4];

		$apps[4] = $temp;

		$data['apps'] = $apps;*/

		$data['page'] = $page+1;

		$data['app'] =  $this->Apps_model->get_latest_apps($page);

		$data['header_adds']=$this->get_adds_from_db('header',1);

		$data['footer_adds']=$this->get_adds_from_db('footer',1);

		if(count($this->get_adds_from_db('side',1))==0) { //if all add scripts are in inactive mode display

			$data['app_adds']=$this->get_apps_as_adds(1,4);

		}

		else {

			$data['side_adds']=$this->get_adds_from_db('side',1);

		}

		$data['meta_tags'] = $this->get_meta_tags('apps_categories',10);

		$data['menu_names']=$this->get_category_names_menu();

		$this->load->view('templates/view_header',$data);

		$this->load->view('view_index');

		$this->load->view('templates/view_footer');

	}

	public function list_apps(){

		$apps =  $this->Apps_model->get_all_apps();

		$data['apps'] = $apps;

		$this->load->view('list_apps',$data);

	}



	public function update_app_settings($app_id){$post_data = $this->input->post();

		//$post_data['global_object_json'] = '{"image_objects":{"image_object_count0":{"position_x":"89","position_y":"49","width":"182","height":"172","shape":"circle","angle":"0"}},"friends_names_objects":{},"other_text_objects":{"other_text_objects0":{"position_x":"89","position_y":"235","width":"185","height":"71","type":"own_name","color":"00,00,00","size":"20","style":"robotocondensedbold.ttf","text":"sample text","angle":"0"}}}';

		$post_data_decoded = json_decode($post_data['global_object_json']);

		//$this->print_exit($post_data_decoded);

		$image_objects = $post_data_decoded->image_objects;

		$friend_text_object="";

		if(isset($post_data_decoded->friends_names_objects)){

			if(count(get_object_vars($post_data_decoded->friends_names_objects))){

				$friend_text_object = get_object_vars($post_data_decoded->friends_names_objects);

				ksort($friend_text_object);

			}

		}

		$other_text_object = $post_data_decoded->other_text_objects;

		$image_objects = get_object_vars($image_objects);

		$other_text_object = get_object_vars($other_text_object);

		ksort($image_objects);

		ksort($other_text_object);

		$inserted_images = array();

		for($i=0;$i<count($image_objects);$i++){

			$type = "";

			if($i==0){

				$type = "profile_pic";

			}else{

				$type = "friend_pic";

			}

			$object = array("app_id"=>$app_id,"type"=>$type,"shape"=>$image_objects["image_object_count".$i]->shape,

				"height"=>$image_objects["image_object_count".$i]->height,"width"=>$image_objects["image_object_count".$i]->width,

				"position_x"=>$image_objects["image_object_count".$i]->position_x,"position_y"=>$image_objects["image_object_count".$i]->position_y,

				"angle"=>$image_objects["image_object_count".$i]->angle

			);

			$inserted_images[$i] = $this->Apps_model->InsertData('image_objects',$object);

		}

		for($i=0;$i<count($other_text_object);$i++){

			//"style"=>$other_text_object['other_text_object'.$i]->style

			$img_refference = "";

			if($i==0){

				$img_refference = $inserted_images[0];

			}else{

				$img_refference = "";

			}

			$object =  array("app_id"=>$app_id,"type"=>$other_text_object['other_text_objects'.$i]->type,

				"size"=>$other_text_object['other_text_objects'.$i]->size,

				"style"=>$other_text_object['other_text_objects'.$i]->style,

				"position_x"=>$other_text_object['other_text_objects'.$i]->position_x,

				"position_y"=>$other_text_object['other_text_objects'.$i]->position_y,

				"width"=>$other_text_object['other_text_objects'.$i]->width,

				"height"=>$other_text_object['other_text_objects'.$i]->height,

				"angle"=>$other_text_object['other_text_objects'.$i]->angle,

				"img_reference"=>$img_refference

			);

			$this->Apps_model->InsertData('text_objects',$object);

		}

		if($friend_text_object!=""){

			for($i=0;$i<count($friend_text_object);$i++){

				//"style"=>$friend_text_object['friends_names_objects'.$i]->style

				$object = array("app_id"=>$app_id,"type"=>$friend_text_object['friends_names_objects'.$i]->type,

					"size"=>$friend_text_object['friends_names_objects'.$i]->size,

					"style"=>$friend_text_object['friends_names_objects'.$i]->style,

					"position_x"=>$friend_text_object['friends_names_objects'.$i]->position_x,"position_y"=>$friend_text_object['friends_names_objects'.$i]->position_y,"angle"=>$friend_text_object['friends_names_objects'.$i]->angle,

					"img_reference"=>$inserted_images[$i+1]

				);

				$this->Apps_model->InsertData('text_objects',$object);

			}

		}



		redirect('http://localhost/witalks_admin/list-apps');

	}

	public function app_settings($app_id){

		$app = $this->Apps_model->get_all_app_details($app_id);

		$images = glob("assets/fonts/*.ttf");

		for($i=0;$i<count($images);$i++){

			$fonts_array[$i]=explode("/", $images[$i]);

		}

		//$this->print_exit($app);

		$data['fonts']=$fonts_array;

		$data['app']=$app;



		$this->load->view('app_settings',$data);

		//$this->load->view('admin_panel',$data);

	}

	public function add_app(){

	



		$login_details = 	$this->input->post();

		

		if($login_details['name']=='admin_sam' && $login_details['pass']=='admin@sam'){

		//$this->session->set_userdata($login_details);

		

		$user_data['login_name'] = $login_details['name'];

		$user_data['login_pass'] = $login_details['pass'];

		$this->session->set_userdata($user_data);

		$this->load->view('add_new_app');

		//$this->print_exit($this->input->post());

		

		}

		else{

			echo "***Fuck off***";

		}



	}

	public function addmin_add_app(){

	$user_name = $this->session->userdata("login_name");

	

	if($this->session->userdata("login_name")=='admin_sam'){

		if($this->input->post('submit')){

			$post_data = 	$this->input->post();

			

			$data = array("category_id"=>$post_data['category_id'],"title"=>$post_data['title'],

				"meta_keywords"=>$post_data['meta_keywords'],

				"meta_title"=>$post_data["meta_title"],"meta_description"=>$post_data["meta_description"],"status"=>"inactive",

				"type"=>$post_data['type'],"background"=>$post_data['background'],"image_object_count"=>$post_data['image_objects_count'],"friends_names_objects"=>$post_data['friends_names_objects'],"other_text_objects"=>$post_data['other_text_objects']);

			//$this->print_exit($data);

			$last_insert_id = $this->Apps_model->InsertData('apps',$data);

			$temp_inputs_images = glob("container/temp-inputs/*.jpg");

			if (!file_exists("container/input-images/" . $last_insert_id)) {

				mkdir("container/input-images/" . $last_insert_id, 0777, true);

			}

			if (!file_exists("container/output-images/" . $last_insert_id)) {

				mkdir("container/output-images/" . $last_insert_id, 0777, true);

				mkdir("container/output-images/" . $last_insert_id."/friends", 0777, true);

			}

			redirect('app-settings/'.$last_insert_id);

		}else{

			//$this->load->view('view_add_app');

			$this->load->view('add_new_app');

		}

	}

	else{

		echo "You are at wrong place";

	}

		

	}

	public function get_latest_apps_category($category_id,$required_apps){

		

		return $this->Apps_model->get_latest_apps_category($category_id,$required_apps);

	}

	public function get_category_apps($category_id,$page){

		

		return $this->Apps_model->get_category_apps($category_id,$page);

	}

	public function get_apps_count($category_id){

		

		return $this->Apps_model->get_apps_count($category_id);

	}

    public function view_ads(){

        $ap_links = array("http://witalks.com/app/telugu/your-favorite-pellichoopulu-dialogue/1/212","http://witalks.com/app/telugu/which-dialogue-of-mahesh-is-apt-for-you-/1/204","http://witalks.com/app/telugu/which-dialogue-of-allu-arjun-is-apt-for-you-/1/203","http://witalks.com/app/telugu/which-dialogue-of-jr-ntr-is-apt-for-you/1/200");

        $ads  = array(212,204,203,200);

        $source = rand(0,count($ads)-1);

        $data['ad'] = $ads[$source];

        $data['app_link']=$ap_links[$source];

        $this->load->view('view_ads',$data);

    }

	public function view_all_apps($category_name,$category_id,$page){

		$apps['category_name'] = $category_name;

		$apps['category_id']   =$category_id;

		$apps = $this->get_category_apps($category_id,$page);

		$apps_count = $this->get_apps_count($category_id);

		if(count($apps)){

			$data['apps'] = $apps;

			$data['apps_count'] = $apps_count;

			$data['menu_names']=$this->get_category_names_menu();

			$data['header_adds']=$this->get_adds_from_db('header',1);

			$data['footer_adds']=$this->get_adds_from_db('footer',1);

			if(count($this->get_adds_from_db('side',1))==0) { //if all add scripts are in inactive mode display

				if($category_id==1){

					$data['app_adds']=$this->get_apps_as_adds($category_id+1,4);

				}elseif($category_id==9){

					$data['app_adds']=$this->get_apps_as_adds($category_id-1,4);

				}elseif($category_id>1 && $category_id<9){

					$data['app_adds']=$this->get_apps_as_adds($category_id+1,4);

				}

			}

			else{

				$data['side_adds']=$this->get_adds_from_db('side',$category_id);

			}

			$data['meta_tags'] = $this->get_meta_tags('apps_categories',$category_id);

			$this->load->view('templates/view_header',$data);

			$this->load->view('view_category');

			$this->load->view('templates/view_footer');

		}else{

			echo "Error";

		}

	}

	public function get_app_details($app_id){

		

		return $this->Apps_model->get_all_app_details($app_id);

	}

	public function redirect_to_logic($category_name,$app_id){

             if($this->input->get('share') || $this->session->userdata('share')=='no'){

                    $this->session->set_userdata("share","no");                

                }else{

                    $this->session->set_userdata("share","yes");

                }

		$app = $this->get_app_details($app_id);

		$app[0]->category_name = $category_name;
                if($this->session->userdata('user_id')==null){
                    $this->session->set_userdata('session_lost',true);
                    $this->view_single_app(strtolower($app[0]->category_name),strtolower(str_replace("?","",str_replace(" ","-",$app[0]->title))),$app[0]->category_id,$app[0]->id);
                }
		$logic = $app[0]->logic;

		$this->{$logic}($app);

	}

	public function view_single_app($category_name,$app_title,$category_id,$app_id){

                   

		$app = $this->get_app_details($app_id);

		$login_url = "";

		$login_type = "";

		if(count($app)>0) {

			if ($this->is_logged_in()) {

				$is_first_time = $this->session->userdata("is_first_time_" . $app_id);

				if ($is_first_time == NULL) {

					$this->increment_count("users", $app_id);

					$this->session->set_userdata("is_first_time_" . $app_id, "true");

				}

				$login_type ="normal";
                                if($this->session->userdata('session_lost')){
                                    redirect("app/redirect-to-logic/$category_name/$app_id");
                                }
                                redirect("app/redirect-to-logic/$category_name/$app_id");
				$login_url = base_url() . "app/redirect-to-logic/$category_name/$app_id";

			} else {

				$login_type ="facebook";

				$this->session->set_userdata(array("redirect_app" => array("category_name" => $category_name, "category_id" => $category_id, "app_title" => $app_title, "app_id" => $app_id)));

				$login_url = $this->get_login_url();
                                if($this->session->userdata('session_lost')){
                                    redirect($login_url);
                                }
			}

		}

		$app[0]->category_name = $category_name;

		$app[0]->category_id = $category_id;

		$data['login_url'] = $login_url;

		$data['login_type'] = $login_type;

		$data['app'] = $app;

		$data['menu_names']=$this->get_category_names_menu();

		$data['header_adds']=$this->get_adds_from_db('header',1);

		$data['footer_adds']=$this->get_adds_from_db('footer',1);

		//$data['apps'] = $this->get_apps_like_index();

		$data['suggestion_apps'] = $this->get_latest_apps_category($category_id, 12);

		if(count($this->get_adds_from_db('side',1))==0) { //if all add scripts are in inactive mode display

			if($category_id==1){

				$data['app_adds']=$this->get_apps_as_adds($category_id+1,4);

			}elseif($category_id==9){

				$data['app_adds']=$this->get_apps_as_adds($category_id-1,4);

			}

			elseif($category_id==6){

				$data['app_adds']=$this->get_apps_as_adds($category_id-1,4);

			}elseif($category_id>1 && $category_id<9){

				$data['app_adds']=$this->get_apps_as_adds($category_id+1,4);

			}

		}

		else{

			$data['side_adds']=$this->get_adds_from_db('side',$category_id);

		}

		$data['page'] = 'view_apps';

		$data['meta_tags']= $this->get_meta_tags('apps',$app_id);



		$this->load->view('templates/view_header',$data);

		$this->load->view('view_apps');

		$this->load->view('templates/view_footer');





	}

	public function profile_pic_logic($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = 1;

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/".$app[0]->id)) {

			mkdir("container/output-images/".$app[0]->id, 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$this->merge_images_jpg_png("container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		////

		

		$access_token = $this->session->userdata("access_token");

		$persmissions = file_get_contents("https://graph.facebook.com/me/permissions/publish_actions?access_token=$access_token");

                //$persmissions = $this->print_exit(json_decode($persmissions));

                $persmissions = json_decode($persmissions);

                if($persmissions->data[0]->status=='granted'){

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://graph.facebook.com/me/photos?access_token=" . $access_token);

		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, "url=http://premam.in/container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg");

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);

		curl_close ($ch);

		$array_response = json_decode($server_output);

		$this->session->set_userdata(array('array_response'=>$array_response->id));

		}

		else{

			$this->session->sess_destroy();

			redirect('http://bit.do/APSpecial');

		}

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-$source_img");

	}
	public function true_name($app)
	{
		$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");
		$source_img = rand(1, count($images) - 2);
		$user_id = $this->session->userdata("user_id");
		$user_name = $this->session->userdata("name");
		if (!file_exists("container/output-images/" . $app[0]->id)) {
			mkdir("container/output-images/" . $app[0]->id, 0777, true);
		}
		//$this->crop_pic("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", $pic_dimensions[0]);
		exec('convert -size 150x150 xc:none -fill "/home/premam/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 75,75,1,75" "/home/premam/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 26, 102);
		//$this->crop_pic("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 500);
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");
		$black = imagecolorallocate($jpg_image, 255, 255, 255);
		$font_path = 'assets/fonts/BebasNeueBold.ttf';
		$names_list = array("RULER","LEADER","POWER","SOLDIER","HAPPINESS","FIGHTER","CONQuEROR","ACTOR","PROFESSOR","CHAMPION","PROTECTOR","REBEL","DEMI GOD");
		shuffle($names_list);
		imagettftext($jpg_image, 50, 0,355, 125, $black, $font_path,explode(" ",$user_name)[0]);
		imagettftext($jpg_image, 30, 0,310, 170, $black, $font_path,"Your name means");
		imagettftext($jpg_image, 75, 0,330, 260, $black, $font_path,$names_list[0]);
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");
		imagedestroy($jpg_image);
		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");
	}
public function bikes_logic($app)
	{
		$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");
		$source_img = rand(1, count($images) - 2);
		$user_id = $this->session->userdata("user_id");
		if (!file_exists("container/output-images/" . $app[0]->id)) {
			mkdir("container/output-images/" . $app[0]->id, 0777, true);
		}
		//$this->crop_pic("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", $pic_dimensions[0]);
		exec('convert -size 174x174 xc:none -fill "/home/premam/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 87,87,1,87" "/home/premam/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 530, 78);
		//$this->crop_pic("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 500);
		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");
	}
	public function love_location($app){

		$location=$this->get_user_location();

		$love_location=array();

		if($app[0]->id==327){

			$love_location=array();

			if(isset($location->photos)){

				$photos_count=count($location->photos->data);

				$count=0;

				for($i=0;$i<$photos_count;$i++){

					if(isset($location->photos->data[$i]->place)){

						$love_location[$count]=$location->photos->data[$i]->place->name;

						$count++;

					}

				}

			}

			if($love_location==NULL){

				$love_location[0]="You Cannot Find Your SOULMATE";

				if($app[0]->id==327){

					$love_location[0]="You Cannot Find Your LOVE";

				}



			}

			//$this->print_exit($love_location);

			$source_img =1;

			$random=rand(1,10);

			$user_id = $this->session->userdata("user_id");

			if (!file_exists("container/output-images/".$app[0]->id)) {

				mkdir("container/output-images/".$app[0]->id, 0777, true);

			}

			shuffle($love_location);

			//$this->print_exit($love_location);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);

			$user_feed = $this->get_user_feed();

			if (isset($user_feed->feed)) {

				$best_friends = $this->best_friends_from_feed($user_feed->feed->data);

				if (count($best_friends)) {

					$this->get_profile_pics_new($best_friends, 5);

					$first_names = $this->get_first_names_new($best_friends, 5);

					$best_friends_keys = array_keys($best_friends);

				}

			}

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 160, 160);

			$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 160, 160);

			$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 160, 160);

			$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[3] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg", 160, 160);

			$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[4] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[4]."_cropped.jpg", 160, 160);

			$random_number=rand(0,4);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/".$app[0]->id."/".$user_id."-$random.jpg",$pic_positions[0],$pic_positions[1]);

			$this->merge_images("container/output-images/".$app[0]->id."/".$user_id."-$random.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random_number]."_cropped.jpg","container/output-images/".$app[0]->id."/".$user_id."-$random.jpg",554,105);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$random");

			//

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id ."-$random".".jpg");

			$black = imagecolorallocate($jpg_image, 255, 255, 255);

			$white = imagecolorallocate($jpg_image, 44, 44, 44);

			$red = imagecolorallocate($jpg_image, 174, 32, 32);

			$user_name = $this->session->userdata('first_name');

			$font_path = 'assets/fonts/Kozuka.otf';



			if($love_location[0]!="You Cannot Find Your LOVE"){

				if(strlen($first_names[$best_friends_keys[0]])<10){

					imagettftext($jpg_image, 20, 0, 200, 58, $black, $font_path, $first_names[$best_friends_keys[$random_number]]." Will Find Your Love");

				}

				if(strlen($first_names[$best_friends_keys[0]])>10){

					imagettftext($jpg_image, 16, 0, 182, 55, $black, $font_path, $first_names[$best_friends_keys[$random_number]]." Will Find Your Love");



				}

			}

			if($love_location[0]=="You Cannot Find Your LOVE"){

				if(strlen($first_names[$best_friends_keys[$random_number]])<15) {

					imagettftext($jpg_image, 16, 0, 190, 58, $black, $font_path, $first_names[$best_friends_keys[$random_number]] . " Cannot Find Your Love");

				}

				if(strlen($first_names[$best_friends_keys[0]])>15) {

					imagettftext($jpg_image, 12, 0, 175, 58, $black, $font_path, $first_names[$best_friends_keys[$random_number]] . " Cannot Find Your Love");

				}

			}



			if(strlen($love_location[0])>35){

				imagettftext($jpg_image, 16, 0, 238, 100, $white, $font_path, wordwrap($love_location[0],30,"\n"));



			}

			if(strlen($love_location[0])>=20&& strlen($love_location[0])<34){

				imagettftext($jpg_image, 18, 0, 240, 120, $white, $font_path, $love_location[0]);



			}

			if(strlen($love_location[0])<19){

				imagettftext($jpg_image, 20, 0, 245, 120, $white, $font_path, $love_location[0]);



			}



			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id ."-$random". ".jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-$random");



		}

		if($app[0]->id==326){

			$love_location=array();

			if(isset($location->photos)){

				$photos_count=count($location->photos->data);

				$count=0;

				for($i=0;$i<$photos_count;$i++){

					if(isset($location->photos->data[$i]->place)){

						$love_location[$count]=$location->photos->data[$i]->place->name;

						$count++;

					}

				}

			}

			if($love_location==NULL){

				$love_location[0]="You Cannot Find Your SOULMATE";

				if($app[0]->id==327){

					$love_location[0]="You Cannot Find Your LOVE";

				}



			}

			//$this->print_exit($love_location);

			$source_img =1;

			$random=rand(1,10);

			$user_id = $this->session->userdata("user_id");

			if (!file_exists("container/output-images/".$app[0]->id)) {

				mkdir("container/output-images/".$app[0]->id, 0777, true);

			}

			shuffle($love_location);

			//$this->print_exit($love_location);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);



			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/".$app[0]->id."/".$user_id."-$random.jpg",$pic_positions[0],$pic_positions[1]);

			$this->merge_images_jpg_png("container/output-images/".$app[0]->id."/".$user_id."-$random.jpg","container/input-images/".$app[0]->id."/1.png","container/output-images/".$app[0]->id."/".$user_id."-$random.jpg",82,85);

			//$this->merge_images_jpg_png("container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg","container/input-images/".$app[0]->id."/1.png","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",300,140);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$random");

			////

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id ."-$random".".jpg");

			$black = imagecolorallocate($jpg_image, 255, 255, 255);

			$white = imagecolorallocate($jpg_image, 0, 0, 0);

			$red = imagecolorallocate($jpg_image, 174, 32, 32);

			$user_name = $this->session->userdata('first_name');

			$font_path = 'assets/fonts/Kozuka.otf';



			if($love_location[0]!="You Cannot Find Your SOULMATE"){

				imagettftext($jpg_image, 12, 0, 380, 40, $white, $font_path, $user_name."'s Soulmate is waiting in");

			}



			if(strlen($love_location[0])>35){

				imagettftext($jpg_image, 18, 0, 370, 80, $white, $font_path, wordwrap($love_location[0],30,"\n"));



			}

			if(strlen($love_location[0])>=20&& strlen($love_location[0])<34){

				imagettftext($jpg_image, 18, 0, 370, 100, $white, $font_path, $love_location[0]);



			}

			if(strlen($love_location[0])<19){

				imagettftext($jpg_image, 32, 0, 370, 100, $white, $font_path, $love_location[0]);



			}



			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id ."-$random". ".jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-$random");



		}

		if(isset($location->photos)){

			$photos_count=count($location->photos->data);

			for($i=0;$i<$photos_count;$i++){

				if(isset($location->photos->data[$i]->place)){

					$love_location=$location->photos->data[$i]->place->name;

					break;

				}

			}

		}

		if($love_location==NULL){

			$love_location="You Cannot Find Your LOVE";

		}

		//$this->print_exit($love_location);

		$source_img =1;

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/".$app[0]->id)) {

			mkdir("container/output-images/".$app[0]->id, 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$this->merge_images_jpg_png("container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",85,40);

		$this->merge_images_jpg_png("container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg","container/input-images/".$app[0]->id."/1.png","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",300,140);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		////

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id ."-$source_img".".jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);

		$red = imagecolorallocate($jpg_image, 174, 32, 32);

		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/Kozuka.otf';

		if($love_location!="You Cannot Find Your LOVE"){

			imagettftext($jpg_image, 12, 0, 370, 30, $white, $font_path, $user_name."'s Love is waiting in");

		}

		if(strlen($love_location)>=20){

			imagettftext($jpg_image, 18, 0, 370, 100, $white, $font_path, $love_location);



		}

		if(strlen($love_location)<15){

			imagettftext($jpg_image, 35, 0, 370, 100, $white, $font_path, $love_location);



		}

		if(strlen($love_location)>15&& strlen($love_location)<20){

			imagettftext($jpg_image, 30, 0, 360, 100, $white, $font_path, $love_location);



		}

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id ."-$source_img". ".jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-$source_img");





	}

	public function love_count($app)

	{

		$source_img = 100;///rand(1, count($images) - 2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		//$this->crop_pic2("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 138,138);

		exec('convert -size 200x200 xc:none -fill "/home/premam/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 200,200,1,200" "/home/premam/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

		$merged_image = $this->get_merged_images_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "", 35, 82);

		$black = imagecolorallocate($merged_image, 0, 0, 0);

                


		$font_path = 'assets/fonts/gillsansbold.ttf';



		if($this->session->userdata("love_count")==NULL || count($this->session->userdata("love_count"))<1){

			$love_count = array('00','10','05','02','04','06','07','08','20','30','40','50','55','05','08','12','15','18','33','35','22','25','28','42','44','48','45','52','59');

			shuffle($love_count);



			//$letters_random = array_rand($number,26);

			$this->session->set_userdata("love_count",$love_count);



		}

		//$this->print_exit($this->session->userdata("number"));

		imagettftext($merged_image, 55, 0, 510, 160, $black, $font_path, $this->session->userdata("love_count")[0]);





		imagejpeg($merged_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-".$this->session->userdata("love_count")[0].".jpg", 80);

		imagedestroy($merged_image);

		$love_count = $this->session->userdata("love_count");



		$removed_hour = $love_count[0];

		array_shift($love_count);

		$this->session->set_userdata("love_count",$love_count);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-".$removed_hour);

	}

	public function logic_twenty($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_photos = $this->get_user_photos("","");

		$best_friends = $this->best_friends($user_photos);

		$random_percentages[0] = rand(60,100);

		$random_percentages[1] = rand(60,100);

		$random_percentages[2] = rand(60,100);

		if(count($best_friends)){

			$this->get_profile_pics_new($best_friends,3);

			$first_names = $this->get_first_names_new($best_friends,3);

			$best_friends_keys = array_keys($best_friends);

			$top=61;

			$left=42;

			//$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",180);

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",180,180);

			$this->merge_images("container/input-images/" . $app[0]->id . "/1.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", $left, $top);

			$top=99;

			$left =305;

			for($i=0;$i<3;$i++) {

				$this->crop_pic("container/user-photos/".$user_id."/friends/$best_friends_keys[$i].jpg","container/user-photos/".$user_id."/friends/$best_friends_keys[$i]_cropped.jpg",100);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", "container/user-photos/" . $user_id . "/" . "/friends/$best_friends_keys[$i]_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", $left, $top);

				$left = $left + 147;

				if($i==1){

					$left = $left-4;

				}

			}

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

			$white = imagecolorallocate($jpg_image,0,0,0);

			$font_path = 'assets/fonts/Kozuka.otf';

			$first_names_keys = array_keys($first_names);

			$top = 80;

			$left = 10;

			//imagettftext($jpg_image, 15, 0, $left, $top, $white , $font_path, $this->session->userdata('first_name'));

			$left = 300;

			$percentages_left = 340;

			for($i=0;$i<count($first_names_keys);$i++){



				if(strlen($first_names[$first_names_keys[$i]])<=5){

					$temp_left = $left+40;

				}elseif(strlen($first_names[$first_names_keys[$i]])>=5 && strlen($first_names[$first_names_keys[$i]])<11){

					$temp_left = $left+30;

				}

				else{

					$temp_left = $left;

				}

				

				imagettftext($jpg_image, 15, 0, $temp_left, $top, $white , $font_path, $first_names[$first_names_keys[$i]]);

				imagettftext($jpg_image, 25, 0, $percentages_left, $top+150, $white , $font_path, $random_percentages[$i]."%");

				$left= $left+140;

				$percentages_left = $percentages_left+140;

			}

			imagejpeg($jpg_image,"container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg",100);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id);

		}else{

			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

			redirect('apps/home');

		}

	}

	public function top_liker($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_feed = $this->get_user_feed();

		if($app[0]->id==401){

			$total_likes=0;

			$photos_count=0;

			$posts=$user_feed->feed->data;

			//$photos=$user_feed->feed->data[1]->type;

			//$this->print_exit($photos);

			for($i=0;$i<count($posts);$i++){

				$total_likes=$total_likes+$user_feed->feed->data[$i]->likes->summary->total_count;

				if($user_feed->feed->data[$i]->type=="photo"){

					$photos_count=$photos_count+1;

				}

			}

			$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 180, 180);

			$this->merge_images("container/input-images/" . $app[0]->id . "/200.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 52, 80);

			$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/2.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 40, 74);



			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

			$white = imagecolorallocate($jpg_image, 44, 44, 44);

			$black = imagecolorallocate($jpg_image, 255, 255, 255);



			$user_name = $this->session->userdata('first_name');

			$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

			$font='assets/fonts/RobotoCondensed-Bold.ttf';

			imagettftext($jpg_image, 20, 0, 60, 300, $white, $font_path, $user_name);

			imagettftext($jpg_image, 22, 0, 300, 113, $white, $font, $total_likes);

			imagettftext($jpg_image, 22, 0, 565, 115, $white, $font, count($posts));

			imagettftext($jpg_image, 22, 0, 440, 113, $white, $font, $photos_count);



			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);

		}

		if($app[0]->id==402){

			$has_liked=0;

			$posts=$user_feed->feed->data;

			//$photos=$user_feed->feed->data[1]->type;

			//$this->print_exit($photos);

			for($i=0;$i<count($posts);$i++){

				$has_liked=$has_liked+$user_feed->feed->data[$i]->likes->summary->has_liked;



			}

			$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 180, 180);

			$this->merge_images("container/input-images/" . $app[0]->id . "/100.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 52, 80);

			$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 44, 74);

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

			$white = imagecolorallocate($jpg_image, 44, 44, 44);

			$black = imagecolorallocate($jpg_image, 17, 52, 106);



			$user_name = $this->session->userdata('first_name');

			$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

			$font='assets/fonts/RobotoCondensed-Bold.ttf';

			imagettftext($jpg_image, 20, 0, 60, 300, $white, $font_path, $user_name);

			imagettftext($jpg_image, 80, 0, 300, 113, $black, $font, $has_liked);



			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);



		}

		if (isset($user_feed->feed)) {

			$best_friends = $this->best_friends_from_likes($user_feed->feed->data);

			if (count($best_friends)) {

				$this->get_profile_pics_new($best_friends, 1);

				$first_names = $this->get_first_names_new($best_friends, 1);

				$best_friends_keys = array_keys($best_friends);

				if($app[0]->id==407){

					$this->get_profile_pics($best_friends, 1);

					$first_names = $this->get_first_names($best_friends, 1);

					$best_friends_keys = array_keys($best_friends);

					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 160, 160);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 160, 160);



					$this->merge_images("container/input-images/" . $app[0]->id . "/1.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 525, 97);

					$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 45, 100);



					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

					$white = imagecolorallocate($jpg_image, 0, 0, 0);

					$black = imagecolorallocate($jpg_image, 255, 255, 255);

					$blue = imagecolorallocate($jpg_image, 42, 46, 76);





					$user_name = $this->session->userdata('first_name');

					$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

					imagettftext($jpg_image, 18, 0, 70, 315, $blue, $font_path, strtoupper($first_names[$best_friends_keys[0]]));

					imagettftext($jpg_image, 16, 0, 550, 310, $blue, $font_path,strtoupper($user_name));



					imagettftext($jpg_image, 42, 0, 370, 145, $blue, $font_path, $best_friends[$best_friends_keys[0]]);



					//imagettftext($jpg_image, 20, 0, 250, 30, $black, $font_path, $first_names[$best_friends_keys[0]]);



					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

					imagedestroy($jpg_image);



					redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);



				}

				if($app[0]->id==338){

					$this->get_profile_pics_new($best_friends, 3);

					$first_names = $this->get_first_names_new($best_friends, 3);

					$best_friends_keys = array_keys($best_friends);

					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 140, 140);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 140, 140);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 140, 140);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 140, 140);



					$this->merge_images("container/input-images/" . $app[0]->id . "/1.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 56, 95);

					$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 250, 95);

					$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 400, 95);

					$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 550, 95);

					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 243, 90);

					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 393, 90);

					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 543, 90);



					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

					$white = imagecolorallocate($jpg_image, 0, 0, 0);

					$black = imagecolorallocate($jpg_image, 255, 255, 255);



					$user_name = $this->session->userdata('first_name');

					$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

					imagettftext($jpg_image, 18, 0, 60, 300, $black, $font_path, $user_name);

					imagettftext($jpg_image, 15, 0, 255, 85, $black, $font_path, $first_names[$best_friends_keys[0]]);

					imagettftext($jpg_image, 15, 0, 405, 85, $black, $font_path, $first_names[$best_friends_keys[1]]);

					imagettftext($jpg_image, 15, 0, 560, 85, $black, $font_path, $first_names[$best_friends_keys[2]]);



					imagettftext($jpg_image, 42, 0, 290, 290, $black, $font_path, $best_friends[$best_friends_keys[0]]);

					imagettftext($jpg_image, 42, 0, 430, 290, $black, $font_path, $best_friends[$best_friends_keys[1]]);

					imagettftext($jpg_image, 42, 0, 565, 290, $black, $font_path, $best_friends[$best_friends_keys[2]]);



					//imagettftext($jpg_image, 20, 0, 250, 30, $black, $font_path, $first_names[$best_friends_keys[0]]);



					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

					imagedestroy($jpg_image);



					redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);



				}

				$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 180, 180);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 180, 180);

				

				$this->merge_images("container/input-images/" . $app[0]->id . "/100.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 62, 95);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 502, 95);



				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

				$white = imagecolorallocate($jpg_image, 0, 0, 0);

				$black = imagecolorallocate($jpg_image, 255, 255, 255);



				$user_name = $this->session->userdata('first_name');

				$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

				imagettftext($jpg_image, 22, 0, 60, 320, $black, $font_path, $user_name);

				imagettftext($jpg_image, 22, 0, 500, 320, $white, $font_path, $first_names[0]);

				imagettftext($jpg_image, 42, 0, 310, 170, $black, $font_path, $best_friends[$best_friends_keys[0]]);

				//imagettftext($jpg_image, 20, 0, 250, 30, $black, $font_path, $first_names[$best_friends_keys[0]]);

				

				imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

				imagedestroy($jpg_image);



				redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);

			} else {

				$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

				redirect('apps/home');

			}

		}else {

			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

			redirect('apps/home');

		}

	}

	public function get_full_names_new($friends,$limit){

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		if(array_key_exists($user_id,$friends)){

			unset($friends[$user_id]);

		}

		$friends_keys = array_keys($friends);

		$full_names = array();

		for($i=0;$i<$limit;$i++){

			$user_json = file_get_contents("https://graph.facebook.com/$friends_keys[$i]?fields=name&access_token=".$access_token);

			$user_decoded = json_decode($user_json);

			$full_names[$friends_keys[$i]]=$user_decoded->name;

		}

		return $full_names;

	}

	public function test_rankings($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		$user_name = $this->session->userdata("name");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_photos = $this->get_user_photos("","");

		$best_friends = $this->best_friends($user_photos);





		$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';



		if(count($best_friends)>10) {

			//$this->get_profile_pics($best_friends,3);

			//$full_names = $this->get_first_names($best_friends, 20);



			$jpg_image = imagecreatefromjpeg("container/input-images/" . $app[0]->id . "/1.jpg");

			$white = imagecolorallocate($jpg_image, 24, 49, 89);

			$color = imagecolorallocate($jpg_image, 255, 255, 255);

			$font_path1 = 'assets/fonts/gillsansbold.ttf';

			$first_names = $this->get_full_names_new($best_friends,count($best_friends)-1);

			$friends_keys = array_keys($first_names);

			$friends_name=array();

			for($i=0;$i<count($first_names);$i++){

				$friends_name[$i]=$first_names[$friends_keys[$i]];

			}

			//$friends_name = array($first_names[0],$first_names[1],$first_names[2],$first_names[3],$first_names[4],$first_names[5],$first_names[6],$first_names[7],$first_names[8],$first_names[9],$first_names[10],$first_names[11],$first_names[12],$first_names[13],$first_names[14],$first_names[15],$first_names[16],$first_names[17],$first_names[18]);



			shuffle($friends_name);

			$friends_list=array($user_name,$friends_name[0],$friends_name[1],$friends_name[2],$friends_name[3],$friends_name[4],$friends_name[5],$friends_name[6],$friends_name[7],$friends_name[8]);

			shuffle($friends_list);

			//imagettftext($jpg_image, 22, 0, 505, 150, $black, $name_font_path, explode(" ",$user_name)[0]);

			//imagettftext($jpg_image, 70, 0, 510, 250, $black, $font_path, $this->session->userdata("friends_name")[0]);

			//$friends_name = $this->session->userdata("friends_name");

			//array_shift($friends_name);

			//$this->session->set_userdata("friends_name",$friends_name);



			$black=imagecolorallocate($jpg_image,0, 0, 0);



			$odi_score2=rand(1,10);

			if($app[0]->id==425 || $app[0]->id==426 || $app[0]->id==427 || $app[0]->id==428){

				if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}

				$pic_positions = explode("X",$app[0]->picture_position);

				$pic_dimensions = explode("X",$app[0]->picture_dimensions);





				$source_img=1;

				$friends_list=array($friends_name[0],$friends_name[1],$friends_name[2],$friends_name[3],$friends_name[4],$friends_name[5],$friends_name[6],$friends_name[7],$friends_name[8],$friends_name[9]);

				shuffle($friends_list);


				$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

				$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

				$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

				

				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

				$black = imagecolorallocate($jpg_image, 255, 255, 255);

				$white = imagecolorallocate($jpg_image, 0, 0, 0);



				$font_path = 'assets/fonts/BebasNeueBold.ttf';



				imagettftext($jpg_image, 21, 0, 154, 90, $black , $font_path, strtoupper($user_name));

				imagettftext($jpg_image, 21, 0, 154, 152, $black , $font_path, strtoupper($friends_list[0]));

				imagettftext($jpg_image, 21, 0, 154, 210, $black , $font_path, strtoupper($friends_list[1]));

				imagettftext($jpg_image, 21, 0, 154, 274, $black , $font_path, strtoupper($friends_list[2]));

				imagettftext($jpg_image, 21, 0, 154, 332, $black , $font_path, strtoupper($friends_list[3]));



				if($app[0]->id==425){

					$list1=rand(4000,4400);

					$list2=rand(3800,3990);

					$list3=rand(3600,3790);

					$list4=rand(3400,3590);

					$list5=rand(3200,3390);

				}

				if($app[0]->id==426){

					$list1=rand(120,150);

					$list2=rand(110,120);

					$list3=rand(100,110);

					$list4=rand(90,100);

					$list5=rand(80,90);

				}

				if($app[0]->id==427){

					$list1=rand(240,260);

					$list2=rand(220,240);

					$list3=rand(210,220);

					$list4=rand(200,210);

					$list5=rand(100,200);

				}

				if($app[0]->id==428){

					$list1=rand(115,120);

					$list2=rand(110,115);

					$list3=rand(105,110);

					$list4=rand(100,105);

					$list5=rand(95,100);

				}





				imagettftext($jpg_image, 24, 0, 84, 90, $black , $font_path, $list1);

				imagettftext($jpg_image, 24, 0, 84, 152, $black , $font_path, $list2);

				imagettftext($jpg_image, 24, 0, 84, 210, $black , $font_path, $list3);

				imagettftext($jpg_image, 24, 0, 84, 274, $black , $font_path, $list4);

				imagettftext($jpg_image, 24, 0, 84, 332, $black , $font_path, $list5);





				imagejpeg($jpg_image,"container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",100);

				imagedestroy($jpg_image);

				redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");



			}

			if($app[0]->id==393){

				if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}

				$source_img=rand(1,8);

				$friends_list=array($friends_name[0],$friends_name[1],$friends_name[2],$friends_name[3],$friends_name[4],$friends_name[5],$friends_name[6],$friends_name[7],$friends_name[8],$friends_name[9]);

				shuffle($friends_list);

				exec('convert -size 144x144 xc:none -fill "/home/premam/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 72,72,1,72" "/home/premam/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

				//exec('convert -size 144x144 xc:none -fill ""container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 72,72,1,72" ""container/user-photos/' . $user_id . '/profile_pic_circle.png"');

				$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/$source_img.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 585, 32);

				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

				$font_path = 'assets/fonts/Kozuka.otf';

				$black=imagecolorallocate($jpg_image,39, 39, 39);

				$white=imagecolorallocate($jpg_image,255, 255, 255);

				

				imagettftext($jpg_image, 13, 0, 142, 68, $black , $font_path, strtoupper($user_name." (C)"));

				imagettftext($jpg_image, 13, 0, 142, 93, $black , $font_path, strtoupper($friends_list[0]));

				imagettftext($jpg_image, 13, 0, 142, 118, $black , $font_path, strtoupper($friends_list[1]));

				imagettftext($jpg_image, 13, 0, 142, 142, $black , $font_path, strtoupper($friends_list[2]));

				imagettftext($jpg_image, 13, 0, 142, 165, $black , $font_path, strtoupper($friends_list[3])." (VC)");

				imagettftext($jpg_image, 13, 0, 142, 192, $black , $font_path, strtoupper($friends_list[4])." (WK)");

				imagettftext($jpg_image, 13, 0, 142, 218, $black , $font_path, strtoupper($friends_list[5]));

				imagettftext($jpg_image, 13, 0, 142, 243, $black , $font_path, strtoupper($friends_list[6]));

				imagettftext($jpg_image, 13, 0, 142, 268, $black , $font_path, strtoupper($friends_list[7]));

				imagettftext($jpg_image, 13, 0, 142, 294, $black , $font_path, strtoupper($friends_list[8]));

				imagettftext($jpg_image, 13, 0, 142, 318, $black , $font_path, strtoupper($friends_list[9]));

				

				imagejpeg($jpg_image,"container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",100);

				imagedestroy($jpg_image);

				redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");



			}

			if($app[0]->id==344){

				$font_path = 'assets/fonts/Kozuka.otf';

				$black=imagecolorallocate($jpg_image,39, 39, 39);

				$white=imagecolorallocate($jpg_image,255, 255, 255);

				$matches=array(rand(440,480),rand(400,430),rand(420,460),rand(380,400),rand(360,380),rand(350,360),rand(330,350),rand(300,330),rand(290,300),rand(280,290));

				$runs=array(rand(16000,18500),rand(15000,16000),rand(14000,15000),rand(13000,14000),rand(12000,13000),rand(11000,12000),rand(10000,11000),rand(9500,10000),rand(8000,9500),rand(7000,8000));

				$fifty=array(rand(80,90),rand(70,80),rand(65,70),rand(65,75),rand(55,65),rand(50,55),rand(45,60),rand(40,55),rand(35,40),rand(30,40));

				$hundred=array(rand(45,50),rand(40,45),rand(38,42),rand(35,38),rand(30,35),rand(28,32),rand(25,28),rand(22,25),rand(20,22),rand(15,20));

				$best=array(rand(240,265),rand(230,250),rand(210,220),rand(200,210),rand(190,200),rand(180,190),rand(170,180),rand(160,200),rand(170,190),rand(160,180));

				imagettftext($jpg_image, 13, 0, 30, 89, $black , $font_path, strtoupper($friends_list[0]));

				imagettftext($jpg_image, 13, 0, 30, 115, $black , $font_path, strtoupper($friends_list[1]));

				imagettftext($jpg_image, 13, 0, 30, 140, $black , $font_path, strtoupper($friends_list[2]));

				imagettftext($jpg_image, 13, 0, 30, 165, $black , $font_path, strtoupper($friends_list[3]));

				imagettftext($jpg_image, 13, 0, 30, 192, $black , $font_path, strtoupper($friends_list[4]));

				imagettftext($jpg_image, 13, 0, 30, 220, $black , $font_path, strtoupper($friends_list[5]));

				imagettftext($jpg_image, 13, 0, 30, 245, $black , $font_path, strtoupper($friends_list[6]));

				imagettftext($jpg_image, 13, 0, 30, 270, $black , $font_path, strtoupper($friends_list[7]));

				imagettftext($jpg_image, 13, 0, 30, 296, $black , $font_path, strtoupper($friends_list[8]));

				imagettftext($jpg_image, 13, 0, 30, 322, $black , $font_path, strtoupper($friends_list[9]));

				imagettftext($jpg_image, 13, 0, 340, 89, $black , $font_path, $matches[0]);

				imagettftext($jpg_image, 13, 0, 340, 115, $black , $font_path, ($matches[1]));

				imagettftext($jpg_image, 13, 0, 340, 140, $black , $font_path, ($matches[2]));

				imagettftext($jpg_image, 13, 0, 340, 165, $black , $font_path, ($matches[3]));

				imagettftext($jpg_image, 13, 0, 340, 192, $black , $font_path, ($matches[4]));

				imagettftext($jpg_image, 13, 0, 340, 220, $black , $font_path, ($matches[5]));

				imagettftext($jpg_image, 13, 0, 340, 245, $black , $font_path, ($matches[6]));

				imagettftext($jpg_image, 13, 0, 340, 270, $black , $font_path, ($matches[7]));

				imagettftext($jpg_image, 13, 0, 340, 296, $black , $font_path, ($matches[8]));

				imagettftext($jpg_image, 13, 0, 340, 322, $black , $font_path, ($matches[9]));

				imagettftext($jpg_image, 13, 0, 425, 89, $black , $font_path, $runs[0]);

				imagettftext($jpg_image, 13, 0, 425, 115, $black , $font_path, ($runs[1]));

				imagettftext($jpg_image, 13, 0, 425, 140, $black , $font_path, ($runs[2]));

				imagettftext($jpg_image, 13, 0, 425, 165, $black , $font_path, ($runs[3]));

				imagettftext($jpg_image, 13, 0, 425, 192, $black , $font_path, ($runs[4]));

				imagettftext($jpg_image, 13, 0, 425, 220, $black , $font_path, ($runs[5]));

				imagettftext($jpg_image, 13, 0, 425, 245, $black , $font_path, ($runs[6]));

				imagettftext($jpg_image, 13, 0, 425, 270, $black , $font_path, ($runs[7]));

				imagettftext($jpg_image, 13, 0, 425, 296, $black , $font_path, ($runs[8]));

				imagettftext($jpg_image, 13, 0, 425, 322, $black , $font_path, ($runs[9]));

				imagettftext($jpg_image, 13, 0, 515, 89, $black , $font_path, $fifty[0]);

				imagettftext($jpg_image, 13, 0, 515, 115, $black , $font_path, ($fifty[1]));

				imagettftext($jpg_image, 13, 0, 515, 140, $black , $font_path, ($fifty[2]));

				imagettftext($jpg_image, 13, 0, 515, 165, $black , $font_path, ($fifty[3]));

				imagettftext($jpg_image, 13, 0, 515, 192, $black , $font_path, ($fifty[4]));

				imagettftext($jpg_image, 13, 0, 515, 220, $black , $font_path, ($fifty[5]));

				imagettftext($jpg_image, 13, 0, 515, 245, $black , $font_path, ($fifty[6]));

				imagettftext($jpg_image, 13, 0, 515, 270, $black , $font_path, ($fifty[7]));

				imagettftext($jpg_image, 13, 0, 515, 296, $black , $font_path, ($fifty[8]));

				imagettftext($jpg_image, 13, 0, 515, 322, $black , $font_path, ($fifty[9]));



				imagettftext($jpg_image, 13, 0, 570, 89, $black , $font_path, $hundred[0]);

				imagettftext($jpg_image, 13, 0, 570, 115, $black , $font_path, ($hundred[1]));

				imagettftext($jpg_image, 13, 0, 570, 140, $black , $font_path, ($hundred[2]));

				imagettftext($jpg_image, 13, 0, 570, 165, $black , $font_path, ($hundred[3]));

				imagettftext($jpg_image, 13, 0, 570, 192, $black , $font_path, ($hundred[4]));

				imagettftext($jpg_image, 13, 0, 570, 220, $black , $font_path, ($hundred[5]));

				imagettftext($jpg_image, 13, 0, 570, 245, $black , $font_path, ($hundred[6]));

				imagettftext($jpg_image, 13, 0, 570, 270, $black , $font_path, ($hundred[7]));

				imagettftext($jpg_image, 13, 0, 570, 296, $black , $font_path, ($hundred[8]));

				imagettftext($jpg_image, 13, 0, 570, 322, $black , $font_path, ($hundred[9]));



				imagettftext($jpg_image, 18, 0, 650, 93, $white , $font_path, $best[0]);

				imagettftext($jpg_image, 18, 0, 650, 119, $white , $font_path, ($best[1]."*"));

				imagettftext($jpg_image, 18, 0, 650, 144, $white , $font_path, ($best[2]."*"));

				imagettftext($jpg_image, 18, 0, 650, 169, $white , $font_path, ($best[3]));

				imagettftext($jpg_image, 18, 0, 650, 196, $white , $font_path, ($best[4]));

				imagettftext($jpg_image, 18, 0, 650, 224, $white , $font_path, ($best[5]."*"));

				imagettftext($jpg_image, 18, 0, 650, 249, $white , $font_path, ($best[6]));

				imagettftext($jpg_image, 18, 0, 650, 274, $white , $font_path, ($best[7]."*"));

				imagettftext($jpg_image, 18, 0, 650, 300, $white , $font_path, ($best[8]));

				imagettftext($jpg_image, 18, 0, 650, 326, $white , $font_path, ($best[9]."*"));



				imagejpeg($jpg_image,"container/output-images/" . $app[0]->id . "/" . $user_id ."-".$odi_score2. ".jpg",100);

				imagedestroy($jpg_image);

				redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-".$odi_score2);



			}

			$r1=rand(960,1050);

			$r2=rand(930,950);

			$r3=rand(900,929);

			$r4=rand(870,899);

			$r5=rand(850,869);

			$r6=rand(830,849);

			$r7=rand(800,829);

			$r8=rand(775,799);

			$r9=rand(750,774);

			$r10=rand(700,749);



			//imagettftext($jpg_image, 14, 0, 160, 130, $color , $font_path, $user_name);

			imagettftext($jpg_image, 13, 0, 70, 114, $black , $font_path, strtoupper($friends_list[0]));

			imagettftext($jpg_image, 13, 0, 70, 135, $black , $font_path, strtoupper($friends_list[1]));

			imagettftext($jpg_image, 13, 0, 70, 157, $black , $font_path, strtoupper($friends_list[2]));

			imagettftext($jpg_image, 13, 0, 70, 182, $black , $font_path, strtoupper($friends_list[3]));

			imagettftext($jpg_image, 13, 0, 70, 204, $black , $font_path, strtoupper($friends_list[4]));

			imagettftext($jpg_image, 13, 0, 70, 226, $black , $font_path, strtoupper($friends_list[5]));

			imagettftext($jpg_image, 13, 0, 70, 250, $black , $font_path, strtoupper($friends_list[6]));

			imagettftext($jpg_image, 13, 0, 70, 273, $black , $font_path, strtoupper($friends_list[7]));

			imagettftext($jpg_image, 13, 0, 70, 295, $black , $font_path, strtoupper($friends_list[8]));

			imagettftext($jpg_image, 13, 0, 72, 317, $black , $font_path, strtoupper($friends_list[9]));



			imagettftext($jpg_image, 15, 0, 506, 114, $black , $font_path, strtoupper($r1));

			imagettftext($jpg_image, 15, 0, 510, 135, $black , $font_path, strtoupper($r2));

			imagettftext($jpg_image, 15, 0, 510, 157, $black , $font_path, strtoupper($r3));

			imagettftext($jpg_image, 15, 0, 510, 182, $black , $font_path, strtoupper($r4));

			imagettftext($jpg_image, 15, 0, 510, 204, $black , $font_path, strtoupper($r5));

			imagettftext($jpg_image, 15, 0, 510, 226, $black , $font_path, strtoupper($r6));

			imagettftext($jpg_image, 15, 0, 510, 250, $black , $font_path, strtoupper($r7));

			imagettftext($jpg_image, 15, 0, 510, 273, $black , $font_path, strtoupper($r8));

			imagettftext($jpg_image, 15, 0, 510, 295, $black , $font_path, strtoupper($r9));

			imagettftext($jpg_image, 15, 0, 510, 318, $black , $font_path, strtoupper($r10));







			imagejpeg($jpg_image,"container/output-images/" . $app[0]->id . "/" . $user_id ."-".$odi_score2. ".jpg",100);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-".$odi_score2);



		}





		else{

			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

			redirect('apps/home');

		}

	}

	public function destiny_logic_new($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;

		$luck =rand(80,100);

		$love=rand(25,35);

		$kids=rand(1,5);

		$health=rand(80,100);

		$car=array("Renault","Lamborghini","BMW","Ferrari","Volkswagon","Ford","Hyundai","Toyota","Maruti");

		shuffle($car);



		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/" .$user_id."/"."profile_pic.jpg","container/user-photos/" .$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);


		exec('convert -size 200x200 xc:none -fill "/home/premam/public_html/container/user-photos/'. $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 100,100,1,100" "/home/premam/public_html/container/user-photos/'. $user_id . '/profile_pic_circle.png"');

		$this->merge_images_jpg_png("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/" .$user_id."/"."profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg",495,66);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$luck");

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);



		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/Kozuka.otf';

		imagettftext($jpg_image, 18, 0, 535, 312, $black, $font_path, $user_name);

		imagettftext($jpg_image, 24, 0, 190, 44, $black, $font_path, $luck."%");

		imagettftext($jpg_image, 24, 0, 190, 96, $black, $font_path, "Married at ".$love);

		imagettftext($jpg_image, 24, 0, 190, 152, $black, $font_path, $kids);

		imagettftext($jpg_image, 24, 0, 190, 205, $black, $font_path, $health."%");

		imagettftext($jpg_image, 24, 0, 190, 262, $black, $font_path, "Wealthy");

		imagettftext($jpg_image, 24, 0, 190, 312, $black, $font_path, $car[0]);





		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$luck");

	}

	public function future_prediction($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$name = $this->session->userdata("first_name");

		if($name[0]=="A"||$name[0]=="C"||$name[0]=="L"||$name[0]=="X"){

			$source_img = 1;

		}

		if($name[0]=="B"){

			$source_img = 2;

		}

		if($name[0]=="D"){

			$source_img = 3;

		}

		if($name[0]=="G"||$name[0]=="S"){

			$source_img = 4;

		}

		if($name[0]=="H"){

			$source_img = 5;

		}if($name[0]=="K"){

			$source_img = 6;

		}if($name[0]=="M"){

			$source_img = 7;

		}if($name[0]=="N"||$name[0]=="Y"){

			$source_img = 8;

		}if($name[0]=="P"||$name[0]=="F"){

			$source_img = 9;

		}if($name[0]=="Q"||$name[0]=="J"||$name[0]=="Z"){

			$source_img = 10;

		}if($name[0]=="R"||$name[0]=="T"){

			$source_img = 11;

		}if($name[0]=="U"||$name[0]=="E"||$name[0]=="O"||$name[0]=="I"||$name[0]=="V"||$name[0]=="W"){

			$source_img = 12;

		}

		

	

		

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}

	public function personality_logic($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;

		$user_id = $this->session->userdata("user_id");

		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/BebasNeueBold.ttf';

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		if($app[0]->id==430){

			//$love=rand(80,100);

			$love=array("Hugs","Spreading joy","Spreading Happiness","To Play Hard","Family","Friends","Kids","being optimistic","To Travel and Discover");

			//$year=rand(2018,2026);

			$hate=array("Nagging","Traitors","Work Hard","Liars","Stress","Phonies");

			$dontcare=array("Unnecessary drama","About Fashion","Being Productive","About fake rumours","Everything else","Pointless Conversations","About gossiping");



			shuffle($love);shuffle($hate);shuffle($dontcare);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);


			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			exec('convert -size 200x200 xc:none -fill "/home/premam/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 100,100,1,100" "/home/premam/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

			$this->merge_images_jpg_png("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",502,50);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

			

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 0, 0, 0);

			$white = imagecolorallocate($jpg_image, 255, 255, 255);





			//imagettftext($jpg_image, 24, 0, 46, 280, $white, $font_path, $user_name);

			imagettftext($jpg_image, 26, 0, 220, 85, $black, $font_path, $love[0]);

			imagettftext($jpg_image, 26, 0, 220, 165, $black, $font_path, $hate[0]);

			imagettftext($jpg_image, 26, 0, 280, 248, $black, $font_path, $dontcare[0]);





			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");





		}

		if($app[0]->id==429){

			$gender = $this->session->userdata("gender");

			if($gender=="male"){

				$source_img =1;

				$body=array("Skinny","Curvy","Athletic","Tanned","Little Fat","Chubby");

				$looks=array("Tall","Short","Pretty","Beautiful","Attractive");

			}

			else{

				$source_img =2;

				$body=array("Skinny","Muscular","Athletic","Tanned","Little Fat","Six Pack");

				$looks=array("Tall","Short","Handsome","Attractive");

			}

			$hair=array("Black","Blonde","Red","Brown");

			$skin=array("Black","Fair","Olive","Brown","Very Fair","White");

			$eyes=array("Brown","Baby Blue","Black","Blue");





			shuffle($body);shuffle($looks);shuffle($hair);shuffle($skin);shuffle($eyes);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",495,70);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

			

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 0, 0, 0);

			$white = imagecolorallocate($jpg_image, 255, 255, 255);





			//imagettftext($jpg_image, 24, 0, 46, 280, $white, $font_path, $user_name);

			imagettftext($jpg_image, 30, 0, 120, 82, $white, $font_path, $hair[0]);

			imagettftext($jpg_image, 30, 0, 170, 132, $white, $font_path, $skin[0]);

			imagettftext($jpg_image, 30, 0, 120, 182, $white, $font_path, $eyes[0]);

			imagettftext($jpg_image, 30, 0, 130, 232, $white, $font_path, $body[0]);

			imagettftext($jpg_image, 30, 0, 130, 282, $white, $font_path, $looks[0]);





			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");





		}

		if($app[0]->id==422){

			//$love=rand(80,100);

			$date=rand(1,28);

			//$year=rand(2018,2026);

			$month=array("JAN","FEB","MAR","APR","MAY","JUNE","JULY","AUG","SEP","OCT","NOV","DEC");

			$age=rand(25,36);

			$letters=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

			$marriage=array("LOVE","ARRANGE");

			shuffle($month);shuffle($marriage);shuffle($letters);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",410,75);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

			

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 0, 0, 0);

			$white = imagecolorallocate($jpg_image, 255, 255, 255);





			//imagettftext($jpg_image, 24, 0, 46, 280, $white, $font_path, $user_name);

			imagettftext($jpg_image, 32, 0, 240, 122, $black, $font_path, $month[0]." ".$date);

			imagettftext($jpg_image, 32, 0, 240, 185, $black, $font_path, $age." Yrs");

			imagettftext($jpg_image, 32, 0, 240, 248, $black, $font_path, $marriage[0]);

			imagettftext($jpg_image, 44, 0, 338, 314, $white, $font_path, $letters[0]);





			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");





		}

		if($app[0]->id==417){

			$source_img =rand(1,10);

			$love=rand(80,100);

			$when=rand(45,80);

			$where=array("Pakistan","Tehran","Bagdhdad","Abu Dabhi","Islamabad","USA","New York","Moscow","Kabul","Delhi");

			shuffle($where);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",24,42);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

			

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 0, 0, 0);

			$white = imagecolorallocate($jpg_image, 255, 255, 255);





			imagettftext($jpg_image, 24, 0, 46, 280, $white, $font_path, $user_name);

			imagettftext($jpg_image, 32, 0, 364, 100, $black, $font_path, "AT THE AGE OF ".$when);

			imagettftext($jpg_image, 32, 0, 364, 180, $black, $font_path, "AT ".$where[0]);









			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");





		}

		if($app[0]->id==407){

			$love=rand(80,100);

			$hate=rand(0,20);

			$jealous=rand(0,30);

			$kindness=rand(80,100);

			$anger=rand(0,60);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",480,65);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

			

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 255, 255, 255);

			$white = imagecolorallocate($jpg_image, 0, 0, 0);





			imagettftext($jpg_image, 24, 0, 500, 312, $black, $font_path, $user_name);

			imagettftext($jpg_image, 36, 0, 190, 60, $black, $font_path, $love."%");

			imagettftext($jpg_image, 36, 0, 216, 120, $black, $font_path, $jealous."%");

			imagettftext($jpg_image, 36, 0, 230, 182, $black, $font_path, $kindness."%");

			imagettftext($jpg_image, 36, 0, 190, 245, $black, $font_path, $hate."%");

			imagettftext($jpg_image, 36, 0, 190, 312, $black, $font_path, $anger."%");







			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");





		}

		

		$color =array("RED","BLUE","YELLOW","BLACK","WHITE","VIOLET","GREEN","GOLD","SILVER","ORANGE");

		$love=array("BEAUTY","NATURE","PETS","MUSIC","SPORTS","HUMOR","SUNSHINE","FUN");

		$hates=array("INJUSTICES","POLITICIANS","BAD MUSIC");

		$strength=array("SELF-CONFIDENCE","INTELLIGENCE","LOYALITY","LOGIC");

		$mistake=array("PERFECTIONIST","HESITANT","SELFISH","CAN'T SAY NO");

		$personality=array("FIGHTER","LEADER","HAPPY","WARM","COOL");

		shuffle($color);shuffle($love);shuffle($hates);shuffle($strength);shuffle($mistake);shuffle($personality);



		

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",41,42);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 44, 44, 44);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);



		

		imagettftext($jpg_image, 24, 0, 50, 300, $black, $font_path, $user_name);

		imagettftext($jpg_image, 26, 0, 550, 40, $black, $font_path, $color[0]);

		imagettftext($jpg_image, 26, 0, 550, 90, $black, $font_path, $love[0]);

		imagettftext($jpg_image, 26, 0, 550, 142, $black, $font_path, $hates[0]);

		imagettftext($jpg_image, 26, 0, 550, 200, $black, $font_path, $strength[0]);

		imagettftext($jpg_image, 26, 0, 550, 252, $black, $font_path, $mistake[0]);

		imagettftext($jpg_image, 30, 0, 550, 310, $black, $font_path, $personality[0]);





		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");

	}

	public function destiny_logic($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;

		$luck =rand(80,100);

		$love=rand(25,35);

		$kids=rand(1,5);

		$health=rand(80,100);

		$car=array("Renault","Lamborghini","BMW","Ferrari","Volkswagon","Ford","Hyundai","Toyota","Maruti");

		shuffle($car);



		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$luck");

		////

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);



		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/Kozuka.otf';

		imagettftext($jpg_image, 18, 0, 458, 300, $black, $font_path, $user_name);

		imagettftext($jpg_image, 24, 0, 210, 78, $black, $font_path, $luck."%");

		imagettftext($jpg_image, 24, 0, 210, 118, $black, $font_path, "Married at ".$love);

		imagettftext($jpg_image, 24, 0, 210, 160, $black, $font_path, $kids);

		imagettftext($jpg_image, 24, 0, 210, 206, $black, $font_path, $health."%");

		imagettftext($jpg_image, 24, 0, 210, 250, $black, $font_path, "Wealthy");

		imagettftext($jpg_image, 24, 0, 210, 286, $black, $font_path, $car[0]);





		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$luck");

	}

	public function kissing($app)

	{

		$source_img = 1;///rand(1, count($images) - 2);

		$user_id = $this->session->userdata("user_id");

		$first_name = $this->session->userdata("first_name");



		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$kiss_percentage=rand(120,200);



		$this->crop_pic2("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 180,180);


		exec('convert -size 180x180 xc:none -fill "/home/premam/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 90,90,1,90" "/home/premam/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');



		//$merged_image = $this->get_merged_images_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "", 60, 95);

		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/1.jpg","container/user-photos/" . $user_id . "/" . "profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" . $user_id . "-".$kiss_percentage.".jpg",60, 95);

		$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . "-".$kiss_percentage.".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . "-".$kiss_percentage.".jpg",170,180);

		$merged_image=imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-".$kiss_percentage.".jpg");

		$red = imagecolorallocate($merged_image, 191, 0, 0);

		$black = imagecolorallocate($merged_image, 44, 44, 44);



		$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

		$font_path1 = 'assets/fonts/Kozuka.otf';



		//$this->print_exit($this->session->userdata("number"));

		imagettftext($merged_image, 24, 0, 340, 120, $black, $font_path,$first_name.' is a' );

		if($kiss_percentage<150){

			imagettftext($merged_image, 25, 0, 340, 160, $black, $font_path,'Professional Kisser' );



		}

		if($kiss_percentage>150&& $kiss_percentage<180){

			imagettftext($merged_image, 25, 0, 340, 160, $black, $font_path,'Romantic Kisser' );



		}

		if($kiss_percentage>180){

			imagettftext($merged_image, 25, 0, 340, 160, $black, $font_path,'Hardcore Kisser' );



		}

		imagettftext($merged_image, 60, 0, 360, 260, $red, $font_path1,$kiss_percentage.'%' );





		imagejpeg($merged_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-".$kiss_percentage.".jpg", 80);

		imagedestroy($merged_image);



		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-".$kiss_percentage);

	}

	public function lucky($app)

	{

		$source_img = 100;///rand(1, count($images) - 2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		//$this->crop_pic2("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 138,138);

		exec('convert -size 200x200 xc:none -fill "/home/premam/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 100,100,1,100" "/home/premam/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

		$merged_image = $this->get_merged_images_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "", 35, 71);

		$black = imagecolorallocate($merged_image, 0, 0, 0);

		$white = imagecolorallocate($merged_image, 255, 255, 255);

		$blue = imagecolorallocate($merged_image, 0, 51, 102);



		$font_path = 'assets/fonts/gillsansmt.ttf';



		if($this->session->userdata("number")==NULL || count($this->session->userdata("number"))<1 && $this->session->userdata("colors")==NULL || count($this->session->userdata("colors"))<1 && $this->session->userdata("days")==NULL || count($this->session->userdata("days"))<1){

			$number = array('01','02','03','04','05','06','07','08','09','10','11','55','12','18','44','17','19','50','22','33','99','88','66','77');

			$colors = array('Red','Blue','Green','Yellow','White','Black','Orange','Pink');

			$days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

			shuffle($number);

			shuffle($colors);

			shuffle($days);



			//$letters_random = array_rand($number,26);

			$this->session->set_userdata("number",$number);

			$this->session->set_userdata("colors",$colors);

			$this->session->set_userdata("days",$days);



		}

		//$this->print_exit($this->session->userdata("number"));

		imagettftext($merged_image, 60, 0, 590, 330, $black, $font_path, $this->session->userdata("number")[0]);

		imagettftext($merged_image, 30, 0, 570, 120, $white, $font_path, $this->session->userdata("colors")[0]);

		imagettftext($merged_image, 30, 2, 295, 230, $blue, $font_path, $this->session->userdata("days")[0]);





		imagejpeg($merged_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-".$this->session->userdata("number")[0].".jpg", 80);

		imagedestroy($merged_image);

		$number = $this->session->userdata("number");

		$colors = $this->session->userdata("colors");

		$days = $this->session->userdata("days");



		$removed_letter = $number[0];

		array_shift($number);

		array_shift($colors);

		array_shift($days);

		$this->session->set_userdata("number",$number);

		$this->session->set_userdata("colors",$colors);

		$this->session->set_userdata("days",$days);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-".$removed_letter);

	}

	public function feeling($app)

	{

		$source_img = 100;///rand(1, count($images) - 2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		//$this->crop_pic2("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 138,138);

		exec('convert -size 200x200 xc:none -fill "/home/premam/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 100,100,1,100" ""/home/premam/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

		$merged_image = $this->get_merged_images_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "", 35, 71);

		$black = imagecolorallocate($merged_image, 255, 255, 255);

		$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';



		if($this->session->userdata("numbers")==NULL || count($this->session->userdata("numbers"))<1){

			$numbers = array('0','90','100','95','80','85','75','70','60','65','50','55');

			shuffle($numbers);

			//$letters_random = array_rand($letters,26);

			$this->session->set_userdata("numbers",$numbers);

		}

		//$this->print_exit($this->session->userdata("letters"));

		imagettftext($merged_image, 21, 0, 343, 206, $black, $font_path, $this->session->userdata("numbers")[0]."%");

		imagettftext($merged_image, 21, 0, 493, 186, $black, $font_path, $this->session->userdata("numbers")[1]."%");

		imagettftext($merged_image, 21, 0, 635, 206, $black, $font_path, $this->session->userdata("numbers")[2]."%");

		imagettftext($merged_image, 21, 0, 645, 65, $black, $font_path, $this->session->userdata("numbers")[3]."%");

		imagettftext($merged_image, 21, 0, 508, 55, $black, $font_path, $this->session->userdata("numbers")[4]."%");

		imagettftext($merged_image, 21, 0, 332, 62, $black, $font_path, $this->session->userdata("numbers")[5]."%");



		imagejpeg($merged_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-".$this->session->userdata("numbers")[0].".jpg", 80);

		imagedestroy($merged_image);

		$numbers = $this->session->userdata("numbers");

		$removed_number = $numbers[0];

		array_shift($numbers);

		$this->session->set_userdata("numbers",$numbers);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-".$removed_number);

	}

	public function profile_views($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		$source_img=1;

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}



		exec('convert -size 200x200 xc:none -fill "/home/premam/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 100,100,1,100" "/home/premam/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/$source_img.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 70, 70);



		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/Kozuka.otf';

		$male=rand(75,150);

		$female=rand(130,200);

		$total=$male+$female;

		imagettftext($jpg_image, 40, 0, 455, 130, $black, $font_path, $male);

		imagettftext($jpg_image, 40, 0, 450, 250, $black, $font_path, $female);

		imagettftext($jpg_image, 50, 0, 572, 225, $black, $font_path, $total);



		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 100);

		imagedestroy($jpg_image);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



	}

	public function ipl_career_logic($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =rand(1,8);

		$matches =rand(150,200);

		$runs= rand(2500,4500);

		$fifty=rand(20,60);

		$average =rand(400,650)/10;

		$strike_rate=rand(1200,3000)/10;

		$best_score=rand(100,175);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);



		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/BebasNeueBold.ttf';



		imagettftext($jpg_image, 24, 0, 120, 86, $white, $font_path, $user_name);

		imagettftext($jpg_image, 26, 0, 280, 118, $black, $font_path, $matches);

		imagettftext($jpg_image, 26, 0, 280, 158, $black, $font_path, $runs);

		imagettftext($jpg_image, 26, 0, 280, 194, $black, $font_path, $fifty);

		imagettftext($jpg_image, 26, 0, 280, 235, $black, $font_path, $average);

		imagettftext($jpg_image, 26, 0, 280, 274, $black, $font_path, $strike_rate);

		imagettftext($jpg_image, 28, 0, 320, 316, $black, $font_path, $best_score."*");

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");

	}

	public function ipl_auction($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		$source_img=rand(1,8);

		//$date=date("Y/m/d");

		//$this->print_exit($date);



		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",200);

		$this->merge_images("container/input-images/" . $app[0]->id . "/$source_img.jpg", "container/user-photos/".$user_id."/"."profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 24, 40);



		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);

		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/BebasNeueBold.ttf';

		$base_price=rand(1,2);

		$auction_price=rand(20,200)/10;

		imagettftext($jpg_image, 38, 0, 250, 135, $black, $font_path, $base_price." Cr");

		imagettftext($jpg_image, 40, 0, 250, 215, $black, $font_path, $auction_price." Cr");

		imagettftext($jpg_image, 22, 0, 42, 330, $white, $font_path, $user_name);



		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);

		imagedestroy($jpg_image);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" .""."-". $user_id . "$source_img");



	}

	public function logic_google($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		

		

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		////

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 0, 0, 0);

		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/RobotoCondensed-Regular.ttf';

		$name=explode(" ",$user_name)[0];

		imagettftext($jpg_image, 12, 0, 102, 28, $black, $font_path,'Describe '.$name.' in a word' );

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");

	}

	public function people_search($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;

		$line1=array("is a genius ","works out","is very clever","read books alot","is a phiolosopher","is a leader");

		$line2=array("knows how to cook","love puppies","is a cricket player","is a football player","is a tennis player");

		$line3=array("likes sunny leone","is romantic","is a good lover","can sing","can dance","thinks dirty thoughts");

		$line4=array("likes horror movies","likes adventure movies","is a fighter","is a good kisser");

		shuffle($line1);shuffle($line2);shuffle($line3);shuffle($line4);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		

		$jpg_image = imagecreatefromjpeg("container/input-images/".$app[0]->id."/".$source_img.".jpg");

		$black = imagecolorallocate($jpg_image, 0, 0, 0);



		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/Kozuka.otf';

		$font_path1 = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

		if(strlen($user_name)<20){

			imagettftext($jpg_image, 22, 0, 76, 152, $black, $font_path1, $user_name);

			imagettftext($jpg_image, 22, 0, 76, 202, $black, $font_path1, $user_name." ".$line1[0]);

			imagettftext($jpg_image, 22, 0, 76, 252, $black, $font_path1, $user_name." ".$line2[0]);

			imagettftext($jpg_image, 22, 0, 76, 302, $black, $font_path1, $user_name." ".$line3[0]);

			imagettftext($jpg_image, 22, 0, 76, 352, $black, $font_path1, $user_name." ".$line4[0]);

		}

		else if(strlen($user_name)>20){

			imagettftext($jpg_image, 18, 0, 76, 152, $black, $font_path1, $user_name);

			imagettftext($jpg_image, 18, 0, 76, 202, $black, $font_path1, $user_name." ".$line1[0]);

			imagettftext($jpg_image, 18, 0, 76, 252, $black, $font_path1, $user_name." ".$line2[0]);

			imagettftext($jpg_image, 18, 0, 76, 302, $black, $font_path1, $user_name." ".$line3[0]);

			imagettftext($jpg_image, 18, 0, 76, 352, $black, $font_path1, $user_name." ".$line4[0]);

		}





		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");

	}

	public function true_love_meet($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;



		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 239, 14, 63);

		$white = imagecolorallocate($jpg_image, 23, 30, 66);

		$month=array("June","July","August","September","October","November","December");

		$day=array("1st","2nd","3rd","4th","5th","6th","7th","8th","9th","10th","12th","15th","18th","21st","22nd","23rd","25th");

		shuffle($month);shuffle($day);



		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/Kozuka.otf';

		$font_path1 = 'assets/fonts/BebasNeueBold.ttf';

		imagettftext($jpg_image, 34, 0, 315, 290, $white, $font_path, $day[0]." ".$month[0]);

		imagettftext($jpg_image, 26, 0, 38, 368, $white, $font_path1, $user_name);

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");

	}

        public function fb_distance($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;



		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 239, 14, 63);

		$white = imagecolorallocate($jpg_image, 0, 13, 34);

		$distance=rand(1000,2500)/100;



		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/Kozuka.otf';

		$font_path1 = 'assets/fonts/BebasNeueBold.ttf';

		imagettftext($jpg_image, 54, 0, 342, 255, $white, $font_path1, $distance);

		imagettftext($jpg_image, 18, 0, 102, 30, $white, $font_path1, $user_name);

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");

	}

	public function logic_one($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}

	public function friends_gang($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_feed = $this->get_user_feed();

		if (isset($user_feed->feed)) {

			//$best=$this->best_friends_from_feed($user_feed->feed->data);



			$best_friends = $this->best_friends_from_feed($user_feed->feed->data);

			if (count($best_friends)) {

				$this->get_profile_pics_old($best_friends, 10);

				$first_names = $this->get_first_names_old($best_friends, 10);

				$best_friends_keys = array_keys($best_friends);

                                //shuffle($best_friends_keys);

				if($app[0]->id==446){

					$this->get_profile_pics_new($best_friends, 3);

					$first_names = $this->get_full_names_new($best_friends, 3);

					$best_friends_keys = array_keys($best_friends);

					$source_img=1;

					if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

						mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

					}



					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 120, 120);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 90, 90);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 90, 90);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 90, 90);

					//$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[3] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg", 90, 90);



						$this->merge_images("container/input-images/" . $app[0]->id . "/$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 165, 90);

						//$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 30, 100);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 165, 189);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 165, 285);

						//$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 580, 150);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .$user_id . "$source_img.jpg", 25, 45);





					$jpg_image = @imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .$user_id . "$source_img.jpg");

					$black = imagecolorallocate($jpg_image, 44, 44, 44);

					$white = imagecolorallocate($jpg_image, 255, 255, 255);

					//$this->print_exit($auction_price);

					$user_name = $this->session->userdata('name');

					$font_path = 'assets/fonts/BebasNeueBold.ttf';



						//$this->print_exit($first_names);

						imagettftext($jpg_image, 18, 0, 40, 190, $black, $font_path, wordwrap($user_name,10,"\n"));

						imagettftext($jpg_image, 30, 0, 270, 145, $white, $font_path, $first_names[$best_friends_keys[0]]);

						imagettftext($jpg_image, 30, 0, 270, 245, $white, $font_path, $first_names[$best_friends_keys[1]]);

						imagettftext($jpg_image, 30, 0, 270, 345, $white, $font_path, $first_names[$best_friends_keys[2]]);











					@imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .$user_id . "$source_img.jpg", 100);

					@imagedestroy($jpg_image);



					redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/". $user_id . "$source_img");



				}

				

				if($app[0]->id==432){

				$this->get_profile_pics_old($best_friends, 4);

				$first_names = $this->get_first_names_old($best_friends, 4);

				$best_friends_keys = array_keys($best_friends);

				$this->has_access_token($app);

				$this->refresh_access_token();

					$gender = $this->session->userdata("gender");

					if($gender=="male"){

						$source_img=1;

					}

					else{

						$source_img=2;

					}





					if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

						mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

					}



					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped1.jpg", 160, 160);

					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 130, 130);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 130, 130);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 130, 130);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 130, 130);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[3] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg", 130, 130);

					if($source_img==1){

						$this->merge_images("container/input-images/" . $app[0]->id . "/$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 220, 170);

						//$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 30, 100);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 390, 170);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 560, 170);

						//$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 580, 150);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 45, 170);



					}

					else{

						$this->merge_images("container/input-images/" . $app[0]->id . "/$source_img.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 140, 175);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 312, 175);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 478, 175);



					}



					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");

					$black = imagecolorallocate($jpg_image, 206, 14, 35);

					$white = imagecolorallocate($jpg_image, 255, 255, 255);

					//$this->print_exit($auction_price);

					$user_name = $this->session->userdata('first_name');

					$font_path = 'assets/fonts/BebasNeueBold.ttf';

					if($source_img==1){

						//$this->print_exit($first_names);

						imagettftext($jpg_image, 20, 0, 48, 324, $white, $font_path, $user_name);

						imagettftext($jpg_image, 20, 0, 228, 324, $white, $font_path, $first_names[$best_friends_keys[0]]);

						imagettftext($jpg_image, 20, 0, 393, 324, $white, $font_path, $first_names[$best_friends_keys[1]]);

						imagettftext($jpg_image, 20, 0, 563, 324, $white, $font_path, $first_names[$best_friends_keys[2]]);





					}

					else if($source_img==2){

						imagettftext($jpg_image, 20, 0, 148, 328, $white, $font_path, $user_name);

						imagettftext($jpg_image, 20, 0, 320, 328, $white, $font_path, $first_names[$best_friends_keys[1]]);

						imagettftext($jpg_image, 20, 0, 485, 328, $white, $font_path, $first_names[$best_friends_keys[0]]);



					}



					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);

					imagedestroy($jpg_image);



					redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" .""."-". $user_id . "$source_img");



				}

					



				$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 178, 178);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 178, 178);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 178, 178);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 178, 178);



				$this->merge_images("container/input-images/" . $app[0]->id . "/1.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 22, 82);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 200, 82);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 380, 82);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 555, 82);

				$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/200.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 22, 0);



				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

				$white = imagecolorallocate($jpg_image, 0, 0, 0);

				$black = imagecolorallocate($jpg_image, 44, 44, 44);



				$user_name = $this->session->userdata('first_name');

				$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

				imagettftext($jpg_image, 16, 0, 60, 60, $black, $font_path, $user_name);

				//imagettftext($jpg_image, 22, 0, 500, 300, $white, $font_path, $first_names[0]);

				//imagettftext($jpg_image, 42, 0, 300, 170, $black, $font_path, $best_friends[$best_friends_keys[0]]);

				imagettftext($jpg_image, 16, 0, 240, 60, $black, $font_path, $first_names[$best_friends_keys[0]]);

				imagettftext($jpg_image, 16, 0, 410, 60, $black, $font_path, $first_names[$best_friends_keys[1]]);

				imagettftext($jpg_image, 16, 0, 580, 60, $black, $font_path, $first_names[$best_friends_keys[2]]);



				imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

				imagedestroy($jpg_image);



				redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);

			} else {

				$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

				redirect('apps/home');

			}

		}else {

			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

			redirect('apps/home');

		}

	}

	

	public function generic_function($app){

		//$this->print_exit($app);

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_id = $this->session->userdata("user_id");

		$where_conditon = array("app_id"=>$app[0]->id,"status"=>"active");

		$image_objects = $this->Apps_model->FetchData('image_objects','*',$where_conditon,'','desc');

		$text_objects = $this->Apps_model->FetchData('text_objects','*',$where_conditon,'','desc');

		$source_img = "source.jpg";

		$random_number = "";

		if($app[0]->background=='random'){

			$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");

            $random_number = rand(1, count($images) - 3);

			$source_img =  $random_number.".jpg";

		}elseif ($app[0]->background=='static'){

            $random_number = rand(1,10);

        }

        $source_img = "container/input-images/".$app[0]->id."/".$source_img;

		$this->merge_data($source_img,$image_objects,$text_objects,"container/output-images/".$app[0]->id."/".$user_id."-".$random_number.".jpg",$user_id,$app[0]->type);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title)))

            . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" .$user_id."-".$random_number);

	}

	public function valentine_letter($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		$source_img=rand(1,26);

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}



		exec('convert -size 180x180 xc:none -fill "/home/premam/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 90,90,1,90" "/home/premam/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/$source_img.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 60, 70);



		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 255, 207, 207);

		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';



		imagettftext($jpg_image, 40, 0, 440, 240, $black, $font_path, $user_name[0]);

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 100);

		imagedestroy($jpg_image);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



	}

	public function tattoo($app)

	{

		$user_id = $this->session->userdata("user_id");

		$user_name = $this->session->userdata('first_name');

		$gender = $this->session->userdata("gender");

		$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");

		if($app[0]->id==415){

			$full_name = $this->session->userdata("first_name");

			if($gender=="male"){

				$source_img = rand(1, 5);

			}

			else{

				$source_img = rand(6, 8);

			}







			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 200,200);


			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 20, 60);


			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 22, 31, 62);

			$font_path = 'assets/fonts/Kozuka.otf';

			//imagettftext($jpg_image, 14, 0, 202, 275, $black, $font_path, $full_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		if($app[0]->id==377){

			$full_name = $this->session->userdata("first_name");

			if($gender=="male"){

				$source_img = rand(1, 25);

			}

			else{

				$source_img = rand(26, 34);

			}







			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 160,160);


			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 200, 100);


			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 22, 31, 62);

			$font_path = 'assets/fonts/Kozuka.otf';

			imagettftext($jpg_image, 14, 0, 202, 275, $black, $font_path, $full_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		if($app[0]->id==374){

			$full_name = $this->session->userdata("first_name");

			if($gender=="male"){

				$source_img = rand(1, 16);

			}

			else{

				$source_img = rand(17, 26);

			}







			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 160,160);


			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 200, 100);


			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 22, 31, 62);

			$font_path = 'assets/fonts/Kozuka.otf';

			imagettftext($jpg_image, 14, 0, 202, 275, $black, $font_path, $full_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		if($app[0]->id==369){

			$full_name = $this->session->userdata("name");

			if($gender=="male"){

				$source_img = rand(1, 14);

			}

			else{

				$source_img = rand(15, 22);

			}







			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 160,160);


			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 200, 100);


			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 38, 53, 80);

			$font_path = 'assets/fonts/Kozuka.otf';

			imagettftext($jpg_image, 14, 0, 202, 276, $black, $font_path, $user_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		

		if($app[0]->id==365){

			$full_name = $this->session->userdata("name");

			if($gender=="male"){

				$source_img = rand(1, 14);

			}

			else{

				$source_img = rand(15, 22);

			}

			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 180,180);

			$this->crop_pic("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_small.jpg", 60,60);




			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_small.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 90, 5);

			$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 185, 105);




			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 0, 0, 0);

			$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

			imagettftext($jpg_image, 15, 0, 165, 25, $black, $font_path, $full_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		if($app[0]->id==364){

			$full_name = $this->session->userdata("name");

			if($gender=="male"){

				$source_img = rand(1, 18);

			}

			else{

				$source_img = rand(19, 28);

			}







			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 160,160);

			//exec('convert -size 200x200 xc:none -fill "/home/premam/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 100,100,1,100" "/home/premam/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 200, 100);

			//$this->crop_pic("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 500);

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 38, 53, 80);

			$font_path = 'assets/fonts/Kozuka.otf';

			imagettftext($jpg_image, 14, 0, 202, 276, $black, $font_path, $user_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		

	}

	public function merge_data($source_name,$images_objects,$text_objects,$output_name,$user_id,$type){

		list($width_x, $height_x) = getimagesize($source_name);

		$source_image = imagecreatefromjpeg($source_name);

		$output_image = imagecreatetruecolor($width_x, $height_x);

		imagecopy($output_image, $source_image, 0, 0, 0, 0, $width_x, $height_x);

        $friend_final_array = "";

		$j=0;

		$array_to_map_text = "";

		$friend_names = "";

		if($type=='friends') {

            if(!$this->session->userdata('friends')) {

                $user_feed = $this->get_user_feed();

                if (isset($user_feed->feed)) {

                    $friends = $this->best_friends_from_feed($user_feed->feed->data);

                    if (count($friends) >= (count($images_objects) - 1)) {

                        if (array_key_exists($user_id, $friends)) {

                            unset($friends[$user_id]);

                        }

                        $random_array = $this->unique_random_numbers_within_range(0, count($friends) - 1, count($images_objects) - 1);

                        $best_friends_keys = array_keys($friends);

                        for ($i = 0; $i < count($random_array); $i++) {

                            $friend_final_array[$i] = $best_friends_keys[$random_array[$i]];

                        }

                        //$this->print_exit($friend_final_array);

                        $this->get_profile_pics($friend_final_array);

                        $friend_names = $this->get_first_names($friend_final_array);

                    } else {

                        $this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

                        redirect('apps/home');

                    }

                } else {

                    $this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

                    redirect('apps/home');

                }

            }else{

                $friends = $this->session->userdata('friends');

                if (count($friends) >= (count($images_objects) - 1)) {

                    if (array_key_exists($user_id, $friends)) {

                        unset($friends[$user_id]);

                    }

                    $random_array = $this->unique_random_numbers_within_range(0, count($friends) - 1, count($images_objects) - 1);

                    $best_friends_keys = array_keys($friends);

                    for ($i = 0; $i < count($random_array); $i++) {

                        $friend_final_array[$i] = $best_friends_keys[$random_array[$i]];

                    }

                    //$this->print_exit($friend_final_array);

                    $this->get_profile_pics($friend_final_array);

                    $friend_names = $this->get_first_names($friend_final_array);

                } else {

                    $this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

                    redirect('apps/home');

                }

            }

		}

		for($i=0;$i<count($images_objects);$i++) {

			$path_of_image_to_merge = "";

			if($images_objects[$i]['type']=='profile_pic'){

				$path_of_image_to_merge="container/user-photos/$user_id/profile_pic";

			}else if($images_objects[$i]['type']='friend_pic'){

				$path_of_image_to_merge= "container/user-photos/$user_id/friends/".$friend_final_array[$j];

				$array_to_map_text[$images_objects[$i]['id']]=$friend_final_array[$j];

				$j++;

			}

			$image_to_merge= imagecreatefromjpeg($path_of_image_to_merge.".jpg");

			if ($images_objects[$i]['width'] < 400 || $images_objects[$i]['height'] < 400) {

				/* read the source image */

				$width = imagesx($image_to_merge);

				$height = imagesy($image_to_merge);

				/* find the "desired height" of this thumbnail, relative to the desired width  */

				$desired_height = floor($height * ($images_objects[$i]['width'] / $width));

				/* create a new, "virtual" image */

				$virtual_image = imagecreatetruecolor($images_objects[$i]['width'],$images_objects[$i]['height']);

				/* copy source image at a resized size */

				imagecopyresampled($virtual_image, $image_to_merge, 0, 0, 0, 0, $images_objects[$i]['width'], $images_objects[$i]['height'], $width, $height);

				/* create the physical thumbnail image to its destination */

				$image_to_merge = $virtual_image;

			}

			if($images_objects[$i]['shape']=='circle'){

                    imagejpeg($image_to_merge,$path_of_image_to_merge."_cropped.jpg",100);

                    exec('convert -size '.imagesx($image_to_merge).'x'.imagesx($image_to_merge).' xc:none -fill "/home/premam/public_html/'.

                        $path_of_image_to_merge.'_cropped.jpg" -draw "circle '.(imagesx($image_to_merge)/2).','.(imagesx($image_to_merge)/2).',1,'.

                        (imagesx($image_to_merge)/2).'" "/home/premam/public_html/'.$path_of_image_to_merge.'_circle.png"');

                $image_to_merge= imagecreatefrompng($path_of_image_to_merge."_circle.png");

			}

			imagecopy($output_image, $image_to_merge, $images_objects[$i]['position_x'], $images_objects[$i]['position_y'], 0, 0, imagesx($image_to_merge), imagesy($image_to_merge));  //  top, left, border,border

			//imagedestroy($virtual_image);

			//imagedestroy($image_to_merge);

		}

		for($i=0;$i<count($text_objects);$i++){

			$color_comb = explode(",",$text_objects[$i]['color']);

			$color = imagecolorallocate($output_image, $color_comb[0], $color_comb[1], $color_comb[2]);

			$font_path = 'assets/fonts/'.$text_objects[$i]['style'];

			$text = $text_objects[$i]['text'];

			if($text_objects[$i]['type']=='random'){

				$text_array = explode("||",$text_objects[$i]['text']);

				$random_number = rand(0,count($text_array)-1);

				$text = $text_array[$random_number];

			}elseif($text_objects[$i]['type']=='own_name'){

				$text = explode(" ",$this->session->userdata('name'))[0];



			}elseif($text_objects[$i]['type']=='friend_name'){

				$text =explode(" ",$friend_names[$array_to_map_text[$text_objects[$i]['img_reference']]])[0];

			}

			imagettftext($output_image, $text_objects[$i]['size'],$text_objects[$i]['angle'],$text_objects[$i]['position_x'], $text_objects[$i]['position_y'], $color, $font_path,$text);

		}

		imagejpeg($output_image, $output_name, 100);

		imagedestroy($source_image);

		imagedestroy($output_image);

	}

	public function view_output($category_name,$app_title,$category_id,$app_id,$user_id)

	{



		$data['app'] = array("category_name" => $category_name, "app_titile" => $app_title, "category_id" => $category_id, "app_id" => $app_id, "user_id" => $user_id);

		$data['menu_names'] = $this->get_category_names_menu();

		//$data['header_adds'] = $this->get_adds_from_db('header', 1);

		//$data['footer_adds'] = $this->get_adds_from_db('footer', 1);

		if (count($this->get_adds_from_db('side', 1)) == 0) { //if all add scripts are in inactive mode display

			$data['app_adds'] = $this->get_apps_as_adds(5, 4);

			if ($category_id == 5) {

				$data['app_adds'] = $this->get_apps_as_adds(4, 4);

			} else{

				$data['app_adds'] = $this->get_apps_as_adds(5, 4);

			}

		} else {

			$data['side_adds'] = $this->get_adds_from_db('side', $category_id);

		}

	

		/*if($this->session->userdata('user_id')){

			$data['try_again'] = "yes";

		}else{

			$data['try_again'] = "no";

		}*/
                
                

		if($this->session->userdata('user_id')){

                $data['try_again'] = "yes";

                $first_time = $this->session->userdata('first_time');

                $date=explode("-",$user_id);

                

               

                $share = $this->session->userdata('share');

                        

                   if ($first_time == NULL) {

                   

                        if($share=='yes'){

                           

			$this->refresh_access_token();	

                         $access_token = $this->session->userdata("access_token");

                         $persmissions = @file_get_contents("https://graph.facebook.com/me/permissions/publish_actions?access_token=$access_token");

                          

                         //$persmissions = $this->print_exit(json_decode($persmissions));

                         $persmissions = json_decode($persmissions);
                           @file_get_contents("https://graph.facebook.com/?id=$link&scrape=true&access_token=$access_token");
                          if(count($persmissions->data)>0){

                         if($persmissions->data[0]->status=='granted'){

                         $ch = curl_init();

                         curl_setopt($ch, CURLOPT_URL,"https://graph.facebook.com/me/feed?access_token=" . $access_token);

                         curl_setopt($ch, CURLOPT_POST, 1);

                        $link = "link=".base_url()."app/fun/$app_title/$category_id/$app_id?ouid=$user_id";



                       
                         curl_setopt($ch, CURLOPT_POSTFIELDS, $link);                             

                         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                         $server_output = curl_exec($ch);

                         curl_close ($ch);    
                            //file_put_contents('application/logs/shared_urls.txt',$link."\n",FILE_APPEND);
                         }

                        }

                         $this->session->set_userdata("first_time", "true"); 

                   }

                   }

                

                }else{

			$data['try_again'] = "no";

		}

		$data['page'] = 'view_output';

		$data['meta_tags'] = $this->get_meta_tags('apps', $app_id);

		//$data['apps'] = $this->get_apps_like_index();

		$data['suggestion_apps'] = $this->get_latest_apps_category($category_id, 12);



			$this->load->view('templates/view_header', $data);

			$this->load->view('view_output');

			$this->load->view('templates/view_footer');

	}

	public function get_login_url()

	{

		return "https://www.facebook.com/dialog/oauth?client_id=".$this->FB_APP_ID."&redirect_uri=".base_url()."apps/redirect-to&scope=email,user_photos,user_posts,publish_actions";



	}

	public function redirect_to()

	{

		$arr = $_GET;

		if (isset($arr['code'])) {



		if ($arr['code']) {

			$arrRes = @file_get_contents("https://graph.facebook.com/oauth/access_token?client_id=" . $this->FB_APP_ID . "&redirect_uri=" . urlencode(base_url() . "apps/redirect-to/?share=".$this->input->get('share'))."&client_secret=" . $this->FB_SECRET_KEY . "&code=" . $arr['code']);

                       $share = $this->input->get('share');
            if($arrRes===false){

              
                redirect("https://www.facebook.com/dialog/oauth?client_id=".$this->FB_APP_ID."&redirect_uri=".urlencode(base_url()."apps/redirect-to/?share".$this->input->get('share'))."&scope=email,user_photos,user_posts");

            }
                          if($this->input->get('share')=='yes' ){

                            $this->session->set_userdata("share","yes");
                           //file_put_contents('application/logs/with_share.txt',"with_share "."\n",FILE_APPEND);
                    }else{
                            $this->session->set_userdata("share","no");     
                          file_put_contents('application/logs/without_share.txt',"without_share "."\n",FILE_APPEND);
                    }
                    
  $this->session->set_userdata('first_login','true');
            /*if($this->session->userdata("login-failed")){

                log_message('error','Request got success for '.$this->session->userdata("login-failed")."th time");

            }else{

                log_message('error',"Request got success for 0th time");

            }*/

			$explod1 = explode('&', $arrRes);

			$access_token = json_decode($explod1[0])->access_token;

			$user = @file_get_contents("https://graph.facebook.com/me?fields=id,name,email,first_name,last_name,age_range,gender&access_token=" . $access_token);

            $user = json_decode($user);

			$is_user_exists = $this->is_user_exists($user->id);

			//$photos_json = file_get_contents("https://graph.facebook.com/me/photos?fields=id,created_time,from,likes.limit(5000).summary(true){id,name},comments.limit(1000).summary(true){from,message,id,likes}&limit=2000&access_token=" . $access_token);

			//$date = date('Y-m-d H:i:s');

			//$photos_data = array('photos_details'=>$photos_json,'status'=>'active'

			//,'created_at'=>$date,'updated_at'=>$date);

			$user_data = array("facebook_id" => $user->id, "first_name" => $user->first_name

			, "last_name" => $user->last_name, "access_token" => $access_token, "name" => $user->name);

			if (isset($user->email)) {

				$user_data['email'] = $user->email;

			}

			if (isset($user->gender)) {

				$user_data['gender'] = $user->gender;

			}

			if (isset($user->age_range)) {

				$user_data['age_range'] = $user->age_range->min;

			}

			if (!count($is_user_exists)) {



				$inserted_id = $this->insert_user($user_data);

				if (count($inserted_id)) {

					//	$photos_data['user_id'] = $inserted_id;

					//	$this->Apps_model->InsertData('user_photos',$photos_data);

					$user_data['user_id'] = $inserted_id;

					$this->session->set_userdata($user_data);

					$redirect_app = $this->session->userdata("redirect_app");

					$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=200&width=200" . "&access_token=" . $access_token);

                                        

					if (!file_exists("container/user-photos/" . $inserted_id)) {

						mkdir("container/user-photos/" . $inserted_id, 0777, true);

					}

					$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $inserted_id . "/profile_pic.jpg");

					redirect("app/" . $redirect_app['category_name'] . "/" . $redirect_app['app_title'] . "/" . $redirect_app['category_id'] . "/" . $redirect_app['app_id']);

				}

			} else {

				$old_user = $this->get_user($user->id);

				



				$user_data['user_id'] = $old_user[0]->id;

				

				$this->session->set_userdata($user_data);

				$redirect_app = $this->session->userdata("redirect_app");

				$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=200&width=200" . "&access_token=" . $access_token);

                if (!file_exists("container/user-photos/" . $old_user[0]->id)) {

					mkdir("container/user-photos/" . $old_user[0]->id, 0777, true);

				}

				$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $old_user[0]->id . "/profile_pic.jpg");

				$this->session->unset_userdata("redirect_app");

				redirect("app/" . $redirect_app['category_name'] . "/" . $redirect_app['app_title'] . "/" . $redirect_app['category_id'] . "/" . $redirect_app['app_id']);

			}

		}

		}else{

			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

			redirect('apps/home');

		}

	}

	public function redirect_to_login($access_token,$app_id){



		$user = file_get_contents("https://graph.facebook.com/me?fields=id,name,email,first_name,last_name,age_range&access_token=".$access_token);

		$user = json_decode($user);

		$is_user_exists = $this->is_user_exists($user->id);

		$user_data = array("facebook_id"=>$user->id,"first_name"=>$user->first_name

		,"last_name"=>$user->last_name,"access_token"=>$access_token,"name"=>$user->name);

		if(!count($is_user_exists)){

			if(isset($user->email)){

				$user_data['email']=$user->email;

			}

			if(isset($user->gender)){

				$user_data['gender']=$user->gender;

			}

			if(isset($user->age_range)){

				$user_data['age_range'] = $user->age_range->min;

			}

			$inserted_id = $this->insert_user($user_data);

			if(count($inserted_id)){

				$user_data['user_id'] = $inserted_id;

				$this->session->set_userdata($user_data);

				$redirect_app = $this->get_app_details($app_id);

				$picture = file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=200&width=200"."&access_token=".$access_token);

				if (!file_exists("container/user-photos/".$inserted_id)) {

					mkdir("container/user-photos/".$inserted_id, 0777, true);

				}

				$this->save_image(json_decode($picture)->data->url,"container/user-photos/".$inserted_id."/profile_pic.jpg");

			}

		}else{

			$old_user = $this->get_user($user->id);

			$user_data['user_id'] = $old_user[0]->id;

			$this->session->set_userdata($user_data);

			$redirect_app = $this->get_app_details($app_id);

			$picture = file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=200&width=200"."&access_token=".$access_token);

			if (!file_exists("container/user-photos/".$old_user[0]->id)) {

				mkdir("container/user-photos/".$old_user[0]->id, 0777, true);

			}

			$this->save_image(json_decode($picture)->data->url,"container/user-photos/".$old_user[0]->id."/profile_pic.jpg");

		}

		echo base_url()."app/telugu/" .strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $redirect_app[0]->title)))  . "/" . $redirect_app[0]->category_id . "/" . $redirect_app[0]->id;

	}

	public function is_user_exists($facebook_id){

		

		return $this->Apps_model->is_user_exists($facebook_id);

	}

	public function is_logged_in(){

		return $this->session->userdata('user_id');

	}

	public function insert_user($user_data){

		

		return $this->Apps_model->insert_user($user_data);

	}

	public function get_user($facebook_id){

		

		return $this->Apps_model->get_user($facebook_id);

	}

	public function merge_images($picture_one, $picture_two, $filename_result,$left,$top) {

		// Get dimensions for specified images

		list($width_x, $height_x) = getimagesize($picture_one);

		list($width_y, $height_y) = getimagesize($picture_two);

		// Create new image with desired dimensions

		$image = imagecreatetruecolor($width_x, $height_x);

		// Load images and then copy to destination image

		$image_x = imagecreatefromjpeg($picture_one);

		$image_y = imagecreatefromjpeg($picture_two);

		imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);                     //  top, left, border,border

		imagecopy($image, $image_y, $left, $top,0, 0, $width_y, $height_y);

		// Save the resulting image to disk (as JPEG)

		imagejpeg($image, $filename_result,80);

		// Clean up

		imagedestroy($image);

		imagedestroy($image_x);

		imagedestroy($image_y);

	}

	public function save_image($source_url,$target_path){

	$ch = curl_init ($source_url);

	curl_setopt($ch, CURLOPT_HEADER, 0);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);

	curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 0);

	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$rawdata=curl_exec($ch);

	curl_close ($ch);

	$fp = fopen($target_path,'w');

	fwrite($fp, $rawdata);

	fclose($fp);

}



	public function print_exit($array) {

		echo "<pre>";

		print_r($array);

		echo "</pre>";

		exit;

	}

	public function get_adds_from_db($add_type,$add_num) {

		

		return $this->Apps_model->get_adds_from_db($add_type,$add_num);

	}



	public function get_apps_as_adds($category_id,$required_apps) {

		

		return $this->Apps_model->get_apps_as_adds($category_id,$required_apps);

	}



	public function get_category_names_menu() {

		

		return $this->Apps_model->get_category_names_menu();

	}

	public function crop_pic($src, $dest, $desired_width) {



			/* read the source image */

			$source_image = imagecreatefromjpeg($src);

			$width = imagesx($source_image);

			$height = imagesy($source_image);

                        if(!file_exists($src)){

                            $function =   debug_backtrace();

                          

                            log_message('error','function_name : '.$function[0]['function'].' url : '.$function[0]['object']->uri->uri_string.' user_id : '.json_encode($function[0]['object']->session->userdata) . "session_id ". session_id());

                        }

			/* find the "desired height" of this thumbnail, relative to the desired width  */

			$desired_height = floor($height * ($desired_width / $width));



			/* create a new, "virtual" image */

			$virtual_image = imagecreatetruecolor($desired_width, $desired_height);



			/* copy source image at a resized size */

			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);



			/* create the physical thumbnail image to its destination */

			imagejpeg($virtual_image, $dest,100);

	}

	public function get_meta_tags($table,$id){

			$this->load->model('Apps_model');

			$meta_tags = $this->Apps_model->get_meta_tags($table,$id);

			return $meta_tags;

	}

	public function increment_count($column,$app_id){

		$this->load->model('Apps_model');

		$this->Apps_model->increment_count($column,$app_id);

	}

	public function update_pycker_clicks($slot_id){

		$this->load->model('Apps_model');

		$this->Apps_model->update_pycker_clicks($slot_id);

	}

	public function admin_home() {



		$this->load->view('admin_home');

	}

	public function admin_panel() {



		$this->load->view('admin_panel');

	}



	public function data_submitted() {

		$data = array(

			'name' => $this->input->post('appname'),

			'title' => $this->input->post('apptitle'),

			'category_id' => $this->input->post('category'),

			'picture_dimensions' => $this->input->post('picdimn'),

			'picture_position' => $this->input->post('picposn'),

			'meta_keywords' => $this->input->post('metakeywords'),

			'meta_title' => $this->input->post('metatitle'),

			'meta_description' => $this->input->post('metadesc')

		);



		$this->insert_app($data);

	}

	public function insert_app($data)

	{

		$last_insert_id = $this->Apps_model->insert_app($data);

		if(!file_exists("container/input-images/".$last_insert_id)){

			mkdir("container/input-images/".$last_insert_id);

		}

		if(!file_exists("container/output-images/".$last_insert_id)){

			mkdir("container/output-images/".$last_insert_id);

		}

	}

	 public function privacy_policy(){

		 $data['header_adds']=$this->get_adds_from_db('header',1);

		 $data['footer_adds']=$this->get_adds_from_db('footer',1);

		 if(count($this->get_adds_from_db('side',1))==0) { //if all add scripts are in inactive mode display

			 $data['app_adds']=$this->get_apps_as_adds(1,3);

		 }

		 else {

			 $data['side_adds']=$this->get_adds_from_db('side',1);

		 }

		 $data['meta_tags'] = $this->get_meta_tags('apps_categories',9);

		 $data['menu_names']=$this->get_category_names_menu();

   		$this->load->view('templates/view_header',$data);

        $this->load->view('terms_and_conditions');

     

    }

	public function get_first_names_new($friends,$limit){

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		if(array_key_exists($user_id,$friends)){

			unset($friends[$user_id]);

		}

		$friends_keys = array_keys($friends);

		$first_names = array();

		for($i=0;$i<$limit;$i++){

			$user_json = file_get_contents("https://graph.facebook.com/$friends_keys[$i]?fields=name&access_token=".$access_token);

			$user_decoded = json_decode($user_json);

			$first_names[$friends_keys[$i]] = explode(" ",$user_decoded->name)[0];

		}

		return $first_names;

	}

	public function get_profile_pics_new($friends,$limit) {

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		if(array_key_exists($user_id,$friends)){

			unset($friends[$user_id]);

		}

		$friends_keys = array_keys($friends);



		for($i=0;$i<$limit;$i++){



			$picture = file_get_contents("https://graph.facebook.com/$friends_keys[$i]/picture?redirect=false&height=200&width=200&access_token=".$access_token);

			if (!file_exists("container/user-photos/".$user_id."/friends")) {

				mkdir("container/user-photos/".$user_id."/friends", 0777, true);

			}

			$this->save_image(json_decode($picture)->data->url,"container/user-photos/".$user_id."/friends"."/$friends_keys[$i].jpg");

		}

	}

	public function get_profile_pics_old($friends,$limit) {

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		if(array_key_exists($user_id,$friends)){

			unset($friends[$user_id]);

		}

		$friends_keys = array_keys($friends);



		for($i=0;$i<$limit;$i++){



			$picture = file_get_contents("https://graph.facebook.com/$friends_keys[$i]/picture?redirect=false&height=200&width=200&access_token=".$access_token);

			if (!file_exists("container/user-photos/".$user_id."/friends")) {

				mkdir("container/user-photos/".$user_id."/friends", 0777, true);

			}

			$this->save_image(json_decode($picture)->data->url,"container/user-photos/".$user_id."/friends"."/$friends_keys[$i].jpg");

		}

	}

    public function get_profile_pics($friends_final) {

        $this->refresh_access_token();

        $access_token = $this->session->userdata('access_token');

        $user_id = $this->session->userdata('user_id');

        if (!file_exists("container/user-photos/".$user_id."/friends")) {

            mkdir("container/user-photos/".$user_id."/friends", 0777, true);

        }

        for($i=0;$i<count($friends_final);$i++){

            if(!file_exists("container/user-photos/".$user_id."/friends/".$friends_final[$i])){

                $picture = file_get_contents("https://graph.facebook.com/$friends_final[$i]/picture?redirect=false&height=200&width=200&access_token=".$access_token);

                $this->save_image(json_decode($picture)->data->url,"container/user-photos/".$user_id."/friends"."/$friends_final[$i].jpg");

            }

        }

    }

	public function best_friends($photos){

		$common_ids = array();

		if(isset($photos->photos)){

			$facebook_id = $this->session->userdata("facebook_id");

			$likes_arrays_by_photo = array();

			$tags_arrays_by_photo = array();

			$from_arrays_by_photo = array();

			$tags_arrays_offset = 0;

			for($i=0;$i<count($photos->photos->data);$i++){

				if(isset($photos->photos->data[$i]->likes)){

					$likes_arrays_by_photo[$i] = $this->pluck($photos->photos->data[$i]->likes->data,"id");

				}else{

					$likes_arrays_by_photo[$i] = array();

				}

				if(count($photos->photos->data[$i]->tags->data)>1){

					if($from_arrays_by_photo[$i] = $photos->photos->data[$i]->from->id == $facebook_id){

						for($j=0;$j<count($photos->photos->data[$i]->tags->data);$j++){

							$tags_arrays_by_photo[$tags_arrays_offset] = $photos->photos->data[$i]->tags->data[$j]->id;

							$tags_arrays_offset ++;

						}

					}

				}

				$from_arrays_by_photo[$i] = $photos->photos->data[$i]->from->id;

			}



			$likes_array = array();

			for($i=0;$i<count($likes_arrays_by_photo);$i++){

				$likes_array = array_merge($likes_array,$likes_arrays_by_photo[$i]);

			}

			$ids_with_likes_count 	= array_count_values($likes_array);

			$ids_with_from_count 	= array_count_values($from_arrays_by_photo);

			$ids_with_tags_count 	= array_count_values($tags_arrays_by_photo);

			arsort($ids_with_likes_count);

			arsort($ids_with_from_count);

			arsort($ids_with_tags_count);



			unset($ids_with_tags_count[$facebook_id]);

			unset($ids_with_from_count[$facebook_id]);

			unset($ids_with_likes_count[$facebook_id]);





			if(!(count($common_ids)>=10)){

				if(count($ids_with_tags_count) > count($common_ids)){

					$commonids_tags_and_commonids = array_diff_key ($ids_with_tags_count,$common_ids);

					$common_ids = $common_ids+$commonids_tags_and_commonids;

				}

			}

			if(!(count($common_ids)>=10)){

				if(count($ids_with_from_count) > count($common_ids)){

					$array_diff = array_diff_key ($ids_with_from_count,$common_ids);

					$common_ids = $common_ids+$array_diff;

				}

			}

			arsort($common_ids);

			if(!(count($common_ids)>=10)){

				if(count($ids_with_likes_count)>count($common_ids)){

					$array_diff = array_diff_key ($ids_with_likes_count,$common_ids);

					arsort($array_diff);

					$common_ids = $common_ids+$array_diff;

				}

			}

		}

		return $common_ids;

	}



	public  function pluck($collection, $property)

	{

		$plucked = array_map(function ($value) use ($property) {

			return $this->get($value, $property);

		}, (array) $collection);

		// Convert back to object if necessary

		if (is_object($collection)) {

			$plucked = (object) $plucked;

		}

		return $plucked;

	}

	public  function get($collection, $key, $default = null)

	{

		if (is_null($key)) {

			return $collection;

		}

		$collection = (array) $collection;

		if (isset($collection[$key])) {

			return $collection[$key];

		}

		// Crawl through collection, get key according to object or not

		foreach (explode('.', $key) as $segment) {

			$collection = (array) $collection;

			if (!isset($collection[$segment])) {

				return $default instanceof Closure ? $default() : $default;

			}

			$collection = $collection[$segment];

		}

		return $collection;

	}

	public function refresh_access_token(){

		$access_token = $this->session->userdata("access_token");

		$fresh_token = file_get_contents("https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=" . $this->FB_APP_ID . "&client_secret=". $this->FB_SECRET_KEY . "&fb_exchange_token=" . $access_token);

		$explod1 = explode('&', $fresh_token);

		$access_token = json_decode($explod1[0])->access_token;

		$this->session->set_userdata("access_token",$access_token);

	}

	public  function get_user_photos($since,$until)

	{

		$this->refresh_access_token();

		$access_token = $this->session->userdata("access_token");

		$photos_json = file_get_contents("https://graph.facebook.com/me?fields=photos.limit(50){id,created_time,likes{id,name},from,tags{id}}&access_token=" .$access_token);

		$photos_decoded = json_decode($photos_json);

		return $photos_decoded;

	}

	function logout()

	{

		$user_data = $this->session->all_userdata();

		foreach ($user_data as $key => $value) {

			if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {

				$this->session->unset_userdata($key);

			}

		}

		$this->session->sess_destroy();

		redirect('apps/home');

	}

	public function get_statistics(){



		$where_conditon = array("status"=>"active");

		$details = $this->Apps_model->FetchData('apps','*',$where_conditon,'','DESC');

		$details = array_reverse($details);

		$where_conditon = array("status"=>"active");

		$total = $this->Apps_model->FetchData('apps','sum(users) as total_users,sum(shares) as total_shares',$where_conditon);

		//$this->print_exit($total);

		echo "<table align='center' border='1'>";

		echo "<tr><td>S.no</td><td>Name</td><td>Users</td><td>Shares</td></tr>";

		for($i=0;$i<count($details);$i++){

			echo "<tr>";

			echo "<td>".($i+1)."</td>";

			echo "<td>".$details[$i]['title']."</td>";

			echo "<td>".$details[$i]['users']."</td>";

			echo "<td>".$details[$i]['shares']."</td>";

			echo "</tr>";

		}

		echo "<tr style='text-align: center;background: green'><td colspan='2' >Total</td><td>".$total[0]['total_users']."</td><td>".$total[0]['total_shares']."</td></tr>";

		echo "</table>";

	}

	public function unique_random_numbers_within_range($min, $max, $quantity) {

		$numbers = range($min, $max);

		shuffle($numbers);

		return array_slice($numbers, 0, $quantity);

	}

	public function get_first_names_old($friends,$limit){

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		if(array_key_exists($user_id,$friends)){

			unset($friends[$user_id]);

		}

		$friends_keys = array_keys($friends);

		$first_names = array();

		for($i=0;$i<$limit;$i++){

			$user_json = file_get_contents("https://graph.facebook.com/$friends_keys[$i]?fields=name&access_token=".$access_token);

			$user_decoded = json_decode($user_json);

			$first_names[$friends_keys[$i]] = explode(" ",$user_decoded->name)[0];

		}

		return $first_names;

	}

    public function get_first_names($friend_final_array){

        $this->refresh_access_token();

        $access_token = $this->session->userdata('access_token');

        $user_id = $this->session->userdata('user_id');

        $first_names = array();

        for($i=0;$i<count($friend_final_array);$i++){

            $user_json = file_get_contents("https://graph.facebook.com/$friend_final_array[$i]?fields=name&access_token=".$access_token);

            $user_decoded = json_decode($user_json);

            $first_names[$friend_final_array[$i]] = explode(" ",$user_decoded->name)[0];

        }

        return $first_names;

    }

	public function get_apps_like_index(){

		$apps = array();

		$key=0;

		for($key;$key<7;$key++)

		{

			$apps[$key]= $this->get_latest_apps_category($key + 1,3);

		}

		$temp= $apps[0];

		$apps[0] = $apps[4];

		$apps[4] = $temp;

		return $apps;

	}

	public function has_access_token($app){

		$access_token = $this->session->userdata("access_token");

		if($access_token==NULL || $access_token=="" || $access_token=='' || strlen($access_token)<1 || $access_token==FALSE || empty($access_token) || !isset($access_token) ){

			$user_data = $this->session->all_userdata();

			foreach ($user_data as $key => $value) {

				if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {

					$this->session->unset_userdata($key);

				}

			}

			$this->session->sess_destroy();

			redirect("app/".strtolower($app[0]->category_name)."/".strtolower(str_replace("?","",str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id);

		}

	}

	public function best_friends_from_feed($feed)

	{



		$common_ids = array();



		$facebook_id = $this->session->userdata("facebook_id");



		$likes_arrays_by_feed = array();



		$tags_arrays_by_feed = array();



		$from_arrays_by_feed = array();



		$tags_arrays_offset = 0;



		for ($i = 0; $i < count($feed); $i++) {



			if (isset($feed[$i]->likes)) {



				$likes_arrays_by_feed[$i] = $this->pluck($feed[$i]->likes->data, "id");



			} else {



				$likes_arrays_by_feed[$i] = array();



			}



			if (isset($feed[$i]->with_tags)) {



				if ($from_arrays_by_feed[$i] = $feed[$i]->from->id == $facebook_id) {



					for ($j = 0; $j < count($feed[$i]->with_tags->data); $j++) {



						$tags_arrays_by_feed[$tags_arrays_offset] = $feed[$i]->with_tags->data[$j]->id;



						$tags_arrays_offset++;



					}



				}



			}



			if (isset($feed[$i]->message_tags)) {



				if ($feed[$i]->from->id == $facebook_id) {



					for($j=0;$j<count($feed[$i]->message_tags);$j++) {



						if(isset($feed[$i]->message_tags[$j]->type)){



							if ($feed[$i]->message_tags[$j]->type == 'user') {



								$tags_arrays_by_feed[$tags_arrays_offset] = $feed[$i]->message_tags[$j]->id;



								$tags_arrays_offset++;



							}



						}



					}



				}



			}



			$from_arrays_by_feed[$i] = $feed[$i]->from->id;



		}







		$likes_array = array();



		for ($i = 0; $i < count($likes_arrays_by_feed); $i++) {



			$likes_array = array_merge($likes_array, $likes_arrays_by_feed[$i]);



		}



		$ids_with_likes_count = array_count_values($likes_array);



		$ids_with_from_count = array_count_values($from_arrays_by_feed);



		$ids_with_tags_count = array_count_values($tags_arrays_by_feed);



		arsort($ids_with_likes_count);



		arsort($ids_with_from_count);



		arsort($ids_with_tags_count);







		unset($ids_with_likes_count[$facebook_id]);



		unset($ids_with_from_count[$facebook_id]);



		unset($ids_with_tags_count[$facebook_id]);







		if (!(count($common_ids) >= 10)) {



			if (count($ids_with_tags_count) > count($common_ids)) {



				$commonids_tags_and_commonids = array_diff_key($ids_with_tags_count, $common_ids);



				$common_ids = $common_ids + $commonids_tags_and_commonids;



			}



		}



		if (!(count($common_ids) >= 10)) {



			if (count($ids_with_from_count) > count($common_ids)) {



				$array_diff = array_diff_key($ids_with_from_count, $common_ids);



				$common_ids = $common_ids + $array_diff;



			}



		}



		arsort($common_ids);



		if (!(count($common_ids) >= 10)) {



			if (count($ids_with_likes_count) > count($common_ids)) {



				$array_diff = array_diff_key($ids_with_likes_count, $common_ids);



				arsort($array_diff);



				$common_ids = $common_ids + $array_diff;



			}



		}



		return $common_ids;

	}

	public function crop_pic2($src,$dest,$desired_width,$desired_height){

		/* read the source image */

		$source_image = imagecreatefromjpeg($src);

		$width = imagesx($source_image);

		$height = imagesy($source_image);



		/* find the "desired height" of this thumbnail, relative to the desired width  */

		//$desired_height = floor($height * ($desired_width / $width));



		/* create a new, "virtual" image */

		$virtual_image = imagecreatetruecolor($desired_width, $desired_height);



		/* copy source image at a resized size */

		imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);



		/* create the physical thumbnail image to its destination */

		imagejpeg($virtual_image, $dest,100);

	}

	public function merge_images_jpg_png($picture_one, $picture_two, $filename_result, $left, $top)

	{

		// Get dimensions for specified images

		list($width_x, $height_x) = getimagesize($picture_one);

		list($width_y, $height_y) = getimagesize($picture_two);

		// Create new image with desired dimensions

		$image = imagecreatetruecolor($width_x, $height_x);

		// Load images and then copy to destination image

		$image_x = imagecreatefromjpeg($picture_one);

		$image_y = imagecreatefrompng($picture_two);

		imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);                     //  top, left, border,border

		imagecopy($image, $image_y, $left, $top, 0, 0, $width_y, $height_y);

		// Save the resulting image to disk (as JPEG)

		imagejpeg($image, $filename_result, 80);

		// Clean up

		imagedestroy($image);

		imagedestroy($image_x);

		imagedestroy($image_y);

	}

	public function get_user_feed()

	{

		$this->refresh_access_token();



		$access_token = $this->session->userdata("access_token");



		//$feed_json = @file_get_contents("https://graph.facebook.com/me?fields=feed.limit(200).until(1483142460){id,name,type,from,status_type,with_tags,message_tags,likes.limit(1000).summary(true){id}}&access_token=" . $access_token);

		$feed_json = @file_get_contents("https://graph.facebook.com/me?fields=feed.limit(200).since(1483142460){id,name,type,from,status_type,with_tags,message_tags,likes.limit(1000).summary(true){id}}&access_token=" . $access_token);

		$feed_decoded = json_decode($feed_json);



		return $feed_decoded;

	}

	public function get_user_location()

	{

		$this->refresh_access_token();

		$access_token = $this->session->userdata("access_token");

		$feed_json = file_get_contents("https://graph.facebook.com/me?fields=photos.limit(50){place}&access_token=" . $access_token);

		$feed_decoded = json_decode($feed_json);

		return $feed_decoded;

	}

	public function get_photos($photo_ids){

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		for($i=0;$i<count($photo_ids);$i++){

			$picture = file_get_contents("https://graph.facebook.com/$photo_ids[$i]/picture?redirect=false&height=200&width=200"."&access_token=".$access_token);

			if (!file_exists("container/user-photos/".$user_id."")) {

				mkdir("container/user-photos/".$user_id."", 0777, true);

			}

			$this->save_image(json_decode($picture)->data->url,"container/user-photos/".$user_id.""."/$photo_ids[$i].jpg");

		}

	}

	public function pic_of_year($photos,$user_id){

		$own_pics = array();

		for($i=0;$i<count($photos->data);$i++){

			if($photos->data[$i]->from->id == $user_id){

				if(isset($photos->data[$i]->likes)){

					$own_pics[$photos->data[$i]->id] = $photos->data[$i]->likes->summary->total_count;

				}else{

					$own_pics[$photos->data[$i]->id] = 0;

				}



			}

		}

		if(count($own_pics)>0){

			arsort($own_pics);

		}else{

			for($i=0;$i<count($photos->data);$i++){

				if(isset($photos->data[$i]->likes)){

					$own_pics[$photos->data[$i]->id] = $photos->data[$i]->likes->summary->total_count;

				}else{

					$own_pics[$photos->data[$i]->id] = 0;

				}

			}

			arsort($own_pics);

		}

		return $own_pics;

	}

	public function best_friends_from_likes($feed)

	{



		$common_ids = array();

		$facebook_id = $this->session->userdata("facebook_id");

		$likes_arrays_by_feed = array();

		for ($i = 0; $i < count($feed); $i++) {

			if (isset($feed[$i]->likes)) {

				$likes_arrays_by_feed[$i] = $this->pluck($feed[$i]->likes->data, "id");

			} else {

				$likes_arrays_by_feed[$i] = array();

			}

		}



		$likes_array = array();

		for ($i = 0; $i < count($likes_arrays_by_feed); $i++) {

			$likes_array = array_merge($likes_array, $likes_arrays_by_feed[$i]);

		}

		$ids_with_likes_count = array_count_values($likes_array);

		arsort($ids_with_likes_count);

		unset($ids_with_likes_count[$facebook_id]);

		arsort($common_ids);

		if (!(count($common_ids) >= 10)) {

			if (count($ids_with_likes_count) > count($common_ids)) {

				$array_diff = array_diff_key($ids_with_likes_count, $common_ids);

				arsort($array_diff);

				$common_ids = $common_ids + $array_diff;

			}

		}

		return $common_ids;

	}



	public function update_shared_pic($app_id,$path){

		$where_conditions = array("app_id"=>$app_id,"imagepath"=>$path);

		$data = array("status"=>"active");

		$this->Apps_model->UpdateData("generated_images",$data,$where_conditions);

	}

	public function add_new_app(){



		if($this->input->post('submit'))	{

			$post_data = 	$this->input->post();

			$data = array("category_id"=>$post_data['category_id'],"name"=>$post_data['title'],"title"=>$post_data['title'],

				"picture_dimensions"=>$post_data["picture_dimensions"],"picture_position"=>$post_data['picture_position'],

				"logic"=>'generic_function',"meta_keywords"=>$post_data['meta_keywords'],

				"meta_title"=>$post_data["meta_title"],"meta_description"=>$post_data["meta_description"],"status"=>$post_data["status"]);

			//$this->print_exit($data);

			$last_insert_id = $this->Apps_model->InsertData('apps',$data);

			$temp_inputs_images = glob("container/temp-inputs/*.jpg");

			if (!file_exists("container/input-images/" . $last_insert_id)) {

				mkdir("container/input-images/" . $last_insert_id, 0777, true);

			}

			$i=1;



			foreach($temp_inputs_images as $image)

			{

				$size = getimagesize($image);

				$this->compress_images($image,"container/input-images/$last_insert_id/$i.jpg",$size[0],$size[1]);

				unlink($image);

				$i++;

			}

			$data['maxvalue'] = count($temp_inputs_images);

			$data['app_id'] = $last_insert_id;

			//$data['profile_pic_type'] = $post_data['profile_pic_type'];

			//$data['picture_position'] = $post_data['picture_position'];

			$this->load->view('display_all_images',$data);

		}else{

			$this->load->view('add_new_app');

		}

	}

	public function select_cover_pic($app_id,$image_number,$profile_pic_type,$picture_postion){

		$position = explode("X",$picture_postion);

		$this->merge_images("container/input-images/" . $app_id . "/$image_number.jpg", "assets/imgs/" .$profile_pic_type, "container/input-images/" . $app_id . "/cover_pic.jpg", $position[0], $position[1]);

		$this->crop_pic2("container/input-images/" . $app_id . "/" . "cover_pic.jpg", "container/input-images/" . $app_id . "/" . "cover_pic_thumb.jpg", 315,160);

	}

	public function compress_images($src, $dest, $desired_width,$desired_height) {

		/* read the source image */

		$source_image = imagecreatefromjpeg($src);

		$width = imagesx($source_image);

		$height = imagesy($source_image);



		/* find the "desired height" of this thumbnail, relative to the desired width  */

		// $desired_height = floor($height * ($desired_width / $width));



		/* create a new, "virtual" image */

		$virtual_image = imagecreatetruecolor($desired_width, $desired_height);



		/* copy source image at a resized size */

		imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);



		/* create the physical thumbnail image to its destination */

		imagejpeg($virtual_image, $dest,80);

	}

	public function careers(){

		$data['header_adds']=$this->get_adds_from_db('header',1);

		$data['footer_adds']=$this->get_adds_from_db('footer',1);

		if(count($this->get_adds_from_db('side',1))==0) { //if all add scripts are in inactive mode display

			$data['app_adds']=$this->get_apps_as_adds(1,3);

		}

		else {

			$data['side_adds']=$this->get_adds_from_db('side',1);

		}

		$data['meta_tags'] = $this->get_meta_tags('apps_categories',9);

		$data['menu_names']=$this->get_category_names_menu();

		$this->load->view('templates/view_header',$data);

		$this->load->view('careers');

	}

	public function get_merged_images($picture_one, $picture_two, $filename_result, $left, $top)

	{

		list($width_x, $height_x) = getimagesize($picture_one);

		list($width_y, $height_y) = getimagesize($picture_two);

		$image = imagecreatetruecolor($width_x, $height_x);

		$image_x = imagecreatefromjpeg($picture_one);

		$image_y = imagecreatefromjpeg($picture_two);

		imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);                     //  top, left, border,border

		imagecopy($image, $image_y, $left, $top, 0, 0, $width_y, $height_y);

		return $image;

	}

	public function get_merged_images_png($picture_one, $picture_two, $filename_result, $left, $top)

	{

		list($width_x, $height_x) = getimagesize($picture_one);

		list($width_y, $height_y) = getimagesize($picture_two);

		$image = imagecreatetruecolor($width_x, $height_x);

		$image_x = imagecreatefromjpeg($picture_one);

		$image_y = imagecreatefrompng($picture_two);

		imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);                     //  top, left, border,border

		imagecopy($image, $image_y, $left, $top, 0, 0, $width_y, $height_y);

		return $image;

	}

        public function get_output_path($app_id,$user_id){

             $date =  date('y-m-d');          

             $date = explode("-", $date);

            return "container/output-images/$date[0]/$date[1]/$date[2]/$app_id/$user_id";

        }

}