<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Apps extends CI_Controller {



	/**6

	 * Index Page for this controller.l

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	function __construct() {

		parent::__construct();

		$this->load->model("Apps_model");
		$this->load->library("pagination");
		$this->load->library('facebook');
		$this->load->helper(array('form', 'url'));
		$this->load->helper(array('cookie', 'url')); 

                //session_write_close( );

	}

	private $FB_APP_ID = "487651168108939";

	private $FB_SECRET_KEY = "a2aa032e417ac39047414760631a6f75";

	public function testing_generic_function($app){

		//$this->print_exit($app);

		$user_id = 37;//$this->session->userdata("user_id");

		$where_conditon = array("app_id"=>$app[0]->id,"status"=>"active");

		$image_objects = $this->Apps_model->FetchData('image_objects','*',$where_conditon,'','desc');

		$text_objects = $this->Apps_model->FetchData('text_objects','*',$where_conditon,'','desc');

		$source_img = "container/input-images/".$app[0]->id."/source.jpg";

		if($app[0]->type=='random'){

			$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");

			$source_img =  "container/input-images/" . $app[0]->id."/".rand(1, count($images) - 2).".jpg";

		}

		$this->merge_data($source_img,$image_objects,$text_objects,"container/output-images/".$app[0]->id."/".$user_id.".jpg",$user_id,$app[0]->type);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" .$user_id);

	}

	public function testing_redirect_to_logic($category_name,$app_id){

            

		$app = $this->get_app_details($app_id);

		$app[0]->category_name = $category_name;

		$logic = 'testing_generic_function';

		$this->{$logic}($app);

	}

	public function index($page=0){
	    $data['total_vote_count']=$this->Apps_model->get_total_counts();
        $data['total_polls_count']=count($this->Apps_model->get_polls_index_admin());
		$config = array();
 
            $config["base_url"] = base_url();
 
            //$config["total_rows"] = $this->Apps_model->record_count($category_ids);
 
            $config["per_page"] = 10;
 
            $config["uri_segment"] = 4;
 
            $this->pagination->initialize($config);
            if($this->input->get("page")!=NULL){
                $page=$this->input->get("page");
            }
            else{
                $page=1;
            }
            $page=($page-1)*$config["per_page"];
            
        $data['polls']=$this->Apps_model->get_polls_index_limit($config["per_page"],$page);
		$data['latest_polls'] =$this->Apps_model->get_latest_polls_index();
        //$this->print_exit($data['polls'][0]->id);
        for($i=0;$i<count($data['polls']);$i++){
            
            $data['total_count'][$i]=$this->Apps_model->get_total_count_by_poll_id($data['polls'][$i]->id);
        }
        $data['trending_polls']=array_reverse($this->Apps_model->get_trending_poll());
        for($i=0;$i<3;$i++){
            $categories[$i]=$data['trending_polls'][$i]->poll_id;
            
        }
        $data['trending_polls_data']=$this->Apps_model->get_trending_polls($categories);
		//$this->print_exit($data);
        
        $data['page_name']="index_page";
		$this->load->view('index_page',$data);

	

	}
	public function view_polls_by_division($category_id){
	    $data['polls']=$this->Apps_model->get_polls_by_category($category_id);
        for($i=0;$i<count($data['polls']);$i++){
            
            $data['total_count'][$i]=$this->Apps_model->get_total_count_by_poll_id($data['polls'][$i]->id);
        }
        $data['page_name']="index_page";
		$this->load->view('view_polls_by_division',$data);
	}
	public function polls_by_category($language,$category){
	    //$data['total_polls_count']=$this->Apps_model->get_total_counts();
	    $data['polls']=$this->Apps_model->get_polls_by_category($category);
        //$this->print_exit($data['polls'][0]->id);
        for($i=0;$i<count($data['polls']);$i++){
            
            $data['total_count'][$i]=$this->Apps_model->get_total_count_by_poll_id($data['polls'][$i]->id);
        }
        $data['page_name']="index_page";
		$this->load->view('polls_by_category_page',$data);
	}
    public function polling_page($tile,$poll_id){
        //$data['total_polls_count']=$this->Apps_model->get_total_counts();
        $data['polls']=$this->Apps_model->get_polls($poll_id);
        $data['options']=$this->Apps_model->get_options_by_poll_id($poll_id);
        $data['page_name']="polling_page";
        $data['poll_id']=$poll_id;
        
        
        $get_cookie=get_cookie('poll_id');
        if($get_cookie==$poll_id){
            redirect(base_url()."poll-result/".$poll_id);
        }
        else{
            $this->load->view('polling_page',$data);
        }
		
        
    }
    public function poll_redirect($poll_id){
        $post=$this->input->post();
        
        if($post!=NULL){
            $data['poll_id']=$poll_id;
            $data['polls']=$this->Apps_model->get_polls($poll_id);
            $user_data=array("poll_id"=>$poll_id,"option_id"=>$post['option']);
            
            $this->Apps_model->insert_poll($user_data);
            set_cookie('poll_id',$poll_id,'86400');
            redirect(base_url()."poll-result/".$poll_id);
            //$data['poll_result']=$this->Apps_model->get_poll_result($poll_id);
            //$data['options']=$this->Apps_model->get_options_by_poll_id($poll_id);
            //$data['total_count']=$this->Apps_model->get_total_count_by_poll_id($poll_id);
            //$this->print_exit($data);
            //$this->load->view('poll_result_page',$data);
        }
        else{
            redirect(base_url()."polls/get-back/".$poll_id);
        }
        
    }
    public function poll_result($poll_id){
            $data['poll_id']=$poll_id;
            $data['polls']=$this->Apps_model->get_polls($poll_id);
            
            $data["page_name"]="poll_result_page";
            $data['poll_result']=$this->Apps_model->get_poll_result($poll_id);
            $data['options']=$this->Apps_model->get_options_by_poll_id($poll_id);
            $data['total_count']=$this->Apps_model->get_total_count_by_poll_id($poll_id);
            //$data['total_polls_count']=$this->Apps_model->get_total_counts();
            
            $data['suggestion_polls']=$this->Apps_model->view_polls_by_category($data['polls'][0]->category_id);
        for($i=0;$i<count($data['suggestion_polls']);$i++){
            
            $data['suggestion_poll_total_count'][$i]=$this->Apps_model->get_total_count_by_poll_id($data['suggestion_polls'][$i]->id);
        }

            $this->load->view('poll_result_page',$data);
    }
    public function poll_result_copy($poll_id){
            $data['poll_id']=$poll_id;
            $data['polls']=$this->Apps_model->get_polls($poll_id);
            
            $data["page_name"]="poll_result_page";
            $data['trending_polls']=array_reverse($this->Apps_model->get_trending_poll());
            
        for($i=0;$i<3;$i++){
            $categories[$i]=$data['trending_polls'][$i]->poll_id;
            
        }
        $data['poll_result']=$this->Apps_model->get_poll_result($poll_id);
        $data['trending_polls_data']=$this->Apps_model->get_trending_polls($categories);
        
            
            $data['options']=$this->Apps_model->get_options_by_poll_id($poll_id);
            $data['total_count']=$this->Apps_model->get_total_count_by_poll_id($poll_id);
            $data['total_polls_count']=$this->Apps_model->get_total_counts();
            
            $data['suggestion_polls']=$this->Apps_model->view_polls_by_category($data['polls'][0]->category_id);
        for($i=0;$i<count($data['suggestion_polls']);$i++){
            
            $data['suggestion_poll_total_count'][$i]=$this->Apps_model->get_total_count_by_poll_id($data['suggestion_polls'][$i]->id);
        }

            $this->load->view('poll_result_page_copy',$data);
    }
	public function list_apps(){

		$apps =  $this->Apps_model->get_all_apps();

		$data['apps'] = $apps;

		$this->load->view('list_apps',$data);

	}



	public function update_app_settings($app_id){$post_data = $this->input->post();

		//$post_data['global_object_json'] = '{"image_objects":{"image_object_count0":{"position_x":"89","position_y":"49","width":"182","height":"172","shape":"circle","angle":"0"}},"friends_names_objects":{},"other_text_objects":{"other_text_objects0":{"position_x":"89","position_y":"235","width":"185","height":"71","type":"own_name","color":"00,00,00","size":"20","style":"robotocondensedbold.ttf","text":"sample text","angle":"0"}}}';

		$post_data_decoded = json_decode($post_data['global_object_json']);

		//$this->print_exit($post_data_decoded);

		$image_objects = $post_data_decoded->image_objects;

		$friend_text_object="";

		if(isset($post_data_decoded->friends_names_objects)){

			if(count(get_object_vars($post_data_decoded->friends_names_objects))){

				$friend_text_object = get_object_vars($post_data_decoded->friends_names_objects);

				ksort($friend_text_object);

			}

		}

		$other_text_object = $post_data_decoded->other_text_objects;

		$image_objects = get_object_vars($image_objects);

		$other_text_object = get_object_vars($other_text_object);

		ksort($image_objects);

		ksort($other_text_object);

		$inserted_images = array();

		for($i=0;$i<count($image_objects);$i++){

			$type = "";

			if($i==0){

				$type = "profile_pic";

			}else{

				$type = "friend_pic";

			}

			$object = array("app_id"=>$app_id,"type"=>$type,"shape"=>$image_objects["image_object_count".$i]->shape,

				"height"=>$image_objects["image_object_count".$i]->height,"width"=>$image_objects["image_object_count".$i]->width,

				"position_x"=>$image_objects["image_object_count".$i]->position_x,"position_y"=>$image_objects["image_object_count".$i]->position_y,

				"angle"=>$image_objects["image_object_count".$i]->angle

			);

			$inserted_images[$i] = $this->Apps_model->InsertData('image_objects',$object);

		}

		for($i=0;$i<count($other_text_object);$i++){

			//"style"=>$other_text_object['other_text_object'.$i]->style

			$img_refference = "";

			if($i==0){

				$img_refference = $inserted_images[0];

			}else{

				$img_refference = "";

			}

			$object =  array("app_id"=>$app_id,"type"=>$other_text_object['other_text_objects'.$i]->type,

				"size"=>$other_text_object['other_text_objects'.$i]->size,

				"style"=>$other_text_object['other_text_objects'.$i]->style,

				"position_x"=>$other_text_object['other_text_objects'.$i]->position_x,

				"position_y"=>$other_text_object['other_text_objects'.$i]->position_y,

				"width"=>$other_text_object['other_text_objects'.$i]->width,

				"height"=>$other_text_object['other_text_objects'.$i]->height,

				"angle"=>$other_text_object['other_text_objects'.$i]->angle,

				"img_reference"=>$img_refference

			);

			$this->Apps_model->InsertData('text_objects',$object);

		}

		if($friend_text_object!=""){

			for($i=0;$i<count($friend_text_object);$i++){

				//"style"=>$friend_text_object['friends_names_objects'.$i]->style

				$object = array("app_id"=>$app_id,"type"=>$friend_text_object['friends_names_objects'.$i]->type,

					"size"=>$friend_text_object['friends_names_objects'.$i]->size,

					"style"=>$friend_text_object['friends_names_objects'.$i]->style,

					"position_x"=>$friend_text_object['friends_names_objects'.$i]->position_x,"position_y"=>$friend_text_object['friends_names_objects'.$i]->position_y,"angle"=>$friend_text_object['friends_names_objects'.$i]->angle,

					"img_reference"=>$inserted_images[$i+1]

				);

				$this->Apps_model->InsertData('text_objects',$object);

			}

		}



		redirect('http://localhost/witapps_admin/list-apps');

	}

	public function app_settings($app_id){

		$app = $this->Apps_model->get_all_app_details($app_id);

		$images = glob("assets/fonts/*.ttf");

		for($i=0;$i<count($images);$i++){

			$fonts_array[$i]=explode("/", $images[$i]);

		}

		//$this->print_exit($app);

		$data['fonts']=$fonts_array;

		$data['app']=$app;



		$this->load->view('app_settings',$data);

		//$this->load->view('admin_panel',$data);

	}

	public function add_app(){

	



		$login_details = 	$this->input->post();

		

		if($login_details['name']=='admin_sam' && $login_details['pass']=='admin@sam'){

		//$this->session->set_userdata($login_details);

		

		$user_data['login_name'] = $login_details['name'];

		$user_data['login_pass'] = $login_details['pass'];

		$this->session->set_userdata($user_data);

		$this->load->view('add_new_app');

		//$this->print_exit($this->input->post());

		

		}

		else{

			echo "***Fuck off***";

		}



	}

	public function addmin_add_app(){

	$user_name = $this->session->userdata("login_name");

	

	if($this->session->userdata("login_name")=='admin_sam'){

		if($this->input->post('submit')){

			$post_data = 	$this->input->post();

			

			$data = array("category_id"=>$post_data['category_id'],"title"=>$post_data['title'],

				"meta_keywords"=>$post_data['meta_keywords'],

				"meta_title"=>$post_data["meta_title"],"meta_description"=>$post_data["meta_description"],"status"=>"inactive",

				"type"=>$post_data['type'],"background"=>$post_data['background'],"image_object_count"=>$post_data['image_objects_count'],"friends_names_objects"=>$post_data['friends_names_objects'],"other_text_objects"=>$post_data['other_text_objects']);

			//$this->print_exit($data);

			$last_insert_id = $this->Apps_model->InsertData('apps',$data);

			$temp_inputs_images = glob("container/temp-inputs/*.jpg");

			if (!file_exists("container/input-images/" . $last_insert_id)) {

				mkdir("container/input-images/" . $last_insert_id, 0777, true);

			}

			if (!file_exists("container/output-images/" . $last_insert_id)) {

				mkdir("container/output-images/" . $last_insert_id, 0777, true);

				mkdir("container/output-images/" . $last_insert_id."/friends", 0777, true);

			}

			redirect('app-settings/'.$last_insert_id);

		}else{

			//$this->load->view('view_add_app');

			$this->load->view('add_new_app');

		}

	}

	else{

		echo "You are at wrong place";

	}

		

	}

	public function get_latest_apps_category($category_id,$required_apps){

		

		return $this->Apps_model->get_latest_apps_category($category_id,$required_apps);

	}

	public function get_category_apps($category_id,$page){

		

		return $this->Apps_model->get_category_apps($category_id,$page);

	}

	public function get_apps_count($category_id){

		

		return $this->Apps_model->get_apps_count($category_id);

	}

    public function view_ads(){

        $ap_links = array("http://witapps.in/app/telugu/your-favorite-pellichoopulu-dialogue/1/212","http://witapps.in/app/telugu/which-dialogue-of-mahesh-is-apt-for-you-/1/204","http://witapps.in/app/telugu/which-dialogue-of-allu-arjun-is-apt-for-you-/1/203","http://witapps.in/app/telugu/which-dialogue-of-jr-ntr-is-apt-for-you/1/200");

        $ads  = array(212,204,203,200);

        $source = rand(0,count($ads)-1);

        $data['ad'] = $ads[$source];

        $data['app_link']=$ap_links[$source];

        $this->load->view('view_ads',$data);

    }

	public function view_all_apps($category_name,$category_id,$page){

		$apps['category_name'] = $category_name;

		$apps['category_id']   =$category_id;

		$apps = $this->get_category_apps($category_id,$page);

		$apps_count = $this->get_apps_count($category_id);

		if(count($apps)){

			$data['apps'] = $apps;

			$data['apps_count'] = $apps_count;

			$data['menu_names']=$this->get_category_names_menu();

			$data['header_adds']=$this->get_adds_from_db('header',1);

			$data['footer_adds']=$this->get_adds_from_db('footer',1);

			if(count($this->get_adds_from_db('side',1))==0) { //if all add scripts are in inactive mode display

				if($category_id==1){

					$data['app_adds']=$this->get_apps_as_adds($category_id+1,4);

				}elseif($category_id==9){

					$data['app_adds']=$this->get_apps_as_adds($category_id-1,4);

				}elseif($category_id>1 && $category_id<9){

					$data['app_adds']=$this->get_apps_as_adds($category_id+1,4);

				}

			}

			else{

				$data['side_adds']=$this->get_adds_from_db('side',$category_id);

			}

			$data['meta_tags'] = $this->get_meta_tags('apps_categories',$category_id);

			$this->load->view('templates/view_header',$data);

			$this->load->view('view_category');

			$this->load->view('templates/view_footer');

		}else{

			echo "Error";

		}

	}

	public function get_app_details($app_id){

		

		return $this->Apps_model->get_all_app_details($app_id);

	}

	public function redirect_to_logic($category_name,$app_id){

             if($this->input->get('share') || $this->session->userdata('share')=='no'){

                    $this->session->set_userdata("share","no");                

                }else{

                    $this->session->set_userdata("share","yes");

                }

		$app = $this->get_app_details($app_id);

		$app[0]->category_name = $category_name;
                if($this->session->userdata('user_id')==null){
                    $this->session->set_userdata('session_lost',true);
                    $this->view_single_app(strtolower($app[0]->category_name),strtolower(str_replace("?","",str_replace(" ","-",$app[0]->title))),$app[0]->category_id,$app[0]->id);
                }
		$logic = $app[0]->logic;

		$this->{$logic}($app);

	}
	public function upload_index(){
	    

			$this->load->view('view_upload');

			
	}
	public function index_page(){
	    
            $data['category_data']=$this->Apps_model->get_category();

			$this->load->view('index',$data);

			
	}
	public function unique_random_numbers($min, $max, $quantity) {
        $numbers = range($min, $max);
        shuffle($numbers);
        return array_slice($numbers, 0, $quantity);
    }
	public function trending_by_language($language){
	        //$random_numbers=$this->unique_random_numbers(1,20,10);
	        $data['language']=$language;
	        $data['page_name']="trending_page";
            $data['category_data']=$this->Apps_model->get_category_by_language($language);
            $category_ids=array();
            for($i=0;$i<count($data['category_data']);$i++){
                array_push($category_ids,$data['category_data'][$i]->id);
            }
            //$this->print_exit($category_ids);
            $data['trending_data']=$this->Apps_model->get_trending_images_by_language($category_ids);
            
            //$this->print_exit($data);
			$this->load->view('trending_by_language',$data);

			
	}
	public function api_get_languages(){
	    $data['status']="true";
	    $data['msg']="success";
	    $data['data']=$this->Apps_model->get_languages();
	    echo json_encode($data);
	}
	public function api_get_category_by_language($language){
	    $data['status']="true";
	    $data['msg']="success";
	    $data['data']=$this->Apps_model->get_category_by_language($language);
	    echo json_encode($data);
	}
	public function api_get_images_category($category){
	        $data['status']="true";
	        $data['msg']="success";
	        $data['data']=$this->Apps_model->get_images_by_category($category);
	        $data['category_data']=$this->Apps_model->get_category();
            echo json_encode($data);
			
	}
	public function api_trending_by_language($language){
	        $data['msg']="success";
	        $data['language']=$language;
	        $data['page_name']="trending_page";
            $data['category_data']=$this->Apps_model->get_category_by_language($language);
            $category_ids=array();
            $data['status']="true";
            for($i=0;$i<count($data['category_data']);$i++){
                array_push($category_ids,$data['category_data'][$i]->id);
            }
            
            
            $data['data']=$this->Apps_model->get_trending_images_by_language($category_ids);
            
            echo json_encode($data);
			//$this->load->view('trending_by_language',$data);

			
	}
	public function static_page(){
	    
            $data['category_data']=$this->Apps_model->get_category();
			$this->load->view('static_page',$data);

			
	}
	public function admin_index(){
	        if($this->input->post('username')=="witalks"&&$this->input->post('password')=="witalks@123"){
	            $this->session->set_userdata("username",$this->input->post('username'));
	            $this->session->set_userdata("password",$this->input->post('password'));
	            //$data['category_data']=$this->Apps_model->get_category();
			    $this->load->view('admin_home');
	        }
	        
            else{
	            redirect("admin");
	        }

			
	}
	public function add_polls(){
	        if($this->session->userdata("username")&&$this->session->userdata("password")){
	            //$data['category_data']=$this->Apps_model->get_category();
	       $data['divisions']=$this->Apps_model->get_divisions();     
			$this->load->view('add_polls',$data);
	        }
            else{
	            redirect("admin");
	        }

			
	}
	public function upload_images(){
	        if($this->session->userdata("username")&&$this->session->userdata("password")){
	            $data['category_data']=$this->Apps_model->get_category();
	        //$this->print_exit($data);
            
			$this->load->view('upload_images',$data);
	        }
	        else{
	            redirect("admin");
	        }

			
	}
	public function images_category($category){
	        $data['images_data']=$this->Apps_model->get_images_by_category($category);
	        $data['category_data']=$this->Apps_model->get_category();
            
			$this->load->view('images_category',$data);

			
	}
	public function display_images_by_category($language,$category_name, $category_id){
	        $data['page_name']='category_page';
	        $data['category_name']=$this->Apps_model->get_category_name($category_id);
	        
	        $data['images_data']=$this->Apps_model->get_images_by_category($category_id);
	        $data['category_data']=$this->Apps_model->get_category_by_language($language);
            $data['language']=$language;
			$this->load->view('display_images_by_category',$data);

			
	}
	public function edit_images($id,$category_id){
	        $data['images_data']=$this->Apps_model->edit_images_by_category($id,$category_id);

	        $data['category_data']=$this->Apps_model->get_category();
	        $data['id']=$id;
	        $data['category_id']=$category_id;
            //$this->print_exit($data);
			$this->load->view('edit_images',$data);

			
	}
	public function create_image($language,$category_id,$image_id){
	        $data['images_data']=$this->Apps_model->edit_images_by_category($category_id,$image_id);
	        $data['page_name']="create_page";
	        $data['category_name']=$this->Apps_model->get_category_name($category_id);
	        
            $data['language']=$language;
	        $data['category_data']=$this->Apps_model->get_category_by_language($language);
	        $data['id']=$image_id;
	        $data['category_id']=$category_id;
            //$this->print_exit($data);
			$this->load->view('create_image_index',$data);

			
	}
	public function view_category($language){
            $data['language']=$language;
	        $data['category_data']=$this->Apps_model->view_category_by_language($language);
			$this->load->view('view_category',$data);

			
	}
	public function update_trending_category($language,$category_id){
	        $var=$this->input->post();
	        $is_trending=0;
	        if(isset($var)&&isset($var['is_trending'])){
	            $is_trending=1;
	        }
            $this->Apps_model->update_trending_category($category_id,$is_trending);
			redirect("view-category/".$language);

			
	}
	public function display_output_image($language,$insert_id){
	        $data['page_name']="result_page";
	        $data['image_data']=$this->Apps_model->get_statistics($insert_id);
	        $data['category_data']=$this->Apps_model->get_category_by_language($language);
	        $category_id=$data['image_data'][0]->category_id;
	        $data['category_name']=$this->Apps_model->get_category_name($category_id);
	        //$this->print_exit($data);
	        $data['insert_id']=$insert_id;
	        $data['language']=$language;
			$this->load->view('display_output_image',$data);

			
	}
	public function download_image_without_name($image_id){
	    //$this->Apps_model->update_image_downloads($insert_id);
	    $image_data=$this->Apps_model->get_image_data_by_id($image_id);
	    
	    $statistics_data=array("category_id"=>$image_data[0]->category_id,"image_id"=>$image_data[0]->id,"image_path"=>$image_data[0]->path,"is_without_name"=>1,"is_downloaded"=>1,"downloads_count"=>1);
		$insert_id=$this->Apps_model->insert_statistics($statistics_data);
	    
	    
	    $filepath = $image_data[0]->path;
    
    // Process download
    if(file_exists($filepath)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        flush(); // Flush system output buffer
        readfile($filepath);
        exit;
    }
	}
	public function download_image($insert_id){
	    $this->Apps_model->update_image_downloads($insert_id);
	    $image_data=$this->Apps_model->get_statistics($insert_id);
	    
	    $filepath = $image_data[0]->image_path;
    
    // Process download
    if(file_exists($filepath)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        flush(); // Flush system output buffer
        readfile($filepath);
        exit;
    }
	}
	
	public function create_output_image($language,$category_id,$image_id){
	        $username=$this->input->post();
	        
	       // print_r($username);exit;
	       
	        if(isset($username)){
            
	            $data['images_data']=$this->Apps_model->edit_images_by_category($category_id,$image_id);
	            
	            $data['category_data']=$this->Apps_model->get_category();
	            
	            $data['id']=$image_id;
	            $data['category_id']=$category_id;
	            $data['username']=$username['username'];
	            $color=$data['images_data'][0]->text_color;
	            $x=$data['images_data'][0]->text_x;
	            $y=$data['images_data'][0]->text_y;
	            $text_size=$data['images_data'][0]->text_size;
	            $font_path=$data['images_data'][0]->font;
	            
	            if($data['images_data'][0]->image_type=="png"){
	                $jpg_image = imagecreatefrompng($data['images_data'][0]->path);
	            }
	            else{
	                $jpg_image = imagecreatefromjpeg($data['images_data'][0]->path);
	            }
	            //$this->print_exit(($_FILES['files']['error']));
	            //$this->print_exit($username);
	            if(isset($username['profile_pic'])){
	                $image_path=$this->Apps_model->get_profile_pic($username['profile_pic_id']);
	                
	                if (!file_exists("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id)) {

					    mkdir("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id, 0777, true);

				    }
                    $uploadPath = "container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id;
                    $file_extension = strtolower(substr(strrchr($image_path[0]->path,"."),1));
                    
                    //for circle images
                    if($data['images_data'][0]->img_shape=="circle"){
                      if($file_extension=="jpeg"){
                        $this->crop_pic($image_path[0]->path,"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$username['profile_pic_id']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
                    else if($file_extension=="jpg"){
                        $this->crop_pic($image_path[0]->path,"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$username['profile_pic_id']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
                    
                    else{
                        $this->crop_pic_png($image_path[0]->path,"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$username['profile_pic_id']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
            
    	            
    	            exec('convert -size 100x100 xc:none -fill "/home/brochill/public_html/container/inputs/'.date("y").'/'.date("m").'/'.date("d").'/'.$category_id.'/'.$username['profile_pic_id'].'_cropped.jpg" -draw "circle 50,50,1,50" "/home/brochill/public_html/container/inputs/'.date("y").'/'.date("m").'/'.date("d").'/'.$category_id.'/'.$username['profile_pic_id'].'_cropped.png"');
    	            $this->merge_images_jpg_png($data['images_data'][0]->path,"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$username['profile_pic_id']."_cropped.png","container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/output_image.jpg",$data['images_data'][0]->img_x,$data['images_data'][0]->img_y);
    		        $jpg_image=imagecreatefromjpeg("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/output_image.jpg");
    		          
                }
                else{
                    
                    if($file_extension=="jpeg"){
                        $this->crop_pic($image_path[0]->path,"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$username['profile_pic_id']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
                    else if($file_extension=="jpg"){
                        $this->crop_pic($image_path[0]->path,"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$username['profile_pic_id']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
                    else{
                        $this->crop_pic_png($image_path[0]->path,"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$username['profile_pic_id']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
    	            $this->merge_images($data['images_data'][0]->path,"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$username['profile_pic_id']."_cropped.jpg","container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/output_image.jpg",$data['images_data'][0]->img_x,$data['images_data'][0]->img_y);
    		        $jpg_image=imagecreatefromjpeg("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/output_image.jpg");
    		        
                }
                $profile_data=array("id"=>uniqid(),"path"=>"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$username['profile_pic_id'].".$file_extension");
                //$this->print_exit($username['profile_pic_id']);
                //$insert_id=$this->Apps_model->insert_profile_statistics($profile_data); 
                //set_cookie('profile_pic_id',$profile_data['id'],'2592000');
                //set_cookie('username',$username['username'],'2592000');
                
	            }
	            else{
	               
		        if($_FILES['files']['error']<1){
		            
		  
		      $filesCount = count($_FILES['files']['name']);
		            
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['file']['name']     = $_FILES['files']['name'];
                    $_FILES['file']['type']     = $_FILES['files']['type'];
                    $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'];
                    $_FILES['file']['error']     = $_FILES['files']['error'];
                    $_FILES['file']['size']     = $_FILES['files']['size']; 
                    // File upload configuration
                //$this->print_exit($_FILES['files']);
                if (!file_exists("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id)) {

					    mkdir("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id, 0777, true);

				    }
                $uploadPath = "container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id;

                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name']=uniqid();
                
                
                // Load and initialize upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                
                // Upload file to server
                if($this->upload->do_upload('file')){
                    // Uploaded file data
                    $fileData = $this->upload->data();
                    
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                }
            }
                $file_extension = strtolower(substr(strrchr($uploadData[0]['file_name'],"."),1));
                //$this->print_exit($file_extension);
                if($data['images_data'][0]->img_shape=="circle"){
                      if($file_extension=="jpeg"){
                        $this->crop_pic("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name'].".jpeg","container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
                    else if($file_extension=="jpg"){
                        $this->crop_pic("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name'].".jpg","container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
                    
                    else{
                        $this->crop_pic_png("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name'].".png","container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
            
    	            
    	            exec('convert -size 100x100 xc:none -fill "/home/brochill/public_html/container/inputs/'.date("y").'/'.date("m").'/'.date("d").'/'.$category_id.'/'.$config['file_name'].'_cropped.jpg" -draw "circle 50,50,1,50" "/home/brochill/public_html/container/inputs/'.date("y").'/'.date("m").'/'.date("d").'/'.$category_id.'/'.$config['file_name'].'_cropped.png"');
    	            $this->merge_images_jpg_png($data['images_data'][0]->path,"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name']."_cropped.png","container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/output_image.jpg",$data['images_data'][0]->img_x,$data['images_data'][0]->img_y);
    		        $jpg_image=imagecreatefromjpeg("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/output_image.jpg");
    		          
                }
                else{
                    
                    if($file_extension=="jpeg"){
                        $this->crop_pic("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name'].".jpeg","container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
                    else if($file_extension=="jpg"){
                        $this->crop_pic("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name'].".jpg","container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
                    else{
                        $this->crop_pic_png("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name'].".png","container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name']."_cropped.jpg",$data['images_data'][0]->img_width,$data['images_data'][0]->img_height);
                    }
    	            $this->merge_images($data['images_data'][0]->path,"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name']."_cropped.jpg","container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/output_image.jpg",$data['images_data'][0]->img_x,$data['images_data'][0]->img_y);
    		        $jpg_image=imagecreatefromjpeg("container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/output_image.jpg");
    		        
                }
                $profile_data=array("id"=>uniqid(),"path"=>"container/inputs/".date("y")."/".date("m")."/".date("d")."/".$category_id."/".$config['file_name'].".$file_extension");
                $insert_id=$this->Apps_model->insert_profile_statistics($profile_data); 
                set_cookie('profile_pic_id',$profile_data['id'],'2592000');
                set_cookie('username',$username['username'],'2592000');
		        } 
	            }
	            
		        
		        
		        $text_color = imagecolorallocate($jpg_image, explode(",",$color)[0],explode(",",$color)[1],explode(",",$color)[2]);
		        
		        imagettftext($jpg_image, $text_size, 0, $x, $y, $text_color, $font_path, "-".$username['username']);
		        if (!file_exists("container/uploads/".date("y")."/".date("m")."/".date("d")."/".$category_id)) {

					    mkdir("container/uploads/".date("y")."/".date("m")."/".date("d")."/".$category_id, 0777, true);

				    }
				$uniqid=uniqid();
		        imagejpeg($jpg_image, "container/uploads/".date("y")."/".date("m")."/".date("d")."/".$category_id. "/".$uniqid.".jpg", 100);
		        imagedestroy($jpg_image);
		        $image_path="container/uploads/".date("y")."/".date("m")."/".date("d")."/".$category_id. "/".$uniqid.".jpg";
		        $statistics_data=array("category_id"=>$category_id,"image_id"=>$image_id,"image_path"=>$image_path,"user_name"=>$username['username'],"is_without_name"=>0);
		        $insert_id=$this->Apps_model->insert_statistics($statistics_data);
                
			    redirect("lang/".$language."/result/".$insert_id);
	        }
	        else{
	            redirect(base_url());
	        }

			
	}
	public function get_profile_pic($profile_pic_id){
	    $profile_data=$this->Apps_model->get_profile_pic($profile_pic_id);
	    $path=$profile_data[0]->path;
	    $filename = basename($path);
        $file_extension = strtolower(substr(strrchr($filename,"."),1));
        
        switch( $file_extension ) {
            
            case "gif": $ctype="image/gif"; break;
            case "png": $ctype="image/png"; break;
            case "jpeg":$ctype="image/jpeg"; break;
            case "jpg": $ctype="image/jpeg"; break;
            default:
        }

        header('Content-type: ' . $ctype);
        readfile($path);

	}
	public function api_create_output_image($language,$category_id,$image_id){
	        $username=$this->input->post();
	       // print_r($username);exit;
	        if(isset($username)){
	            
	            $data['images_data']=$this->Apps_model->edit_images_by_category($category_id,$image_id);
	            $data['category_data']=$this->Apps_model->get_category();
	            
	            $data['id']=$image_id;
	            $data['category_id']=$category_id;
	            $data['username']=$username['username'];
	            $color=$data['images_data'][0]->text_color;
	            $x=$data['images_data'][0]->text_x;
	            $y=$data['images_data'][0]->text_y;
	            $text_size=$data['images_data'][0]->text_size;
	            $font_path=$data['images_data'][0]->font;
	            
	            if($data['images_data'][0]->image_type=="png"){
	                $jpg_image = imagecreatefrompng($data['images_data'][0]->path);
	            }
	            else{
	                $jpg_image = imagecreatefromjpeg($data['images_data'][0]->path);
	            }
	            
	            
		        $text_color = imagecolorallocate($jpg_image, explode(",",$color)[0],explode(",",$color)[1],explode(",",$color)[2]);
		        
		        imagettftext($jpg_image, $text_size, 0, $x, $y, $text_color, $font_path, "-".$username['username']);
		        if (!file_exists("container/uploads/".date("y")."/".date("m")."/".date("d")."/".$category_id)) {

					    mkdir("container/uploads/".date("y")."/".date("m")."/".date("d")."/".$category_id, 0777, true);

				    }
				$uniqid=uniqid();
		        imagejpeg($jpg_image, "container/uploads/".date("y")."/".date("m")."/".date("d")."/".$category_id. "/".$uniqid.".jpg", 100);
		        imagedestroy($jpg_image);
		        $image_path="container/uploads/".date("y")."/".date("m")."/".date("d")."/".$category_id. "/".$uniqid.".jpg";
		        $statistics_data=array("category_id"=>$category_id,"image_id"=>$image_id,"image_path"=>$image_path,"user_name"=>$username['username'],"is_without_name"=>0);
		        $insert_id=$this->Apps_model->insert_statistics($statistics_data);
		        $image_data=array("status"=>"true","message"=>"success","path"=>$image_path);
                echo json_encode($image_data);
			    //redirect("lang/".$language."/result/".$insert_id);
	        }
	        else{
	            redirect(base_url());
	        }

			
	}
	public function update_images_db($id,$category_id){
	        $post_data=$this->input->post();
	        //$this->print_exit($post_data);
	        $data['images_data']=$this->Apps_model->update_images_db($id,$category_id,$post_data);

	       // $data['category_data']=$this->Apps_model->get_category();
	        
            //$this->print_exit($data);
			$this->load->view('success',$data);

			
	}
	public function add_options(){
	    $data['polls']=$this->Apps_model->get_polls_index_admin();
	    $this->load->view('add_options',$data);
	}
	public function add_options_db(){
	    $post_data=$this->input->post();
	    $this->Apps_model->add_options_db($post_data);
	    redirect('add-options');
	}
	public function update_polls_index($category_id){
	        $data['polls_data']=$this->Apps_model->view_polls_by_category($category_id);
			$data['category_id'] = $category_id;
			$this->load->view('view_polls_by_category',$data);

			
	}
	public function update_polls($poll_id, $category_id){
	        $poll_data=$this->input->post();
	        $this->Apps_model->update_polls($poll_id,$poll_data);
			redirect("update-polls-index/".$category_id);

			
	}
	public function add_polls_db(){
	        
	        $post_data=$this->input->post();
	        if(isset($post_data)){
	            {
            $filesCount = count($_FILES['files']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $upload_polls_data['category_id']=$post_data['category_id'];
                $upload_polls_data['title']=$post_data['title'];
                $upload_polls_data['slug']=$post_data['slug'];
				$upload_polls_data['is_latest']=$post_data['is_latest'];
                $upload_polls_data['comment_vote']=$post_data['comment_vote'];
                $upload_polls_data['comments']=$post_data['comments'];
                $upload_polls_data['description']=$post_data['description'];
                $upload_polls_data['status']=$post_data['status'];
				
                $insert_id=$this->Apps_model->add_polls_db($upload_polls_data);
                
                
                $_FILES['file']['name']     = $_FILES['files']['name'];
                $_FILES['file']['type']     = $_FILES['files']['type'];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'];
                $_FILES['file']['error']     = $_FILES['files']['error'];
                $_FILES['file']['size']     = $_FILES['files']['size'];
                
                // File upload configuration
                
                if (!file_exists("container/input-images/".$insert_id)) {

					    mkdir("container/input-images/".$insert_id, 0777, true);

				    }
                $uploadPath = "container/input-images/".$insert_id;
                
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name']="cover_pic";
                
                
                // Load and initialize upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                // Upload file to server
                if($this->upload->do_upload('file')){
                    
                    // Uploaded file data
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    
                    $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                }
                
            }
            $this->load->view('success');
           
        }
    
	}
	    else{
                redirect("admin-home");
            }
	
            
			
	}
	public function upload_images_db(){
	        $post_data=$this->input->post();
	        $data['category_data']=$this->Apps_model->get_category();
	        if(isset($post_data)){
	            {
            $filesCount = count($_FILES['files']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['file']['name']     = $_FILES['files']['name'][$i];
                $_FILES['file']['type']     = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error']     = $_FILES['files']['error'][$i];
                $_FILES['file']['size']     = $_FILES['files']['size'][$i];
                
                // File upload configuration
                //$this->print_exit($post_data['category_id']);
                if (!file_exists("container/uploads/".$post_data['category_id'])) {

					    mkdir("container/uploads/".$post_data['category_id'], 0777, true);

				    }
                $uploadPath = 'container/uploads/'.$post_data['category_id'];
                
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name']=uniqid();
                
                // Load and initialize upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                // Upload file to server
                if($this->upload->do_upload('file')){
                    
                    // Uploaded file data
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    
                    $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                }
                
            }
            
            for($i=0;$i<count($uploadData);$i++){
                $upload_image_data['category_id']=$post_data['category_id'];
                $upload_image_data['path']=$uploadPath.'/'.$uploadData[$i]['file_name'];
                $upload_image_data['image_type']=$post_data['type'];
                $upload_image_data['text_x']=$post_data['text_x'];
                $upload_image_data['text_y']=$post_data['text_y'];
                $upload_image_data['text_size']=$post_data['text_size'];
                $upload_image_data['text_color']=$post_data['text_color'];
                $upload_image_data['font']=$post_data['font'];
                $upload_image_data['status']=$post_data['status'];
                //$this->print_exit($upload_image_data);
                $this->Apps_model->upload_images_db($upload_image_data);
                $this->load->view('success',$data);
            }
           
        }
    
	}
	    else{
                redirect("admin-home");
            }
	
            
			

			
	}
	
	
    public function do_upload()
        {
                $config['upload_path']          = 'container/uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
                $this->print_exit($config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        $this->load->view('view_upload', $error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());

                        $this->load->view('upload_success', $data);
                }
        }
	public function view_single_app_short($app_id){

                   

		$app = $this->get_app_details($app_id);
		//$this->print_exit($app);
	

		if(count($app)>0) {
	$app_desc=$this->Apps_model->get_app_details($app_id);
        
		$data['app_desc'] = array("category_name" => "fun", "app_titile" => $app[0]->title, "category_id" => $app[0]->category_id, "app_id" => $app[0]->id,"app_description" => $app_desc[0]->app_description);



		$login_url = "";

		$login_type = "";
			if ($this->is_logged_in()) {

				$is_first_time = $this->session->userdata("is_first_time_" . $app_id);

				if ($is_first_time == NULL) {

					$this->increment_count("users", $app_id);

					$this->session->set_userdata("is_first_time_" . $app_id, "true");

				}

				$login_type ="normal";
                                if($this->session->userdata('session_lost')){
                                    redirect("app/redirect-to-logic/$category_name/$app_id");
                                }
                                if($app[0]->type=='single'){
                                    redirect("app/redirect-to-logic/$category_name/$app_id");    
                                }
                                $logged_by = $this->session->userdata("logged_by_friends");
                                
                                if($logged_by=='true'){
                                    redirect("app/redirect-to-logic/$category_name/$app_id");  
                                }
                                if($app[0]->type=='friends'){
                                  
                                    $friend_first_time = $this->session->userdata("friend_first_time_" . $app_id);

				if ($friend_first_time == NULL) {
                                   
                                        $login_type ="facebook";
                                        $this->session->set_userdata(array("redirect_app" => array("category_name" => $category_name, "category_id" => $category_id, "app_title" => $app_title, "app_id" => $app_id)));
					$this->session->set_userdata("friend_first_time_" . $app_id, "true");
                                        
                                }else{
                                     redirect("app/redirect-to-logic/$category_name/$app_id");   
                                }
                                }
				$login_url = base_url() . "app/redirect-to-logic/$category_name/$app_id";

			}else {

				$login_type ="facebook";
                                
                                if($app[0]->type=='friends'){
                                $this->session->set_userdata("logged_by_friends", "true");    
                               }else{
                                    $this->session->set_userdata("logged_by_friends", "false");       
                               }
                                
				
                                $this->session->set_userdata(array("redirect_app" => array("category_name" => "fun", "category_id" => $app[0]->category_id, "app_title" => $app[0]->title, "app_id" => $app[0]->id)));

				$login_url = $this->get_login_url();
                                if($this->session->userdata('session_lost')){
                                    redirect($login_url);
                                }
			}

	$app[0]->category_name = "fun";

		$app[0]->category_id = $app[0]->category_id;

		$data['login_url'] = $login_url;

		$data['login_type'] = $login_type;

		$data['app'] = $app;
		$data['authURL'] =  $this->facebook->login_url();
		$data['menu_names']=$this->get_category_names_menu();

		$data['header_adds']=$this->get_adds_from_db('header',1);

		$data['footer_adds']=$this->get_adds_from_db('footer',1);

		//$data['apps'] = $this->get_apps_like_index();

		$data['suggestion_apps'] = $this->get_latest_apps_category($app[0]->category_id, 12);

		if(count($this->get_adds_from_db('side',1))==0) { //if all add scripts are in inactive mode display

			if($app[0]->category_id==1){

				$data['app_adds']=$this->get_apps_as_adds($app[0]->category_id+1,4);

			}elseif($app[0]->category_id==9){

				$data['app_adds']=$this->get_apps_as_adds($app[0]->category_id-1,4);

			}

			elseif($app[0]->category_id==6){

				$data['app_adds']=$this->get_apps_as_adds($app[0]->category_id-1,4);

			}elseif($app[0]->category_id>1 && $app[0]->category_id<9){

				$data['app_adds']=$this->get_apps_as_adds($app[0]->category_id+1,4);

			}

		}

		else{

			$data['side_adds']=$this->get_adds_from_db('side',$app[0]->category_id);

		}

		$data['page'] = 'view_apps';

		$data['meta_tags']= $this->get_meta_tags('apps',$app[0]->id);



		$this->load->view('templates/view_header',$data);

		$this->load->view('view_apps');

		$this->load->view('templates/view_footer');

		}else{
		      $this->load->view('view_apps_not_found'); 
		}

	




	}

	public function view_single_app($category_name,$app_title,$category_id,$app_id){

                   

		$app = $this->get_app_details($app_id);
	

		if(count($app)>0) {
	$app_desc=$this->Apps_model->get_app_details($app_id);
        
		$data['app_desc'] = array("category_name" => $category_name, "app_titile" => $app_title, "category_id" => $category_id, "app_id" => $app_id,"app_description" => $app_desc[0]->app_description);



		$login_url = "";

		$login_type = "";
			if ($this->is_logged_in()) {

				$is_first_time = $this->session->userdata("is_first_time_" . $app_id);

				if ($is_first_time == NULL) {

					$this->increment_count("users", $app_id);

					$this->session->set_userdata("is_first_time_" . $app_id, "true");

				}

				$login_type ="normal";
                                if($this->session->userdata('session_lost')){
                                    redirect("app/redirect-to-logic/$category_name/$app_id");
                                }
                                if($app[0]->type=='single'){
                                    redirect("app/redirect-to-logic/$category_name/$app_id");    
                                }
                                $logged_by = $this->session->userdata("logged_by_friends");
                                
                                if($logged_by=='true'){
                                    redirect("app/redirect-to-logic/$category_name/$app_id");  
                                }
                                if($app[0]->type=='friends'){
                                  
                                    $friend_first_time = $this->session->userdata("friend_first_time_" . $app_id);

				if ($friend_first_time == NULL) {
                                   
                                        $login_type ="facebook";
                                        $this->session->set_userdata(array("redirect_app" => array("category_name" => $category_name, "category_id" => $category_id, "app_title" => $app_title, "app_id" => $app_id)));
					$this->session->set_userdata("friend_first_time_" . $app_id, "true");
                                        
                                }else{
                                     redirect("app/redirect-to-logic/$category_name/$app_id");   
                                }
                                }
				$login_url = base_url() . "app/redirect-to-logic/$category_name/$app_id";

			}else {

				$login_type ="facebook";
                                
                                if($app[0]->type=='friends'){
                                $this->session->set_userdata("logged_by_friends", "true");    
                               }else{
                                    $this->session->set_userdata("logged_by_friends", "false");       
                               }
                                
				
                                $this->session->set_userdata(array("redirect_app" => array("category_name" => $category_name, "category_id" => $category_id, "app_title" => $app_title, "app_id" => $app_id)));

				$login_url = $this->get_login_url();
                                if($this->session->userdata('session_lost')){
                                    redirect($login_url);
                                }
			}

	$app[0]->category_name = $category_name;

		$app[0]->category_id = $category_id;

		$data['login_url'] = $login_url;

		$data['login_type'] = $login_type;

		$data['app'] = $app;
		$data['authURL'] =  $this->facebook->login_url();
		$data['menu_names']=$this->get_category_names_menu();

		$data['header_adds']=$this->get_adds_from_db('header',1);

		$data['footer_adds']=$this->get_adds_from_db('footer',1);

		//$data['apps'] = $this->get_apps_like_index();

		$data['suggestion_apps'] = $this->get_latest_apps_category($category_id, 12);

		if(count($this->get_adds_from_db('side',1))==0) { //if all add scripts are in inactive mode display

			if($category_id==1){

				$data['app_adds']=$this->get_apps_as_adds($category_id+1,4);

			}elseif($category_id==9){

				$data['app_adds']=$this->get_apps_as_adds($category_id-1,4);

			}

			elseif($category_id==6){

				$data['app_adds']=$this->get_apps_as_adds($category_id-1,4);

			}elseif($category_id>1 && $category_id<9){

				$data['app_adds']=$this->get_apps_as_adds($category_id+1,4);

			}

		}

		else{

			$data['side_adds']=$this->get_adds_from_db('side',$category_id);

		}

		$data['page'] = 'view_apps';

		$data['meta_tags']= $this->get_meta_tags('apps',$app_id);



		$this->load->view('templates/view_header',$data);

		$this->load->view('view_apps');

		$this->load->view('templates/view_footer');

		}else{
		      $this->load->view('view_apps_not_found'); 
		}

	




	}

	public function profile_pic_logic($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = 1;

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/".$app[0]->id)) {

			mkdir("container/output-images/".$app[0]->id, 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$this->merge_images_jpg_png("container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		////

		

		$access_token = $this->session->userdata("access_token");

		$persmissions = file_get_contents("https://graph.facebook.com/me/permissions/publish_actions?access_token=$access_token");

                //$persmissions = $this->print_exit(json_decode($persmissions));

                $persmissions = json_decode($persmissions);

                if($persmissions->data[0]->status=='granted'){

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://graph.facebook.com/me/photos?access_token=" . $access_token);

		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, "url=http://witapps.in/container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg");

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);

		curl_close ($ch);

		$array_response = json_decode($server_output);

		$this->session->set_userdata(array('array_response'=>$array_response->id));

		}

		else{

			$this->session->sess_destroy();

			redirect('http://bit.do/APSpecial');

		}

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-$source_img");

	}
	public function telugu_award($app)
	{
		$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");
		$source_img = rand(1, count($images) - 2);
		$user_id = $this->session->userdata("user_id");
		$user_name = $this->session->userdata("name");
		if (!file_exists("container/output-images/" . $app[0]->id)) {
			mkdir("container/output-images/" . $app[0]->id, 0777, true);
		}
		$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg",110,110);
		exec('convert -size 110x110 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 55,55,1,55" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 592, 20);
		//$this->crop_pic("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 500);
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");
		$black = imagecolorallocate($jpg_image, 0, 0, 0);
		$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
		if(strlen($user_name)<=12){
		    imagettftext($jpg_image, 22, 0,234, 152, $black, $font_path, $user_name);
		    imagettftext($jpg_image, 20, 0,75, 325, $black, $font_path, "Date: ".date("d")."/".date("m")."/".date("y"));
		}
		else{
		    imagettftext($jpg_image, 24, 0,242, 152, $black, $font_path,explode(" ",$user_name)[0]);
		    imagettftext($jpg_image, 20, 0,75, 325, $black, $font_path, "Date: ".date("d")."/".date("m")."/".date("y"));
		}
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");
		imagedestroy($jpg_image);
		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");
	}
	public function true_name($app)
	{
		$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");
		$source_img = rand(1, count($images) - 2);
		$user_id = $this->session->userdata("user_id");
		$user_name = $this->session->userdata("name");
		if (!file_exists("container/output-images/" . $app[0]->id)) {
			mkdir("container/output-images/" . $app[0]->id, 0777, true);
		}
		//$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", $pic_dimensions[0]);
		exec('convert -size 150x150 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 75,75,1,75" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 26, 102);
		//$this->crop_pic("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 500);
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");
		$black = imagecolorallocate($jpg_image, 255, 255, 255);
		$font_path = 'assets/fonts/BebasNeueBold.ttf';
		$names_list = array("RULER","LEADER","POWER","SOLDIER","HAPPINESS","FIGHTER","CONQuEROR","ACTOR","PROFESSOR","CHAMPION","PROTECTOR","REBEL","DEMI GOD");
		shuffle($names_list);
		imagettftext($jpg_image, 50, 0,355, 125, $black, $font_path,explode(" ",$user_name)[0]);
		imagettftext($jpg_image, 30, 0,310, 170, $black, $font_path,"Your name means");
		imagettftext($jpg_image, 75, 0,330, 260, $black, $font_path,$names_list[0]);
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");
		imagedestroy($jpg_image);
		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");
	}
	public function fate_logic($app){
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");
		$source_img =1;
		$luck =rand(95,100);
		$love=rand(24,32);
		$kids=rand(1,4);
		$health=rand(90,100);
		$car=array("Renault","Lamborghini","BMW","Ferrari","Volkswagon","Ford","Hyundai","Toyota","Maruti","BENZ","Ambassador","Audi","Nissan","Lexus","Rolce-Roys");
		$career=array("Teacher","Doctor","Engineer","Actor","Director","Magician","Musician","Politician");
		shuffle($car);shuffle($career);

		$user_id = $this->session->userdata("user_id");
		if (!file_exists("container/output-images/" . $app[0]->id."/".date("m")."/".date("d")."/".date("h"))) {
					mkdir("container/output-images/" . $app[0]->id."/".date("m")."/".date("d")."/".date("h"), 0777, true);
				}
		$pic_positions = explode("X",$app[0]->picture_position);
		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);
		//exec('magick -size 200x200 xc:none -fill "C:/wamp/www/witapps/container/user-photos/'.date('y').'/'.date('m').'/'.date('d').'/'.date('h').'/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 100,100,1,100" "C:/wamp/www/witapps/container/user-photos/' .date('y').'/'.date('m').'/'.date('d').'/'.date('h').'/' . $user_id .'/profile_pic_circle.png"');
		//exec('convert -size 200x200 xc:none -fill "/home/witapps/public_html/container/user-photos/'. $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 100,100,1,100" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/".date("m")."-".date("d")."-".date("h")."-". $user_id . "$luck.jpg",491,60);
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$luck");
		//$this->Apps_model->InsertData('generated_images',$data);
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/".date("m")."-".date("d")."-".date("h")."-". $user_id . "$luck.jpg");
		$black = imagecolorallocate($jpg_image, 0, 0, 0);
		$white = imagecolorallocate($jpg_image, 255, 255, 255);

		$user_name = $this->session->userdata('first_name');
		$font_path = 'assets/fonts/BebasNeueBold.ttf';
		imagettftext($jpg_image, 18, 0, 530, 285, $black, $font_path, $user_name);
		imagettftext($jpg_image, 25, 0, 260, 52, $black, $font_path, "Married at ".$love);
		imagettftext($jpg_image, 25, 0, 260, 98, $black, $font_path, $luck."%");
		imagettftext($jpg_image, 25, 0, 260, 142, $black, $font_path, $kids);
		imagettftext($jpg_image, 25, 0, 260, 190, $black, $font_path, $car[0]);
		imagettftext($jpg_image, 25, 0, 260, 235, $black, $font_path, $health."%");
		imagettftext($jpg_image, 25, 0, 260, 288, $black, $font_path, $career[0]);


		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/".date("m")."-".date("d")."-".date("h")."-". $user_id . "$luck.jpg", 80);
		imagedestroy($jpg_image);
		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .date("m")."-".date("d")."-".date("h")."-". $user_id ."$luck");
	}
	public function horoscope_logic($app)
	{
		$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");
		$source_img = rand(1, count($images) - 2);
		$user_id = $this->session->userdata("user_id");
		if (!file_exists("container/output-images/" . $app[0]->id)) {
			mkdir("container/output-images/" . $app[0]->id, 0777, true);
		}
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
	
		$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 300,300);
		exec('convert -size 300x300 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 150,150,1,150" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 30, 30);
		//$this->crop_pic("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 500);
		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");
	}
    public function bikes_logic($app)
	{
		$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");
		$source_img = rand(1, count($images) - 2);
		$user_id = $this->session->userdata("user_id");
		if (!file_exists("container/output-images/" . $app[0]->id)) {
			mkdir("container/output-images/" . $app[0]->id, 0777, true);
		}
		$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 174,174);
		exec('convert -size 174x174 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 87,87,1,87" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 530, 78);
		//$this->crop_pic("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 500);
		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");
	}
	public function soul_mate($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		$gender = $this->session->userdata("gender");

		$user_name = $this->session->userdata('first_name');



		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_feed = $this->get_user_feed();

		if (isset($user_feed->feed)) {

			$best_friends=$this->best_friends_from_feed($user_feed->feed->data);



			//$best_friends = $this->best_friends_from_likes($user_feed->feed->data);

			if (count($best_friends)) {
			    if($app[0]->id==610){
			        if (count($best_friends)>=4) {
					$this->has_access_token($app);
					$this->refresh_access_token();
					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 240, 240);
					$this->merge_images("container/input-images/" . $app[0]->id . "/source.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 30, 100);
                    $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 0, 0);
					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");
					$white = imagecolorallocate($jpg_image, 244, 235, 66);
					$black = imagecolorallocate($jpg_image, 255, 255, 255);
					$font_path = 'assets/fonts/BebasNeueBold.ttf';
					$font_path1 = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
					
						$first_names = $this->get_first_names_new($best_friends, 4);
						shuffle($first_names);
						imagettftext($jpg_image, 24, 0, 320, 125, $black, $font_path1, $user_name.", this name");
						imagettftext($jpg_image, 24, 0, 320, 165, $black, $font_path1, "changed your life:");

						if(strlen($first_names[0])<12){
							imagettftext($jpg_image, 70, 0, 320, 255, $white, $font_path, strtoupper($first_names[0]));
						}
						else{
							imagettftext($jpg_image, 50, 0, 300, 255, $white, $font_path, strtoupper($first_names[0]));
						}

						//imagettftext($jpg_image, 42, 0, 300, 170, $black, $font_path, $best_friends[$best_friends_keys[0]]);
						//imagettftext($jpg_image, 20, 0, 250, 30, $black, $font_path, $first_names[$best_friends_keys[0]]);

						imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);
						imagedestroy($jpg_image);

						redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);
			        }
					else {

				        $this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

				        redirect('apps/home');

			            }
					
			    }
			    if($app[0]->id==588){
			        if (count($best_friends)>=4) {
					$this->has_access_token($app);
					$this->refresh_access_token();
					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 200, 200);
					$this->merge_images("container/input-images/" . $app[0]->id . "/1.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 35, 85);

					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");
					$white = imagecolorallocate($jpg_image, 255, 51, 51);
					$black = imagecolorallocate($jpg_image, 44, 44, 44);
					$font_path = 'assets/fonts/BebasNeueBold.ttf';
					$font_path1 = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
					
						$first_names = $this->get_first_names_new($best_friends, 4);
						shuffle($first_names);
						imagettftext($jpg_image, 24, 0, 304, 125, $black, $font_path1, $user_name.", this name");
						imagettftext($jpg_image, 24, 0, 300, 165, $black, $font_path1, "changed your life:");

						if(strlen($first_names[0])<12){
							imagettftext($jpg_image, 60, 0, 310, 255, $white, $font_path, strtoupper($first_names[0]));
						}
						else{
							imagettftext($jpg_image, 45, 0, 290, 255, $white, $font_path, strtoupper($first_names[0]));
						}

						//imagettftext($jpg_image, 42, 0, 300, 170, $black, $font_path, $best_friends[$best_friends_keys[0]]);
						//imagettftext($jpg_image, 20, 0, 250, 30, $black, $font_path, $first_names[$best_friends_keys[0]]);

						imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);
						imagedestroy($jpg_image);

						redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);
			        }
					else {

				        $this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

				        redirect('apps/home');

			            }
					
			    }

				$this->get_profile_pics($best_friends, 1);

				$first_names = $this->get_first_names_new($best_friends, 1);

				$best_friends_keys = array_keys($best_friends);

				for($i=0;$i<count($best_friends_keys);$i++){

					//$this->print_exit($best_friends_keys[$i]);

					//$female_friends=$this->get_female_friends($best_friends_keys[$i]);

				}



				$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 250, 250);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 250, 250);



				$this->merge_images("container/input-images/" . $app[0]->id . "/100.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 35, 15);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 400, 15);

				$friend_first_letter=array("A","B","C","D","E","F","G","H","I","J","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

				for($j=0;$j<count($friend_first_letter);$j++){

					if($first_names[0][0]==$friend_first_letter[$j]){

						$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id ."/".$friend_first_letter[$j]. ".png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 400, 15);



					}



				}

				for($j=0;$j<count($friend_first_letter);$j++){

					if($user_name[0]==$friend_first_letter[$j]){

						$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/white/".$friend_first_letter[$j].".png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 35, 15);

					}

				}







				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

				$white = imagecolorallocate($jpg_image, 0, 0, 0);

				$black = imagecolorallocate($jpg_image, 255, 255, 255);

				$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

				imagettftext($jpg_image, 22, 0, 75, 300, $black, $font_path, strtoupper($user_name));

				imagettftext($jpg_image, 22, 0, 440, 300, $white, $font_path, strtoupper($first_names[0]));

				//imagettftext($jpg_image, 42, 0, 300, 170, $black, $font_path, $best_friends[$best_friends_keys[0]]);

				//imagettftext($jpg_image, 20, 0, 250, 30, $black, $font_path, $first_names[$best_friends_keys[0]]);



				imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

				imagedestroy($jpg_image);



				redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);

			} else {

				$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

				redirect('apps/home');

			}

		}else {

			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

			redirect('apps/home');

		}

	}
	public function love_location($app){

		$location=$this->get_user_location();

		$love_location=array();
		$user_id = $this->session->userdata("user_id");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {
					mkdir("container/user-photos/" . $user_id, 0777, true);
            }
	    $this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		
        if($app[0]->id==545){
            if(isset($location->photos)){

			$photos_count=count($location->photos->data);

			for($i=0;$i<$photos_count;$i++){

				if(isset($location->photos->data[$i]->place)){

					$love_location[0]=$location->photos->data[$i]->place->name;

					break;

				}

			}

		}

		if($love_location==NULL){

			$love_location=array("Bangalore", "Mysore", "Coorg", "Hampi", "Chikmagalore", "Nandi Hills", "Gokarna", "Mangaluru");
            shuffle($love_location);
		}

		//$this->print_exit($love_location);

		$source_img =1;

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/".$app[0]->id)) {

			mkdir("container/output-images/".$app[0]->id, 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$this->merge_images_jpg_png("container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg","container/input-images/".$app[0]->id."/1.png","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",270,150);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		////

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id ."-$source_img".".jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);

		$red = imagecolorallocate($jpg_image, 174, 32, 32);

		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/Kozuka.otf';

		if($love_location!="You Cannot Find Your LOVE"){

			imagettftext($jpg_image, 12, 0, 370, 30, $white, $font_path, $user_name."'s Love is waiting in");

		}

		if(strlen($love_location[0])>=20){

			imagettftext($jpg_image, 18, 0, 370, 100, $white, $font_path, $love_location[0]);



		}

		if(strlen($love_location[0])<15){

			imagettftext($jpg_image, 35, 0, 370, 100, $white, $font_path, $love_location[0]);



		}

		if(strlen($love_location[0])>15&& strlen($love_location[0])<20){

			imagettftext($jpg_image, 30, 0, 360, 100, $white, $font_path, $love_location[0]);



		}

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id ."-$source_img". ".jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-$source_img");

        }
		if($app[0]->id==327){

			$love_location=array();

			if(isset($location->photos)){

				$photos_count=count($location->photos->data);

				$count=0;

				for($i=0;$i<$photos_count;$i++){

					if(isset($location->photos->data[$i]->place)){

						$love_location[$count]=$location->photos->data[$i]->place->name;

						$count++;

					}

				}

			}

			if($love_location==NULL){

				$love_location[0]="You Cannot Find Your SOULMATE";

				if($app[0]->id==327){

					$love_location[0]="You Cannot Find Your LOVE";

				}



			}

			//$this->print_exit($love_location);

			$source_img =1;

			$random=rand(1,10);

			$user_id = $this->session->userdata("user_id");

			if (!file_exists("container/output-images/".$app[0]->id)) {

				mkdir("container/output-images/".$app[0]->id, 0777, true);

			}

			shuffle($love_location);

			//$this->print_exit($love_location);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);

			$user_feed = $this->get_user_feed();

			if (isset($user_feed->feed)) {

				$best_friends = $this->best_friends_from_feed($user_feed->feed->data);

				if (count($best_friends)) {

					$this->get_profile_pics_new($best_friends, 5);

					$first_names = $this->get_first_names_new($best_friends, 5);

					$best_friends_keys = array_keys($best_friends);

				}

			}

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 160, 160);

			$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 160, 160);

			$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 160, 160);

			$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[3] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg", 160, 160);

			$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[4] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[4]."_cropped.jpg", 160, 160);

			$random_number=rand(0,4);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/".$app[0]->id."/".$user_id."-$random.jpg",$pic_positions[0],$pic_positions[1]);

			$this->merge_images("container/output-images/".$app[0]->id."/".$user_id."-$random.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random_number]."_cropped.jpg","container/output-images/".$app[0]->id."/".$user_id."-$random.jpg",554,105);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$random");

			//

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id ."-$random".".jpg");

			$black = imagecolorallocate($jpg_image, 255, 255, 255);

			$white = imagecolorallocate($jpg_image, 44, 44, 44);

			$red = imagecolorallocate($jpg_image, 174, 32, 32);

			$user_name = $this->session->userdata('first_name');

			$font_path = 'assets/fonts/Kozuka.otf';



			if($love_location[0]!="You Cannot Find Your LOVE"){

				if(strlen($first_names[$best_friends_keys[0]])<10){

					imagettftext($jpg_image, 20, 0, 200, 58, $black, $font_path, $first_names[$best_friends_keys[$random_number]]." Will Find Your Love");

				}

				if(strlen($first_names[$best_friends_keys[0]])>10){

					imagettftext($jpg_image, 16, 0, 182, 55, $black, $font_path, $first_names[$best_friends_keys[$random_number]]." Will Find Your Love");



				}

			}

			if($love_location[0]=="You Cannot Find Your LOVE"){

				if(strlen($first_names[$best_friends_keys[$random_number]])<15) {

					imagettftext($jpg_image, 16, 0, 190, 58, $black, $font_path, $first_names[$best_friends_keys[$random_number]] . " Cannot Find Your Love");

				}

				if(strlen($first_names[$best_friends_keys[0]])>15) {

					imagettftext($jpg_image, 12, 0, 175, 58, $black, $font_path, $first_names[$best_friends_keys[$random_number]] . " Cannot Find Your Love");

				}

			}



			if(strlen($love_location[0])>35){

				imagettftext($jpg_image, 16, 0, 238, 100, $white, $font_path, wordwrap($love_location[0],30,"\n"));



			}

			if(strlen($love_location[0])>=20&& strlen($love_location[0])<34){

				imagettftext($jpg_image, 18, 0, 240, 120, $white, $font_path, $love_location[0]);



			}

			if(strlen($love_location[0])<19){

				imagettftext($jpg_image, 20, 0, 245, 120, $white, $font_path, $love_location[0]);



			}



			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id ."-$random". ".jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-$random");



		}

		if($app[0]->id==326){

			$love_location=array();

			if(isset($location->photos)){

				$photos_count=count($location->photos->data);

				$count=0;

				for($i=0;$i<$photos_count;$i++){

					if(isset($location->photos->data[$i]->place)){

						$love_location[$count]=$location->photos->data[$i]->place->name;

						$count++;

					}

				}

			}

			if($love_location==NULL){

				$love_location[0]="You Cannot Find Your SOULMATE";

				if($app[0]->id==327){

					$love_location[0]="You Cannot Find Your LOVE";

				}



			}

			//$this->print_exit($love_location);

			$source_img =1;

			$random=rand(1,10);

			$user_id = $this->session->userdata("user_id");

			if (!file_exists("container/output-images/".$app[0]->id)) {

				mkdir("container/output-images/".$app[0]->id, 0777, true);

			}

			shuffle($love_location);

			//$this->print_exit($love_location);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);



			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/".$app[0]->id."/".$user_id."-$random.jpg",$pic_positions[0],$pic_positions[1]);

			$this->merge_images_jpg_png("container/output-images/".$app[0]->id."/".$user_id."-$random.jpg","container/input-images/".$app[0]->id."/1.png","container/output-images/".$app[0]->id."/".$user_id."-$random.jpg",82,85);

			//$this->merge_images_jpg_png("container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg","container/input-images/".$app[0]->id."/1.png","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",300,140);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$random");

			////

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id ."-$random".".jpg");

			$black = imagecolorallocate($jpg_image, 255, 255, 255);

			$white = imagecolorallocate($jpg_image, 0, 0, 0);

			$red = imagecolorallocate($jpg_image, 174, 32, 32);

			$user_name = $this->session->userdata('first_name');

			$font_path = 'assets/fonts/Kozuka.otf';



			if($love_location[0]!="You Cannot Find Your SOULMATE"){

				imagettftext($jpg_image, 12, 0, 380, 40, $white, $font_path, $user_name."'s Soulmate is waiting in");

			}



			if(strlen($love_location[0])>35){

				imagettftext($jpg_image, 18, 0, 370, 80, $white, $font_path, wordwrap($love_location[0],30,"\n"));



			}

			if(strlen($love_location[0])>=20&& strlen($love_location[0])<34){

				imagettftext($jpg_image, 18, 0, 370, 100, $white, $font_path, $love_location[0]);



			}

			if(strlen($love_location[0])<19){

				imagettftext($jpg_image, 32, 0, 370, 100, $white, $font_path, $love_location[0]);



			}



			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id ."-$random". ".jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-$random");



		}

		if(isset($location->photos)){

			$photos_count=count($location->photos->data);

			for($i=0;$i<$photos_count;$i++){

				if(isset($location->photos->data[$i]->place)){

					$love_location=$location->photos->data[$i]->place->name;

					break;

				}

			}

		}

		if($love_location==NULL){

			$love_location="You Cannot Find Your LOVE";

		}

		//$this->print_exit($love_location);

		$source_img =1;

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/".$app[0]->id)) {

			mkdir("container/output-images/".$app[0]->id, 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$this->merge_images_jpg_png("container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",85,40);

		$this->merge_images_jpg_png("container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg","container/input-images/".$app[0]->id."/1.png","container/output-images/".$app[0]->id."/".$user_id."-$source_img.jpg",300,140);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		////

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id ."-$source_img".".jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);

		$red = imagecolorallocate($jpg_image, 174, 32, 32);

		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/Kozuka.otf';

		if($love_location!="You Cannot Find Your LOVE"){

			imagettftext($jpg_image, 12, 0, 370, 30, $white, $font_path, $user_name."'s Love is waiting in");

		}

		if(strlen($love_location)>=20){

			imagettftext($jpg_image, 18, 0, 370, 100, $white, $font_path, $love_location);



		}

		if(strlen($love_location)<15){

			imagettftext($jpg_image, 35, 0, 370, 100, $white, $font_path, $love_location);



		}

		if(strlen($love_location)>15&& strlen($love_location)<20){

			imagettftext($jpg_image, 30, 0, 360, 100, $white, $font_path, $love_location);



		}

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id ."-$source_img". ".jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-$source_img");





	}

	public function love_count($app)

	{

		$source_img = 100;///rand(1, count($images) - 2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		//$this->crop_pic2("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 138,138);

		exec('convert -size 200x200 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 200,200,1,200" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

		$merged_image = $this->get_merged_images_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "", 35, 82);

		$black = imagecolorallocate($merged_image, 0, 0, 0);

                


		$font_path = 'assets/fonts/gillsansbold.ttf';



		if($this->session->userdata("love_count")==NULL || count($this->session->userdata("love_count"))<1){

			$love_count = array('00','10','05','02','04','06','07','08','20','30','40','50','55','05','08','12','15','18','33','35','22','25','28','42','44','48','45','52','59');

			shuffle($love_count);



			//$letters_random = array_rand($number,26);

			$this->session->set_userdata("love_count",$love_count);



		}

		//$this->print_exit($this->session->userdata("number"));

		imagettftext($merged_image, 55, 0, 510, 160, $black, $font_path, $this->session->userdata("love_count")[0]);





		imagejpeg($merged_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-".$this->session->userdata("love_count")[0].".jpg", 80);

		imagedestroy($merged_image);

		$love_count = $this->session->userdata("love_count");



		$removed_hour = $love_count[0];

		array_shift($love_count);

		$this->session->set_userdata("love_count",$love_count);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-".$removed_hour);

	}

	public function logic_twenty($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_photos = $this->get_user_photos("","");

		$best_friends = $this->best_friends($user_photos);

		$random_percentages[0] = rand(60,100);

		$random_percentages[1] = rand(60,100);

		$random_percentages[2] = rand(60,100);

		if(count($best_friends)){

			$this->get_profile_pics_new($best_friends,3);

			$first_names = $this->get_first_names_new($best_friends,3);

			$best_friends_keys = array_keys($best_friends);

			$top=61;

			$left=42;

			//$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",180);

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",180,180);

			$this->merge_images("container/input-images/" . $app[0]->id . "/1.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", $left, $top);

			$top=99;

			$left =305;

			for($i=0;$i<3;$i++) {

				$this->crop_pic("container/user-photos/".$user_id."/friends/$best_friends_keys[$i].jpg","container/user-photos/".$user_id."/friends/$best_friends_keys[$i]_cropped.jpg",100);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", "container/user-photos/" . $user_id . "/" . "/friends/$best_friends_keys[$i]_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", $left, $top);

				$left = $left + 147;

				if($i==1){

					$left = $left-4;

				}

			}

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

			$white = imagecolorallocate($jpg_image,0,0,0);

			$font_path = 'assets/fonts/Kozuka.otf';

			$first_names_keys = array_keys($first_names);

			$top = 80;

			$left = 10;

			//imagettftext($jpg_image, 15, 0, $left, $top, $white , $font_path, $this->session->userdata('first_name'));

			$left = 300;

			$percentages_left = 340;

			for($i=0;$i<count($first_names_keys);$i++){



				if(strlen($first_names[$first_names_keys[$i]])<=5){

					$temp_left = $left+40;

				}elseif(strlen($first_names[$first_names_keys[$i]])>=5 && strlen($first_names[$first_names_keys[$i]])<11){

					$temp_left = $left+30;

				}

				else{

					$temp_left = $left;

				}

				

				imagettftext($jpg_image, 15, 0, $temp_left, $top, $white , $font_path, $first_names[$first_names_keys[$i]]);

				imagettftext($jpg_image, 25, 0, $percentages_left, $top+150, $white , $font_path, $random_percentages[$i]."%");

				$left= $left+140;

				$percentages_left = $percentages_left+140;

			}

			imagejpeg($jpg_image,"container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg",100);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id);

		}else{

			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

			redirect('apps/home');

		}

	}
	public function get_user_feed_2017()
	{
	
		//$accessToken = $_SESSION['fb_access_token'];
		$this->refresh_access_token();
		$access_token = $this->session->userdata("access_token");
		$feed_json = @file_get_contents("https://graph.facebook.com/me?fields=feed.limit(200).since(1483259546){id,name,type,from,status_type,with_tags,message_tags,likes.limit(1000).summary(true){id}}&access_token=" . $access_token);
		//$feed_json = $this->facebook->request('get','/me?fields=feed.limit(200).since(1483259546){id,name,type,from,status_type,with_tags,message_tags,likes.limit(1000).summary(true){id}}', $access_token);
		$feed_decoded = json_decode($feed_json);
		
		return $feed_decoded;
	}
	public function get_user_photos_2017()
	{
	
		//$accessToken = $_SESSION['fb_access_token'];
		$this->refresh_access_token();
		$access_token = $this->session->userdata("access_token");
		$feed_json = @file_get_contents("https://graph.facebook.com/me?fields=feed.limit(200).since(1483259546){id,name,type,likes.limit(1000).summary(true){id}}&access_token=" . $access_token);
		//$feed_json = $this->facebook->request('get','/me?fields=feed.limit(200).since(1483259546){id,name,type,from,status_type,with_tags,message_tags,likes.limit(1000).summary(true){id}}', $access_token);
		$feed_decoded = json_decode($feed_json);
		return $feed_decoded;
	}
    public function top_liker_2017($app){
		$this->has_access_token($app);
		$this->refresh_access_token();
		$user_id = $this->session->userdata("user_id");
		if (!file_exists("container/output-images/" . $app[0]->id)) {
			mkdir("container/output-images/" . $app[0]->id, 0777, true);
		}
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {
            mkdir("container/user-photos/" . $user_id, 0777, true);
            	}
        $this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$user_feed = $this->get_user_feed_2017();
		if (isset($user_feed->feed)) {
			$best_friends = $this->best_friends_from_likes($user_feed->feed->data);
			if (count($best_friends)) {
				$this->get_profile_pics_new($best_friends, 3);
				$first_names = $this->get_first_names_new($best_friends, 3);
				$best_friends_keys = array_keys($best_friends);
                $this->get_profile_pics_new($best_friends, 3);
				$first_names = $this->get_first_names_new($best_friends, 3);
				$best_friends_keys = array_keys($best_friends);
                if($app[0]->id==657){
                $this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 378, 378);
				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 140, 140);
				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 135, 135);
				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 135, 135);
				$this->merge_images("container/input-images/" . $app[0]->id . "/source.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 0, 0);
				$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg",0,0);
				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 598, 96);
                $this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 452, 134);
                $this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 304, 154);
                
				
				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");
				$white = imagecolorallocate($jpg_image, 255, 255, 255);
				$black = imagecolorallocate($jpg_image, 0, 0, 0);
				$user_name = $this->session->userdata('first_name');
				$font_path = 'assets/fonts/Calibri.ttf';
				imagettftext($jpg_image, 20, 0, 602, 90, $black, $font_path, explode(" ",$first_names[$best_friends_keys[0]])[0]);
				imagettftext($jpg_image, 20, 0, 456, 124, $black, $font_path, explode(" ",$first_names[$best_friends_keys[1]])[0]);
				imagettftext($jpg_image, 20, 0, 308, 150, $black, $font_path, explode(" ",$first_names[$best_friends_keys[2]])[0]);
				imagettftext($jpg_image, 52, 0, 610, 286, $black, $font_path, $best_friends[$best_friends_keys[0]]);
				imagettftext($jpg_image, 28, 0, 614, 320, $black, $font_path, "Likes");
				imagettftext($jpg_image, 26, 0, 456, 320, $black, $font_path, $best_friends[$best_friends_keys[1]]." Likes");
				imagettftext($jpg_image, 26, 0, 306, 320, $black, $font_path, $best_friends[$best_friends_keys[2]]." Likes");
				
				
				imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);
				imagedestroy($jpg_image);

				redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);
			
                }
				if($app[0]->id==607){
				$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 378, 378);
				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 378, 378);
				
				$this->merge_images("container/input-images/" . $app[0]->id . "/source.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 0, 0);
				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 378, 0);
                $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg",0,0);
				
				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");
				
				
				
				imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);
				imagedestroy($jpg_image);

				redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);
			
				}
				$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 378, 378);
				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 378, 378);
				
				$this->merge_images("container/input-images/" . $app[0]->id . "/source.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 0, 0);
				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 378, 0);
                $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg",0,0);
				
				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");
				$white = imagecolorallocate($jpg_image, 255, 255, 255);
				$black = imagecolorallocate($jpg_image, 0, 0, 0);
				$user_name = $this->session->userdata('first_name');
				$font_path = 'assets/fonts/BebasNeueBold.ttf';
				//imagettftext($jpg_image, 26, 0, 100, 60, $black, $font_path, $user_name.", ".$first_names[0]."  is your top liker of 2017");
				//imagettftext($jpg_image, 22, 0, 500, 320, $white, $font_path, $first_names[0]);
				imagettftext($jpg_image, 45, 0, 364, 238, $black, $font_path, $best_friends[$best_friends_keys[0]]);
				//imagettftext($jpg_image, 20, 0, 250, 30, $black, $font_path, $first_names[$best_friends_keys[0]]);
				
				imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);
				imagedestroy($jpg_image);

				redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);
			} else {
				$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');
				redirect('apps/home');
			}
		}else {
			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');
			redirect('apps/home');
		}
	}
	public function top_liker($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_feed = $this->get_user_feed();

		if($app[0]->id==401){

			$total_likes=0;

			$photos_count=0;

			$posts=$user_feed->feed->data;

			//$photos=$user_feed->feed->data[1]->type;

			//$this->print_exit($photos);

			for($i=0;$i<count($posts);$i++){

				$total_likes=$total_likes+$user_feed->feed->data[$i]->likes->summary->total_count;

				if($user_feed->feed->data[$i]->type=="photo"){

					$photos_count=$photos_count+1;

				}

			}

			$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 180, 180);

			$this->merge_images("container/input-images/" . $app[0]->id . "/200.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 52, 80);

			$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/2.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 40, 74);



			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

			$white = imagecolorallocate($jpg_image, 44, 44, 44);

			$black = imagecolorallocate($jpg_image, 255, 255, 255);



			$user_name = $this->session->userdata('first_name');

			$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

			$font='assets/fonts/RobotoCondensed-Bold.ttf';

			imagettftext($jpg_image, 20, 0, 60, 300, $white, $font_path, $user_name);

			imagettftext($jpg_image, 22, 0, 300, 113, $white, $font, $total_likes);

			imagettftext($jpg_image, 22, 0, 565, 115, $white, $font, count($posts));

			imagettftext($jpg_image, 22, 0, 440, 113, $white, $font, $photos_count);



			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);

		}

		if($app[0]->id==402){

			$has_liked=0;

			$posts=$user_feed->feed->data;

			//$photos=$user_feed->feed->data[1]->type;

			//$this->print_exit($photos);

			for($i=0;$i<count($posts);$i++){

				$has_liked=$has_liked+$user_feed->feed->data[$i]->likes->summary->has_liked;



			}

			$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 180, 180);

			$this->merge_images("container/input-images/" . $app[0]->id . "/100.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 52, 80);

			$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 44, 74);

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

			$white = imagecolorallocate($jpg_image, 44, 44, 44);

			$black = imagecolorallocate($jpg_image, 17, 52, 106);



			$user_name = $this->session->userdata('first_name');

			$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

			$font='assets/fonts/RobotoCondensed-Bold.ttf';

			imagettftext($jpg_image, 20, 0, 60, 300, $white, $font_path, $user_name);

			imagettftext($jpg_image, 80, 0, 300, 113, $black, $font, $has_liked);



			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);



		}

		if (isset($user_feed->feed)) {

			$best_friends = $this->best_friends_from_likes($user_feed->feed->data);

			if (count($best_friends)) {

				$this->get_profile_pics_new($best_friends, 1);

				$first_names = $this->get_first_names_new($best_friends, 1);

				$best_friends_keys = array_keys($best_friends);

				if($app[0]->id==407){

					$this->get_profile_pics($best_friends, 1);

					$first_names = $this->get_first_names($best_friends, 1);

					$best_friends_keys = array_keys($best_friends);

					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 160, 160);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 160, 160);



					$this->merge_images("container/input-images/" . $app[0]->id . "/1.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 525, 97);

					$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 45, 100);



					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

					$white = imagecolorallocate($jpg_image, 0, 0, 0);

					$black = imagecolorallocate($jpg_image, 255, 255, 255);

					$blue = imagecolorallocate($jpg_image, 42, 46, 76);





					$user_name = $this->session->userdata('first_name');

					$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

					imagettftext($jpg_image, 18, 0, 70, 315, $blue, $font_path, strtoupper($first_names[$best_friends_keys[0]]));

					imagettftext($jpg_image, 16, 0, 550, 310, $blue, $font_path,strtoupper($user_name));



					imagettftext($jpg_image, 42, 0, 370, 145, $blue, $font_path, $best_friends[$best_friends_keys[0]]);



					//imagettftext($jpg_image, 20, 0, 250, 30, $black, $font_path, $first_names[$best_friends_keys[0]]);



					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

					imagedestroy($jpg_image);



					redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);



				}

				if($app[0]->id==338){

					$this->get_profile_pics_new($best_friends, 3);

					$first_names = $this->get_first_names_new($best_friends, 3);

					$best_friends_keys = array_keys($best_friends);

					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 140, 140);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 140, 140);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 140, 140);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 140, 140);



					$this->merge_images("container/input-images/" . $app[0]->id . "/1.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 56, 95);

					$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 250, 95);

					$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 400, 95);

					$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 550, 95);

					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 243, 90);

					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 393, 90);

					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 543, 90);



					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

					$white = imagecolorallocate($jpg_image, 0, 0, 0);

					$black = imagecolorallocate($jpg_image, 255, 255, 255);



					$user_name = $this->session->userdata('first_name');

					$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

					imagettftext($jpg_image, 18, 0, 60, 300, $black, $font_path, $user_name);

					imagettftext($jpg_image, 15, 0, 255, 85, $black, $font_path, $first_names[$best_friends_keys[0]]);

					imagettftext($jpg_image, 15, 0, 405, 85, $black, $font_path, $first_names[$best_friends_keys[1]]);

					imagettftext($jpg_image, 15, 0, 560, 85, $black, $font_path, $first_names[$best_friends_keys[2]]);



					imagettftext($jpg_image, 42, 0, 290, 290, $black, $font_path, $best_friends[$best_friends_keys[0]]);

					imagettftext($jpg_image, 42, 0, 430, 290, $black, $font_path, $best_friends[$best_friends_keys[1]]);

					imagettftext($jpg_image, 42, 0, 565, 290, $black, $font_path, $best_friends[$best_friends_keys[2]]);



					//imagettftext($jpg_image, 20, 0, 250, 30, $black, $font_path, $first_names[$best_friends_keys[0]]);



					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

					imagedestroy($jpg_image);



					redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);



				}

				$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 180, 180);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 180, 180);

				

				$this->merge_images("container/input-images/" . $app[0]->id . "/100.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 62, 95);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 502, 95);



				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

				$white = imagecolorallocate($jpg_image, 0, 0, 0);

				$black = imagecolorallocate($jpg_image, 255, 255, 255);



				$user_name = $this->session->userdata('first_name');

				$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

				imagettftext($jpg_image, 22, 0, 60, 320, $black, $font_path, $user_name);

				imagettftext($jpg_image, 22, 0, 500, 320, $white, $font_path, $first_names[0]);

				imagettftext($jpg_image, 42, 0, 310, 170, $black, $font_path, $best_friends[$best_friends_keys[0]]);

				//imagettftext($jpg_image, 20, 0, 250, 30, $black, $font_path, $first_names[$best_friends_keys[0]]);

				

				imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

				imagedestroy($jpg_image);



				redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);

			} else {

				$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

				redirect('apps/home');

			}

		}else {

			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

			redirect('apps/home');

		}

	}

	public function get_full_names_new($friends,$limit){

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		if(array_key_exists($user_id,$friends)){

			unset($friends[$user_id]);

		}

		$friends_keys = array_keys($friends);

		$full_names = array();

		for($i=0;$i<$limit;$i++){

			$user_json = file_get_contents("https://graph.facebook.com/$friends_keys[$i]?fields=name&access_token=".$access_token);

			$user_decoded = json_decode($user_json);

			$full_names[$friends_keys[$i]]=$user_decoded->name;

		}

		return $full_names;

	}

	public function test_rankings($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		$user_name = $this->session->userdata("name");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_photos = $this->get_user_photos("","");

		$best_friends = $this->best_friends($user_photos);





		$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';



		if(count($best_friends)>10) {

			//$this->get_profile_pics($best_friends,3);

			//$full_names = $this->get_first_names($best_friends, 20);



			$jpg_image = imagecreatefromjpeg("container/input-images/" . $app[0]->id . "/1.jpg");

			$white = imagecolorallocate($jpg_image, 24, 49, 89);

			$color = imagecolorallocate($jpg_image, 255, 255, 255);

			$font_path1 = 'assets/fonts/gillsansbold.ttf';

			$first_names = $this->get_full_names_new($best_friends,count($best_friends)-1);

			$friends_keys = array_keys($first_names);

			$friends_name=array();

			for($i=0;$i<count($first_names);$i++){

				$friends_name[$i]=$first_names[$friends_keys[$i]];

			}

			//$friends_name = array($first_names[0],$first_names[1],$first_names[2],$first_names[3],$first_names[4],$first_names[5],$first_names[6],$first_names[7],$first_names[8],$first_names[9],$first_names[10],$first_names[11],$first_names[12],$first_names[13],$first_names[14],$first_names[15],$first_names[16],$first_names[17],$first_names[18]);



			shuffle($friends_name);

			$friends_list=array($user_name,$friends_name[0],$friends_name[1],$friends_name[2],$friends_name[3],$friends_name[4],$friends_name[5],$friends_name[6],$friends_name[7],$friends_name[8]);

			shuffle($friends_list);

			//imagettftext($jpg_image, 22, 0, 505, 150, $black, $name_font_path, explode(" ",$user_name)[0]);

			//imagettftext($jpg_image, 70, 0, 510, 250, $black, $font_path, $this->session->userdata("friends_name")[0]);

			//$friends_name = $this->session->userdata("friends_name");

			//array_shift($friends_name);

			//$this->session->set_userdata("friends_name",$friends_name);



			$black=imagecolorallocate($jpg_image,0, 0, 0);



			$odi_score2=rand(1,10);

			if($app[0]->id==425 || $app[0]->id==426 || $app[0]->id==427 || $app[0]->id==428){

				if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}

				$pic_positions = explode("X",$app[0]->picture_position);

				$pic_dimensions = explode("X",$app[0]->picture_dimensions);





				$source_img=1;

				$friends_list=array($friends_name[0],$friends_name[1],$friends_name[2],$friends_name[3],$friends_name[4],$friends_name[5],$friends_name[6],$friends_name[7],$friends_name[8],$friends_name[9]);

				shuffle($friends_list);


				$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

				$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

				$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

				

				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

				$black = imagecolorallocate($jpg_image, 255, 255, 255);

				$white = imagecolorallocate($jpg_image, 0, 0, 0);



				$font_path = 'assets/fonts/BebasNeueBold.ttf';



				imagettftext($jpg_image, 21, 0, 154, 90, $black , $font_path, strtoupper($user_name));

				imagettftext($jpg_image, 21, 0, 154, 152, $black , $font_path, strtoupper($friends_list[0]));

				imagettftext($jpg_image, 21, 0, 154, 210, $black , $font_path, strtoupper($friends_list[1]));

				imagettftext($jpg_image, 21, 0, 154, 274, $black , $font_path, strtoupper($friends_list[2]));

				imagettftext($jpg_image, 21, 0, 154, 332, $black , $font_path, strtoupper($friends_list[3]));



				if($app[0]->id==425){

					$list1=rand(4000,4400);

					$list2=rand(3800,3990);

					$list3=rand(3600,3790);

					$list4=rand(3400,3590);

					$list5=rand(3200,3390);

				}

				if($app[0]->id==426){

					$list1=rand(120,150);

					$list2=rand(110,120);

					$list3=rand(100,110);

					$list4=rand(90,100);

					$list5=rand(80,90);

				}

				if($app[0]->id==427){

					$list1=rand(240,260);

					$list2=rand(220,240);

					$list3=rand(210,220);

					$list4=rand(200,210);

					$list5=rand(100,200);

				}

				if($app[0]->id==428){

					$list1=rand(115,120);

					$list2=rand(110,115);

					$list3=rand(105,110);

					$list4=rand(100,105);

					$list5=rand(95,100);

				}





				imagettftext($jpg_image, 24, 0, 84, 90, $black , $font_path, $list1);

				imagettftext($jpg_image, 24, 0, 84, 152, $black , $font_path, $list2);

				imagettftext($jpg_image, 24, 0, 84, 210, $black , $font_path, $list3);

				imagettftext($jpg_image, 24, 0, 84, 274, $black , $font_path, $list4);

				imagettftext($jpg_image, 24, 0, 84, 332, $black , $font_path, $list5);





				imagejpeg($jpg_image,"container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",100);

				imagedestroy($jpg_image);

				redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");



			}

			if($app[0]->id==393 || $app[0]->id==480){

				if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}
				if($app[0]->id==480){
					$source_img=1;
				}
				else{
					$source_img=rand(1,8);
				}
				

				$friends_list=array($friends_name[0],$friends_name[1],$friends_name[2],$friends_name[3],$friends_name[4],$friends_name[5],$friends_name[6],$friends_name[7],$friends_name[8],$friends_name[9]);

				shuffle($friends_list);
				$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",144,144);

				exec('convert -size 144x144 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 72,72,1,72" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

				//exec('convert -size 144x144 xc:none -fill ""container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 72,72,1,72" ""container/user-photos/' . $user_id . '/profile_pic_circle.png"');

				$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/$source_img.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 585, 32);

				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

				$font_path = 'assets/fonts/Kozuka.otf';

				$black=imagecolorallocate($jpg_image,39, 39, 39);

				$white=imagecolorallocate($jpg_image,255, 255, 255);

				

				imagettftext($jpg_image, 13, 0, 142, 68, $black , $font_path, strtoupper($user_name." (C)"));

				imagettftext($jpg_image, 13, 0, 142, 93, $black , $font_path, strtoupper($friends_list[0]));

				imagettftext($jpg_image, 13, 0, 142, 118, $black , $font_path, strtoupper($friends_list[1]));

				imagettftext($jpg_image, 13, 0, 142, 142, $black , $font_path, strtoupper($friends_list[2]));

				imagettftext($jpg_image, 13, 0, 142, 165, $black , $font_path, strtoupper($friends_list[3])." (VC)");

				imagettftext($jpg_image, 13, 0, 142, 192, $black , $font_path, strtoupper($friends_list[4])." (WK)");

				imagettftext($jpg_image, 13, 0, 142, 218, $black , $font_path, strtoupper($friends_list[5]));

				imagettftext($jpg_image, 13, 0, 142, 243, $black , $font_path, strtoupper($friends_list[6]));

				imagettftext($jpg_image, 13, 0, 142, 268, $black , $font_path, strtoupper($friends_list[7]));

				imagettftext($jpg_image, 13, 0, 142, 294, $black , $font_path, strtoupper($friends_list[8]));

				imagettftext($jpg_image, 13, 0, 142, 318, $black , $font_path, strtoupper($friends_list[9]));

				

				imagejpeg($jpg_image,"container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",100);

				imagedestroy($jpg_image);

				redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");



			}

			if($app[0]->id==344){

				$font_path = 'assets/fonts/Kozuka.otf';

				$black=imagecolorallocate($jpg_image,39, 39, 39);

				$white=imagecolorallocate($jpg_image,255, 255, 255);

				$matches=array(rand(440,480),rand(400,430),rand(420,460),rand(380,400),rand(360,380),rand(350,360),rand(330,350),rand(300,330),rand(290,300),rand(280,290));

				$runs=array(rand(16000,18500),rand(15000,16000),rand(14000,15000),rand(13000,14000),rand(12000,13000),rand(11000,12000),rand(10000,11000),rand(9500,10000),rand(8000,9500),rand(7000,8000));

				$fifty=array(rand(80,90),rand(70,80),rand(65,70),rand(65,75),rand(55,65),rand(50,55),rand(45,60),rand(40,55),rand(35,40),rand(30,40));

				$hundred=array(rand(45,50),rand(40,45),rand(38,42),rand(35,38),rand(30,35),rand(28,32),rand(25,28),rand(22,25),rand(20,22),rand(15,20));

				$best=array(rand(240,265),rand(230,250),rand(210,220),rand(200,210),rand(190,200),rand(180,190),rand(170,180),rand(160,200),rand(170,190),rand(160,180));

				imagettftext($jpg_image, 13, 0, 30, 89, $black , $font_path, strtoupper($friends_list[0]));

				imagettftext($jpg_image, 13, 0, 30, 115, $black , $font_path, strtoupper($friends_list[1]));

				imagettftext($jpg_image, 13, 0, 30, 140, $black , $font_path, strtoupper($friends_list[2]));

				imagettftext($jpg_image, 13, 0, 30, 165, $black , $font_path, strtoupper($friends_list[3]));

				imagettftext($jpg_image, 13, 0, 30, 192, $black , $font_path, strtoupper($friends_list[4]));

				imagettftext($jpg_image, 13, 0, 30, 220, $black , $font_path, strtoupper($friends_list[5]));

				imagettftext($jpg_image, 13, 0, 30, 245, $black , $font_path, strtoupper($friends_list[6]));

				imagettftext($jpg_image, 13, 0, 30, 270, $black , $font_path, strtoupper($friends_list[7]));

				imagettftext($jpg_image, 13, 0, 30, 296, $black , $font_path, strtoupper($friends_list[8]));

				imagettftext($jpg_image, 13, 0, 30, 322, $black , $font_path, strtoupper($friends_list[9]));

				imagettftext($jpg_image, 13, 0, 340, 89, $black , $font_path, $matches[0]);

				imagettftext($jpg_image, 13, 0, 340, 115, $black , $font_path, ($matches[1]));

				imagettftext($jpg_image, 13, 0, 340, 140, $black , $font_path, ($matches[2]));

				imagettftext($jpg_image, 13, 0, 340, 165, $black , $font_path, ($matches[3]));

				imagettftext($jpg_image, 13, 0, 340, 192, $black , $font_path, ($matches[4]));

				imagettftext($jpg_image, 13, 0, 340, 220, $black , $font_path, ($matches[5]));

				imagettftext($jpg_image, 13, 0, 340, 245, $black , $font_path, ($matches[6]));

				imagettftext($jpg_image, 13, 0, 340, 270, $black , $font_path, ($matches[7]));

				imagettftext($jpg_image, 13, 0, 340, 296, $black , $font_path, ($matches[8]));

				imagettftext($jpg_image, 13, 0, 340, 322, $black , $font_path, ($matches[9]));

				imagettftext($jpg_image, 13, 0, 425, 89, $black , $font_path, $runs[0]);

				imagettftext($jpg_image, 13, 0, 425, 115, $black , $font_path, ($runs[1]));

				imagettftext($jpg_image, 13, 0, 425, 140, $black , $font_path, ($runs[2]));

				imagettftext($jpg_image, 13, 0, 425, 165, $black , $font_path, ($runs[3]));

				imagettftext($jpg_image, 13, 0, 425, 192, $black , $font_path, ($runs[4]));

				imagettftext($jpg_image, 13, 0, 425, 220, $black , $font_path, ($runs[5]));

				imagettftext($jpg_image, 13, 0, 425, 245, $black , $font_path, ($runs[6]));

				imagettftext($jpg_image, 13, 0, 425, 270, $black , $font_path, ($runs[7]));

				imagettftext($jpg_image, 13, 0, 425, 296, $black , $font_path, ($runs[8]));

				imagettftext($jpg_image, 13, 0, 425, 322, $black , $font_path, ($runs[9]));

				imagettftext($jpg_image, 13, 0, 515, 89, $black , $font_path, $fifty[0]);

				imagettftext($jpg_image, 13, 0, 515, 115, $black , $font_path, ($fifty[1]));

				imagettftext($jpg_image, 13, 0, 515, 140, $black , $font_path, ($fifty[2]));

				imagettftext($jpg_image, 13, 0, 515, 165, $black , $font_path, ($fifty[3]));

				imagettftext($jpg_image, 13, 0, 515, 192, $black , $font_path, ($fifty[4]));

				imagettftext($jpg_image, 13, 0, 515, 220, $black , $font_path, ($fifty[5]));

				imagettftext($jpg_image, 13, 0, 515, 245, $black , $font_path, ($fifty[6]));

				imagettftext($jpg_image, 13, 0, 515, 270, $black , $font_path, ($fifty[7]));

				imagettftext($jpg_image, 13, 0, 515, 296, $black , $font_path, ($fifty[8]));

				imagettftext($jpg_image, 13, 0, 515, 322, $black , $font_path, ($fifty[9]));



				imagettftext($jpg_image, 13, 0, 570, 89, $black , $font_path, $hundred[0]);

				imagettftext($jpg_image, 13, 0, 570, 115, $black , $font_path, ($hundred[1]));

				imagettftext($jpg_image, 13, 0, 570, 140, $black , $font_path, ($hundred[2]));

				imagettftext($jpg_image, 13, 0, 570, 165, $black , $font_path, ($hundred[3]));

				imagettftext($jpg_image, 13, 0, 570, 192, $black , $font_path, ($hundred[4]));

				imagettftext($jpg_image, 13, 0, 570, 220, $black , $font_path, ($hundred[5]));

				imagettftext($jpg_image, 13, 0, 570, 245, $black , $font_path, ($hundred[6]));

				imagettftext($jpg_image, 13, 0, 570, 270, $black , $font_path, ($hundred[7]));

				imagettftext($jpg_image, 13, 0, 570, 296, $black , $font_path, ($hundred[8]));

				imagettftext($jpg_image, 13, 0, 570, 322, $black , $font_path, ($hundred[9]));



				imagettftext($jpg_image, 18, 0, 650, 93, $white , $font_path, $best[0]);

				imagettftext($jpg_image, 18, 0, 650, 119, $white , $font_path, ($best[1]."*"));

				imagettftext($jpg_image, 18, 0, 650, 144, $white , $font_path, ($best[2]."*"));

				imagettftext($jpg_image, 18, 0, 650, 169, $white , $font_path, ($best[3]));

				imagettftext($jpg_image, 18, 0, 650, 196, $white , $font_path, ($best[4]));

				imagettftext($jpg_image, 18, 0, 650, 224, $white , $font_path, ($best[5]."*"));

				imagettftext($jpg_image, 18, 0, 650, 249, $white , $font_path, ($best[6]));

				imagettftext($jpg_image, 18, 0, 650, 274, $white , $font_path, ($best[7]."*"));

				imagettftext($jpg_image, 18, 0, 650, 300, $white , $font_path, ($best[8]));

				imagettftext($jpg_image, 18, 0, 650, 326, $white , $font_path, ($best[9]."*"));



				imagejpeg($jpg_image,"container/output-images/" . $app[0]->id . "/" . $user_id ."-".$odi_score2. ".jpg",100);

				imagedestroy($jpg_image);

				redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-".$odi_score2);



			}

			$r1=rand(960,1050);

			$r2=rand(930,950);

			$r3=rand(900,929);

			$r4=rand(870,899);

			$r5=rand(850,869);

			$r6=rand(830,849);

			$r7=rand(800,829);

			$r8=rand(775,799);

			$r9=rand(750,774);

			$r10=rand(700,749);



			//imagettftext($jpg_image, 14, 0, 160, 130, $color , $font_path, $user_name);

			imagettftext($jpg_image, 13, 0, 70, 114, $black , $font_path, strtoupper($friends_list[0]));

			imagettftext($jpg_image, 13, 0, 70, 135, $black , $font_path, strtoupper($friends_list[1]));

			imagettftext($jpg_image, 13, 0, 70, 157, $black , $font_path, strtoupper($friends_list[2]));

			imagettftext($jpg_image, 13, 0, 70, 182, $black , $font_path, strtoupper($friends_list[3]));

			imagettftext($jpg_image, 13, 0, 70, 204, $black , $font_path, strtoupper($friends_list[4]));

			imagettftext($jpg_image, 13, 0, 70, 226, $black , $font_path, strtoupper($friends_list[5]));

			imagettftext($jpg_image, 13, 0, 70, 250, $black , $font_path, strtoupper($friends_list[6]));

			imagettftext($jpg_image, 13, 0, 70, 273, $black , $font_path, strtoupper($friends_list[7]));

			imagettftext($jpg_image, 13, 0, 70, 295, $black , $font_path, strtoupper($friends_list[8]));

			imagettftext($jpg_image, 13, 0, 72, 317, $black , $font_path, strtoupper($friends_list[9]));



			imagettftext($jpg_image, 15, 0, 506, 114, $black , $font_path, strtoupper($r1));

			imagettftext($jpg_image, 15, 0, 510, 135, $black , $font_path, strtoupper($r2));

			imagettftext($jpg_image, 15, 0, 510, 157, $black , $font_path, strtoupper($r3));

			imagettftext($jpg_image, 15, 0, 510, 182, $black , $font_path, strtoupper($r4));

			imagettftext($jpg_image, 15, 0, 510, 204, $black , $font_path, strtoupper($r5));

			imagettftext($jpg_image, 15, 0, 510, 226, $black , $font_path, strtoupper($r6));

			imagettftext($jpg_image, 15, 0, 510, 250, $black , $font_path, strtoupper($r7));

			imagettftext($jpg_image, 15, 0, 510, 273, $black , $font_path, strtoupper($r8));

			imagettftext($jpg_image, 15, 0, 510, 295, $black , $font_path, strtoupper($r9));

			imagettftext($jpg_image, 15, 0, 510, 318, $black , $font_path, strtoupper($r10));







			imagejpeg($jpg_image,"container/output-images/" . $app[0]->id . "/" . $user_id ."-".$odi_score2. ".jpg",100);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".$user_id."-".$odi_score2);



		}





		else{

			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

			redirect('apps/home');

		}

	}

	public function destiny_logic_new($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;

		$luck =rand(80,100);

		$love=rand(25,35);

		$kids=rand(1,5);

		$health=rand(80,100);

		$car=array("Renault","Lamborghini","BMW","Ferrari","Volkswagon","Ford","Hyundai","Toyota","Maruti");

		shuffle($car);



		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/" .$user_id."/"."profile_pic.jpg","container/user-photos/" .$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);


		exec('convert -size 200x200 xc:none -fill "/home/witapps/public_html/container/user-photos/'. $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 100,100,1,100" "/home/witapps/public_html/container/user-photos/'. $user_id . '/profile_pic_circle.png"');

		$this->merge_images_jpg_png("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/" .$user_id."/"."profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg",495,66);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$luck");

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);



		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/Kozuka.otf';

		imagettftext($jpg_image, 18, 0, 535, 312, $black, $font_path, $user_name);

		imagettftext($jpg_image, 24, 0, 190, 44, $black, $font_path, $luck."%");

		imagettftext($jpg_image, 24, 0, 190, 96, $black, $font_path, "Married at ".$love);

		imagettftext($jpg_image, 24, 0, 190, 152, $black, $font_path, $kids);

		imagettftext($jpg_image, 24, 0, 190, 205, $black, $font_path, $health."%");

		imagettftext($jpg_image, 24, 0, 190, 262, $black, $font_path, "Wealthy");

		imagettftext($jpg_image, 24, 0, 190, 312, $black, $font_path, $car[0]);





		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$luck");

	}

	public function future_prediction($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$name = $this->session->userdata("first_name");

		if($name[0]=="A"||$name[0]=="C"||$name[0]=="L"||$name[0]=="X"){

			$source_img = 1;

		}

		if($name[0]=="B"){

			$source_img = 2;

		}

		if($name[0]=="D"){

			$source_img = 3;

		}

		if($name[0]=="G"||$name[0]=="S"){

			$source_img = 4;

		}

		if($name[0]=="H"){

			$source_img = 5;

		}if($name[0]=="K"){

			$source_img = 6;

		}if($name[0]=="M"){

			$source_img = 7;

		}if($name[0]=="N"||$name[0]=="Y"){

			$source_img = 8;

		}if($name[0]=="P"||$name[0]=="F"){

			$source_img = 9;

		}if($name[0]=="Q"||$name[0]=="J"||$name[0]=="Z"){

			$source_img = 10;

		}if($name[0]=="R"||$name[0]=="T"){

			$source_img = 11;

		}if($name[0]=="U"||$name[0]=="E"||$name[0]=="O"||$name[0]=="I"||$name[0]=="V"||$name[0]=="W"){

			$source_img = 12;

		}

		

	

		

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}

	public function personality_logic($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;

		$user_id = $this->session->userdata("user_id");

		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/BebasNeueBold.ttf';

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		if($app[0]->id==430){

			//$love=rand(80,100);

			$love=array("Hugs","Spreading joy","Spreading Happiness","To Play Hard","Family","Friends","Kids","being optimistic","To Travel and Discover");

			//$year=rand(2018,2026);

			$hate=array("Nagging","Traitors","Work Hard","Liars","Stress","Phonies");

			$dontcare=array("Unnecessary drama","About Fashion","Being Productive","About fake rumours","Everything else","Pointless Conversations","About gossiping");



			shuffle($love);shuffle($hate);shuffle($dontcare);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);


			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			exec('convert -size 200x200 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 100,100,1,100" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

			$this->merge_images_jpg_png("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",502,50);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

			

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 0, 0, 0);

			$white = imagecolorallocate($jpg_image, 255, 255, 255);





			//imagettftext($jpg_image, 24, 0, 46, 280, $white, $font_path, $user_name);

			imagettftext($jpg_image, 26, 0, 220, 85, $black, $font_path, $love[0]);

			imagettftext($jpg_image, 26, 0, 220, 165, $black, $font_path, $hate[0]);

			imagettftext($jpg_image, 26, 0, 280, 248, $black, $font_path, $dontcare[0]);





			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");





		}

		if($app[0]->id==429){

			$gender = $this->session->userdata("gender");

			if($gender=="male"){

				$source_img =1;

				$body=array("Skinny","Curvy","Athletic","Tanned","Little Fat","Chubby");

				$looks=array("Tall","Short","Pretty","Beautiful","Attractive");

			}

			else{

				$source_img =2;

				$body=array("Skinny","Muscular","Athletic","Tanned","Little Fat","Six Pack");

				$looks=array("Tall","Short","Handsome","Attractive");

			}

			$hair=array("Black","Blonde","Red","Brown");

			$skin=array("Black","Fair","Olive","Brown","Very Fair","White");

			$eyes=array("Brown","Baby Blue","Black","Blue");





			shuffle($body);shuffle($looks);shuffle($hair);shuffle($skin);shuffle($eyes);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",495,70);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

			

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 0, 0, 0);

			$white = imagecolorallocate($jpg_image, 255, 255, 255);





			//imagettftext($jpg_image, 24, 0, 46, 280, $white, $font_path, $user_name);

			imagettftext($jpg_image, 30, 0, 120, 82, $white, $font_path, $hair[0]);

			imagettftext($jpg_image, 30, 0, 170, 132, $white, $font_path, $skin[0]);

			imagettftext($jpg_image, 30, 0, 120, 182, $white, $font_path, $eyes[0]);

			imagettftext($jpg_image, 30, 0, 130, 232, $white, $font_path, $body[0]);

			imagettftext($jpg_image, 30, 0, 130, 282, $white, $font_path, $looks[0]);





			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");





		}

		if($app[0]->id==422){

			//$love=rand(80,100);

			$date=rand(1,28);

			//$year=rand(2018,2026);

			$month=array("JAN","FEB","MAR","APR","MAY","JUNE","JULY","AUG","SEP","OCT","NOV","DEC");

			$age=rand(25,36);

			$letters=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

			$marriage=array("LOVE","ARRANGE");

			shuffle($month);shuffle($marriage);shuffle($letters);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",410,75);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

			

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 0, 0, 0);

			$white = imagecolorallocate($jpg_image, 255, 255, 255);





			//imagettftext($jpg_image, 24, 0, 46, 280, $white, $font_path, $user_name);

			imagettftext($jpg_image, 32, 0, 240, 122, $black, $font_path, $month[0]." ".$date);

			imagettftext($jpg_image, 32, 0, 240, 185, $black, $font_path, $age." Yrs");

			imagettftext($jpg_image, 32, 0, 240, 248, $black, $font_path, $marriage[0]);

			imagettftext($jpg_image, 44, 0, 338, 314, $white, $font_path, $letters[0]);





			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");





		}

		if($app[0]->id==417){

			$source_img =rand(1,10);

			$love=rand(80,100);

			$when=rand(45,80);

			$where=array("Pakistan","Tehran","Bagdhdad","Abu Dabhi","Islamabad","USA","New York","Moscow","Kabul","Delhi");

			shuffle($where);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",24,42);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

			

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 0, 0, 0);

			$white = imagecolorallocate($jpg_image, 255, 255, 255);





			imagettftext($jpg_image, 24, 0, 46, 280, $white, $font_path, $user_name);

			imagettftext($jpg_image, 32, 0, 364, 100, $black, $font_path, "AT THE AGE OF ".$when);

			imagettftext($jpg_image, 32, 0, 364, 180, $black, $font_path, "AT ".$where[0]);









			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");





		}

		if($app[0]->id==407){

			$love=rand(80,100);

			$hate=rand(0,20);

			$jealous=rand(0,30);

			$kindness=rand(80,100);

			$anger=rand(0,60);

			$pic_positions = explode("X",$app[0]->picture_position);

			$pic_dimensions = explode("X",$app[0]->picture_dimensions);

			$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

			$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",480,65);

			$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

			

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 255, 255, 255);

			$white = imagecolorallocate($jpg_image, 0, 0, 0);





			imagettftext($jpg_image, 24, 0, 500, 312, $black, $font_path, $user_name);

			imagettftext($jpg_image, 36, 0, 190, 60, $black, $font_path, $love."%");

			imagettftext($jpg_image, 36, 0, 216, 120, $black, $font_path, $jealous."%");

			imagettftext($jpg_image, 36, 0, 230, 182, $black, $font_path, $kindness."%");

			imagettftext($jpg_image, 36, 0, 190, 245, $black, $font_path, $hate."%");

			imagettftext($jpg_image, 36, 0, 190, 312, $black, $font_path, $anger."%");







			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

			imagedestroy($jpg_image);

			redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");





		}

		

		$color =array("RED","BLUE","YELLOW","BLACK","WHITE","VIOLET","GREEN","GOLD","SILVER","ORANGE");

		$love=array("BEAUTY","NATURE","PETS","MUSIC","SPORTS","HUMOR","SUNSHINE","FUN");

		$hates=array("INJUSTICES","POLITICIANS","BAD MUSIC");

		$strength=array("SELF-CONFIDENCE","INTELLIGENCE","LOYALITY","LOGIC");

		$mistake=array("PERFECTIONIST","HESITANT","SELFISH","CAN'T SAY NO");

		$personality=array("FIGHTER","LEADER","HAPPY","WARM","COOL");

		shuffle($color);shuffle($love);shuffle($hates);shuffle($strength);shuffle($mistake);shuffle($personality);



		

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",41,42);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 44, 44, 44);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);



		

		imagettftext($jpg_image, 24, 0, 50, 300, $black, $font_path, $user_name);

		imagettftext($jpg_image, 26, 0, 550, 40, $black, $font_path, $color[0]);

		imagettftext($jpg_image, 26, 0, 550, 90, $black, $font_path, $love[0]);

		imagettftext($jpg_image, 26, 0, 550, 142, $black, $font_path, $hates[0]);

		imagettftext($jpg_image, 26, 0, 550, 200, $black, $font_path, $strength[0]);

		imagettftext($jpg_image, 26, 0, 550, 252, $black, $font_path, $mistake[0]);

		imagettftext($jpg_image, 30, 0, 550, 310, $black, $font_path, $personality[0]);





		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$source_img");

	}

	public function destiny_logic($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;

		$luck =rand(80,100);

		$love=rand(25,35);

		$kids=rand(1,5);

		$health=rand(80,100);

		$car=array("Renault","Lamborghini","BMW","Ferrari","Volkswagon","Ford","Hyundai","Toyota","Maruti");

		shuffle($car);



		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$luck");

		////

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);



		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/Kozuka.otf';

		imagettftext($jpg_image, 18, 0, 458, 300, $black, $font_path, $user_name);

		imagettftext($jpg_image, 24, 0, 210, 78, $black, $font_path, $luck."%");

		imagettftext($jpg_image, 24, 0, 210, 118, $black, $font_path, "Married at ".$love);

		imagettftext($jpg_image, 24, 0, 210, 160, $black, $font_path, $kids);

		imagettftext($jpg_image, 24, 0, 210, 206, $black, $font_path, $health."%");

		imagettftext($jpg_image, 24, 0, 210, 250, $black, $font_path, "Wealthy");

		imagettftext($jpg_image, 24, 0, 210, 286, $black, $font_path, $car[0]);





		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$luck");

	}

	public function kissing($app)

	{

		$source_img = 1;///rand(1, count($images) - 2);

		$user_id = $this->session->userdata("user_id");

		$first_name = $this->session->userdata("first_name");



		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$kiss_percentage=rand(120,200);



		$this->crop_pic2("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 180,180);


		exec('convert -size 180x180 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 90,90,1,90" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');



		//$merged_image = $this->get_merged_images_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "", 60, 95);

		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/1.jpg","container/user-photos/" . $user_id . "/" . "profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" . $user_id . "-".$kiss_percentage.".jpg",60, 95);

		$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . "-".$kiss_percentage.".jpg","container/input-images/" . $app[0]->id . "/1.png","container/output-images/" . $app[0]->id . "/" . $user_id . "-".$kiss_percentage.".jpg",170,180);

		$merged_image=imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-".$kiss_percentage.".jpg");

		$red = imagecolorallocate($merged_image, 191, 0, 0);

		$black = imagecolorallocate($merged_image, 44, 44, 44);



		$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

		$font_path1 = 'assets/fonts/Kozuka.otf';



		//$this->print_exit($this->session->userdata("number"));

		imagettftext($merged_image, 24, 0, 340, 120, $black, $font_path,$first_name.' is a' );

		if($kiss_percentage<150){

			imagettftext($merged_image, 25, 0, 340, 160, $black, $font_path,'Professional Kisser' );



		}

		if($kiss_percentage>150&& $kiss_percentage<180){

			imagettftext($merged_image, 25, 0, 340, 160, $black, $font_path,'Romantic Kisser' );



		}

		if($kiss_percentage>180){

			imagettftext($merged_image, 25, 0, 340, 160, $black, $font_path,'Hardcore Kisser' );



		}

		imagettftext($merged_image, 60, 0, 360, 260, $red, $font_path1,$kiss_percentage.'%' );





		imagejpeg($merged_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-".$kiss_percentage.".jpg", 80);

		imagedestroy($merged_image);



		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-".$kiss_percentage);

	}

	public function lucky($app)

	{
        $images = glob("container/input-images/".$app[0]->id."/*.jpg");
		$source_img = rand(1, count($images) - 2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		//$this->crop_pic2("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 138,138);

		exec('convert -size 200x200 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 100,100,1,100" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

		$merged_image = $this->get_merged_images_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "", 35, 71);

		$black = imagecolorallocate($merged_image, 0, 0, 0);

		$white = imagecolorallocate($merged_image, 255, 255, 255);

		$blue = imagecolorallocate($merged_image, 0, 51, 102);



		$font_path = 'assets/fonts/gillsansmt.ttf';



		if($this->session->userdata("number")==NULL || count($this->session->userdata("number"))<1 && $this->session->userdata("colors")==NULL || count($this->session->userdata("colors"))<1 && $this->session->userdata("days")==NULL || count($this->session->userdata("days"))<1){

			$number = array('01','02','03','04','05','06','07','08','09','10','11','55','12','18','44','17','19','50','22','33','99','88','66','77');

			$colors = array('Red','Blue','Green','Yellow','White','Black','Orange','Pink');

			$days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

			shuffle($number);

			shuffle($colors);

			shuffle($days);



			//$letters_random = array_rand($number,26);

			$this->session->set_userdata("number",$number);

			$this->session->set_userdata("colors",$colors);

			$this->session->set_userdata("days",$days);



		}

		//$this->print_exit($this->session->userdata("number"));

		imagettftext($merged_image, 60, 0, 590, 330, $black, $font_path, $this->session->userdata("number")[0]);

		//imagettftext($merged_image, 30, 0, 570, 120, $white, $font_path, $this->session->userdata("colors")[0]);

		imagettftext($merged_image, 30, 2, 295, 230, $blue, $font_path, $this->session->userdata("days")[0]);





		imagejpeg($merged_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-".$this->session->userdata("number")[0].".jpg", 80);

		imagedestroy($merged_image);

		$number = $this->session->userdata("number");

		$colors = $this->session->userdata("colors");

		$days = $this->session->userdata("days");



		$removed_letter = $number[0];

		array_shift($number);

		array_shift($colors);

		array_shift($days);

		$this->session->set_userdata("number",$number);

		$this->session->set_userdata("colors",$colors);

		$this->session->set_userdata("days",$days);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-".$removed_letter);

	}
	public function lucky_telugu($app)

	{
        $images = glob("container/input-images/".$app[0]->id."/*.jpg");
		$source_img = rand(1, count($images) - 2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		//$this->crop_pic2("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 138,138);

		exec('convert -size 200x200 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 100,100,1,100" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
        $this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 35, 70);



		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 0, 0, 0);
		
		$font_path = 'assets/fonts/gillsansmt.ttf';

        $number = array('01','02','03','04','05','06','07','08','09','10','11','55','12','18','44','17','19','50','22','33','99','88','66','77');
        shuffle($number);

		//$this->print_exit($this->session->userdata("number"));

		imagettftext($jpg_image, 60, 0, 590, 330, $black, $font_path, $number[0]);

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

		imagedestroy($jpg_image);

		
		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-".$source_img);

	}

	public function feeling($app)

	{

		$source_img = 100;///rand(1, count($images) - 2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		//$this->crop_pic2("container/user-photos/" . $user_id . "/" . "profile_pic.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 138,138);

		exec('convert -size 200x200 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 100,100,1,100" ""/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

		$merged_image = $this->get_merged_images_png("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "", 35, 71);

		$black = imagecolorallocate($merged_image, 255, 255, 255);

		$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';



		if($this->session->userdata("numbers")==NULL || count($this->session->userdata("numbers"))<1){

			$numbers = array('0','90','100','95','80','85','75','70','60','65','50','55');

			shuffle($numbers);

			//$letters_random = array_rand($letters,26);

			$this->session->set_userdata("numbers",$numbers);

		}

		//$this->print_exit($this->session->userdata("letters"));

		imagettftext($merged_image, 21, 0, 343, 206, $black, $font_path, $this->session->userdata("numbers")[0]."%");

		imagettftext($merged_image, 21, 0, 493, 186, $black, $font_path, $this->session->userdata("numbers")[1]."%");

		imagettftext($merged_image, 21, 0, 635, 206, $black, $font_path, $this->session->userdata("numbers")[2]."%");

		imagettftext($merged_image, 21, 0, 645, 65, $black, $font_path, $this->session->userdata("numbers")[3]."%");

		imagettftext($merged_image, 21, 0, 508, 55, $black, $font_path, $this->session->userdata("numbers")[4]."%");

		imagettftext($merged_image, 21, 0, 332, 62, $black, $font_path, $this->session->userdata("numbers")[5]."%");



		imagejpeg($merged_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-".$this->session->userdata("numbers")[0].".jpg", 80);

		imagedestroy($merged_image);

		$numbers = $this->session->userdata("numbers");

		$removed_number = $numbers[0];

		array_shift($numbers);

		$this->session->set_userdata("numbers",$numbers);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-".$removed_number);

	}

	public function profile_views($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		$source_img=1;

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}



		exec('convert -size 200x200 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 100,100,1,100" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/$source_img.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 70, 70);



		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/Kozuka.otf';

		$male=rand(75,150);

		$female=rand(130,200);

		$total=$male+$female;

		imagettftext($jpg_image, 40, 0, 455, 130, $black, $font_path, $male);

		imagettftext($jpg_image, 40, 0, 450, 250, $black, $font_path, $female);

		imagettftext($jpg_image, 50, 0, 572, 225, $black, $font_path, $total);



		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 100);

		imagedestroy($jpg_image);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



	}

	public function ipl_career_logic($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =rand(1,8);

		$matches =rand(150,200);

		$runs= rand(2500,4500);

		$fifty=rand(20,60);

		$average =rand(400,650)/10;

		$strike_rate=rand(1200,3000)/10;

		$best_score=rand(100,175);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);



		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/BebasNeueBold.ttf';



		imagettftext($jpg_image, 24, 0, 120, 86, $white, $font_path, $user_name);

		imagettftext($jpg_image, 26, 0, 280, 118, $black, $font_path, $matches);

		imagettftext($jpg_image, 26, 0, 280, 158, $black, $font_path, $runs);

		imagettftext($jpg_image, 26, 0, 280, 194, $black, $font_path, $fifty);

		imagettftext($jpg_image, 26, 0, 280, 235, $black, $font_path, $average);

		imagettftext($jpg_image, 26, 0, 280, 274, $black, $font_path, $strike_rate);

		imagettftext($jpg_image, 28, 0, 320, 316, $black, $font_path, $best_score."*");

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");

	}

	public function ipl_auction($app){

		

		$user_id = $this->session->userdata("user_id");

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		//$date=date("Y/m/d");

		//$this->print_exit($date);



		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",200);

		$this->merge_images("container/input-images/" . $app[0]->id . "/$source_img.jpg", "container/user-photos/".$user_id."/"."profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 24, 40);



		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);

		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/BebasNeueBold.ttf';

		$base_price=rand(1,2);

		$auction_price=rand(20,200)/10;

		imagettftext($jpg_image, 38, 0, 250, 135, $black, $font_path, $base_price." Cr");

		imagettftext($jpg_image, 40, 0, 250, 215, $black, $font_path, $auction_price." Cr");

		imagettftext($jpg_image, 22, 0, 42, 330, $white, $font_path, $user_name);



		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);

		imagedestroy($jpg_image);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" .""."-". $user_id . "$source_img");



	}

	public function logic_google($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		

		

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		////

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 0, 0, 0);

		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/RobotoCondensed-Regular.ttf';

		$name=explode(" ",$user_name)[0];

		imagettftext($jpg_image, 12, 0, 102, 28, $black, $font_path,'Describe '.$name.' in a word' );

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");

	}

	public function people_search($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;

		$line1=array("is a genius ","works out","is very clever","read books alot","is a phiolosopher","is a leader");

		$line2=array("knows how to cook","love puppies","is a cricket player","is a football player","is a tennis player");

		$line3=array("likes sunny leone","is romantic","is a good lover","can sing","can dance","thinks dirty thoughts");

		$line4=array("likes horror movies","likes adventure movies","is a fighter","is a good kisser");

		shuffle($line1);shuffle($line2);shuffle($line3);shuffle($line4);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		

		$jpg_image = imagecreatefromjpeg("container/input-images/".$app[0]->id."/".$source_img.".jpg");

		$black = imagecolorallocate($jpg_image, 0, 0, 0);



		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/Kozuka.otf';

		$font_path1 = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

		if(strlen($user_name)<20){

			imagettftext($jpg_image, 22, 0, 76, 152, $black, $font_path1, $user_name);

			imagettftext($jpg_image, 22, 0, 76, 202, $black, $font_path1, $user_name." ".$line1[0]);

			imagettftext($jpg_image, 22, 0, 76, 252, $black, $font_path1, $user_name." ".$line2[0]);

			imagettftext($jpg_image, 22, 0, 76, 302, $black, $font_path1, $user_name." ".$line3[0]);

			imagettftext($jpg_image, 22, 0, 76, 352, $black, $font_path1, $user_name." ".$line4[0]);

		}

		else if(strlen($user_name)>20){

			imagettftext($jpg_image, 18, 0, 76, 152, $black, $font_path1, $user_name);

			imagettftext($jpg_image, 18, 0, 76, 202, $black, $font_path1, $user_name." ".$line1[0]);

			imagettftext($jpg_image, 18, 0, 76, 252, $black, $font_path1, $user_name." ".$line2[0]);

			imagettftext($jpg_image, 18, 0, 76, 302, $black, $font_path1, $user_name." ".$line3[0]);

			imagettftext($jpg_image, 18, 0, 76, 352, $black, $font_path1, $user_name." ".$line4[0]);

		}





		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");

	}

	public function true_love_meet($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;



		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 239, 14, 63);

		$white = imagecolorallocate($jpg_image, 23, 30, 66);

		$month=array("June","July","August","September","October","November","December");

		$day=array("1st","2nd","3rd","4th","5th","6th","7th","8th","9th","10th","12th","15th","18th","21st","22nd","23rd","25th");

		shuffle($month);shuffle($day);



		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/Kozuka.otf';

		$font_path1 = 'assets/fonts/BebasNeueBold.ttf';

		imagettftext($jpg_image, 34, 0, 315, 290, $white, $font_path, $day[0]." ".$month[0]);

		imagettftext($jpg_image, 26, 0, 38, 368, $white, $font_path1, $user_name);

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");

	}

        public function fb_distance($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;



		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 239, 14, 63);

		$white = imagecolorallocate($jpg_image, 0, 13, 34);

		$distance=rand(1000,2500)/100;



		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/Kozuka.otf';

		$font_path1 = 'assets/fonts/BebasNeueBold.ttf';

		imagettftext($jpg_image, 54, 0, 342, 255, $white, $font_path1, $distance);

		imagettftext($jpg_image, 18, 0, 102, 30, $white, $font_path1, $user_name);

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");

	}
    public function god_friend($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {
            mkdir("container/user-photos/" . $user_id, 0777, true);
            	}
        $this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		$user_feed = $this->get_user_feed();
        $best_friends_keys = array();
		if (isset($user_feed->feed)) {

			$best_friends = $this->best_friends_from_feed($user_feed->feed->data);

			if (count($best_friends)>=1) {
			    if($app[0]->id==649||$app[0]->id==650){
				    $this->get_profile_pics_new($best_friends, 5);

				    $first_names = $this->get_first_names_new($best_friends, 5);

				    $best_friends_keys = array_keys($best_friends);
				    if(count($best_friends)>=5){
				        $random=rand(0,4);
				    }
				    else{
				        $random=0;
				    }
				    
				    $friend_names=array();
				    
					$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",250,250);
					$this->crop_pic("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[$random] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random]."_cropped.jpg", 250, 250);
					exec('convert -size 250x250 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_cropped.jpg" -draw "circle 125,125,1,125" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
					exec('convert -size 250x250 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/friends/' .$best_friends_keys[$random]. '_cropped.jpg" -draw "circle 125,125,1,125" "/home/witapps/public_html/container/user-photos/' . $user_id . '/friends/' .$best_friends_keys[$random]. '_cropped.png"');

					
					$user_name = $this->session->userdata('first_name');
					$source_img = 1;
            
					$font_path = 'assets/fonts/Kozuka.otf';
                
		    
            
					$this->merge_images_jpg_png("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",120,65);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random]."_cropped.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",325,68);

					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
					$black = imagecolorallocate($jpg_image, 0, 0, 0);
					$red = imagecolorallocate($jpg_image, 221, 18, 0);
		    
					//imagettftext($jpg_image, 22, 0, 45, 300, $black, $font_path, $user_name);
					imagettftext($jpg_image, 24, 0, 90, 360, $black, $font_path, $first_names[$best_friends_keys[$random]]." is your best friend in 2017");

					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
					imagedestroy($jpg_image);

					redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

				}
			    if($app[0]->id==631||$app[0]->id==648){
				    $this->get_profile_pics_new($best_friends, 5);

				    $first_names = $this->get_full_names_new($best_friends, 5);

				    $best_friends_keys = array_keys($best_friends);
				    if(count($best_friends)>=5){
				        $random=rand(0,4);
				    }
				    else{
				        $random=0;
				    }
				    
				    $friend_names=array();
				    
					$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
					$this->crop_pic("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[$random] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random]."_cropped.jpg", 234, 234);
					$user_name = $this->session->userdata('name');
					$source_img = 1;
					$budget=rand(10,90);
            
					$font_path = 'assets/fonts/BebasNeueBold.ttf';
                
		    
            
					$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
                    $this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",456,0);
					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
					$black = imagecolorallocate($jpg_image, 0, 0, 0);
					$red = imagecolorallocate($jpg_image, 221, 18, 0);
		    
					//imagettftext($jpg_image, 22, 0, 45, 300, $black, $font_path, $user_name);
					imagettftext($jpg_image, 25, 0, 480, 262, $black, $font_path, $first_names[$best_friends_keys[$random]]);
                    imagettftext($jpg_image, 40, 0, 482, 310, $black, $font_path, $budget);
                    
					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
					imagedestroy($jpg_image);

					redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

				}
			    if($app[0]->id==625){
				    $this->get_profile_pics_new($best_friends, 5);

				    $first_names = $this->get_first_names_new($best_friends, 5);

				    $best_friends_keys = array_keys($best_friends);
				    if(count($best_friends)>=5){
				        $random=rand(0,4);
				    }
				    else{
				        $random=0;
				    }
				    
				    $friend_names=array();
				    
					$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",300,300);
					$this->crop_pic("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[$random] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random]."_cropped.jpg", 300, 300);
					$user_name = $this->session->userdata('first_name');
					$source_img = 1;
            
					$font_path = 'assets/fonts/BebasNeueBold.ttf';
                
		    
            
					$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",460,70);
					$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,70);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);

					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
					$black = imagecolorallocate($jpg_image, 0, 0, 0);
					$red = imagecolorallocate($jpg_image, 221, 18, 0);
		    
					//imagettftext($jpg_image, 22, 0, 45, 300, $black, $font_path, $user_name);
					imagettftext($jpg_image, 30, 0, 160, 350, $black, $font_path, "Why Did ".$first_names[$best_friends_keys[$random]]." Killed ".$user_name."?");

					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
					imagedestroy($jpg_image);

					redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

				}
			    if($app[0]->id==622||$app[0]->id==623){
				    $this->get_profile_pics_new($best_friends, 5);

				    $first_names = $this->get_first_names_new($best_friends, 5);

				    $best_friends_keys = array_keys($best_friends);
				    if(count($best_friends)>=5){
				        $random=rand(0,4);
				    }
				    else{
				        $random=0;
				    }
				    
				    $friend_names=array();
				    
					$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
					$this->crop_pic("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[$random] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random]."_cropped.jpg", 378, 378);
					$user_name = $this->session->userdata('first_name');
					$source_img = 1;
            
					$font_path = 'assets/fonts/DobkinScript';
                
		    
            
					$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
					$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",378,0);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);

					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
					$black = imagecolorallocate($jpg_image, 30, 84, 38);
					$red = imagecolorallocate($jpg_image, 221, 18, 0);
		    
					//imagettftext($jpg_image, 22, 0, 45, 300, $black, $font_path, $user_name);
					imagettftext($jpg_image, 40, 0, 450, 260, $black, $font_path, $first_names[$best_friends_keys[$random]]);

					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
					imagedestroy($jpg_image);

					redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

				}
			    if($app[0]->id==611){
				    $this->get_profile_pics_new($best_friends, 5);

				    $first_names = $this->get_first_names_new($best_friends, 5);

				    $best_friends_keys = array_keys($best_friends);
				    if(count($best_friends)>=5){
				        $random=rand(0,4);
				    }
				    else{
				        $random=0;
				    }
				    
				    $friend_names=array();
				    
					$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",270,270);
					$this->crop_pic("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[$random] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random]."_cropped.jpg", 270, 270);
					$user_name = $this->session->userdata('first_name');
					$source_img = 1;
            
					$font_path = 'assets/fonts/Kozuka.otf';
                
		    
            
					$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,62);
					$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[$random]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",478,62);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);

					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
					$black = imagecolorallocate($jpg_image, 255, 255, 255);
					$red = imagecolorallocate($jpg_image, 221, 18, 0);
		    
					//imagettftext($jpg_image, 22, 0, 45, 300, $black, $font_path, $user_name);
					imagettftext($jpg_image, 25, 0, 330, 370, $black, $font_path, $first_names[$best_friends_keys[$random]]."!");

					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
					imagedestroy($jpg_image);

					redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

				}

				$this->get_profile_pics_new($best_friends, 1);

				$first_names = $this->get_first_names_new($best_friends, 1);

				$best_friends_keys = array_keys($best_friends);
				
				if($app[0]->id==604){
					$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",310,310);
					$this->crop_pic("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 310, 310);
					$user_name = $this->session->userdata('first_name');
					$source_img = 1;
            
					$font_path = 'assets/fonts/BebasNeueBold.ttf';
                
		    
            
					$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
					$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",438,00);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);

					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
					$black = imagecolorallocate($jpg_image, 0, 0, 0);
					$red = imagecolorallocate($jpg_image, 221, 18, 0);
		    
					imagettftext($jpg_image, 22, 0, 45, 300, $black, $font_path, $user_name);
					imagettftext($jpg_image, 22, 0, 505, 300, $black, $font_path, $first_names[$best_friends_keys[0]]);

					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
					imagedestroy($jpg_image);

					redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

				}
				if($app[0]->id==586){
					$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",280,280);
					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 280, 280);
					exec('convert -size 280x280 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/friends/' .$best_friends_keys[0]. '_cropped.jpg" -draw "circle 140,140,1,140" "/home/witapps/public_html/container/user-photos/' . $user_id . '/friends/' .$best_friends_keys[0]. '_cropped.png"');
					exec('convert -size 280x280 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_cropped.jpg" -draw "circle 140,140,1,140" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
					$user_name = $this->session->userdata('first_name');
					$source_img = 1;
            
					$font_path = 'assets/fonts/Kozuka.otf';
                
		    
            
					$this->merge_images_jpg_png("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",128,70);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0]. "_cropped.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",354,70);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);

					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
					$black = imagecolorallocate($jpg_image, 0, 0, 0);
					$red = imagecolorallocate($jpg_image, 221, 18, 0);
		    
					//imagettftext($jpg_image, 20, 0, 140, 28, $black, $font_path, $user_name.", God sent you ".$first_names[$best_friends_keys[0]]);

					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
					imagedestroy($jpg_image);

					redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

				}
		        $this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 200, 200);
                exec('convert -size 200x200 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/friends/' .$best_friends_keys[0]. '_cropped.jpg" -draw "circle 100,100,1,100" "/home/witapps/public_html/container/user-photos/' . $user_id . '/friends/' .$best_friends_keys[0]. '_cropped.png"');

			    $user_name = $this->session->userdata('first_name');
		        $source_img = 1;
            
                $font_path = 'assets/fonts/Kozuka.otf';
                
		    
            
                $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
                $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
                $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0]. "_cropped.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",455,48);

		        $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		        $black = imagecolorallocate($jpg_image, 0, 0, 0);
		        $red = imagecolorallocate($jpg_image, 221, 18, 0);
		    
		        imagettftext($jpg_image, 20, 0, 140, 28, $black, $font_path, $user_name.", God sent you ".$first_names[$best_friends_keys[0]]);

		        imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		        imagedestroy($jpg_image);

		        redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

			}
			else {
				$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');
				redirect('apps/home');
			}
		    
	}
	
	}
	public function missing_poster($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");
		$gender = $this->session->userdata("gender");
        if($gender=="male"){
            $source_img = rand(1,10);
        }
        else{
            $source_img = rand(8,18);
        }
		
		$user_id = $this->session->userdata("user_id");
		$user_name = $this->session->userdata("first_name");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");
		$jpg_image = @imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$black = imagecolorallocate($jpg_image, 0, 0, 0);
		$font_path = 'assets/fonts/Kozuka.otf';
		if(strlen(explode(" ",$user_name)[0])<=8){
		    imagettftext($jpg_image, 16, 0, 430, 258, $black, $font_path,explode(" ",$user_name)[0]);
		}
		else{
		    imagettftext($jpg_image, 15, 0, 410, 258, $black, $font_path,explode(" ",$user_name)[0]);
		}
        
        @imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		@imagedestroy($jpg_image);
		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function indian_army($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");
		$gender = $this->session->userdata("gender");
        $source_img = rand(1,5);
		
		$user_id = $this->session->userdata("user_id");
		$user_name = $this->session->userdata("name");
		$number=rand(1000,9999);

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",200,200);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",495,120);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");
		$jpg_image = @imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$black = imagecolorallocate($jpg_image, 0, 0, 0);
		$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
		if(strlen(explode(" ",$user_name)[0])<=10){
		    imagettftext($jpg_image, 18, 0, 120, 134, $black, $font_path,$user_name);
		}
		else{
		    imagettftext($jpg_image, 15, 0, 120, 134, $black, $font_path,explode(" ",$user_name)[0]);
		}
        imagettftext($jpg_image, 18, 0, 155, 172, $black, $font_path,strtoupper($gender[0]));
        imagettftext($jpg_image, 16, 0, 620, 355, $black, $font_path,$number);
        @imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		@imagedestroy($jpg_image);
		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function ceo_logic($app){
         $facebook_id = $this->session->userdata("facebook_id");
		 $images = glob("container/input-images/".$app[0]->id."/*.jpg");
		 $source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");
		$gender = $this->session->userdata("gender");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$user_name = $this->session->userdata("first_name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {
            mkdir("container/user-photos/" . $user_id, 0777, true);

			}
        $this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
        
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",230,230);
		exec('convert -size 230x230 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 115,115,1,115" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		$this->merge_images_jpg_png("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/" . $user_id . "/profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",34,84);
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$black = imagecolorallocate($jpg_image, 255, 255, 255);
		$font_path = 'assets/fonts/Kozuka.otf';
		if($gender=="male"){
			    imagettftext($jpg_image, 22, 0, 384, 110, $black, $font_path, "Mr.".$user_name);
			}
			else{
				imagettftext($jpg_image, 22, 0, 384, 110, $black, $font_path, "Ms.".$user_name);
					}
		
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");
		
	}
	public function tv_breaking_news($app){

		$user_id = $this->session->userdata("user_id");
		$gender = $this->session->userdata("gender");
		if($gender=="male"){
		    $source_img = rand(1,9);
		}
		else{
		    $source_img = rand(10,18);
		}

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function boy_girl($app){
	    
	        $facebook_id = $this->session->userdata("facebook_id");
		    $user_id = $this->session->userdata("user_id");
		    $gender = $this->session->userdata("gender");

		    if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			    mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		    }
		    if($gender=="male"){
		        $source_img = rand(1,7);
		    }
		    else{
		        $source_img = rand(8,14);
		    }
	        
            $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",200,200);
		    exec('convert -size 200x200 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 100,100,1,100" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		    $this->merge_images_jpg_png("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/" . $user_id . "/profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",536,4);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");
		
	}
	public function chess_coin($app){
	    
	        $facebook_id = $this->session->userdata("facebook_id");
		    $user_id = $this->session->userdata("user_id");
		    $gender = $this->session->userdata("gender");

		    if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			    mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		    }
		    $source_img = rand(1,6);
	        
            $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",170,170);
		    exec('convert -size 170x170 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 85,85,1,85" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		    $this->merge_images_jpg_png("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/" . $user_id . "/profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",20,62);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");
		
	}
	public function birthday_month($app){

		$images = glob("container/input-images/".$app[0]->id."/*.png");

		

		$user_id = $this->session->userdata("user_id");
		$dob = $this->session->userdata("birthday");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		if(explode("/",$dob)[0]=="01"){
		    $source_img = 1;
		}else if(explode("/",$dob)[0]=="02"){
		    $source_img = 2;
		}
		else if(explode("/",$dob)[0]=="03"){
		    $source_img = 3;
		}
		else if(explode("/",$dob)[0]=="04"){
		    $source_img = 4;
		}
		else if(explode("/",$dob)[0]=="05"){
		    $source_img = 5;
		}
		else if(explode("/",$dob)[0]=="06"){
		    $source_img = 6;
		}
		else if(explode("/",$dob)[0]=="07"){
		    $source_img = 7;
		}
		else if(explode("/",$dob)[0]=="08"){
		    $source_img = 8;
		}
		else if(explode("/",$dob)[0]=="09"){
		    $source_img = 9;
		}
		else if(explode("/",$dob)[0]=="10"){
		    $source_img = 10;
		}
		else if(explode("/",$dob)[0]=="11"){
		    $source_img = 11;
		}
		else if(explode("/",$dob)[0]=="12"){
		    $source_img = 12;
		}
		else{
		    $source_img = rand(1,12);
		}
		
        
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function book_logic($app){
	  
		$user_id = $this->session->userdata("user_id");
		$gender = $this->session->userdata("gender");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",320,320);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");
            if($app[0]->id==725){
                if($gender=="male"){
                    $source_img = rand(1,21);
                }
                else{
                    $source_img = rand(5,28);
                }
            }
            else{
                $source_img = rand(1,count($images));
            }
		    
		    $user_name = $this->session->userdata('name');
            $font_path = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",50,7);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    if(strlen($user_name)<=18){
		        imagettftext($jpg_image, 18, 12, 185, 345, $black, $font_path, $user_name);
		    }
		    else{
		        imagettftext($jpg_image, 20, 12, 204, 345, $black, $font_path, explode(" ",$user_name)[0]);
		    }
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function fb_id_card($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",180,180);
		    
            $source_img = 1;
		    $username = $this->session->userdata('name');
		    $gender = $this->session->userdata('gender');
		    $dob=$this->session->userdata('birthday');
		    if($dob==NULL){
		        $this->logout();
		    }
		    else{
		        $year=explode("/",$dob)[2];
		    $age=date("Y")-$year;
		    $random_one=rand(1000,9999);
		    $random_two=rand(1000,9999);
		    $random_three=rand(1000,9999);
            $this->merge_images("container/input-images/".$app[0]->id."/$source_img.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",30,103);
            //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $font_path = 'assets/fonts/arial.ttf';
		    $white = imagecolorallocate($jpg_image, 0,0,0);
		    imagettftext($jpg_image, 22, 0, 292, 136, $white, $font_path, $username);
		    imagettftext($jpg_image, 22, 0, 360, 180, $white, $font_path, explode("/",$dob)[1]."/".explode("/",$dob)[0]."/".explode("/",$dob)[2]);
		    imagettftext($jpg_image, 22, 0, 286, 224, $white, $font_path, $age);
		    imagettftext($jpg_image, 22, 0, 286, 270, $white, $font_path, ucfirst($gender));
		    imagettftext($jpg_image, 36, 0, 80, 360, $white, $font_path, $random_one."  ".$random_two."  ".$random_three);
		    
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

		    }
	}
	public function name_careers($app){
	    $images = glob("container/input-images/".$app[0]->id."/*.png");

		
		$user_id = $this->session->userdata("user_id");
		$gender = $this->session->userdata('gender');
            if($gender=="male"){
                $source_img = rand(1,10);
            }
            else{
                $source_img = rand(5,13);
            }

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/Calibri.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 0, 0, 0);
		    
		    imagettftext($jpg_image, 36, 0, 460, 45, $black, $font_path, $user_name);
		    imagettftext($jpg_image, 40, 0, 515, 90, $black, $font_path, "=");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function large_circle($app){
         $facebook_id = $this->session->userdata("facebook_id");
		 $images = glob("container/input-images/".$app[0]->id."/*.jpg");
		 $source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {
            mkdir("container/user-photos/" . $user_id, 0777, true);

			}
        $this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
        if($app[0]->id==680){
            $images = glob("container/input-images/".$app[0]->id."/*.png");
            $source_img = rand(1,count($images));
            
            $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",330,330);
		    exec('convert -size 330x330 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 165,165,1,165" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		    $this->merge_images_jpg_png("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/" . $user_id . "/profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",4,24);
		    $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);

		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 0, 0, 0);
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");
		
        }
        if($app[0]->id==617||$app[0]->id==618){
            $source_img=1;
            $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",220,220);
		    exec('convert -size 220x220 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 110,110,1,110" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		    $this->merge_images_jpg_png("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/" . $user_id . "/profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",264,30);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 0, 0, 0);
		    $white = imagecolorallocate($jpg_image, 255, 255, 255);
		    $font_path = 'assets/fonts/NATS.ttf';
		    $mahesh=rand(20,100);
		    $pawan=100-$mahesh;
		    
		    imagettftext($jpg_image, 45, 0, 245, 295, $black, $font_path, $mahesh."%");
		    imagettftext($jpg_image, 45, 0, 390, 295, $white, $font_path, $pawan."%");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");
		
        }
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",290,290);
		exec('convert -size 290x290 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 145,145,1,145" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		$this->merge_images_jpg_png("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/" . $user_id . "/profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",442,75);
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$black = imagecolorallocate($jpg_image, 0, 11, 30);
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");
		
	} 
	public function ivanka_logic($app){

		
		$gender = $this->session->userdata("gender");
		if($gender=="male"){
		    if($app[0]->id==624){
		        $source_img = rand(1,11);
		    }
		    else{
		        $source_img = rand(1,7);
		    }
		    
		}
		else{
		    if($app[0]->id==624){
		        $source_img = rand(2,13);
		    }
		    else{
		        $source_img = rand(3,10);
		    }
		    
		}

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}


		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",140,140);
        exec('convert -size 140x140 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 70,70,1,70" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
		$this->merge_images_jpg_png("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/" . $user_id . "/profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",582,180);
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$black = imagecolorallocate($jpg_image, 0, 11, 30);
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");
		
	}
	public function profile_pics_two($app){

		$images = glob("container/input-images/".$app[0]->id."/*.png");
		$gender = $this->session->userdata("gender");
        if($app[0]->id==700){
            if($gender=="male"){
                $source_img = rand(1,7);
            }
            else{
                $source_img = rand(6,10);
            }
        }
        else{
            $source_img = rand(1,count($images));
        }
		

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);

		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		$jpg_image = imagecreatefromjpeg("container/user-photos/".$user_id."/"."profile_pic_cropped.jpg");
		imageflip($jpg_image, IMG_FLIP_HORIZONTAL);
		imagejpeg($jpg_image, "container/user-photos/".$user_id."/"."profile_pic_cropped.jpg", 100);
        imagedestroy($jpg_image);
		$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",420,0);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function girl_or_boy($app){

		$images = glob("container/input-images/".$app[0]->id."/*.png");
		$gender = $this->session->userdata("gender");
        if($gender=="male"){
            $source_img = rand(1,5);
        }else{
            $source_img = rand(6,10);
        }
		

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function save_water($app){
           
          //  $images = glob("container/input-images/".$app[0]->id."/*.jpg");
            $user_id = $this->session->userdata("user_id");
            $access_token = $this->session->userdata("access_token");
            $picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=500&width=500" . "&access_token=" . $access_token);
            if (!file_exists("container/user-photos/" . $user_id)) {

						mkdir("container/user-photos/" . $user_id, 0777, true);

					}
            $this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
            //$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",480);


                

		//
                $picture_one = "container/user-photos/".$user_id."/"."profile_pic_cropped.jpg";
            $picture_two = "container/input-images/".$app[0]->id."/source.png";
		list($width_x, $height_x) = getimagesize($picture_one);

		list($width_y, $height_y) = getimagesize($picture_two);

		// Create new image with desired dimensions

		$image = imagecreatetruecolor($width_x, $height_x);

		// Load images and then copy to destination image

		$image_x = imagecreatefromjpeg($picture_one);

		$image_y = imagecreatefrompng($picture_two);

		imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);                     //  top, left, border,border

		imagecopy($image, $image_y, 0, 305, 0, 0, $width_y, $height_y);

		// Save the resulting image to disk (as JPEG)

		imagejpeg($image, "container/output-images/".$app[0]->id."/".$user_id.".jpg", 80);

		// Clean up

		imagedestroy($image);

		imagedestroy($image_x);

		imagedestroy($image_y);
                
                redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" .$user_id);

                //echo "<image src='".base_url()."container/output-images/".$app[0]->id."/$user_id.jpg'>";
        }
    public function gender_logic_jpg($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		

		$user_id = $this->session->userdata("user_id");
		$gender = $this->session->userdata("gender");
		if($gender=="male"){
		    $source_img = rand(1,6);
		}
		else{
		    $source_img = rand(7,12);
		}

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function dictionary_meaning($app){

		$images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images));

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);
		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
		
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}
		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
        //$this->merge_images_jpg_png("container/source.jpg","container/user-photos/".$user_id."/"."profile_pic.png","container/user-photos/".$user_id."/"."profile_pic.jpg",0,0);
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		//
		$font_path = 'assets/fonts/Kozuka.otf';
		$first_name = $this->session->userdata("first_name");
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$white = imagecolorallocate($jpg_image, 0, 0, 0);
		if(strlen($first_name)<=10){
		    imagettftext($jpg_image, 22, 0, 354, 105, $white, $font_path, $first_name);
		}
		 else{
		     imagettftext($jpg_image, 16, 0, 352, 110, $white, $font_path, explode(" ",$first_name)[0]);
		 }
		
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function fb_attraction($app){

		$images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images));

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);
		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
		
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}
		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
        //$this->merge_images_jpg_png("container/source.jpg","container/user-photos/".$user_id."/"."profile_pic.png","container/user-photos/".$user_id."/"."profile_pic.jpg",0,0);
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);
		$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png","container/user-photos/".$user_id."/"."profile_pic_circle.jpg",56,56);
        exec('convert -size 56x56 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_circle.jpg" -draw "circle 28,28,1,28" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
        
		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id."/"."profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",38,20);
		
		//
		$font_path = 'assets/fonts/Kozuka.otf';
		$first_name = $this->session->userdata("name");
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$white = imagecolorallocate($jpg_image, 41, 104, 206);
		if(strlen($first_name)<=12){
		    imagettftext($jpg_image, 15, 0, 100, 55, $white, $font_path, $first_name);
		}
		 else{
		     imagettftext($jpg_image, 15, 0, 100, 55, $white, $font_path, explode(" ",$first_name)[0]);
		 }
		
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function wikipedia_logic($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");
		$user_name = $this->session->userdata("name");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
		date_default_timezone_set("Asia/Kolkata");

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");
        $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$white = imagecolorallocate($jpg_image, 0, 0, 0);
		 
		imagettftext($jpg_image, 18, 0, 165, 70, $white, $font_path, $user_name);
		    
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function faceapp_old_logic($app){
	       
	   $this->load->view('faceapp.php');

        try {
            
        $images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images));

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);
		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

        $FaceApp = new FaceApp("container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",true);
        
        $photoCode = $FaceApp->getPhotoCode();

        $filter = 'old';

        $FaceApp->applyFilter($photoCode, $filter, false);

        if ($FaceApp->savePhoto("container/user-photos/".$user_id."/"."faceapp_pic.jpg")) {
            
            header('Content-Type: image/jpeg');

        }
        $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);
        $this->crop_pic("container/user-photos/".$user_id."/"."faceapp_pic.jpg","container/user-photos/".$user_id."/"."faceapp_pic.jpg",$pic_dimensions[0]);
		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id."/"."faceapp_pic.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",378,0);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

            
        } 
            catch (Exception $e) {
            exit($e->getMessage());
        }
	}
	public function faceapp_twin_logic($app){
	       
	   $this->load->view('faceapp.php');

        try {
            
        $images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images));

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);
		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

        $FaceApp = new FaceApp("container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",true);
        
        $photoCode = $FaceApp->getPhotoCode();

        $filter = 'fun_glasses';

        $FaceApp->applyFilter($photoCode, $filter, false);

        if ($FaceApp->savePhoto("container/user-photos/".$user_id."/"."faceapp_pic.jpg")) {
            
            header('Content-Type: image/jpeg');

        }
        $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);
        $this->crop_pic("container/user-photos/".$user_id."/"."faceapp_pic.jpg","container/user-photos/".$user_id."/"."faceapp_pic.jpg",$pic_dimensions[0]);
		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id."/"."faceapp_pic.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",418,17);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

            
        } 
            catch (Exception $e) {
            exit($e->getMessage());
        }
	}
	public function faceapp_gender_logic($app){
	       
	   $this->load->view('faceapp.php');

        try {
            
        $images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images));

		$user_id = $this->session->userdata("user_id");
		$gender = $this->session->userdata("gender");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);
		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

        $FaceApp = new FaceApp("container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",true);
        
        $photoCode = $FaceApp->getPhotoCode();
        if($gender=="male"){
            $filter = 'female';
        }
        else{
            $filter = 'male';
        }

        
        $FaceApp->applyFilter($photoCode, $filter, false);

        if ($FaceApp->savePhoto("container/user-photos/".$user_id."/"."faceapp_pic.jpg")) {
            
            header('Content-Type: image/jpeg');

        }
        $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);
        $this->crop_pic("container/user-photos/".$user_id."/"."faceapp_pic.jpg","container/user-photos/".$user_id."/"."faceapp_pic.jpg",170);
		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id."/"."faceapp_pic.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",530,21);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

            
        } 
            catch (Exception $e) {
            exit($e->getMessage());
        }
	}
	public function logic_one_large($app){

		$images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images));

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function logic_one_gender($app){

		$images = glob("container/input-images/".$app[0]->id."/*.png");

		

		$user_id = $this->session->userdata("user_id");
		$gender = $this->session->userdata("gender");
		if($gender=="male"){
		    $source_img = rand(1,11);
		}
		else{
		    $source_img = rand(12,17);
		}

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);
		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		//$this->merge_images_jpg_png("container/source.jpg","container/user-photos/".$user_id."/"."profile_pic.png","container/user-photos/".$user_id."/"."profile_pic.jpg",0,0);
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function future_life($app){

		$images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images));

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);
		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$font_path = 'assets/fonts/BebasNeueBold.ttf';
		$username = $this->session->userdata("name");
		

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");
        $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$white = imagecolorallocate($jpg_image, 255, 255, 255);
		$random=rand(1000,9999);
		imagettftext($jpg_image, 22, 0, 460, 112, $white, $font_path, explode(" ",$username)[0].$random);
		
		    
		
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function magazine_cover($app){

		$images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images));

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);
		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$font_path = 'assets/fonts/BebasNeueBold.ttf';
		$username = $this->session->userdata("name");
		

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");
        $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$white = imagecolorallocate($jpg_image, 255, 255, 255);
		if(strlen($username)<=12){
		    imagettftext($jpg_image, 22, 0, 86, 310, $white, $font_path, $username);
		}
		else{
		    imagettftext($jpg_image, 20, 0, 86, 310, $white, $font_path, explode(" ",$username)[0]);
		}
		
		    
		
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function dream_girl_tamil($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		

		$user_id = $this->session->userdata("user_id");
		$gender = $this->session->userdata("gender");
		if($gender=="male"){
		    $source_img = rand(1,5);
		}
		else{
		    $source_img = rand(6,10);
		}

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function logic_one($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function celebrity_twitter($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		

		$user_id = $this->session->userdata("user_id");
		$gender = $this->session->userdata("gender");
		if($app[0]->id==829){
    		    if($gender=="male"){
    		    $source_img = rand(4,14);
    		}
    		else{
    		    $source_img = rand(1,3);
    		}
		}
		else{
		    $source_img = rand(1,count($images)-2);
		}
		

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");
        
        $font_path = 'assets/fonts/Kozuka.otf';
		$first_name = $this->session->userdata("first_name");
		$last_name = $this->session->userdata("last_name");

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");
        $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$white = imagecolorallocate($jpg_image, 2, 196, 214);
		 
		imagettftext($jpg_image, 16, 0, 272, 116, $white, $font_path, "@".$first_name.$last_name);
		    
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	function first_movie($app){
	    $user_id = $this->session->userdata("user_id");

		$user_name = $this->session->userdata('first_name');

		$gender = $this->session->userdata("gender");

		$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");
		if($app[0]->id==835){

			$full_name = $this->session->userdata("name");

			$source_img = rand(1, 8);

			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic_png("container/user-photos/" . $user_id . "/" . "profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 160,160);


			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 378, 110);


			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 38, 53, 80);

			$font_path = 'assets/fonts/BebasNeueBold.ttf';

			imagettftext($jpg_image, 22, 0, 384, 300, $black, $font_path, explode(" ",$user_name)[0]);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}
	    if($app[0]->id==833||$app[0]->id==860||$app[0]->id==861){

			$full_name = $this->session->userdata("name");

			$source_img = rand(1, 8);

			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic_png("container/user-photos/" . $user_id . "/" . "profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 160,160);


			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 205, 110);


			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 38, 53, 80);

			$font_path = 'assets/fonts/BebasNeueBold.ttf';

			imagettftext($jpg_image, 22, 0, 208, 300, $black, $font_path, explode(" ",$user_name)[0]);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}
	}
	public function ipl_career($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =rand(1,8);

		$matches =rand(150,200);

		$runs= rand(3000,4500);

		$fifty=rand(40,80);

		$strike_rate=rand(1200,3000)/10;

		$best_score=rand(100,175);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 255, 255, 255);



		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/Kozuka.otf';



		imagettftext($jpg_image, 24, 0, 62, 92, $white, $font_path, $user_name);

		imagettftext($jpg_image, 26, 0, 250, 150, $black, $font_path, $matches);

		imagettftext($jpg_image, 26, 0, 250, 194, $black, $font_path, $runs);

		imagettftext($jpg_image, 26, 0, 250, 236, $black, $font_path, $fifty);

		imagettftext($jpg_image, 26, 0, 250, 280, $black, $font_path, $strike_rate);

		imagettftext($jpg_image, 26, 0, 250, 320, $black, $font_path, $best_score."*");

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$source_img.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id."/".""."-". $user_id . "$source_img");

	}
	public function breaking_news($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
        $font_path = 'assets/fonts/NATS.ttf';
		date_default_timezone_set("Asia/Kolkata");

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");
        $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$white = imagecolorallocate($jpg_image, 255, 255, 255);
		 
		imagettftext($jpg_image, 24, 0, 614, 345, $white, $font_path, date("H").":".date("i")." ".date("D"));
		    
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function top_photos_2017($app){
	    $photos=$this->get_user_photos_2017();
		    $likes_count=array();
		    $count=0;
		    for($i=0;$i<count($photos->feed->data);$i++){
		        if($photos->feed->data[$i]->type=="photo"){
		            if(isset($photos->feed->data[$i]->likes)){
		                $likes_count[$count]['id']=$photos->feed->data[$i]->id;
		                $likes_count[$count]['likes_count']=$photos->feed->data[$i]->likes->summary->total_count;
		                $count++;
		            }
		             
		    }
		        
		    }
		    function sortByOrder($a, $b) {
            return $a['likes_count'] - $b['likes_count'];
            }

            usort($likes_count, 'sortByOrder');
            $likes_count=array_reverse($likes_count);
            $access_token = $this->session->userdata("access_token");
            $user_id = $this->session->userdata("user_id");
            if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}
			if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

            for($i=0;$i<3;$i++){
                $picture = file_get_contents("https://graph.facebook.com/".$likes_count[$i]['id']."?fields=picture&redirect=false&height=378&width=378" . "&access_token=" . $access_token);
                $this->save_image(json_decode($picture)->picture, "container/user-photos/" . $user_id . "/".$likes_count[$i]['id'].".jpg");
                
            }
            $this->crop_pic("container/user-photos/".$user_id. "/".$likes_count[0]['id'].".jpg","container/user-photos/".$user_id. "/".$likes_count[0]['id']."_cropped.jpg",250,250);
            $this->crop_pic("container/user-photos/".$user_id. "/".$likes_count[1]['id'].".jpg","container/user-photos/".$user_id. "/".$likes_count[1]['id']."_cropped.jpg",250,250);
            $this->crop_pic("container/user-photos/".$user_id. "/".$likes_count[2]['id'].".jpg","container/user-photos/".$user_id. "/".$likes_count[2]['id']."_cropped.jpg",250,250);
		    $source_img=1;
		    $this->merge_images("container/input-images/".$app[0]->id."/$source_img.jpg","container/user-photos/".$user_id. "/".$likes_count[0]['id']."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."-".$user_id . "$source_img.jpg",4,52);
		    $this->merge_images("container/output-images/" . $app[0]->id . "/" .""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id. "/".$likes_count[1]['id']."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."-".$user_id . "$source_img.jpg",254,52);
            $this->merge_images("container/output-images/" . $app[0]->id . "/" .""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id. "/".$likes_count[2]['id']."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."-".$user_id . "$source_img.jpg",502,52);
            
            $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 30, 57, 102);
		    $font_path = 'assets/fonts/Calibri.ttf';
		    imagettftext($jpg_image, 35, 0, 35, 350, $black, $font_path, $likes_count[0]['likes_count']);
		    imagettftext($jpg_image, 35, 0, 285, 350, $black, $font_path, $likes_count[1]['likes_count']);
		    imagettftext($jpg_image, 35, 0, 540, 350, $black, $font_path, $likes_count[2]['likes_count']);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);
            redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");
		    
	}
	public function flight_ticket($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");
		$username = $this->session->userdata("name");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$jpg_image = @imagecreatefromjpeg("container/input-images/".$app[0]->id."/1.jpg");
		$black = imagecolorallocate($jpg_image, 0, 0, 0);
		$font_path = 'assets/fonts/Calibri.ttf';
		$font_path1 = 'assets/fonts/lsansuni.ttf';
		$date=array("1st","2nd","3rd","4th","5th","6th","7th","8th","9th","10th","11th","12th","13th","14th","15th","20th","21st","22nd","23rd","24th","25th","27th","28th");
		$month=array("JAN","FEB","MAR","APR","MAY","JAN","FEB");
		$country=array("Dubai","Hawai","Singapore","Bangkok","Goa","London","Paris","Malaysia","New York","Sydney");
		$seat=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V");
		$number1=rand(1,9);
		$number2=rand(0,9);
		shuffle($date);shuffle($month);shuffle($country);
		
		imagettftext($jpg_image, 20, 0, 105, 160, $black, $font_path, $username);
		imagettftext($jpg_image, 22, 0, 105, 225, $black, $font_path, $date[0]." ".$month[0]);
		imagettftext($jpg_image, 42, 0, 375, 200, $black, $font_path1, $country[0]);
		imagettftext($jpg_image, 18, 0, 380, 275, $black, $font_path1, $number1." ".$number2." ".$seat[0]);
		
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);
						
		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function large_square_default($app){
	    $images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images));

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");

		    $source_img = rand(1,count($images));
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function ipl_team_budget($app){
	    $images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images));

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");

		    $source_img = rand(1,count($images));
		    $user_name = $this->session->userdata('name');
            $font_path = 'assets/fonts/Calibri.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",400,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $white = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagettftext($jpg_image, 18, 0, 95, 100, $white, $font_path, $user_name);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
    public function large_square($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		if($app[0]->id==679||$app[0]->id==681){
		    $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",300,300);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");

		    $source_img = rand(1,count($images));
		    $user_name = $this->session->userdata('first_name');
          
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",480,40);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

		}
		if($app[0]->id==675){
		    $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");
            $gender = $this->session->userdata('gender');
            if($gender=="male"){
                $source_img = rand(1,10);
            }
            else{
                $source_img = rand(11,20);
            }
		    
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

		}
		if($app[0]->id==674||$app[0]->id==708){
		    $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",300,300);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");

		    $source_img = rand(1,count($images));
		    $user_name = $this->session->userdata('first_name');
          
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

		}
		if($app[0]->id==671){
		    $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");
            $gender = $this->session->userdata('gender');
            if($gender=="male"){
                $source_img = rand(1,12);
            }
            else{
                $source_img = rand(3,14);
            }
		    
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

		}
		if($app[0]->id==656||$app[0]->id==664||$app[0]->id==668){
		    $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");

		    $source_img = rand(1,count($images));
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

		}
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);
		
		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function ipl_team($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_feed = $this->get_user_feed_2017();

		if (isset($user_feed->feed)) {
		    
			$best_friends = $this->best_friends_from_feed($user_feed->feed->data);

			if (count($best_friends)>=11) {

				$this->get_profile_pics_old($best_friends, 10);

				$first_names = $this->get_first_names_old($best_friends, 10);

				$best_friends_keys = array_keys($best_friends);

				$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 120, 120);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 100, 100);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 100, 100);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 100, 100);
                $this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[4] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[4]."_cropped.jpg", 100, 100);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[5] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[5]."_cropped.jpg", 100, 100);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[6] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[6]."_cropped.jpg", 100, 100);
                $this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[7] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[7]."_cropped.jpg", 100, 100);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[8] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[8]."_cropped.jpg", 100, 100);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[9] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[9]."_cropped.jpg", 100, 100);
				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[3] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg", 100, 100);



				$this->merge_images("container/input-images/" . $app[0]->id . "/$source_img.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 42, 108);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 204, 50);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 306, 50);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 408, 50);
                $this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 510, 50);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[4]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 610, 50);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[5]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 204, 185);
        	    $this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[6]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 306, 185);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[7]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 408, 185);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[8]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 510, 185);
				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[9]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 610, 185);




				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

				$white = imagecolorallocate($jpg_image, 255, 255, 255);

				$black = imagecolorallocate($jpg_image, 44, 44, 44);



				$user_name = $this->session->userdata('first_name');

				$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

				imagettftext($jpg_image, 12, 0, 60, 260, $white, $font_path, $user_name."(c)");


				imagettftext($jpg_image, 11, 0, 226, 172, $white, $font_path, $first_names[$best_friends_keys[0]]);

				imagettftext($jpg_image, 11, 0, 326, 172, $white, $font_path, $first_names[$best_friends_keys[1]]);

				imagettftext($jpg_image, 11, 0, 426, 172, $white, $font_path, $first_names[$best_friends_keys[2]]);
				
				imagettftext($jpg_image, 11, 0, 526, 172, $white, $font_path, $first_names[$best_friends_keys[3]]);

				imagettftext($jpg_image, 11, 0, 626, 172, $white, $font_path, $first_names[$best_friends_keys[4]]);

				imagettftext($jpg_image, 11, 0, 226, 304, $white, $font_path, $first_names[$best_friends_keys[5]]);
				
				imagettftext($jpg_image, 11, 0, 326, 304, $white, $font_path, $first_names[$best_friends_keys[6]]);

				imagettftext($jpg_image, 11, 0, 426, 304, $white, $font_path, $first_names[$best_friends_keys[7]]);

				imagettftext($jpg_image, 11, 0, 526, 304, $white, $font_path, $first_names[$best_friends_keys[8]]);
				
				imagettftext($jpg_image, 11, 0, 626, 304, $white, $font_path, $first_names[$best_friends_keys[9]]);





				imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

				imagedestroy($jpg_image);



				redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);

			} else {

				$this->session->set_flashdata('no_photos', 'Sorry insufficient data, we are unable to process this app try other apps');

				redirect('apps/home');

			}

		}else {

			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

			redirect('apps/home');

		}

	}
	public function friends_gang($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_feed = $this->get_user_feed();

		if (isset($user_feed->feed)) {

			//$best=$this->best_friends_from_feed($user_feed->feed->data);



			$best_friends = $this->best_friends_from_feed($user_feed->feed->data);

			if (count($best_friends)) {

				$this->get_profile_pics_old($best_friends, 10);

				$first_names = $this->get_first_names_old($best_friends, 10);

				$best_friends_keys = array_keys($best_friends);

                                //shuffle($best_friends_keys);

				if($app[0]->id==481 || $app[0]->id==482){
					$this->get_profile_pics_old($best_friends, 1);
					$first_names = $this->get_first_names_old($best_friends, 1);
					$best_friends_keys = array_keys($best_friends);
					$source_img=rand(1,9);
					if (!file_exists("container/output-images/" . $app[0]->id."/".date("m")."/".date("d")."/".date("h"))) {
						mkdir("container/output-images/" . $app[0]->id."/".date("m")."/".date("d")."/".date("h"), 0777, true);
					}

					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 140, 140);
					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 140, 140);
					
					exec('convert -size 140x140 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 70,70,1,70" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
					exec('convert -size 140x140 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/friends/' . $best_friends_keys[0].'_cropped.jpg" -draw "circle 70,70,1,70" "/home/witapps/public_html/container/user-photos/' . $user_id . '/friends/' . $best_friends_keys[0].'_cropped.png"');
					$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/$source_img.jpg","container/user-photos/" . $user_id . "/profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg", 70, 45);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.png","container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg", 572, 226);
					
					$jpg_image = @imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg");
					$black = imagecolorallocate($jpg_image, 0, 0, 0);
					$white = imagecolorallocate($jpg_image, 0, 11, 30);
					//$this->print_exit($auction_price);
					$user_name = $this->session->userdata('first_name');
					$font_path = 'assets/fonts/ScriptMTBold.ttf';
					$font_path1 = 'assets/fonts/Kozuka.otf';

						//$this->print_exit($first_names);
						imagettftext($jpg_image, 28, 0, 206, 335, $black, $font_path, "-".$first_names[$best_friends_keys[0]]);
						imagettftext($jpg_image, 22, 0, 238, 98, $black, $font_path, "Dear ".$user_name.",");






					@imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg", 100);
					@imagedestroy($jpg_image);

					redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" .date("m")."-".date("d")."-".date("h")."-". $user_id . "$source_img");

				}
				if($app[0]->id==472){
					$this->get_profile_pics($best_friends, 1);
					$first_names = $this->get_full_names($best_friends, 1);
					$best_friends_keys = array_keys($best_friends);
					$source_img=rand(1,13);
					if (!file_exists("container/output-images/" . $app[0]->id."/".date("m")."/".date("d")."/".date("h"))) {
						mkdir("container/output-images/" . $app[0]->id."/".date("m")."/".date("d")."/".date("h"), 0777, true);
					}

					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 135, 135);
					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 135, 135);
					
					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped1.jpg", 46, 46);
					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped1.jpg", 46, 46);
	
					
					$this->merge_images("container/input-images/" . $app[0]->id . "/$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg", 33, 192);
						//$this->merge_images("container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg", 30, 100);
					$this->merge_images("container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg", 33, 50);
                    exec('convert -size 46x46 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped1.jpg" -draw "circle 23,23,1,23" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
					exec('convert -size 46x46 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/friends/' . $best_friends_keys[0].'_cropped1.jpg" -draw "circle 23,23,1,23" "/home/witapps/public_html/container/user-photos/' . $user_id . '/friends/' . $best_friends_keys[0].'_cropped.png"');
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg", 675, 168);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.png","container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg", 232, 50);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.png","container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg", 235, 110);
					$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.png","container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg", 232, 235);
					
					$jpg_image = @imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg");
					$black = imagecolorallocate($jpg_image, 0, 0, 0);
					$white = imagecolorallocate($jpg_image, 0, 11, 30);
					//$this->print_exit($auction_price);
					$user_name = $this->session->userdata('first_name');
					$font_path = 'assets/fonts/BebasNeueBold.ttf';
					$font_path1 = 'assets/fonts/Kozuka.otf';

						//$this->print_exit($first_names);
						imagettftext($jpg_image, 24, 0, 140, 366, $white, $font_path, $first_names[$best_friends_keys[0]]." is your no.1 yaari");
						imagettftext($jpg_image, 14, 0, 282, 68, $black, $font_path1, explode(" ",$first_names[$best_friends_keys[0]])[0]);
						imagettftext($jpg_image, 13, 0, 304, 122, $black, $font_path1, explode(" ",$first_names[$best_friends_keys[0]])[0]);
						imagettftext($jpg_image, 14, 0, 304, 248, $black, $font_path1, explode(" ",$first_names[$best_friends_keys[0]])[0]);
			





					@imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .date("m")."/".date("d")."/".date("h")."/". date("m")."-".date("d")."-".date("h")."-".$user_id . "$source_img.jpg", 100);
					@imagedestroy($jpg_image);

					redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" .date("m")."-".date("d")."-".date("h")."-". $user_id . "$source_img");

				}
				if($app[0]->id==446){

					$this->get_profile_pics_new($best_friends, 3);

					$first_names = $this->get_full_names_new($best_friends, 3);

					$best_friends_keys = array_keys($best_friends);

					$source_img=1;

					if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

						mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

					}



					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 120, 120);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 90, 90);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 90, 90);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 90, 90);

					//$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[3] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg", 90, 90);



						$this->merge_images("container/input-images/" . $app[0]->id . "/$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 165, 90);

						//$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 30, 100);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 165, 189);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 165, 285);

						//$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 580, 150);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .$user_id . "$source_img.jpg", 25, 45);





					$jpg_image = @imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .$user_id . "$source_img.jpg");

					$black = imagecolorallocate($jpg_image, 44, 44, 44);

					$white = imagecolorallocate($jpg_image, 255, 255, 255);

					//$this->print_exit($auction_price);

					$user_name = $this->session->userdata('name');

					$font_path = 'assets/fonts/BebasNeueBold.ttf';



						//$this->print_exit($first_names);

						imagettftext($jpg_image, 18, 0, 40, 190, $black, $font_path, wordwrap($user_name,10,"\n"));

						imagettftext($jpg_image, 30, 0, 270, 145, $white, $font_path, $first_names[$best_friends_keys[0]]);

						imagettftext($jpg_image, 30, 0, 270, 245, $white, $font_path, $first_names[$best_friends_keys[1]]);

						imagettftext($jpg_image, 30, 0, 270, 345, $white, $font_path, $first_names[$best_friends_keys[2]]);











					@imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .$user_id . "$source_img.jpg", 100);

					@imagedestroy($jpg_image);



					redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/". $user_id . "$source_img");



				}

				

				if($app[0]->id==432){

				$this->get_profile_pics_old($best_friends, 4);

				$first_names = $this->get_first_names_old($best_friends, 4);

				$best_friends_keys = array_keys($best_friends);

				$this->has_access_token($app);

				$this->refresh_access_token();

					$gender = $this->session->userdata("gender");

					if($gender=="male"){

						$source_img=1;

					}

					else{

						$source_img=2;

					}





					if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

						mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

					}



					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped1.jpg", 160, 160);

					$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 130, 130);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 130, 130);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 130, 130);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 130, 130);

					$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[3] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg", 130, 130);

					if($source_img==1){

						$this->merge_images("container/input-images/" . $app[0]->id . "/$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 220, 170);

						//$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 30, 100);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 390, 170);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 560, 170);

						//$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[3]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 580, 150);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 45, 170);



					}

					else{

						$this->merge_images("container/input-images/" . $app[0]->id . "/$source_img.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 140, 175);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 312, 175);

						$this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 478, 175);



					}



					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");

					$black = imagecolorallocate($jpg_image, 206, 14, 35);

					$white = imagecolorallocate($jpg_image, 255, 255, 255);

					//$this->print_exit($auction_price);

					$user_name = $this->session->userdata('first_name');

					$font_path = 'assets/fonts/BebasNeueBold.ttf';

					if($source_img==1){

						//$this->print_exit($first_names);

						imagettftext($jpg_image, 20, 0, 48, 324, $white, $font_path, $user_name);

						imagettftext($jpg_image, 20, 0, 228, 324, $white, $font_path, $first_names[$best_friends_keys[0]]);

						imagettftext($jpg_image, 20, 0, 393, 324, $white, $font_path, $first_names[$best_friends_keys[1]]);

						imagettftext($jpg_image, 20, 0, 563, 324, $white, $font_path, $first_names[$best_friends_keys[2]]);





					}

					else if($source_img==2){

						imagettftext($jpg_image, 20, 0, 148, 328, $white, $font_path, $user_name);

						imagettftext($jpg_image, 20, 0, 320, 328, $white, $font_path, $first_names[$best_friends_keys[1]]);

						imagettftext($jpg_image, 20, 0, 485, 328, $white, $font_path, $first_names[$best_friends_keys[0]]);



					}



					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);

					imagedestroy($jpg_image);



					redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" .""."-". $user_id . "$source_img");



				}

					



				$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 178, 178);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[0] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg", 178, 178);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[1] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg", 178, 178);

				$this->crop_pic2("container/user-photos/" . $user_id . "/friends/" .$best_friends_keys[2] .".jpg", "container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg", 178, 178);



				$this->merge_images("container/input-images/" . $app[0]->id . "/1.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 22, 82);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[0]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 200, 82);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[1]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 380, 82);

				$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/user-photos/" . $user_id . "/friends/" . $best_friends_keys[2]."_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 555, 82);

				$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/200.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 22, 0);



				$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");

				$white = imagecolorallocate($jpg_image, 0, 0, 0);

				$black = imagecolorallocate($jpg_image, 44, 44, 44);



				$user_name = $this->session->userdata('first_name');

				$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

				imagettftext($jpg_image, 16, 0, 60, 60, $black, $font_path, $user_name);

				//imagettftext($jpg_image, 22, 0, 500, 300, $white, $font_path, $first_names[0]);

				//imagettftext($jpg_image, 42, 0, 300, 170, $black, $font_path, $best_friends[$best_friends_keys[0]]);

				imagettftext($jpg_image, 16, 0, 240, 60, $black, $font_path, $first_names[$best_friends_keys[0]]);

				imagettftext($jpg_image, 16, 0, 410, 60, $black, $font_path, $first_names[$best_friends_keys[1]]);

				imagettftext($jpg_image, 16, 0, 580, 60, $black, $font_path, $first_names[$best_friends_keys[2]]);



				imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);

				imagedestroy($jpg_image);



				redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);

			} else {

				$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

				redirect('apps/home');

			}

		}else {

			$this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

			redirect('apps/home');

		}

	}
	public function signature_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);
		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);

                    if (!file_exists("container/user-photos/" . $user_id)) {

						mkdir("container/user-photos/" . $user_id, 0777, true);

					}
		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		$source_img = rand(1,10);
        $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/source.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		$black = imagecolorallocate($jpg_image, 255, 255, 255);
		$font_path = 'assets/fonts/breetty.otf';
		if(strlen($user_name)<=15){
		    imagettftext($jpg_image, 35, 30, 390, 300, $black, $font_path, $user_name);
		}
		else{
		    imagettftext($jpg_image, 26, 30, 390, 300, $black, $font_path, $user_name);
		}
		
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);
        redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");
	}
	public function logic_year_change($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);

		    $source_img = rand(1,10);
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",378,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function kaala_font_style($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",350,350);
		    
            $source_img = 1;
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/RhinosRocks.ttf';
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 216, 23, 2);
		    if(strlen($user_name)<=6){
		        imagettftext($jpg_image, 80, 0, 348, 205, $black, $font_path, strtoupper(explode(" ",$user_name)[0]));
		    }
		    if(strlen($user_name)>6 && strlen($user_name)<10){
		        imagettftext($jpg_image, 70, 0, 314, 205, $black, $font_path, strtoupper(explode(" ",$user_name)[0]));
		    }
		    if(strlen($user_name)>=10){
		        imagettftext($jpg_image, 65, 0, 304, 205, $black, $font_path, strtoupper(explode(" ",$user_name)[0]));
		    }
		  
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function life_look_like($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");

		    $source_img = rand(1,count($images));
            
		    $user_name = $this->session->userdata('first_name');

            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 0, 0, 0);
		    $font_path ="assets/fonts/TimmanaRegular.ttf";
		    if(strlen(explode(" ",$user_name)[0])<=10){
		        imagettftext($jpg_image, 30, 0, 20, 35, $black, $font_path, explode(" ",$user_name)[0]." in 2017");
		        imagettftext($jpg_image, 30, 0, 400, 35, $black, $font_path, explode(" ",$user_name)[0]." in 2018");
		    }
		    else{
		        imagettftext($jpg_image, 24, 0, 20, 35, $black, $font_path, explode(" ",$user_name)[0]." in 2017");
		        imagettftext($jpg_image, 24, 0, 400, 35, $black, $font_path, explode(" ",$user_name)[0]." in 2018");
		    }
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function hello_font_style($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		$source_img = 1;
		    $firstname = $this->session->userdata('first_name');
		    //$lastname = $this->session->userdata('last_name');
            
            //$font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    $font_path ="assets/fonts/arial.ttf";
		    list($img_width, $img_height,,) = getimagesize("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
                
                    // find font-size for $txt_width = 80% of $img_width...
                    $font_size = 1; 
                    $txt_max_width = intval(0.55 * ($img_width/2));    

                    do {        
                        $font_size++;
                        $p = imagettfbbox($font_size, 0, $font_path, $firstname);
                        $txt_width = $p[2] - $p[0];
                     // $txt_height=$p[1]-$p[7]; // just in case you need it
                    } while ($txt_width <= $txt_max_width);
                    
                    // now center the text
                    if(strlen($firstname)<=3){
                        $y = $img_height * 0.5;
                    }
                    if(strlen($firstname)>3){
                        $y = $img_height * 0.42;
                    }
                     // baseline of text at 90% of $img_height
                    $x = ($img_width - $txt_width) / 2;
                    

                    imagettftext($jpg_image, $font_size, 8, 360, $y, $black, $font_path, $firstname);
                    
		  
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function love_story_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");

		    $source_img = rand(1,count($images));
            
		    $user_name = $this->session->userdata('first_name');

            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",370,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function god_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",310,310);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");

		    $source_img = rand(1,count($images));
            
		    $user_name = $this->session->userdata('first_name');

            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",438,68);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function tattoo_on_hand($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",234,234);
		    $images = glob("container/input-images/".$app[0]->id."/*.png");
            
		    $source_img = rand(1,count($images));
		    //$source_img = rand(1,17);
		    $first_name = $this->session->userdata('first_name');
		    
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",8,120);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $font_path = 'assets/fonts/ScriptMTBold.ttf';
		    $black = imagecolorallocate($jpg_image, 18,32,54);
		    $purple = imagecolorallocate($jpg_image, 169, 4, 219);
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function be_like_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		$first_name = $this->session->userdata('first_name');
		  $gender = $this->session->userdata('gender');
		  if($gender=="male"){
		      $source_img = rand(1,11);
		       }
		  else{
		      $source_img = rand(12,22);
		     }
           $this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 378, 378);
		   $this->merge_images("container/input-images/" . $app[0]->id . "/source.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 0, 0);
           $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/$source_img.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 0, 0);
		   $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");
		   $black = imagecolorallocate($jpg_image, 0, 0, 0);
		   $font_path = 'assets/fonts/Calibri.ttf';
		   imagettftext($jpg_image, 35, 0, 180, 365, $black, $font_path, "BE LIKE ".strtoupper($first_name));
		   imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);
		   imagedestroy($jpg_image);

			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);
			
	}
	public function true_meaning($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		$first_name = $this->session->userdata('first_name');
		            
                    $source_img = rand(1,14);
                    
		            $this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 240, 240);
					$this->merge_images("container/input-images/" . $app[0]->id . "/source.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 30, 100);
                    $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/$source_img.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 0, 0);
					$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");
					$white = imagecolorallocate($jpg_image, 244, 235, 66);
					$black = imagecolorallocate($jpg_image, 255, 255, 255);
					$font_path = 'assets/fonts/BebasNeueBold.ttf';
					$font_path1 = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
					if($app[0]->id==613){
					    imagettftext($jpg_image, 24, 0, 345, 170, $black, $font_path1, $first_name.", Means:");
					}
					else{
					    imagettftext($jpg_image, 24, 0, 345, 136, $black, $font_path1, $first_name.", Means:");
					}
					
					imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);
					imagedestroy($jpg_image);

					redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);
			
	}
	public function relationship_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $gender = $this->session->userdata("gender");
		    if($gender=="male"){
		        $source_img = rand(1,12);
		    }
		    else{
		        $source_img = rand(4,12);
		    }
            
		    $first_name = $this->session->userdata('first_name');
		    
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $font_path = 'assets/fonts/ScriptMTBold.ttf';
		    $black = imagecolorallocate($jpg_image, 18,32,54);
		    $purple = imagecolorallocate($jpg_image, 169, 4, 219);
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function profile_photo_reveal($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $gender = $this->session->userdata("gender");
		    if($gender=="male"){
		        $source_img = rand(1,7);
		    }
		    else{
		        $source_img = rand(7,13);
		    }
            
		    $first_name = $this->session->userdata('first_name');
		    
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",370,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $font_path = 'assets/fonts/ScriptMTBold.ttf';
		    $black = imagecolorallocate($jpg_image, 18,32,54);
		    $purple = imagecolorallocate($jpg_image, 169, 4, 219);
		    if(strlen($first_name)<=8){
		        imagettftext($jpg_image, 45, 0, 55, 125, $black, $font_path, $first_name);
		    }
		    else{
		        imagettftext($jpg_image, 32, 0, 45, 125, $black, $font_path, $first_name);
		    }
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function animal_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		$source_img = rand(1,14);
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",10,70);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function fb_rank_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",230,230);
		    
            $source_img = 1;
		    $first_name = $this->session->userdata('first_name');
		    $messages=rand(4000,8000);
		    $friend_req=rand(300,1200);
		    $notification=rand(120,300);
		    $rank=rand(1,10000);

            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",26,96);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $font_path = 'assets/fonts/BebasNeueBold.ttf';
		    $white = imagecolorallocate($jpg_image, 255,255,255);
		    imagettftext($jpg_image, 22, 0, 45, 358, $white, $font_path, $first_name);
		    imagettftext($jpg_image, 26, 0, 682, 120, $white, $font_path, $messages);
		    imagettftext($jpg_image, 28, 0, 688, 225, $white, $font_path, $friend_req);
		    imagettftext($jpg_image, 30, 0, 688, 325, $white, $font_path, $notification);
		    if(strlen($rank)<1000){
		        imagettftext($jpg_image, 25, 0, 250, 325, $white, $font_path, "#".$rank);
		    }
		    else{
		        imagettftext($jpg_image, 20, 0, 232, 325, $white, $font_path, "#".$rank);
		    }
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function two_names_meaning($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images));

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
	
		
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		$first_name = $this->session->userdata("first_name");
		$last_name = $this->session->userdata("last_name");
		$this->merge_images("container/input-images/" . $app[0]->id . "/source.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 185, 0);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/input-images/" . $app[0]->id . "/$source_img.png","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 0, 0);
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg");
		$black = imagecolorallocate($jpg_image, 255, 255, 255);
		$font_path = 'assets/fonts/LiarScript.ttf';
		
		if(strlen(explode(" ",$first_name)[0])<=5){
		    imagettftext($jpg_image, 38, 8, 34, 190, $black, $font_path, explode(" ",$first_name)[0]);
		}
		else if(strlen(explode(" ",$first_name)[0])>=6 && strlen(explode(" ",$first_name)[0])<=10){
		    imagettftext($jpg_image, 34, 8, 34, 190, $black, $font_path, explode(" ",$first_name)[0]);
		}
		else{
		    imagettftext($jpg_image, 25, 10, 30, 190, $black, $font_path, explode(" ",$first_name)[0]);
		}
		if(strlen(explode(" ",$last_name)[0])<=5){
		    imagettftext($jpg_image, 36, 8, 530, 132, $black, $font_path, explode(" ",$last_name)[0]);
		}
		else if(strlen(explode(" ",$last_name)[0])>=6 && strlen(explode(" ",$last_name)[0])<=10){
		    imagettftext($jpg_image, 34, 8, 520, 136, $black, $font_path, explode(" ",$last_name)[0]);
		}
		else{
		    imagettftext($jpg_image, 30, 8, 500, 136, $black, $font_path, explode(" ",$last_name)[0]);
		}
		
		
		
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 100);
	    imagedestroy($jpg_image);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id);
			
	}
	public function name_meaning($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.png");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
	
		
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		$first_name = $this->session->userdata("first_name");
		$this->crop_pic2("container/user-photos/" . $user_id . "/profile_pic.jpg", "container/user-photos/" . $user_id . "/profile_pic_cropped.jpg", 378, 378);
		$this->merge_images("container/input-images/" . $app[0]->id . "/source.jpg","container/user-photos/" . $user_id . "/profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" . $user_id . "$source_img.jpg", 0, 0);
        $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" . $user_id.$source_img . ".jpg","container/input-images/" . $app[0]->id . "/$source_img.png","container/output-images/" . $app[0]->id . "/" . $user_id . "$source_img.jpg", 0, 0);
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "$source_img.jpg");
		$black = imagecolorallocate($jpg_image, 0, 0, 0);
		$font_path = 'assets/fonts/LiarScript.ttf';
		
		if(strlen(explode(" ",$first_name)[0])<=5){
		    imagettftext($jpg_image, 40, 12, 350, 150, $black, $font_path, explode(" ",$first_name)[0]);
		}
		else if(strlen(explode(" ",$first_name)[0])>=6 && strlen(explode(" ",$first_name)[0])<=10){
		    imagettftext($jpg_image, 34, 12, 330, 152, $black, $font_path, explode(" ",$first_name)[0]);
		}
		else{
		    imagettftext($jpg_image, 32, 12, 320, 152, $black, $font_path, explode(" ",$first_name)[0]);
		}
		
		
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "$source_img.jpg", 100);
	    imagedestroy($jpg_image);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id.$source_img);
			
	}
	public function hell_heaven_logic($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =rand(1,2);
        if($source_img==1){
            $sins =rand(200,500);

		    $good_deeds=rand(2000,5000);

		    $kindness=rand(150,300);

        }
        else{
            $sins =rand(2000,5000);

		    $good_deeds=rand(200,500);

		    $kindness=rand(50,80);
        }
		$luck=rand(1,6);
        
		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$luck");

		////

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg");

		$black = imagecolorallocate($jpg_image, 255, 255, 255);

		$white = imagecolorallocate($jpg_image, 255, 255, 255);



		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/Calibri.ttf';

		imagettftext($jpg_image, 20, 0, 180, 74, $white, $font_path, $user_name);

		imagettftext($jpg_image, 24, 0, 180, 144, $white, $font_path, number_format($sins));

		imagettftext($jpg_image, 24, 0, 180, 212, $white, $font_path, number_format($good_deeds));

		imagettftext($jpg_image, 24, 0, 180, 276, $white, $font_path, $kindness."%");







		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$luck");

	}
	public function last_name_meaning($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);

            if($app[0]->id==596||$app[0]->id==598||$app[0]->id==601){
                $source_img = rand(1,12);
            }else{
                $source_img = 1;
            }
		    $last_name = $this->session->userdata('last_name');
		    $list=array("Brave","Delightful","Leader","Calm","Eager","Faithful","Gentle","Happy","Adorable","Beautiful","Magnificent","Kind","Proud","Obedient");
		    shuffle($list);

            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $font_path = 'assets/fonts/ScriptMTBold.ttf';
		    $black = imagecolorallocate($jpg_image, 18,32,54);
		    $purple = imagecolorallocate($jpg_image, 169, 4, 219);
		    if(strlen($last_name)<=12){
		        imagettftext($jpg_image, 40, 0, 414, 135, $black, $font_path, $last_name);
		    }
		    else{
		        imagettftext($jpg_image, 32, 0, 410, 135, $black, $font_path, $last_name);
		    }
		    if($app[0]->id==595){
		        imagettftext($jpg_image, 45, 0, 412, 290, $purple, $font_path, $list[0]);
		    }
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function strong_weak_telugu($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		$source_img = rand(1,10);
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            exec('convert -size 272x272 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic_cropped.jpg" -draw "circle 136,136,1,136" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');
            //$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $this->merge_images_jpg_png("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/" . $user_id . "/profile_pic_circle.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",230,96);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 0, 11, 30);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function similarity_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
            $source_img = rand(1,20);
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",190,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function tamil_celebrity($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",324,324);
		    
            $gender = $this->session->userdata('gender');
		    $user_name = $this->session->userdata('first_name');
		    if($gender=="male"){
		        $source_img = rand(1,10);
		    }
		    else{
		        $source_img =rand(11,16);
		    }
		    
           
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",220,0);
            $this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $white = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function telugu_celebrity($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);

            $gender = $this->session->userdata('gender');
		    $user_name = $this->session->userdata('first_name');
		    if($gender=="male"){
		        $source_img = rand(9,18);
		    }
		    else{
		        $source_img =rand(1,8);
		    }
		    
           
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",220,0);
            $this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $white = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function kannada_celebrity($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",324,324);
		    
            $gender = $this->session->userdata('gender');
		    $user_name = $this->session->userdata('first_name');
		    if($gender=="male"){
		        $source_img = rand(1,8);
		    }
		    else{
		        $source_img =rand(9,13);
		    }
		    
           
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",220,0);
            $this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",-80,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $white = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function lover_first_letter($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    
            $source_img = rand(1,26);
		    $user_name = $this->session->userdata('first_name');

            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function profile_photo_age($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    
            $source_img = 1;
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 2, 40, 102);
		    $act=rand(20,30);
		    $think=rand(25,40);
		    $looks=rand(20,28);
		    imagettftext($jpg_image, 40, 0, 445, 96, $black, $font_path, $act);
		    imagettftext($jpg_image, 40, 0, 445, 206, $black, $font_path, $think);
		    imagettftext($jpg_image, 40, 0, 445, 300, $black, $font_path, $looks);
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function animal_anger_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",324,324);
		    
            
		    $user_name = $this->session->userdata('first_name');
		    $source_img = rand(1,10);
           
		    
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",220,0);
            $this->merge_images("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $white = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function cricket_stars($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $source_img = rand(1,25);
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",175,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function kannada_stars_combo($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $source_img = rand(1,12);
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",175,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function teddy_bear_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",300,300);
		    $source_img = rand(1,10);
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",46,18);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function reasons_to_marry($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $gender = $this->session->userdata('gender');
			if($gender=="male"){
				$source_img = rand(1,10);
			}
			else{
				$source_img = rand(11,16);
			}
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function stars_combo_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $gender = $this->session->userdata('gender');
			if($gender=="male"){
				$source_img = rand(1,16);
			}
			else{
				$source_img = rand(17,20);
			}
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",175,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function celebrity_marry($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    $gender = $this->session->userdata('gender');
			if($gender=="male"){
				$source_img = rand(1,13);
			}
			else{
				$source_img = rand(14,18);
			}
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function marriage_date_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    
            
		    $user_name = $this->session->userdata('first_name');
		    $source_img = 1;
            
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            $date=rand(1,28);
            $month=array("JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
		    $year=array("2018","2019","2020","2020","2021","2022","2025");
		    shuffle($month);
            shuffle($year);
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $white = imagecolorallocate($jpg_image, 255, 255, 255);
		    $blue = imagecolorallocate($jpg_image, 26, 57, 107);
		    //imagettftext($jpg_image, 32, 0, 16, 440, $black, $font_path1, explode(" ",$user_name)[0]);
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    imagettftext($jpg_image, 56, 0, 380, 210, $blue, $font_path1, $date."-".$month[0]."-".$year[0]);
		   
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function love_money_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    
            
		    $user_name = $this->session->userdata('first_name');
		    $source_img = 1;
            
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            $good=rand(40,100);
            $bad=100-$good;
		    
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",200,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $white = imagecolorallocate($jpg_image, 255, 255, 255);
		    $red = imagecolorallocate($jpg_image, 221, 18, 0);
		    //imagettftext($jpg_image, 32, 0, 16, 440, $black, $font_path1, explode(" ",$user_name)[0]);
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    imagettftext($jpg_image, 75, 0, 72, 264, $white, $font_path1, $good."%");
		    imagettftext($jpg_image, 75, 0, 575, 264, $white, $font_path1, $bad."%");
		    imagettftext($jpg_image, 24, 0, 322, 352, $white, $font_path1, $user_name);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function nevers_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",330,330);
		    
            
		    $user_name = $this->session->userdata('first_name');
		    $source_img = rand(1,10);
           
		    
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $white = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function odi_career_large($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",280,280);
		    
            $source_img = 1;
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",435,50);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    
		    $matches =rand(250,400);
		    $runs= rand(10000,15000);
		    $fifty=rand(40,70);
		    $hundreds=rand(15,35);
		    $average =rand(450,800)/10;
		    $strike_rate=rand(900,1500)/10;
		    $best_score=rand(158,250);
		    
		    $user_name = $this->session->userdata('name');
		    $font_path = 'assets/fonts/Kozuka.otf';
		    imagettftext($jpg_image, 22, 0, 195, 40, $black, $font_path, $user_name);
		    imagettftext($jpg_image, 15, 0, 368, 144, $black, $font_path, $matches);
		    imagettftext($jpg_image, 15, 0, 368, 173, $black, $font_path, $runs);
		    imagettftext($jpg_image, 15, 0, 368, 200, $black, $font_path, $fifty);
		    imagettftext($jpg_image, 15, 0, 368, 227, $black, $font_path, $hundreds);
		    imagettftext($jpg_image, 15, 0, 368, 253, $black, $font_path, $average);
		    imagettftext($jpg_image, 15, 0, 368, 280, $black, $font_path, $strike_rate);
		    imagettftext($jpg_image, 15, 0, 368, 310, $black, $font_path, $best_score."*");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function acts_thinks_looks($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    
            $source_img = 1;
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    $act=rand(20,30);
		    $think=rand(25,35);
		    $looks=rand(22,30);
		    imagettftext($jpg_image, 40, 0, 430, 104, $black, $font_path, $act);
		    imagettftext($jpg_image, 40, 0, 430, 220, $black, $font_path, $think);
		    imagettftext($jpg_image, 40, 0, 430, 355, $black, $font_path, $looks);
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function celebrity_slefie($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    
            $source_img = rand(1,12);
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",258,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 0, 11, 30);
		    
		    
		    
		   
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function mersal_font_style($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",350,350);
		    
            $source_img = 1;
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/againts.ttf';
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    if(strlen($user_name)<=6){
		        imagettftext($jpg_image, 80, 0, 348, 205, $black, $font_path, explode(" ",$user_name)[0]);
		    }
		    if(strlen($user_name)>6 && strlen($user_name)<10){
		        imagettftext($jpg_image, 70, 0, 314, 205, $black, $font_path, explode(" ",$user_name)[0]);
		    }
		    if(strlen($user_name)>=10){
		        imagettftext($jpg_image, 65, 0, 304, 205, $black, $font_path, explode(" ",$user_name)[0]);
		    }
		  
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function love_hate_logic($app){

		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img =1;
        
		$luck=rand(1,6);
		$love=rand(300,800);
		$hate=rand(10,20);
        
		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

					mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

				}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$luck");

		////

		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg");

		$red = imagecolorallocate($jpg_image, 211, 2, 40);

		$white = imagecolorallocate($jpg_image, 0, 0, 0);



		$user_name = $this->session->userdata('name');

		$font_path = 'assets/fonts/Calibri.ttf';

	
		imagettftext($jpg_image, 80, 0, 318, 210, $red, $font_path, $love);

		imagettftext($jpg_image, 80, 0, 548, 210, $white, $font_path, $hate);







		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/".""."-". $user_id . "$luck.jpg", 80);

		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id . "/" .""."-". $user_id ."$luck");

	}
	public function death_logic($app){

        $user_id = $this->session->userdata("user_id");
		$gender = $this->session->userdata("gender");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}

		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		if($gender=="male"){
		    $source_img = rand(1,9);
		}
		else{
		    $source_img = rand(4,12);
		}

		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",$pic_dimensions[0]);

		$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function good_bad_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    
            
		    $user_name = $this->session->userdata('first_name');
		    $source_img = 1;
            
            $font_path1 = 'assets/fonts/Kozuka.otf';
            if($app[0]->id==547){
                $good=rand(90,100);
                $bad=100-$good;
            }
            else{
                $good=rand(50,100);
                $bad=100-$good;
            }
            
		    
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",200,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $white = imagecolorallocate($jpg_image, 255, 255, 255);
		    $red = imagecolorallocate($jpg_image, 221, 18, 0);
		    //imagettftext($jpg_image, 32, 0, 16, 440, $black, $font_path1, explode(" ",$user_name)[0]);
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    imagettftext($jpg_image, 50, 0, 75, 264, $white, $font_path1, $good."%");
		    imagettftext($jpg_image, 50, 0, 575, 264, $red, $font_path1, $bad."%");
		    imagettftext($jpg_image, 24, 0, 322, 352, $white, $font_path1, $user_name);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function media_questions($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",330,330);
		    
            
		    $user_name = $this->session->userdata('first_name');
		    $source_img = rand(1,15);
            
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            $font_path = 'assets/fonts/NATS.ttf';
		    date_default_timezone_set("Asia/Kolkata");
		    
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $white = imagecolorallocate($jpg_image, 255, 255, 255);
		    //imagettftext($jpg_image, 32, 0, 16, 440, $black, $font_path1, explode(" ",$user_name)[0]);
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    imagettftext($jpg_image, 24, 0, 610, 320, $white, $font_path, date("H").":".date("i")." ".date("D"));
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function celebrity_marriage($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",317,317);
		    
            
		    $user_name = $this->session->userdata('first_name');
		    $gender = $this->session->userdata('gender');
		    if($gender=="male"){
		        $source_img = rand(1,16);
		    }
		    else{
		        $source_img = rand(17,26);
		    }
            $font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,48);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    //imagettftext($jpg_image, 32, 0, 16, 440, $black, $font_path1, explode(" ",$user_name)[0]);
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    imagettftext($jpg_image, 30, 0, 30, 360, $black, $font_path1, explode(" ",$user_name)[0]);
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function jai_lava_kusha_style($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",320,320);
		    
            $source_img = 1;
		    $firstname = $this->session->userdata('first_name');
		    $lastname = $this->session->userdata('last_name');
            $font_path ="assets/fonts/CarnevaleeFreakshow.ttf";
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,35);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 232, 27, 4);
		    $font_path ="assets/fonts/CarnevaleeFreakshow.ttf";
		    list($img_width, $img_height,,) = getimagesize("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
                
                    // find font-size for $txt_width = 80% of $img_width...
                    $font_size = 1; 
                    $txt_max_width = intval(0.8 * ($img_width/2));    

                    do {        
                        $font_size++;
                        $p = imagettfbbox($font_size, 0, $font_path, $firstname);
                        $txt_width = $p[2] - $p[0];
                     // $txt_height=$p[1]-$p[7]; // just in case you need it
                    } while ($txt_width <= $txt_max_width);
                    $font_size1 = 1; 
                    $txt_max_width1 = intval(0.8 * ($img_width/2));    
                    do {        
                        $font_size1++;
                        $p1 = imagettfbbox($font_size1, 0, $font_path, $lastname);
                        $txt_width1 = $p1[2] - $p1[0];
                    // $txt_height=$p[1]-$p[7]; // just in case you need it
                    } while ($txt_width1 <= $txt_max_width1);
                    
                    // now center the text
                    if(strlen($firstname)<=3){
                        $y = $img_height * 0.5;
                    }
                    if(strlen($firstname)>3){
                        $y = $img_height * 0.45;
                    }
                     // baseline of text at 90% of $img_height
                    $x = ($img_width - $txt_width) / 2;
                    $x1 = ($img_width - $txt_width1) / 2;
                    if(strlen($lastname)==2){
                        $y1 = $y+220;
                    }
                    if(strlen($lastname)==3){
                        $y1 = $y+180;
                    }
                    if(strlen($lastname)==4){
                        $y1 = $y+150;
                    }
                    if(strlen($lastname)>=5){
                        $y1 = $y+110;
                    }

                    imagettftext($jpg_image, $font_size, 0, 310, $y, $black, $font_path, strtolower($firstname));
                    imagettftext($jpg_image, $font_size1, 0, 310, $y1, $black, $font_path, strtolower($lastname));
		  
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function future_house_car($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		    
            $source_img = rand(1,18);
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 0, 11, 30);
		    if($app[0]->id==469){
		        if(strlen($user_name)<=8){
		        imagettftext($jpg_image, 30, 0, 120, 365, $black, $font_path, "This is ".explode(" ",$user_name)[0]."'s future house");
		    }
		    if(strlen($user_name)>8){
		        imagettftext($jpg_image, 30, 0, 50, 365, $black, $font_path, "This is ".explode(" ",$user_name)[0]."'s future house");
		    }
		    }
		    if($app[0]->id==470){
		        if(strlen($user_name)<=8){
		        imagettftext($jpg_image, 30, 0, 120, 365, $black, $font_path, "This is ".explode(" ",$user_name)[0]."'s future car");
		    }
		    if(strlen($user_name)>8){
		        imagettftext($jpg_image, 30, 0, 50, 365, $black, $font_path, "This is ".explode(" ",$user_name)[0]."'s future car");
		    }
		    }
		    
		    
		   
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function saaho_poster_logic($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",350,350);
		    
            $source_img = 1;
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/Batman.ttf';
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    //$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    if(strlen($user_name)<=6){
		        imagettftext($jpg_image, 45, 0, 340, 205, $black, $font_path, explode(" ",$user_name)[0]);
		    }
		    if(strlen($user_name)>6 && strlen($user_name)<10){
		        imagettftext($jpg_image, 38, 0, 314, 205, $black, $font_path, explode(" ",$user_name)[0]);
		    }
		    if(strlen($user_name)>=10){
		        imagettftext($jpg_image, 30, 0, 304, 205, $black, $font_path, explode(" ",$user_name)[0]);
		    }
		  
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
	public function spyder_font_style($app){
	    $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",310,310);
		    
            $source_img = 1;
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/PROMETHEUS.ttf';
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    if(strlen($user_name)<=6){
		        imagettftext($jpg_image, 38, 0, 340, 205, $black, $font_path, $this->break_string(explode(" ",$user_name)[0]));
		    }
		    if(strlen($user_name)>6 && strlen($user_name)<10){
		        imagettftext($jpg_image, 32, 0, 314, 205, $black, $font_path, $this->break_string(explode(" ",$user_name)[0]));
		    }
		    if(strlen($user_name)>=10){
		        imagettftext($jpg_image, 22, 0, 304, 205, $black, $font_path, $this->break_string(explode(" ",$user_name)[0]));
		    }
		  
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}
    public function arjun_reddy_style($app){
        $facebook_id = $this->session->userdata("facebook_id");
		$images = glob("container/input-images/".$app[0]->id."/*.jpg");

		$source_img = rand(1,count($images)-2);

		$user_id = $this->session->userdata("user_id");

		if (!file_exists("container/output-images/" . $app[0]->id."/"."")) {

			mkdir("container/output-images/" . $app[0]->id."/"."", 0777, true);

		}
		
		$pic_positions = explode("X",$app[0]->picture_position);

		$pic_dimensions = explode("X",$app[0]->picture_dimensions);
		$user_name = $this->session->userdata("name");
		$access_token = $this->session->userdata("access_token");
		$picture = @file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=378&width=378" . "&access_token=" . $access_token);
        if (!file_exists("container/user-photos/" . $user_id)) {

			mkdir("container/user-photos/" . $user_id, 0777, true);

			}

		$this->save_image(json_decode($picture)->data->url, "container/user-photos/" . $user_id . "/profile_pic.jpg");
		$this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",378,378);
		
		if($app[0]->id==466){
		    $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",317,317);
		    
            $source_img = rand(1,5);
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/".$source_img.".png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 0, 0, 0);
		    //imagettftext($jpg_image, 40, 0, 440, 240, $black, $font_path, $user_name[0]);
		    //imagettftext($jpg_image, 30, 0, 320, 340, $black, $font_path1, "FIRST LETTER OF YOUR VALENTINE");
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

		}
		if($app[0]->id==464){
		    
		    $this->crop_pic("container/user-photos/".$user_id."/"."profile_pic.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg",320,320);
		    $user_name = $this->session->userdata('first_name');
            $font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';
            $font_path1 = 'assets/fonts/BebasNeueBold.ttf';
            $jai=rand(80,100);
            $lava=rand(90,100);
            $kusha=rand(75,100);
            
            $this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",428,1);
            $this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/1.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		
		    $jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		    $black = imagecolorallocate($jpg_image, 255, 255, 255);
		    imagettftext($jpg_image, 42, 0, 320, 75, $black, $font_path, $jai."%");
		    imagettftext($jpg_image, 42, 0, 320, 180, $black, $font_path, $lava."%");
		    imagettftext($jpg_image, 42, 0, 320, 285, $black, $font_path, $kusha."%");
		    
		    
		    imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		    imagedestroy($jpg_image);

		    redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

		}
		//$this->merge_images("container/input-images/".$app[0]->id."/".$source_img.".jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",$pic_positions[0],$pic_positions[1]);
		//convert sea.jpg -size 173x50! caption:"Your text" -geometry +312+66 -composite result.png
		exec('convert "container/input-images/'.$app[0]->id.'/source.png" -size 400x100 -background transparent -fill red -font "assets/fonts/FranklinHeavy.ttf" caption:'.strtoupper(explode(" ",$user_name)[0]).' -geometry +280+100 -composite "container/output-images/' . $app[0]->id . '/' .''.'/'. ''.'-'.$user_id . '1.png"');
		exec('convert "container/output-images/' . $app[0]->id . '/' .''.'/'. ''.'-'.$user_id . '1.png" -size 400x100 -background transparent -fill red -font "assets/fonts/FranklinHeavy.ttf" caption:'.strtoupper(explode(" ",$user_name)[1]).' -geometry +280+170 -composite "container/output-images/' . $app[0]->id . '/' .''.'/'. ''.$user_id .'2.png"');
		$this->merge_images("container/input-images/".$app[0]->id."/source.jpg","container/user-photos/".$user_id."/"."profile_pic_cropped.jpg","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/black.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);

		$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/output-images/" . $app[0]->id . "/".$user_id . "2.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",20,0);
		$this->merge_images_jpg_png("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg","container/input-images/".$app[0]->id."/pattern.png","container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg",0,0);
		

		$data = array("app_id"=>$app[0]->id,"imagepath"=>$user_id."-$source_img");

		//
		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg");
		
		
				//imagettftext($jpg_image, 20, 0, 250, 30, $black, $font_path, $first_names[$best_friends_keys[0]]);
				
		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" .""."/". ""."-".$user_id . "$source_img.jpg", 100);
		imagedestroy($jpg_image);

		redirect("app/output/".$app[0]->category_name."/".strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id. "/" .""."-". $user_id . "$source_img");

	}

	public function break_string($string,  $group = 1, $delimeter = ' ', $reverse = true){
            $string_length = strlen($string);
            $new_string = [];
            while($string_length > 0){
                if($reverse) {
                    array_unshift($new_string, substr($string, $group*(-1)));
                }else{
                    array_unshift($new_string, substr($string, $group));
                }
                $string = substr($string, 0, ($string_length - $group));
                $string_length = $string_length - $group;
            }
            $result = '';
            foreach($new_string as $substr){
                $result.= $substr.$delimeter;
            }
            return trim($result, " ");
        }

	public function generic_function($app){

		//$this->print_exit($app);

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}

		$user_id = $this->session->userdata("user_id");

		$where_conditon = array("app_id"=>$app[0]->id,"status"=>"active");

		$image_objects = $this->Apps_model->FetchData('image_objects','*',$where_conditon,'','desc');

		$text_objects = $this->Apps_model->FetchData('text_objects','*',$where_conditon,'','desc');

		$source_img = "source.jpg";

		$random_number = "";

		if($app[0]->background=='random'){

			$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");

            $random_number = rand(1, count($images) - 3);

			$source_img =  $random_number.".jpg";

		}elseif ($app[0]->background=='static'){

            $random_number = rand(1,10);

        }

        $source_img = "container/input-images/".$app[0]->id."/".$source_img;

		$this->merge_data($source_img,$image_objects,$text_objects,"container/output-images/".$app[0]->id."/".$user_id."-".$random_number.".jpg",$user_id,$app[0]->type);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title)))

            . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" .$user_id."-".$random_number);

	}

	public function valentine_letter($app){

		$this->has_access_token($app);

		$this->refresh_access_token();

		$user_id = $this->session->userdata("user_id");

		$source_img=rand(1,26);

		if (!file_exists("container/output-images/" . $app[0]->id)) {

			mkdir("container/output-images/" . $app[0]->id, 0777, true);

		}



		exec('convert -size 180x180 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 90,90,1,90" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

		$this->merge_images_jpg_png("container/input-images/" . $app[0]->id . "/$source_img.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_circle.png", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 60, 70);



		$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

		$black = imagecolorallocate($jpg_image, 255, 207, 207);

		$user_name = $this->session->userdata('first_name');

		$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';



		imagettftext($jpg_image, 40, 0, 440, 240, $black, $font_path, $user_name[0]);

		imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 100);

		imagedestroy($jpg_image);

		redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



	}

	public function tattoo($app)

	{

		$user_id = $this->session->userdata("user_id");

		$user_name = $this->session->userdata('first_name');

		$gender = $this->session->userdata("gender");

		$images = glob("container/input-images/" . $app[0]->id . "/*.jpg");

		if($app[0]->id==415){

			$full_name = $this->session->userdata("first_name");

			if($gender=="male"){

				$source_img = rand(1, 5);

			}

			else{

				$source_img = rand(6, 8);

			}







			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 200,200);


			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 20, 60);


			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 22, 31, 62);

			$font_path = 'assets/fonts/Kozuka.otf';

			//imagettftext($jpg_image, 14, 0, 202, 275, $black, $font_path, $full_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		if($app[0]->id==377){

			$full_name = $this->session->userdata("first_name");

			if($gender=="male"){

				$source_img = rand(1, 25);

			}

			else{

				$source_img = rand(26, 34);

			}







			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 160,160);


			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 200, 100);


			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 22, 31, 62);

			$font_path = 'assets/fonts/Kozuka.otf';

			imagettftext($jpg_image, 14, 0, 202, 275, $black, $font_path, $full_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		if($app[0]->id==374){

			$full_name = $this->session->userdata("first_name");

			if($gender=="male"){

				$source_img = rand(1, 16);

			}

			else{

				$source_img = rand(17, 26);

			}







			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 160,160);


			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 200, 100);


			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 22, 31, 62);

			$font_path = 'assets/fonts/Kozuka.otf';

			imagettftext($jpg_image, 14, 0, 202, 275, $black, $font_path, $full_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		if($app[0]->id==369 || $app[0]->id==493){

			$full_name = $this->session->userdata("name");

			if($gender=="male"){

				$source_img = rand(1, 14);

			}

			else{

				$source_img = rand(15, 22);

			}

			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 160,160);


			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 200, 100);


			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 38, 53, 80);

			$font_path = 'assets/fonts/Kozuka.otf';

			imagettftext($jpg_image, 14, 0, 202, 276, $black, $font_path, $user_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		

		if($app[0]->id==365){

			$full_name = $this->session->userdata("name");

			if($gender=="male"){

				$source_img = rand(1, 14);

			}

			else{

				$source_img = rand(15, 22);

			}

			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 180,180);

			$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_small.jpg", 60,60);




			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_small.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 90, 5);

			$this->merge_images("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 185, 105);




			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 0, 0, 0);

			$font_path = 'assets/fonts/Kozuka-Gothic-Pro-M_26793.ttf';

			imagettftext($jpg_image, 15, 0, 165, 25, $black, $font_path, $full_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		if($app[0]->id==364){

			$full_name = $this->session->userdata("name");

			if($gender=="male"){

				$source_img = rand(1, 18);

			}

			else{

				$source_img = rand(19, 28);

			}







			if (!file_exists("container/output-images/" . $app[0]->id)) {

				mkdir("container/output-images/" . $app[0]->id, 0777, true);

			}

			$this->crop_pic_png("container/user-photos/".$user_id."/"."profile_pic.png", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", 160,160);

			//exec('convert -size 200x200 xc:none -fill "/home/witapps/public_html/container/user-photos/' . $user_id . '/' . 'profile_pic.jpg" -draw "circle 100,100,1,100" "/home/witapps/public_html/container/user-photos/' . $user_id . '/profile_pic_circle.png"');

			$this->merge_images("container/input-images/" . $app[0]->id . "/" . $source_img . ".jpg", "container/user-photos/" . $user_id . "/" . "profile_pic_cropped.jpg", "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 200, 100);

			//$this->crop_pic("container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg","container/output-images/" . $app[0]->id . "/" . $user_id . ".jpg", 500);

			$jpg_image = imagecreatefromjpeg("container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg");

			$black = imagecolorallocate($jpg_image, 38, 53, 80);

			$font_path = 'assets/fonts/Kozuka.otf';

			imagettftext($jpg_image, 14, 0, 202, 276, $black, $font_path, $user_name);

			imagejpeg($jpg_image, "container/output-images/" . $app[0]->id . "/" . $user_id . "-$source_img.jpg", 80);

			imagedestroy($jpg_image);



			redirect("app/output/" . $app[0]->category_name . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $app[0]->title))) . "/" . $app[0]->category_id . "/" . $app[0]->id . "/" . $user_id."-$source_img");



		}

		

	}

	public function merge_data($source_name,$images_objects,$text_objects,$output_name,$user_id,$type){

		list($width_x, $height_x) = getimagesize($source_name);

		$source_image = imagecreatefromjpeg($source_name);

		$output_image = imagecreatetruecolor($width_x, $height_x);

		imagecopy($output_image, $source_image, 0, 0, 0, 0, $width_x, $height_x);

        $friend_final_array = "";

		$j=0;

		$array_to_map_text = "";

		$friend_names = "";

		if($type=='friends') {

            if(!$this->session->userdata('friends')) {

                $user_feed = $this->get_user_feed();

                if (isset($user_feed->feed)) {

                    $friends = $this->best_friends_from_feed($user_feed->feed->data);

                    if (count($friends) >= (count($images_objects) - 1)) {

                        if (array_key_exists($user_id, $friends)) {

                            unset($friends[$user_id]);

                        }

                        $random_array = $this->unique_random_numbers_within_range(0, count($friends) - 1, count($images_objects) - 1);

                        $best_friends_keys = array_keys($friends);

                        for ($i = 0; $i < count($random_array); $i++) {

                            $friend_final_array[$i] = $best_friends_keys[$random_array[$i]];

                        }

                        //$this->print_exit($friend_final_array);

                        $this->get_profile_pics($friend_final_array);

                        $friend_names = $this->get_first_names($friend_final_array);

                    } else {

                        $this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

                        redirect('apps/home');

                    }

                } else {

                    $this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

                    redirect('apps/home');

                }

            }else{

                $friends = $this->session->userdata('friends');

                if (count($friends) >= (count($images_objects) - 1)) {

                    if (array_key_exists($user_id, $friends)) {

                        unset($friends[$user_id]);

                    }

                    $random_array = $this->unique_random_numbers_within_range(0, count($friends) - 1, count($images_objects) - 1);

                    $best_friends_keys = array_keys($friends);

                    for ($i = 0; $i < count($random_array); $i++) {

                        $friend_final_array[$i] = $best_friends_keys[$random_array[$i]];

                    }

                    //$this->print_exit($friend_final_array);

                    $this->get_profile_pics($friend_final_array);

                    $friend_names = $this->get_first_names($friend_final_array);

                } else {

                    $this->session->set_flashdata('no_photos', 'Sorry we are unable to process this app try other');

                    redirect('apps/home');

                }

            }

		}

		for($i=0;$i<count($images_objects);$i++) {

			$path_of_image_to_merge = "";

			if($images_objects[$i]['type']=='profile_pic'){

				$path_of_image_to_merge="container/user-photos/$user_id/profile_pic";

			}else if($images_objects[$i]['type']='friend_pic'){

				$path_of_image_to_merge= "container/user-photos/$user_id/friends/".$friend_final_array[$j];

				$array_to_map_text[$images_objects[$i]['id']]=$friend_final_array[$j];

				$j++;

			}

			$image_to_merge= imagecreatefromjpeg($path_of_image_to_merge.".jpg");

			if ($images_objects[$i]['width'] < 400 || $images_objects[$i]['height'] < 400) {

				/* read the source image */

				$width = imagesx($image_to_merge);

				$height = imagesy($image_to_merge);

				/* find the "desired height" of this thumbnail, relative to the desired width  */

				$desired_height = floor($height * ($images_objects[$i]['width'] / $width));

				/* create a new, "virtual" image */

				$virtual_image = imagecreatetruecolor($images_objects[$i]['width'],$images_objects[$i]['height']);

				/* copy source image at a resized size */

				imagecopyresampled($virtual_image, $image_to_merge, 0, 0, 0, 0, $images_objects[$i]['width'], $images_objects[$i]['height'], $width, $height);

				/* create the physical thumbnail image to its destination */

				$image_to_merge = $virtual_image;

			}

			if($images_objects[$i]['shape']=='circle'){

                    imagejpeg($image_to_merge,$path_of_image_to_merge."_cropped.jpg",100);

                    exec('convert -size '.imagesx($image_to_merge).'x'.imagesx($image_to_merge).' xc:none -fill "/home/witapps/public_html/'.

                        $path_of_image_to_merge.'_cropped.jpg" -draw "circle '.(imagesx($image_to_merge)/2).','.(imagesx($image_to_merge)/2).',1,'.

                        (imagesx($image_to_merge)/2).'" "/home/witapps/public_html/'.$path_of_image_to_merge.'_circle.png"');

                $image_to_merge= imagecreatefrompng($path_of_image_to_merge."_circle.png");

			}

			imagecopy($output_image, $image_to_merge, $images_objects[$i]['position_x'], $images_objects[$i]['position_y'], 0, 0, imagesx($image_to_merge), imagesy($image_to_merge));  //  top, left, border,border

			//imagedestroy($virtual_image);

			//imagedestroy($image_to_merge);

		}

		for($i=0;$i<count($text_objects);$i++){

			$color_comb = explode(",",$text_objects[$i]['color']);

			$color = imagecolorallocate($output_image, $color_comb[0], $color_comb[1], $color_comb[2]);

			$font_path = 'assets/fonts/'.$text_objects[$i]['style'];

			$text = $text_objects[$i]['text'];

			if($text_objects[$i]['type']=='random'){

				$text_array = explode("||",$text_objects[$i]['text']);

				$random_number = rand(0,count($text_array)-1);

				$text = $text_array[$random_number];

			}elseif($text_objects[$i]['type']=='own_name'){

				$text = explode(" ",$this->session->userdata('name'))[0];



			}elseif($text_objects[$i]['type']=='friend_name'){

				$text =explode(" ",$friend_names[$array_to_map_text[$text_objects[$i]['img_reference']]])[0];

			}

			imagettftext($output_image, $text_objects[$i]['size'],$text_objects[$i]['angle'],$text_objects[$i]['position_x'], $text_objects[$i]['position_y'], $color, $font_path,$text);

		}

		imagejpeg($output_image, $output_name, 100);

		imagedestroy($source_image);

		imagedestroy($output_image);

	}
	 public function after_login(){
            
           echo "<script>history.back();</script>";
            
        }

	public function view_output($category_name,$app_title,$category_id,$app_id,$user_id)

	{


        $app=$this->Apps_model->get_app_details($app_id);
        if(count($app)>0){
		$data['app'] = array("category_name" => $category_name, "app_titile" => $app_title, "category_id" => $category_id, "app_id" => $app_id, "user_id" => $user_id,"app_description" => $app[0]->app_description);

		$data['menu_names'] = $this->get_category_names_menu();

		//$data['header_adds'] = $this->get_adds_from_db('header', 1);

		//$data['footer_adds'] = $this->get_adds_from_db('footer', 1);

		if (count($this->get_adds_from_db('side', 1)) == 0) { //if all add scripts are in inactive mode display

			$data['app_adds'] = $this->get_apps_as_adds(5, 4);

			if ($category_id == 5) {

				$data['app_adds'] = $this->get_apps_as_adds(4, 4);

			} else{

				$data['app_adds'] = $this->get_apps_as_adds(5, 4);

			}

		} else {

			$data['side_adds'] = $this->get_adds_from_db('side', $category_id);

		}

	

		/*if($this->session->userdata('user_id')){

			$data['try_again'] = "yes";

		}else{

			$data['try_again'] = "no";

		}*/
                
                

		if($this->session->userdata('user_id')){

                $data['try_again'] = "yes";

                $first_time = $this->session->userdata('first_time');

                $date=explode("-",$user_id);

                

               

                $share = $this->session->userdata('share');

                        

                   if ($first_time == NULL) {

                   

                        if($share=='yes'){

                           if($app_id!=611){

			$this->refresh_access_token();

                         $access_token = $this->session->userdata("access_token");

                         $persmissions = @file_get_contents("https://graph.facebook.com/me/permissions/publish_actions?access_token=$access_token");

                          

                         //$persmissions = $this->print_exit(json_decode($persmissions));

                         $persmissions = json_decode($persmissions);
                           @file_get_contents("https://graph.facebook.com/?id=$link&scrape=true&access_token=$access_token");
                          if(count($persmissions->data)>0){

                         if($persmissions->data[0]->status=='granted'){

                         $ch = curl_init();
                         

                         curl_setopt($ch, CURLOPT_URL,"https://graph.facebook.com/me/feed?access_token=" . $access_token);

                         curl_setopt($ch, CURLOPT_POST, 1);

                        $link = "link=".base_url()."app/fun/$app_title/$category_id/$app_id?ouid=$user_id";
                       

                       
                         curl_setopt($ch, CURLOPT_POSTFIELDS, $link);                             

                         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                         $server_output = curl_exec($ch);

                         curl_close ($ch);    
                            //file_put_contents('application/logs/shared_urls.txt',$link."\n",FILE_APPEND);
                         }
                          }

                        }

                         $this->session->set_userdata("first_time", "true"); 

                   }

                   }

                

                }else{

			$data['try_again'] = "no";

		}

		$data['page'] = 'view_output';

		$data['meta_tags'] = $this->get_meta_tags('apps', $app_id);

		//$data['apps'] = $this->get_apps_like_index();

		$data['suggestion_apps'] = $this->get_latest_apps_category($category_id, 12);



			$this->load->view('templates/view_header', $data);

			$this->load->view('view_output');

			$this->load->view('templates/view_footer');
	}else{
	      $this->load->view('view_apps_not_found');
	}
	}

	public function get_login_url()

	{

		return "https://www.facebook.com/dialog/oauth?client_id=".$this->FB_APP_ID."&redirect_uri=".base_url()."apps/redirect-to&scope=email,user_photos,user_posts,user_birthday";
                
}

	public function redirect_to()
{

		$userData = array();
        
        // Check if user is logged in
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
            $user = $this->facebook->request('get', '/me?fields=id,name,first_name,last_name,email,gender,picture.width(378).height(378)');
            //$this->print_exit($user);
            
        }else{
            echo $this->facebook->login_url();
        }
        



		if (isset($user['id'])) {
		    
            $this->session->set_userdata('first_login','true');
            
			$access_token = $this->facebook->is_authenticated();
            
            $is_user_exists = $this->is_user_exists($user['id']);
            
            

			$user_data = array("facebook_id" => $user['id'],"first_name" => $user['first_name']

			, "last_name" => $user['last_name'], "access_token" => $access_token, "name" => $user['name']);

			if (isset($user['email'])) {

				$user_data['email'] = $user['email'];

			}if (isset($user['birthday'])) {

				$user_data['birthday'] = $user['birthday'];

			}
                        
                        if($this->input->get('share')=='yes' ){

                            $this->session->set_userdata("share","yes");
                           
                            $this->Apps_model->increment_share_count('with_share',1);
                    }else{
                            $this->session->set_userdata("share","no"); 
                            $this->Apps_model->increment_share_count('without_share',1);
                        //  file_put_contents('application/logs/without_share.txt',"without_share "."\n",FILE_APPEND);
                    }
                

			if (isset($user['gender'])) {

				$user_data['gender'] = $user['gender'];

			}

			if (isset($user['age_range'])) {

				$user_data['age_range'] = $user['age_range']['min'];

			}
			
			//$this->print_exit($user_data);
			if (!count($is_user_exists)) {



				$inserted_id = $this->insert_user($user_data);
				

				if (count($inserted_id)) {

					//	$photos_data['user_id'] = $inserted_id;

					//	$this->Apps_model->InsertData('user_photos',$photos_data);

					$user_data['user_id'] = $inserted_id;

					$this->session->set_userdata($user_data);

					$redirect_app = $this->session->userdata("redirect_app");
					if (!file_exists("container/user-photos/".$inserted_id)) {

					    mkdir("container/user-photos/".$inserted_id, 0777, true);

				    }

					
					$this->save_image($user['picture']['data']['url'], "container/user-photos/" . $inserted_id . "/profile_pic.jpg");

					redirect("app/" . $redirect_app['category_name'] . "/" . $redirect_app['app_title'] . "/" . $redirect_app['category_id'] . "/" . $redirect_app['app_id']);

				}

			} else {

				$old_user = $this->get_user($user['id']);

				//$this->print_exit($user['picture']['data']['url']);



				$user_data['user_id'] = $old_user[0]->id;

				

				$this->session->set_userdata($user_data);

				$redirect_app = $this->session->userdata("redirect_app");

				if (!file_exists("container/user-photos/".$old_user[0]->id)) {

					mkdir("container/user-photos/".$old_user[0]->id, 0777, true);

				}

				$this->save_image($user['picture']['data']['url'], "container/user-photos/" . $old_user[0]->id . "/profile_pic.jpg");

				$this->session->unset_userdata("redirect_app");

				redirect("app/" . $redirect_app['category_name'] . "/" . $redirect_app['app_title'] . "/" . $redirect_app['category_id'] . "/" . $redirect_app['app_id']);

			}

		}

		else{

			$this->session->set_flashdata('no_permission', 'Sorry we are unable to process because not giving permission');

			redirect('apps/home');

		}

	}

	public function redirect_to_login($access_token,$app_id){



		$user = file_get_contents("https://graph.facebook.com/me?fields=id,name,email,first_name,last_name,age_range&access_token=".$access_token);

		$user = json_decode($user);

		$is_user_exists = $this->is_user_exists($user->id);

		$user_data = array("facebook_id"=>$user->id,"first_name"=>$user->first_name

		,"last_name"=>$user->last_name,"access_token"=>$access_token,"name"=>$user->name);

		if(!count($is_user_exists)){

			if(isset($user->email)){

				$user_data['email']=$user->email;

			}

			if(isset($user->gender)){

				$user_data['gender']=$user->gender;

			}

			if(isset($user->age_range)){

				$user_data['age_range'] = $user->age_range->min;

			}

			$inserted_id = $this->insert_user($user_data);

			if(count($inserted_id)){

				$user_data['user_id'] = $inserted_id;

				$this->session->set_userdata($user_data);

				$redirect_app = $this->get_app_details($app_id);

				$picture = file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=200&width=200"."&access_token=".$access_token);

				if (!file_exists("container/user-photos/".$inserted_id)) {

					mkdir("container/user-photos/".$inserted_id, 0777, true);

				}

				$this->save_image(json_decode($picture)->data->url,"container/user-photos/".$inserted_id."/profile_pic.jpg");

			}

		}else{

			$old_user = $this->get_user($user->id);

			$user_data['user_id'] = $old_user[0]->id;

			$this->session->set_userdata($user_data);

			$redirect_app = $this->get_app_details($app_id);

			$picture = file_get_contents("https://graph.facebook.com/me/picture?redirect=false&height=200&width=200"."&access_token=".$access_token);

			if (!file_exists("container/user-photos/".$old_user[0]->id)) {

				mkdir("container/user-photos/".$old_user[0]->id, 0777, true);

			}

			$this->save_image(json_decode($picture)->data->url,"container/user-photos/".$old_user[0]->id."/profile_pic.jpg");

		}

		echo base_url()."app/telugu/" .strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $redirect_app[0]->title)))  . "/" . $redirect_app[0]->category_id . "/" . $redirect_app[0]->id;

	}

	public function is_user_exists($facebook_id){

		

		return $this->Apps_model->is_user_exists($facebook_id);

	}

	public function is_logged_in(){

		return $this->session->userdata('user_id');

	}

	public function insert_user($user_data){

		

		return $this->Apps_model->insert_user($user_data);

	}

	public function get_user($facebook_id){

		

		return $this->Apps_model->get_user($facebook_id);

	}

	public function merge_images($picture_one, $picture_two, $filename_result,$left,$top) {

		// Get dimensions for specified images

		list($width_x, $height_x) = getimagesize($picture_one);

		list($width_y, $height_y) = getimagesize($picture_two);

		// Create new image with desired dimensions

		$image = imagecreatetruecolor($width_x, $height_x);

		// Load images and then copy to destination image

		$image_x = imagecreatefromjpeg($picture_one);

		$image_y = imagecreatefromjpeg($picture_two);

		imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);                     //  top, left, border,border

		imagecopy($image, $image_y, $left, $top,0, 0, $width_y, $height_y);

		// Save the resulting image to disk (as JPEG)

		imagejpeg($image, $filename_result,80);

		// Clean up

		imagedestroy($image);

		imagedestroy($image_x);

		imagedestroy($image_y);

	}

  public function save_image($source_url,$target_path){
        
	$ch = curl_init ($source_url);

	curl_setopt($ch, CURLOPT_HEADER, 0);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);

	curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 0);

	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$rawdata=curl_exec($ch);

	curl_close ($ch);

	$fp = fopen($target_path,'w');

	fwrite($fp, $rawdata);

	fclose($fp);

}      
        
		public function save_image_old($source_url,$target_path){
                    //$source_url=base_url()."container/source.jpg";
                    //$facebook_id = $this->session->userdata('facebook_id');
        //$source_url= "http://witalks.com:3000/download?userId=$facebook_id";
	$ch = curl_init ($source_url);

	curl_setopt($ch, CURLOPT_HEADER, 0);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);

	curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 0);

	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$rawdata=curl_exec($ch);

	curl_close ($ch);

	$fp = fopen($target_path,'w');

	fwrite($fp, $rawdata);

	fclose($fp);

}



	public function print_exit($array) {

		echo "<pre>";

		print_r($array);

		echo "</pre>";

		exit;

	}

	public function get_adds_from_db($add_type,$add_num) {

		

		return $this->Apps_model->get_adds_from_db($add_type,$add_num);

	}



	public function get_apps_as_adds($category_id,$required_apps) {

		

		return $this->Apps_model->get_apps_as_adds($category_id,$required_apps);

	}



	public function get_category_names_menu() {

		

		return $this->Apps_model->get_category_names_menu();

	}

	public function crop_pic($src, $dest, $desired_width) {



			/* read the source image */

			$source_image = imagecreatefromjpeg($src);

			$width = imagesx($source_image);

			$height = imagesy($source_image);

                        if(!file_exists($src)){

                            $function =   debug_backtrace();

                          

                            log_message('error','function_name : '.$function[0]['function'].' url : '.$function[0]['object']->uri->uri_string.' user_id : '.json_encode($function[0]['object']->session->userdata) . "session_id ". session_id());

                        }

			/* find the "desired height" of this thumbnail, relative to the desired width  */

			$desired_height = floor($height * ($desired_width / $width));



			/* create a new, "virtual" image */

			$virtual_image = imagecreatetruecolor($desired_width, $desired_height);



			/* copy source image at a resized size */

			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);



			/* create the physical thumbnail image to its destination */

			imagejpeg($virtual_image, $dest,100);

	}
	public function crop_pic_png($src, $dest, $desired_width) {



			/* read the source image */

			$source_image = imagecreatefrompng($src);

			$width = imagesx($source_image);

			$height = imagesy($source_image);

                        if(!file_exists($src)){

                            $function =   debug_backtrace();

                          

                            log_message('error','function_name : '.$function[0]['function'].' url : '.$function[0]['object']->uri->uri_string.' user_id : '.json_encode($function[0]['object']->session->userdata) . "session_id ". session_id());

                        }

			/* find the "desired height" of this thumbnail, relative to the desired width  */

			$desired_height = floor($height * ($desired_width / $width));



			/* create a new, "virtual" image */

			$virtual_image = imagecreatetruecolor($desired_width, $desired_height);



			/* copy source image at a resized size */

			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);



			/* create the physical thumbnail image to its destination */

			imagejpeg($virtual_image, $dest,100);

	}

	public function get_meta_tags($table,$id){

			$this->load->model('Apps_model');

			$meta_tags = $this->Apps_model->get_meta_tags($table,$id);

			return $meta_tags;

	}

	public function increment_count($column,$app_id){

		$this->load->model('Apps_model');

		$this->Apps_model->increment_count($column,$app_id);

	}

	public function update_pycker_clicks($slot_id){

		$this->load->model('Apps_model');

		$this->Apps_model->update_pycker_clicks($slot_id);

	}
	public function admin_login() {



		$this->load->view('admin_login');

	}

	public function admin_home() {


		$this->load->view('admin_home');

	}

	public function admin_panel() {



		$this->load->view('admin_panel');

	}



	public function data_submitted() {

		$data = array(

			'name' => $this->input->post('appname'),

			'title' => $this->input->post('apptitle'),

			'category_id' => $this->input->post('category'),

			'picture_dimensions' => $this->input->post('picdimn'),

			'picture_position' => $this->input->post('picposn'),

			'meta_keywords' => $this->input->post('metakeywords'),

			'meta_title' => $this->input->post('metatitle'),

			'meta_description' => $this->input->post('metadesc')

		);



		$this->insert_app($data);

	}

	public function insert_app($data)

	{

		$last_insert_id = $this->Apps_model->insert_app($data);

		if(!file_exists("container/input-images/".$last_insert_id)){

			mkdir("container/input-images/".$last_insert_id);

		}

		if(!file_exists("container/output-images/".$last_insert_id)){

			mkdir("container/output-images/".$last_insert_id);

		}

	}

	 public function privacy_policy(){

		 $data['header_adds']=$this->get_adds_from_db('header',1);

		 $data['footer_adds']=$this->get_adds_from_db('footer',1);

		 if(count($this->get_adds_from_db('side',1))==0) { //if all add scripts are in inactive mode display

			 $data['app_adds']=$this->get_apps_as_adds(1,3);

		 }

		 else {

			 $data['side_adds']=$this->get_adds_from_db('side',1);

		 }

		 $data['meta_tags'] = $this->get_meta_tags('apps_categories',9);

		 $data['menu_names']=$this->get_category_names_menu();

   		$this->load->view('templates/view_header',$data);

        $this->load->view('terms_and_conditions');

     

    }

	public function get_first_names_new($friends,$limit){

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		if(array_key_exists($user_id,$friends)){

			unset($friends[$user_id]);

		}

		$friends_keys = array_keys($friends);

		$first_names = array();

		for($i=0;$i<$limit;$i++){

			$user_json = file_get_contents("https://graph.facebook.com/$friends_keys[$i]?fields=name&access_token=".$access_token);

			$user_decoded = json_decode($user_json);

			$first_names[$friends_keys[$i]] = explode(" ",$user_decoded->name)[0];

		}

		return $first_names;

	}

	public function get_profile_pics_new($friends,$limit) {

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		if(array_key_exists($user_id,$friends)){

			unset($friends[$user_id]);

		}

		$friends_keys = array_keys($friends);



		for($i=0;$i<$limit;$i++){



			$picture = file_get_contents("https://graph.facebook.com/$friends_keys[$i]/picture?redirect=false&height=378&width=378&access_token=".$access_token);

			if (!file_exists("container/user-photos/".$user_id."/friends")) {

				mkdir("container/user-photos/".$user_id."/friends", 0777, true);

			}

			$this->save_image(json_decode($picture)->data->url,"container/user-photos/".$user_id."/friends"."/$friends_keys[$i].jpg");

		}

	}

	public function get_profile_pics_old($friends,$limit) {

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		if(array_key_exists($user_id,$friends)){

			unset($friends[$user_id]);

		}

		$friends_keys = array_keys($friends);



		for($i=0;$i<$limit;$i++){



			$picture = file_get_contents("https://graph.facebook.com/$friends_keys[$i]/picture?redirect=false&height=200&width=200&access_token=".$access_token);

			if (!file_exists("container/user-photos/".$user_id."/friends")) {

				mkdir("container/user-photos/".$user_id."/friends", 0777, true);

			}

			$this->save_image(json_decode($picture)->data->url,"container/user-photos/".$user_id."/friends"."/$friends_keys[$i].jpg");

		}

	}

    public function get_profile_pics($friends_final) {

        $this->refresh_access_token();

        $access_token = $this->session->userdata('access_token');

        $user_id = $this->session->userdata('user_id');

        if (!file_exists("container/user-photos/".$user_id."/friends")) {

            mkdir("container/user-photos/".$user_id."/friends", 0777, true);

        }

        for($i=0;$i<count($friends_final);$i++){

            if(!file_exists("container/user-photos/".$user_id."/friends/".$friends_final[$i])){

                $picture = file_get_contents("https://graph.facebook.com/$friends_final[$i]/picture?redirect=false&height=200&width=200&access_token=".$access_token);

                $this->save_image(json_decode($picture)->data->url,"container/user-photos/".$user_id."/friends"."/$friends_final[$i].jpg");

            }

        }

    }

	public function best_friends($photos){

		$common_ids = array();

		if(isset($photos->photos)){

			$facebook_id = $this->session->userdata("facebook_id");

			$likes_arrays_by_photo = array();

			$tags_arrays_by_photo = array();

			$from_arrays_by_photo = array();

			$tags_arrays_offset = 0;

			for($i=0;$i<count($photos->photos->data);$i++){

				if(isset($photos->photos->data[$i]->likes)){

					$likes_arrays_by_photo[$i] = $this->pluck($photos->photos->data[$i]->likes->data,"id");

				}else{

					$likes_arrays_by_photo[$i] = array();

				}

				if(count($photos->photos->data[$i]->tags->data)>1){

					if($from_arrays_by_photo[$i] = $photos->photos->data[$i]->from->id == $facebook_id){

						for($j=0;$j<count($photos->photos->data[$i]->tags->data);$j++){

							$tags_arrays_by_photo[$tags_arrays_offset] = $photos->photos->data[$i]->tags->data[$j]->id;

							$tags_arrays_offset ++;

						}

					}

				}

				$from_arrays_by_photo[$i] = $photos->photos->data[$i]->from->id;

			}



			$likes_array = array();

			for($i=0;$i<count($likes_arrays_by_photo);$i++){

				$likes_array = array_merge($likes_array,$likes_arrays_by_photo[$i]);

			}

			$ids_with_likes_count 	= array_count_values($likes_array);

			$ids_with_from_count 	= array_count_values($from_arrays_by_photo);

			$ids_with_tags_count 	= array_count_values($tags_arrays_by_photo);

			arsort($ids_with_likes_count);

			arsort($ids_with_from_count);

			arsort($ids_with_tags_count);



			unset($ids_with_tags_count[$facebook_id]);

			unset($ids_with_from_count[$facebook_id]);

			unset($ids_with_likes_count[$facebook_id]);





			if(!(count($common_ids)>=10)){

				if(count($ids_with_tags_count) > count($common_ids)){

					$commonids_tags_and_commonids = array_diff_key ($ids_with_tags_count,$common_ids);

					$common_ids = $common_ids+$commonids_tags_and_commonids;

				}

			}

			if(!(count($common_ids)>=10)){

				if(count($ids_with_from_count) > count($common_ids)){

					$array_diff = array_diff_key ($ids_with_from_count,$common_ids);

					$common_ids = $common_ids+$array_diff;

				}

			}

			arsort($common_ids);

			if(!(count($common_ids)>=10)){

				if(count($ids_with_likes_count)>count($common_ids)){

					$array_diff = array_diff_key ($ids_with_likes_count,$common_ids);

					arsort($array_diff);

					$common_ids = $common_ids+$array_diff;

				}

			}

		}

		return $common_ids;

	}



	public  function pluck($collection, $property)

	{

		$plucked = array_map(function ($value) use ($property) {

			return $this->get($value, $property);

		}, (array) $collection);

		// Convert back to object if necessary

		if (is_object($collection)) {

			$plucked = (object) $plucked;

		}

		return $plucked;

	}

	public  function get($collection, $key, $default = null)

	{

		if (is_null($key)) {

			return $collection;

		}

		$collection = (array) $collection;

		if (isset($collection[$key])) {

			return $collection[$key];

		}

		// Crawl through collection, get key according to object or not

		foreach (explode('.', $key) as $segment) {

			$collection = (array) $collection;

			if (!isset($collection[$segment])) {

				return $default instanceof Closure ? $default() : $default;

			}

			$collection = $collection[$segment];

		}

		return $collection;

	}

	public function refresh_access_token(){

		$access_token = $this->session->userdata("access_token");

		$fresh_token = file_get_contents("https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=" . $this->FB_APP_ID . "&client_secret=". $this->FB_SECRET_KEY . "&fb_exchange_token=" . $access_token);

		$explod1 = explode('&', $fresh_token);

		$access_token = json_decode($explod1[0])->access_token;

		$this->session->set_userdata("access_token",$access_token);

	}

	public  function get_user_photos($since,$until)

	{

		$this->refresh_access_token();

		$access_token = $this->session->userdata("access_token");

		$photos_json = file_get_contents("https://graph.facebook.com/me?fields=photos.limit(50){id,created_time,likes{id,name},from,tags{id}}&access_token=" .$access_token);

		$photos_decoded = json_decode($photos_json);

		return $photos_decoded;

	}

	function logout()

	{

		$user_data = $this->session->all_userdata();

		foreach ($user_data as $key => $value) {

			if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {

				$this->session->unset_userdata($key);

			}

		}

		$this->session->sess_destroy();

		redirect('apps/home');

	}

	public function get_statistics(){



		$where_conditon = array("status"=>"active");

		$details = $this->Apps_model->FetchData('apps','*',$where_conditon,'','DESC');

		$details = array_reverse($details);

		$where_conditon = array("status"=>"active");

		$total = $this->Apps_model->FetchData('apps','sum(users) as total_users,sum(shares) as total_shares',$where_conditon);

		//$this->print_exit($total);

		echo "<table align='center' border='1'>";

		echo "<tr><td>S.no</td><td>App Id</td><td>Name</td><td>Users</td><td>Shares</td></tr>";

		for($i=0;$i<count($details);$i++){

			echo "<tr>";

			echo "<td>".($i+1)."</td>";
            echo "<td>".$details[$i]['id']."</td>";
			echo "<td>".$details[$i]['name']."</td>";

			echo "<td>".$details[$i]['users']."</td>";

			echo "<td>".$details[$i]['shares']."</td>";

			echo "</tr>";

		}

		echo "<tr style='text-align: center;background: green'><td colspan='2' >Total</td><td>".$total[0]['total_users']."</td><td>".$total[0]['total_shares']."</td></tr>";

		echo "</table>";

	}

	public function unique_random_numbers_within_range($min, $max, $quantity) {

		$numbers = range($min, $max);

		shuffle($numbers);

		return array_slice($numbers, 0, $quantity);

	}

	public function get_first_names_old($friends,$limit){

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		if(array_key_exists($user_id,$friends)){

			unset($friends[$user_id]);

		}

		$friends_keys = array_keys($friends);

		$first_names = array();

		for($i=0;$i<$limit;$i++){

			$user_json = file_get_contents("https://graph.facebook.com/$friends_keys[$i]?fields=name&access_token=".$access_token);

			$user_decoded = json_decode($user_json);

			$first_names[$friends_keys[$i]] = explode(" ",$user_decoded->name)[0];

		}

		return $first_names;

	}

    public function get_first_names($friend_final_array){

        $this->refresh_access_token();

        $access_token = $this->session->userdata('access_token');

        $user_id = $this->session->userdata('user_id');

        $first_names = array();

        for($i=0;$i<count($friend_final_array);$i++){

            $user_json = file_get_contents("https://graph.facebook.com/$friend_final_array[$i]?fields=name&access_token=".$access_token);

            $user_decoded = json_decode($user_json);

            $first_names[$friend_final_array[$i]] = explode(" ",$user_decoded->name)[0];

        }

        return $first_names;

    }

	public function get_apps_like_index(){

		$apps = array();

		$key=0;

		for($key;$key<7;$key++)

		{

			$apps[$key]= $this->get_latest_apps_category($key + 1,3);

		}

		$temp= $apps[0];

		$apps[0] = $apps[4];

		$apps[4] = $temp;

		return $apps;

	}

	public function has_access_token($app){

		$access_token = $this->session->userdata("access_token");

		if($access_token==NULL || $access_token=="" || $access_token=='' || strlen($access_token)<1 || $access_token==FALSE || empty($access_token) || !isset($access_token) ){

			$user_data = $this->session->all_userdata();

			foreach ($user_data as $key => $value) {

				if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {

					$this->session->unset_userdata($key);

				}

			}

			$this->session->sess_destroy();

			redirect("app/".strtolower($app[0]->category_name)."/".strtolower(str_replace("?","",str_replace(" ","-",$app[0]->title)))."/".$app[0]->category_id."/".$app[0]->id);

		}

	}

	public function best_friends_from_feed($feed)

	{



		$common_ids = array();



		$facebook_id = $this->session->userdata("facebook_id");



		$likes_arrays_by_feed = array();



		$tags_arrays_by_feed = array();



		$from_arrays_by_feed = array();



		$tags_arrays_offset = 0;



		for ($i = 0; $i < count($feed); $i++) {



			if (isset($feed[$i]->likes)) {



				$likes_arrays_by_feed[$i] = $this->pluck($feed[$i]->likes->data, "id");



			} else {



				$likes_arrays_by_feed[$i] = array();



			}



			if (isset($feed[$i]->with_tags)) {



				if ($from_arrays_by_feed[$i] = $feed[$i]->from->id == $facebook_id) {



					for ($j = 0; $j < count($feed[$i]->with_tags->data); $j++) {



						$tags_arrays_by_feed[$tags_arrays_offset] = $feed[$i]->with_tags->data[$j]->id;



						$tags_arrays_offset++;



					}



				}



			}



			if (isset($feed[$i]->message_tags)) {



				if ($feed[$i]->from->id == $facebook_id) {



					for($j=0;$j<count($feed[$i]->message_tags);$j++) {



						if(isset($feed[$i]->message_tags[$j]->type)){



							if ($feed[$i]->message_tags[$j]->type == 'user') {



								$tags_arrays_by_feed[$tags_arrays_offset] = $feed[$i]->message_tags[$j]->id;



								$tags_arrays_offset++;



							}



						}



					}



				}



			}



			$from_arrays_by_feed[$i] = $feed[$i]->from->id;



		}







		$likes_array = array();



		for ($i = 0; $i < count($likes_arrays_by_feed); $i++) {



			$likes_array = array_merge($likes_array, $likes_arrays_by_feed[$i]);



		}



		$ids_with_likes_count = array_count_values($likes_array);



		$ids_with_from_count = array_count_values($from_arrays_by_feed);



		$ids_with_tags_count = array_count_values($tags_arrays_by_feed);



		arsort($ids_with_likes_count);



		arsort($ids_with_from_count);



		arsort($ids_with_tags_count);







		unset($ids_with_likes_count[$facebook_id]);



		unset($ids_with_from_count[$facebook_id]);



		unset($ids_with_tags_count[$facebook_id]);







		if (!(count($common_ids) >= 10)) {



			if (count($ids_with_tags_count) > count($common_ids)) {



				$commonids_tags_and_commonids = array_diff_key($ids_with_tags_count, $common_ids);



				$common_ids = $common_ids + $commonids_tags_and_commonids;



			}



		}



		if (!(count($common_ids) >= 10)) {



			if (count($ids_with_from_count) > count($common_ids)) {



				$array_diff = array_diff_key($ids_with_from_count, $common_ids);



				$common_ids = $common_ids + $array_diff;



			}



		}



		arsort($common_ids);



		if (!(count($common_ids) >= 10)) {



			if (count($ids_with_likes_count) > count($common_ids)) {



				$array_diff = array_diff_key($ids_with_likes_count, $common_ids);



				arsort($array_diff);



				$common_ids = $common_ids + $array_diff;



			}



		}



		return $common_ids;

	}

	public function crop_pic2($src,$dest,$desired_width,$desired_height){

		/* read the source image */

		$source_image = imagecreatefromjpeg($src);

		$width = imagesx($source_image);

		$height = imagesy($source_image);



		/* find the "desired height" of this thumbnail, relative to the desired width  */

		//$desired_height = floor($height * ($desired_width / $width));



		/* create a new, "virtual" image */

		$virtual_image = imagecreatetruecolor($desired_width, $desired_height);



		/* copy source image at a resized size */

		imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);



		/* create the physical thumbnail image to its destination */

		imagejpeg($virtual_image, $dest,100);

	}

	public function merge_images_jpg_png($picture_one, $picture_two, $filename_result, $left, $top)

	{

		// Get dimensions for specified images

		list($width_x, $height_x) = getimagesize($picture_one);

		list($width_y, $height_y) = getimagesize($picture_two);

		// Create new image with desired dimensions

		$image = imagecreatetruecolor($width_x, $height_x);

		// Load images and then copy to destination image

		$image_x = imagecreatefromjpeg($picture_one);

		$image_y = imagecreatefrompng($picture_two);

		imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);                     //  top, left, border,border

		imagecopy($image, $image_y, $left, $top, 0, 0, $width_y, $height_y);

		// Save the resulting image to disk (as JPEG)

		imagejpeg($image, $filename_result, 80);

		// Clean up

		imagedestroy($image);

		imagedestroy($image_x);

		imagedestroy($image_y);

	}

	public function get_user_feed()

	{

		$this->refresh_access_token();



		$access_token = $this->session->userdata("access_token");



		//$feed_json = @file_get_contents("https://graph.facebook.com/me?fields=feed.limit(200).until(1483142460){id,name,type,from,status_type,with_tags,message_tags,likes.limit(1000).summary(true){id}}&access_token=" . $access_token);

		$feed_json = @file_get_contents("https://graph.facebook.com/me?fields=feed.limit(200).since(1483142460){id,name,type,from,status_type,with_tags,message_tags,likes.limit(1000).summary(true){id}}&access_token=" . $access_token);

		$feed_decoded = json_decode($feed_json);



		return $feed_decoded;

	}

	public function get_user_location()

	{

		$this->refresh_access_token();

		$access_token = $this->session->userdata("access_token");

		$feed_json = file_get_contents("https://graph.facebook.com/me?fields=photos.limit(50){place}&access_token=" . $access_token);

		$feed_decoded = json_decode($feed_json);

		return $feed_decoded;

	}

	public function get_photos($photo_ids){

		$this->refresh_access_token();

		$access_token = $this->session->userdata('access_token');

		$user_id = $this->session->userdata('user_id');

		for($i=0;$i<count($photo_ids);$i++){

			$picture = file_get_contents("https://graph.facebook.com/$photo_ids[$i]/picture?redirect=false&height=200&width=200"."&access_token=".$access_token);

			if (!file_exists("container/user-photos/".$user_id."")) {

				mkdir("container/user-photos/".$user_id."", 0777, true);

			}

			$this->save_image(json_decode($picture)->data->url,"container/user-photos/".$user_id.""."/$photo_ids[$i].jpg");

		}

	}

	public function pic_of_year($photos,$user_id){

		$own_pics = array();

		for($i=0;$i<count($photos->data);$i++){

			if($photos->data[$i]->from->id == $user_id){

				if(isset($photos->data[$i]->likes)){

					$own_pics[$photos->data[$i]->id] = $photos->data[$i]->likes->summary->total_count;

				}else{

					$own_pics[$photos->data[$i]->id] = 0;

				}



			}

		}

		if(count($own_pics)>0){

			arsort($own_pics);

		}else{

			for($i=0;$i<count($photos->data);$i++){

				if(isset($photos->data[$i]->likes)){

					$own_pics[$photos->data[$i]->id] = $photos->data[$i]->likes->summary->total_count;

				}else{

					$own_pics[$photos->data[$i]->id] = 0;

				}

			}

			arsort($own_pics);

		}

		return $own_pics;

	}

	public function best_friends_from_likes($feed)

	{



		$common_ids = array();

		$facebook_id = $this->session->userdata("facebook_id");

		$likes_arrays_by_feed = array();

		for ($i = 0; $i < count($feed); $i++) {

			if (isset($feed[$i]->likes)) {

				$likes_arrays_by_feed[$i] = $this->pluck($feed[$i]->likes->data, "id");

			} else {

				$likes_arrays_by_feed[$i] = array();

			}

		}



		$likes_array = array();

		for ($i = 0; $i < count($likes_arrays_by_feed); $i++) {

			$likes_array = array_merge($likes_array, $likes_arrays_by_feed[$i]);

		}

		$ids_with_likes_count = array_count_values($likes_array);

		arsort($ids_with_likes_count);

		unset($ids_with_likes_count[$facebook_id]);

		arsort($common_ids);

		if (!(count($common_ids) >= 10)) {

			if (count($ids_with_likes_count) > count($common_ids)) {

				$array_diff = array_diff_key($ids_with_likes_count, $common_ids);

				arsort($array_diff);

				$common_ids = $common_ids + $array_diff;

			}

		}

		return $common_ids;

	}



	public function update_shared_pic($app_id,$path){

		$where_conditions = array("app_id"=>$app_id,"imagepath"=>$path);

		$data = array("status"=>"active");

		$this->Apps_model->UpdateData("generated_images",$data,$where_conditions);

	}

	public function add_new_app(){



		if($this->input->post('submit'))	{

			$post_data = 	$this->input->post();

			$data = array("category_id"=>$post_data['category_id'],"name"=>$post_data['title'],"title"=>$post_data['title'],

				"picture_dimensions"=>$post_data["picture_dimensions"],"picture_position"=>$post_data['picture_position'],

				"logic"=>'generic_function',"meta_keywords"=>$post_data['meta_keywords'],

				"meta_title"=>$post_data["meta_title"],"meta_description"=>$post_data["meta_description"],"status"=>$post_data["status"]);

			//$this->print_exit($data);

			$last_insert_id = $this->Apps_model->InsertData('apps',$data);

			$temp_inputs_images = glob("container/temp-inputs/*.jpg");

			if (!file_exists("container/input-images/" . $last_insert_id)) {

				mkdir("container/input-images/" . $last_insert_id, 0777, true);

			}

			$i=1;



			foreach($temp_inputs_images as $image)

			{

				$size = getimagesize($image);

				$this->compress_images($image,"container/input-images/$last_insert_id/$i.jpg",$size[0],$size[1]);

				unlink($image);

				$i++;

			}

			$data['maxvalue'] = count($temp_inputs_images);

			$data['app_id'] = $last_insert_id;

			//$data['profile_pic_type'] = $post_data['profile_pic_type'];

			//$data['picture_position'] = $post_data['picture_position'];

			$this->load->view('display_all_images',$data);

		}else{

			$this->load->view('add_new_app');

		}

	}

	public function select_cover_pic($app_id,$image_number,$profile_pic_type,$picture_postion){

		$position = explode("X",$picture_postion);

		$this->merge_images("container/input-images/" . $app_id . "/$image_number.jpg", "assets/imgs/" .$profile_pic_type, "container/input-images/" . $app_id . "/cover_pic.jpg", $position[0], $position[1]);

		$this->crop_pic2("container/input-images/" . $app_id . "/" . "cover_pic.jpg", "container/input-images/" . $app_id . "/" . "cover_pic_thumb.jpg", 315,160);

	}

	public function compress_images($src, $dest, $desired_width,$desired_height) {

		/* read the source image */

		$source_image = imagecreatefromjpeg($src);

		$width = imagesx($source_image);

		$height = imagesy($source_image);



		/* find the "desired height" of this thumbnail, relative to the desired width  */

		// $desired_height = floor($height * ($desired_width / $width));



		/* create a new, "virtual" image */

		$virtual_image = imagecreatetruecolor($desired_width, $desired_height);



		/* copy source image at a resized size */

		imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);



		/* create the physical thumbnail image to its destination */

		imagejpeg($virtual_image, $dest,80);

	}

	public function careers(){
        $data['poll_id']=1;
        
		
		$this->load->view('careers',$data);

	}

	public function get_merged_images($picture_one, $picture_two, $filename_result, $left, $top)

	{

		list($width_x, $height_x) = getimagesize($picture_one);

		list($width_y, $height_y) = getimagesize($picture_two);

		$image = imagecreatetruecolor($width_x, $height_x);

		$image_x = imagecreatefromjpeg($picture_one);

		$image_y = imagecreatefromjpeg($picture_two);

		imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);                     //  top, left, border,border

		imagecopy($image, $image_y, $left, $top, 0, 0, $width_y, $height_y);

		return $image;

	}

	public function get_merged_images_png($picture_one, $picture_two, $filename_result, $left, $top)

	{

		list($width_x, $height_x) = getimagesize($picture_one);

		list($width_y, $height_y) = getimagesize($picture_two);

		$image = imagecreatetruecolor($width_x, $height_x);

		$image_x = imagecreatefromjpeg($picture_one);

		$image_y = imagecreatefrompng($picture_two);

		imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);                     //  top, left, border,border

		imagecopy($image, $image_y, $left, $top, 0, 0, $width_y, $height_y);

		return $image;

	}

        public function get_output_path($app_id,$user_id){

             $date =  date('y-m-d');          

             $date = explode("-", $date);

            return "container/output-images/$date[0]/$date[1]/$date[2]/$app_id/$user_id";

        }

}