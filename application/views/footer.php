<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112308726-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-112308726-2');
</script>
<footer class="footer">
          <div class="container-fluid clearfix">
              <p style="font-size:10px"><b>Disclaimer:</b> Each and every logo, company name, website name or product name displayed on this poll or in any page of website are trademarks and properties of their respective companies. Photos used in polls are just for entertainment purpose, none of them are promoting Witalks Polls.
</p>
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
              <a href="http://witalks.com/" target="_blank">Witalks.com</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"><a href="<?php echo base_url()?>contact">Contact Us</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo "https://witalks.com/apps/privacy-policy"?>">Privacy Policy</a>
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>