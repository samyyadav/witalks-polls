<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta property="og:title" content="Language | Funny Images and Statuses for Whatsapp and Facebook | Brochill" />

  <title>Language | Funny Images and Statuses for Whatsapp and Facebook | Brochill</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url()?>vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url()?>vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
  <meta name="google-site-verification" content="LeJOSAfWoj8eSAhYytDnPffzzoaztLcsyc5jrbbcmcs" />
  <script>
      var BASE_URL="<?php echo base_url()?>";
  </script>
  <script src="<?php echo base_url()?>vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.cookie.js"></script>
  <script src="<?php echo base_url()?>assets/js/custom.js"></script>
  <script>
      var selected_language=$.cookie('language');
if(selected_language&&selected_language!=""){
    window.location=BASE_URL+"trending/"+selected_language;
}
  </script>
  
</head>

<body>
  <div class="main-panel">
        <div class="content-wrapper">
          <h4 class="text-center">
              Select Your Language
          </h4>
          <hr>
          <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card" onclick="save_language('telugu')">
              <div class="card card-statistics" style="background-color:#48adf0;cursor:pointer;color:white">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <img style="height:60px" src="<?php echo base_url()?>assets/icons/login_telugu.png">
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Telugu</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">తెలుగు</h3>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card" onclick="save_language('tamil')">
              <div class="card card-statistics" style="background-color:#77c356;cursor:pointer;color:white">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <img style="height:60px" src="<?php echo base_url()?>assets/icons/login_tamil.png">
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Tamil</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">தமிழ்</h3>
                      </div>
                    </div>
                  </div>
                 
                </div>
              </div>
            </div>
            
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card"onclick="save_language('kannada')">
              <div class="card card-statistics" style="background-color:#6260bb;cursor:pointer;color:white">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <img style="height:60px" src="<?php echo base_url()?>assets/icons/login_kannada.png">
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Kannada</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">ಕನ್ನಡ</h3>
                      </div>
                    </div>
                  </div>
                 
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card"onclick="save_language('english')">
              <div class="card card-statistics" style="background-color:#77c356;cursor:pointer;color:white">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <img style="height:60px" src="<?php echo base_url()?>assets/icons/login_hindi.png">
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">English</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">English</h3>
                      </div>
                    </div>
                  </div>
                 
                </div>
              </div>
            </div>
            
          </div>
          
       
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        
        <!-- partial -->
      </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  
  
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo base_url()?>assets/js/misc.js"></script>
  <!-- endinject -->
 
  
  <!-- End custom js for this page-->
</body>

</html>