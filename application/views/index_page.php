<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
  <?php $this->view('header'); ?>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<style>
.body { margin-top:0px; }
.panel-body:not(.two-col) { padding:0px }
.glyphicon { margin-right:0px; }
.glyphicon-new-window { margin-left:0px; }
.panel-body .radio,.panel-body .checkbox {margin-top: 0px;margin-bottom: 0px;}
.panel-body .list-group {margin-bottom: 0;}
.margin-bottom-none { margin-bottom: 0; }
.panel-body .radio label,.panel-body .checkbox label { display:block; }
.link_copied {
    background: #a94442;
    color: white;
    padding: 5px;
    border-radius: 18px;
}
</style>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->view('navbar'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper" style="padding-top: 2px;">
      <!-- partial:partials/_sidebar.html -->
      <?php $this->view('sidebar'); ?>
      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper" style="padding: 5px;" style="background: white">
            <div class="row" style="background: white">
            <div class="col-lg-12 grid-margin">
                <div class="card">
                <div class="card-body" style="padding:20px">
                    Total Votes:&nbsp;<b style="color:red;font-size:16px"><?php echo $total_vote_count?></b><h2>Latest Polls</h2><img style="margin-top: -40px;width: 80px;margin-left: 180px;" class="img-responsive"src="<?php echo base_url()."assets/imgs/live-online.gif"?>" />
                    
                    <hr>
                    <p style="font-size:10px;">Witalks Polls is not endorsed, sponsored, administered or associated with or by Facebook. Facebook is no way associated with Witalks Polls.</p>
                    <br>
                    <!-- <p style="font-size:13px;margin-right:0px;margin-left:0px;"><b>This polling is closed as per Election Commission rule.Your voting will not be considered from now</b></p> -->
					<div style="margin-top:12px"></div>
                    
           <div class="row" style="background: white;margin-left:-30px;margin-right:-30px">
            <?php for($i=0;$i<3;$i++){
            if($latest_polls[$i]->is_latest){
            ?>
                
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card" style="padding: 0px;" >
              <div class="card card-statistics">
                  
                <div class="card-body" style="padding-top: 0px;padding-bottom: 0px;">
                    Votes:<b style="color:red"> <?php echo $total_count[$i]?></b>
                    <br>
                  <div class="clearfix">
                    <a style="text-decoration:none" href="<?php echo base_url()?>polls/<?php echo $latest_polls[$i]->slug ?>/<?php echo $latest_polls[$i]->id?>">
                    <img style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);border: 1px solid #ddd;border-radius: 12px;padding: 5px;"class="img-responsive" src="<?php echo base_url()?>container/input-images/<?php echo $latest_polls[$i]->id?>/cover_pic.jpg"></img>
                  </div> 
                  <p class="mt-3 mb-0">
                     <h4 style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);text-align: center;background-image: linear-gradient(#0756a9, #043c6b);text-shadow: 1px 1px black;color: #f9f9f9;border-radius: 5px;font-weight: bold;padding:6px"><?php echo $latest_polls[$i]->title?></h4>
                  </p>
                </div>
                </a>
              </div>
            </div>
            <?php }}?>  
            </div>
				<hr>
                </div>
              </div>
              <div class="card">
                <div class="card-body" style="padding:20px">
                    <h2>Trending Polls</h2>
                    <hr>
                    <br>
                    <!-- <p style="font-size:13px;margin-right:0px;margin-left:0px;"><b>This polling is closed as per Election Commission rule.Your voting will not be considered from now</b></p> -->
					<div style="margin-top:12px"></div>
                    
           <div class="row" style="background: white;margin-left:-30px;margin-right:-30px">
            <?php for($i=0;$i<3;$i++){?>
                
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card" style="padding: 0px;" >
              <div class="card card-statistics">
                  
                <div class="card-body" style="padding-top: 0px;padding-bottom: 0px;">
                    Total Votes:<b style="color:red"> <?php echo $trending_polls[$i]->attempts_count?></b>
                    <br>
                  <div class="clearfix">
                    <a style="text-decoration:none" href="<?php echo base_url()?>polls/<?php echo $trending_polls_data[$i]->slug ?>/<?php echo $trending_polls[$i]->poll_id?>">
                    <img style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);border: 1px solid #ddd;border-radius: 12px;padding: 5px;" class="img-responsive" src="<?php echo base_url()?>container/input-images/<?php echo $trending_polls[$i]->poll_id?>/cover_pic.jpg"></img>
                    
                  </div>
                  <p class="mt-3 mb-0">
                     <h4 style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);text-align: center;background-image: linear-gradient(#e82564, #a20841);text-shadow: 1px 1px black;color: #f9f9f9;border-radius: 5px;font-weight: bold;padding:6px"><?php echo $trending_polls_data[$i]->title?></h4>
                  </p>
                </div>
                </a>
              </div>
            </div>
            <?php }?>  
            </div>
				<hr>
                </div>
              </div>
            </div>
          </div>
          <div class="row" style="background: white">
            <div class="col-lg-12 grid-margin"style="padding: 10px;">
                
              
              <div class="row">
            <?php for($i=0;$i<count($polls);$i++){?>
                
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card" style="padding: 0px;" >
              <div class="card card-statistics">
                  
                <div class="card-body" style="padding-top: 0px;padding-bottom: 0px;">
                    Votes:<b style="color:red"> <?php echo $total_count[$i]?></b>
                    <br>
                  <div class="clearfix">
                    <a style="text-decoration:none" href="<?php echo base_url()?>polls/<?php echo $polls[$i]->slug ?>/<?php echo $polls[$i]->id?>">
                    <img style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);border: 1px solid #ddd;border-radius: 12px;padding: 5px;"class="img-responsive" src="<?php echo base_url()?>container/input-images/<?php echo $polls[$i]->id?>/cover_pic.jpg"></img>
                  </div> 
                  <p class="mt-3 mb-0">
                     <h4 style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);text-align: center;background-image: linear-gradient(#0756a9, #043c6b);text-shadow: 1px 1px black;color: #f9f9f9;border-radius: 5px;font-weight: bold;padding:6px"><?php echo $polls[$i]->title?></h4>
                  </p>
                </div>
                </a>
              </div>
            </div>
            <?php }?>  
            </div>
          
            <div class="row">
               <div class="col-md-6">
                    <div class="card-body">
                      <div class="template-demo">
                        
                        
                      </div>
                      <div class="template-demo">
                          
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <?php 
                            for($k=0;$k<$total_polls_count/10;$k++){?>
                                    <a style="text-decoration:none"href="?page=<?php echo $k+1?>">
                                    <button type="button" class="btn btn-light active"><?php echo $k+1?></button>&nbsp;
                                    </a>
                           <?php }
                            ?>
                          
                          
                        </div>
                        
                      </div>
                      
                      
                    </div>
                  </div>
                  
                </div>
          </div>
          
          <!--<div class="template-demo">
            <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" class="btn btn-primary">1</button>
                          <button type="button" class="btn btn-primary">2</button>
                          <button type="button" class="btn btn-primary">3</button>
                        </div>
                        </div>
                        -->
          
        </div>
        
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <?php $this->view('footer'); ?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  
</body>

</html>