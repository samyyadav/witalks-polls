<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="<?php echo base_url()?>">
          <img style="height: 55px; min-width: 207px !important;"src="<?php echo base_url() ?>assets/imgs/logo.png" alt="logo" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="<?php echo base_url()?>">
          <img style="height: 55px; min-width: 207px !important;"src="<?php echo base_url() ?>assets/imgs/logo.png" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
          
          
          
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            
            
          </li>
          <li class="nav-item dropdown">
            
          </li>
          <li class="nav-item dropdown d-none d-xl-inline-block">
            
            
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>&nbsp;<b style="font-size:18px">Menu</b>
        </button>
      </div>
    </nav>