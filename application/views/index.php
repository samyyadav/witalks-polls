<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Star Admin Free Bootstrap Admin Dashboard Template</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url()?>vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url()?>vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand img-responsive" href="">
          <img src="<?php echo base_url()?>assets/imgs/bro.jpg" alt="logo" />
        </a>
        
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              
              
            </a>
            
          </li>
          
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <?php if($this->agent->is_mobile()){ ?>
          <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            
          </li>
          
          
          <li class="nav-item">
            <?php for ($i=0;$i<count($category_data);$i++){ ?>
            <a class="nav-link" href="<?php echo base_url()?>category/<?php echo $category_data[$i]->name ?>/<?php echo $category_data[$i]->id?>">
            
              <i class="menu-icon mdi mdi-restart"></i>
              <span class="menu-title"><?php echo $category_data[$i]->name ?></span>
              
            </a>
            <?php }?>
            
          </li>
        </ul>
      </nav>
     <?php }?>
      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper" style="padding: 9px;">
          <div class="row">
            <div class="col-lg-12 grid-margin">
                
              <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><mark>Welcome</mark> to BroChill home</h4>
              
                </div>
              </div>
            </div>
          </div>
          
           <div class="row">
               <?php for($i=0;$i<count($category_data);$i++){?>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 grid-margin stretch-card">
                        <div class="card card-statistics" >
                            <div class="card-body" style="background-color:red;background-image: url('<?php echo base_url()?>container/default.jpg');">
                              <div class="clearfix">
                                  
                                <div class="float-center">
                                  <a href="<?php echo base_url()."category/".$category_data[$i]->slug."/".$category_data[$i]->id ?>" style="text-decoration:none;color:white">
                                     <?php echo $category_data[$i]->name ?> 
                                   </a>
                            
                          
                                </div>
                                
                             </div>
                              
                    </div>
              </div>
            </div>
               <?php }?>
            
           
            
            
          </div>
          
          
        </div>
        
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
              <a href="http://brochill.com/" target="_blank">BroChill</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="<?php echo base_url()?>vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url()?>vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo base_url()?>assets/js/off-canvas.js"></script>
  <script src="<?php echo base_url()?>assets/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="<?php echo base_url()?>assets/js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>

</html>