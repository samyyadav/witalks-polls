<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->view('header'); ?>
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->view('navbar'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper" style="padding-top: 40px;">
      <!-- partial:partials/_sidebar.html -->
      <?php if($this->agent->is_mobile()){ ?>
          <?php //$this->view('sidebar'); ?>
     <?php }?>
      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper" style="padding: 9px;">
          <div class="row">
            <div class="col-lg-12 grid-margin">
                
              
              <div class="row">
            
                
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                  
                  </div>
                  <b>Write to us: </b><br>
                    witalks2014@gmail.com, rashmi.iiit@gmail.com 
                    <br>

                    <b>Inbox us: </b><br>
                    facebook.com/witalks 
                    <br>
                    
                    <b>Address: </b><br>
                    Drinking Cloud Technologies Private Limited, Flat No: 201,<br> 
                    Suvama Habitat,JaiHind Layout, <br>
                    Madhapur, Hyderabad - 500081, India <br>
                    
                    <b>Join us: </b><br>
                    Wanna be Part of Witalks Team? If you want to work with us full-time or part-time(doing whatever you are, but still exploring your creative side), <br>
                    or even if you want to give some crazy app ideas to the team, <br>
                    then shoot us a mail at witalks2014@gmail.com or inbox us
                    

                  
                </div>
                </a>
              </div>
            </div>
             
            </div>
          </div>
          
          <!--<div class="template-demo">
            <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" class="btn btn-primary">1</button>
                          <button type="button" class="btn btn-primary">2</button>
                          <button type="button" class="btn btn-primary">3</button>
                        </div>
                        </div>
                        -->
          
        </div>
        
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <?php $this->view('footer'); ?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  
</body>

</html>