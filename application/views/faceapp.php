<?php
/**
 * Created by PhpStorm.
 * User: Nariman
 * Date: 17/03/2018
 * Time: 10:17 AM
 */

class FaceApp
{
    private
        $default_api_host = 'https://ph-x.faceapp.io/api/v2.11/photos',
        $default_user_agent = 'FaceApp/2.0.957 (Linux; Android 4.4)',
        $device_id_length = 8,
        $devide_id_letters = '',
        $header_options = [],
        $PhotoOptions = NULL,
        $Photo = NULL,
        $proxyHost = NULL,
        $proxyPort = NULL;

    public function __construct(string $photo_path, bool $turnOnProxy = false)
    {
        $this->devide_id_letters = substr(join('', range('a', 'z')),
            rand(0, 25 - $this->device_id_length),
            $this->device_id_length);
        $this->header_options = [
            'User-Agent:' . $this->default_user_agent,
            'X-FaceApp-DeviceID:' . $this->devide_id_letters
        ];
        $this->PhotoOptions = $this->UploadPhoto($photo_path);
        if ($turnOnProxy == true) {
            $this->setProxy();
        }
    }

    public function getPhotoCode()
    {
        return (!empty($this->PhotoOptions->code)) ? $this->PhotoOptions->code : null;
    }

    private function setProxy()
    {
        $proxys = [
	["144.217.83.77", "3128"],
    ["67.205.178.183","55555"],	
    ["178.19.181.197","41258"],
    ["199.119.1.133","80"],	
    ["203.170.66.21","21776"],
    ["80.83.26.112","21231"],
    ["46.172.229.54","21231"],
    ["92.38.45.74","21231"],
    ["92.50.142.70","8080"],
    ["188.241.105.194","21231"],
    ["81.24.91.54","41258"],
    ["177.73.196.99","53281"],
    ["82.198.187.68","21231"],
    ["138.118.84.212","53281"],
    ["82.160.138.199","8080"],
    ["79.101.33.118","8080"],
    ["78.108.108.192","21231"],	
    ["191.176.146.144","21776"],
    ["194.12.106.58","21231"],
    ["140.227.54.92","3128"],
    ["212.90.16.198","21776"],
    ["78.111.112.50","21231"],
    ["185.142.211.11","21231"],	
    ["138.118.86.120","53281"],	
    ["195.182.202.8","21231"],	
    ["176.60.208.128","21231"],	
    ["94.247.2.70", "21231"],
    ["217.156.116.183","21231"],
    ["92.247.2.70","21231"],
    ["213.111.246.231","21231"],
    ["190.60.103.178","21231"],
    ["213.85.18.146","21231"],
    ["94.130.191.71","3128"],
    ["185.29.159.198","21231"],
    ["212.14.228.78","21231"],
    ["35.233.225.185","8080"],
    ["35.199.103.59","80"],
    ["185.189.103.60 ","21231"],
    ["92.247.2.70 ","21231"],
    ["212.24.153.198 ","21231"],
    ["95.67.92.156","46229"],
    ["176.57.93.129","21231"]

];
        $selectedProxy = $proxys[array_rand($proxys, 1)];
        $this->proxyHost = $selectedProxy[0];
        $this->proxyPort = $selectedProxy[1];
    }

    public function getFilters()
    {
        if (!empty($this->PhotoOptions->filters)) {
            return array_map(function ($array) {
                return $array[0];
            }, $this->PhotoOptions->filters);
        } else {
            return null;
        }
    }

    public function savePhoto(string $photo_path)
    {

        if (!empty($this->Photo)) {
            file_put_contents($photo_path, $this->Photo);
            return true;
        } else {
            throw new \Exception("No Photo Has Been Created!\nFirst Run Apply_Filter() On Photo.");
        }
    }

    private function uploadPhoto($photo_path)
    {
        $this->setProxy();
        if (!file_exists($photo_path)) throw new \Exception("Photo Not Find!");
        if (!getimagesize($photo_path)) throw new \Exception("Input File Is NOT Photo!");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->header_options);
        curl_setopt($ch, CURLOPT_URL, $this->default_api_host);
        if (!empty($this->proxyHost)) {
            
            curl_setopt($ch, CURLOPT_PROXY, $this->proxyHost);
            curl_setopt($ch, CURLOPT_PROXYPORT, $this->proxyPort);
        }
        
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['file' => new \CURLFile($photo_path)]);
        $response = json_decode(curl_exec($ch));
       
        if (empty($response)) exit("no response...!");
        if (isset($response->err)){
            
            throw new \Exception($response->err->desc);
        }
            
        else {
            function getFilter_id($array)
            {
                $croped = (isset($array->only_cropped) && $array->only_cropped == 1) ? 1 : 0;
                return [$array->id, $croped];
            }

            $code = $response->code;
            $free_filters = array_map("getFilter_id", $response->objects[0]->children);
            $pro_filters = array_map("getFilter_id", $response->objects[1]->children);
            $filters = array_merge($free_filters, $pro_filters);
            return (object)['code' => $code, 'filters' => $filters];
        }
    }

    public function applyFilter(string $photoCode, string $filter, bool $crop = false)
    {
        $filters = array_map(function ($array) {
            return $array[0];
        }, $this->PhotoOptions->filters);
        if (!in_array($filter, $filters)) throw new Exception('Filter Not Found!');
        $arraykey = array_search($filter, $filters);

        if ($crop == false && $this->PhotoOptions->filters[$arraykey][1] == true)
            $crop = true;
        $crop = ($crop == true) ? 'true' : 'false';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->header_options);
        if (!empty($this->proxyHost)) {
            curl_setopt($ch, CURLOPT_PROXY, $this->proxyHost);
            curl_setopt($ch, CURLOPT_PROXYPORT, $this->proxyPort);
        }
        curl_setopt($ch, CURLOPT_URL, $this->default_api_host . "/" . $photoCode . "/filters/" . $filter . "?cropped=" . $crop);
        $res = curl_exec($ch);
        if (curl_getinfo($ch)["http_code"] == 200) {
            $this->Photo = $res;
        } else {
            switch (curl_getinfo($ch)["http_code"]) {
                case 400 :
                    throw new Exception('Photo Must be Cropped', 400);
                    break;
                case 404 :
                    throw new Exception('Filter Not Found!', 404);
                    break;
                case 402 :
                    throw new Exception('Filter is not Free!', 402);
                    break;
                default :
                    throw new Exception('Unknown Error : ' . curl_getinfo($ch)["http_code"], curl_getinfo($ch)["http_code"]);
            }
        }

    }
}
?>