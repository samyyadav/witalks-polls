<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
  <?php $this->view('header'); ?>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<style>

    .body { margin-top:0px; }
.panel-body:not(.two-col) { padding:0px }
.glyphicon { margin-right:0px; }
.glyphicon-new-window { margin-left:0px; }
.panel-body .radio,.panel-body .checkbox {margin-top: 0px;margin-bottom: 0px;}
.panel-body .list-group {margin-bottom: 0;}
.margin-bottom-none { margin-bottom: 0; }
.panel-body .radio label,.panel-body .checkbox label { display:block; }
.link_copied {
    background: #a94442;
    color: white;
    padding: 5px;
    border-radius: 18px;
}
</style>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->view('navbar'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper" style="padding-top: 2px;">
      <!-- partial:partials/_sidebar.html -->
      <?php if($this->agent->is_mobile()){ ?>
          <?php //$this->view('sidebar'); ?>
     <?php }?>
      
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper" style="padding: 5px;">
          <div class="row">
            <div class="col-lg-12 grid-margin"style="padding: 0px;">
                
              
              <div class="row">
            <?php for($i=0;$i<count($polls);$i++){?>
                
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card" style="padding: 0px;" >
              <div class="card card-statistics">
                  
                <div class="card-body" style="padding-top: 0px;padding-bottom: 0px;">
                    Votes:<b style="color:red"> <?php echo $total_count[$i]?></b>
                    <br>
                  <div class="clearfix">
                    <a style="text-decoration:none" href="<?php echo base_url()?>polls/<?php echo $polls[$i]->slug ?>/<?php echo $polls[$i]->id?>">
                    <img style="border: 1px solid #ddd;border-radius: 12px;padding: 5px;" class="img-responsive" src="<?php echo base_url()?>container/input-images/<?php echo $polls[$i]->id?>/cover_pic.jpg"></img>
                    
                  </div>
                  <p class="mt-3 mb-0">
                     <h4 style="text-align: center;background: #093850;color: #f9f9f9;border-radius: 12px;font-weight: bold;padding:6px"><?php echo $polls[$i]->title?></h4>
                  </p>
                </div>
                </a>
              </div>
            </div>
            <?php }?>  
            </div>
          </div>
          
          <!--<div class="template-demo">
            <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" class="btn btn-primary">1</button>
                          <button type="button" class="btn btn-primary">2</button>
                          <button type="button" class="btn btn-primary">3</button>
                        </div>
                        </div>
                        -->
          
        </div>
        
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <?php $this->view('footer'); ?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  
</body>

</html>