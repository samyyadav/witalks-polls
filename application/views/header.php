<!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php if(isset($page_name)){
        if($page_name=="index_page"){ echo "Welcome to Online Polls | Daily Political Sports and Other Polls and Results | Witalks Polls" ;}
        else if($page_name=="polling_page"){ echo $polls[0]->title." | Witalks Polls" ;}
        else if($page_name=="poll_result_page"){ echo $polls[0]->title." | Witalks Polls"; }
    } else{ echo "Welcome to Online Polls | Daily Political Sports and Other Polls and Results | Witalks Polls";} ?></title>
  <!-- plugins:css -->
  <meta name="description" content="Index">
    <meta name="keywords" content="index">
    <meta name="robots" content="index, follow">
    <meta property="og:image" content="<?php if(isset($page_name)){
        if($page_name=="polling_page"){if(isset($poll_id)){if($poll_id==4){echo base_url()."container/input-images/".$poll_id."/cover_pic2.jpg";}else{echo base_url()."container/input-images/".$poll_id."/cover_pic.jpg";}}else{echo base_url()."container/input-images/".$poll_id."/cover_pic.jpg";} }
        else if($page_name=="poll_result_page"){ echo base_url()."container/input-images/".$poll_id."/cover_pic.jpg"; }
    } else{ echo base_url()."container/input-images/4/cover_pic2.jpg";} ?>"/>
    <meta name="google-site-verification" content="LeJOSAfWoj8eSAhYytDnPffzzoaztLcsyc5jrbbcmcs" />
    <meta property="og:title" content="<?php if(isset($page_name)){
        if($page_name=="index_page"){ echo "Welcome to Online Polls | Daily Political Sports and Other Polls and Results | Witalks Polls" ;}
        else if($page_name=="polling_page"){ echo $polls[0]->title." | Witalks Polls" ;}
        else if($page_name=="poll_result_page"){ echo $polls[0]->title." | Witalks Polls"; }
    } else{ echo "Welcome to Online Polls | Daily Political Sports and Other Polls and Results | Witalks Polls";} ?>" />
    <meta property="og:description" content="Click here to Poll your vote" />
    <!--<meta property="og:url" content="https://brochill.com"/> -->
    
  <link rel="stylesheet" href="<?php echo base_url()?>vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>vendors/css/vendor.bundle.base.css">
  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/styles.min.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/imgs/favicon.png" />
  <script> var BASE_URL="<?php echo base_url()?>";</script>
  
  <script src="<?php echo base_url()?>assets/js/bundle.libs.js"></script>
  <script src="<?php echo base_url()?>assets/js/custom.js"></script>
  
  <link rel="manifest" href="/manifest.json" />
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
  var OneSignal = window.OneSignal || [];
  OneSignal.push(function() {
    OneSignal.init({
      appId: "663811c9-faea-46d3-90a5-0b28c9e87d51",
    });
  });
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-57096935-7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-57096935-7');
</script>


  
  
  