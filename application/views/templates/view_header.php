<html>

<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>
        <?php echo $meta_tags[0]->meta_title; ?>
    </title>
    <link rel="icon" href="<?php echo base_url()?>assets/imgs/logo-letter.png" type="image/png">
    <meta name="description" content="<?php echo $meta_tags[0]->meta_description; ?>">
    <meta name="keywords" content="<?php echo $meta_tags[0]->meta_keywords; ?>">
    <meta name="robots" content="index, follow">
    <meta property="fb:app_id" content="177097512640235" />
    <meta property="og:image" content="<?php if (isset($page)) {
        if ($page == 'view_apps') {
          if($this->input->get('ouid')){
                     $date = explode("-", $this->input->get('ouid'));
                     if (count($date) > 2) {
                         
                         echo base_url() . "container/output-images/" . $app[0]->id . "/"."$date[0]/$date[1]/$date[2]/".$this->input->get('ouid').".jpg";

                     }else{
                                                 echo base_url() . "container/output-images/" . $app[0]->id . "/".$this->input->get('ouid').".jpg";
 
                     }
                     
            }elseif($this->input->get('arip')){
                $incoming_app = $this->uri->segment(5);
                
                $date = explode("-", $this->input->get('arip'));
                     if (count($date) > 2) {
                         
                         echo "http://ts.appsrgv.com/container/output-images/$incoming_app/"."$date[0]/$date[1]/$date[2]/".$this->input->get('arip').".jpg";

                     }else{
                                                 echo "http://ts.appsrgv.com/container/output-images/$incoming_app/".$this->input->get('arip').".jpg";
 
                     }
            }
            else{
                            echo base_url() . "container/input-images/" . $app[0]->id . "/cover_pic.jpg";
            }
        } elseif ($page == 'view_output') {
            if($this->input->get('arip')){
                $incoming_app = $this->uri->segment(6);
                
                $date = explode("-", $this->input->get('arip'));
                     if (count($date) > 2) {
                         
                         echo "http://ts.appsrgv.com/container/output-images/$incoming_app/"."$date[0]/$date[1]/$date[2]/".$this->input->get('arip').".jpg";

                     }else{
                                                 echo "http://ts.appsrgv.com/container/output-images/$incoming_app/".$this->input->get('arip').".jpg";
 
                     }
            }elseif($this->input->get('brip')){
                $incoming_app = $this->uri->segment(6);
                
                $date = explode("-", $this->input->get('brip'));
                     if (count($date) > 2) {
                         
                         echo "http://premam.in/container/output-images/$incoming_app/"."$date[0]/$date[1]/$date[2]/".$this->input->get('brip').".jpg";

                     }else{
                                                 echo "http://premam.in/container/output-images/$incoming_app/".$this->input->get('brip').".jpg";
 
                     }
            }elseif($this->input->get('crip')){
                $incoming_app = $this->uri->segment(6);
                
                $date = explode("-", $this->input->get('crip'));
                     if (count($date) > 2) {
                         
                         echo "http://apps.premam.in/container/output-images/$incoming_app/"."$date[0]/$date[1]/$date[2]/".$this->input->get('crip').".jpg";

                     }else{
                                                 echo "http://apps.premam.in/container/output-images/$incoming_app/".$this->input->get('crip').".jpg";
 
                     }
            }elseif($this->input->get('drip')){
                $incoming_app = $this->uri->segment(6);
                
                $date = explode("-", $this->input->get('drip'));
                     if (count($date) > 2) {
                         
                         echo "http://witalks.com/container/output-images/$incoming_app/"."$date[0]/$date[1]/$date[2]/".$this->input->get('drip').".jpg";

                     }else{
                                                 echo "http://witalks.com/container/output-images/$incoming_app/".$this->input->get('drip').".jpg";
 
                     }
            }else{
            $date=explode("-",$app['user_id']);
            if(count($date)>2){
                echo base_url()."container/output-images/".$app['app_id']."/".$date[0]."/".$date[1]."/".$date[2]."/".$app['user_id'].".jpg";
            } else {
                echo base_url()."container/output-images/".$app['app_id']."/".$app['user_id'].".jpg";
            }     
            }
           
        }
    } ?>"/>
    <meta property="og:title" content="<?php echo $meta_tags[0]->meta_title; ?>" />
    <meta property="og:description" content="<?php echo $meta_tags[0]->meta_description; ?>" />
    <meta property="">
    <meta property="og:url" content="<?php echo (isset($_SERVER['HTTPS']) ? " https " : "http ") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI] "; ?>"/>
    <meta name="h12-site-verification" content="de88f7c8fe7b20ba413ea5fd3437883a" />
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto|Ubuntu|Tangerine|Rancho|Source+Sans+Pro:900&effect=3d|3d-float|anaglyph|brick-sign|canvas-print|crackle|decaying|destruction|distressed|distressed-wood|emboss|fire|fire-animation|fragile|grass|ice|mitosis|neon|outline|putting-green|scuffed-steel|shadow-multiple|splintered|static|stonewash|vintage|wallpaper">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/buttons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css"> </head>
    
<div id="fb-root"></div>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=487651168108939";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<body>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113925848-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113925848-1');
</script>






    
            <?php if (false) { ?>
        
        <script src='http://ts.appsrgv.com/assets/js/jquery.min.js' /></script>
            <!-- facebook like box starts -->
<!-- popup box stylings -->
<style type="text/css">
#fb-back { display: none; background: rgba(0,0,0,0.8);   width: 100%; height: 100%; position: fixed; top: 0;   left: 0; z-index: 99999;}
#fb-exit { width: 100%; height: 100%; }
.fb-box-inner { width:300px; position: relative; display:block; padding: 20px 0px 0px; margin:0 auto; text-align:center; }
#fb-close { cursor: pointer; position: absolute; top: 5px; right: 5px; font-size: 18px; font-weight:700; color: #000; z-index: 99999; display:inline-block; line-height: 18px; height:18px;width: 18px; }
#fb-close:hover { color:#06c; }
#fb-box { min-width: 340px; min-height: 360px; position: absolute; top: 50%; left: 50%; margin: -220px 0 0 -170px; -webkit-box-shadow: 0px 0px 16px #000; -moz-box-shadow: 0px 0px 16px #000; box-shadow: 0px 0px 16px #000; -webkit-border-radius: 8px;-moz-border-radius: 8px; border-radius: 8px;
background: #fff; /* pop up box bg color */
border-bottom: 40px solid #f0f0f0;  /* pop up bottom border color/size */
}
.fb-box-inner h3 { line-height: 1; margin:0 auto; text-transform:none;letter-spacing:none;
font-size: 23px!important;  /* header size */
color:#06c!important; /* header color */
}
.fb-box-inner p { line-height: 1; margin:0 auto 20px;text-transform:none;letter-spacing:none;
font-size: 13px!important; /* header size  */
color:#333!important; /* text color */
}
a.fb-link { position:relative;margin: 0 auto; display: block; text-align:center; color: #333; /* link color */
bottom: -30px;
}
#fb-box h3,#fb-box p, a.fb-link { max-width:290px; padding:0; }
</style>

<!-- facebook plugin -->
<div id='fb-back'>
<div id="fb-exit"> </div>
  <div id='fb-box'>
   <div class="fb-box-inner">
   <div id="fb-close">X</div>
<!-- edit your popup header text here -->
<h3>We like you..!! You like us ..!!</h3>

<!-- edit your fb name below -->
     <iframe allowtransparency='true' frameborder='0' scrolling='no' src='//www.facebook.com/plugins/likebox.php?href=http://www.facebook.com/witalks&width=290&height=275&colorscheme=light&show_faces=true&border_color=%23ffffff&stream=false&header=false'style='border: 0 none; overflow: hidden; width: 290px; height: 270px;text-align:center;margin:0 auto;'></iframe>
<!-- edit your supporting link here  -->
<a class="fb-link" href="http://facebook.com/witalks">Contact Us</a>  
</div>
 </div>
</div>
<!-- popup plug-in snippet  -->
<script type='text/javascript'>
//<![CDATA[
//grab user's browser info and calculates/saves first visit
jQuery.cookie = function (key, value, options) { if (arguments.length > 1 && String(value) !== "[object Object]") { options = jQuery.extend({}, options); if (value === null || value === undefined) { options.expires = -1; }
if (typeof options.expires === 'number') { var days = options.expires,  t = options.expires = new Date();  t.setDate(t.getDate() + days); } value = String(value); return (document.cookie = [encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value), options.expires ? '; expires=' + options.expires.toUTCString() : '', options.path ? '; path=' + options.path : '', options.domain ? '; domain=' + options.domain : '', options.secure ? '; secure' : ''].join('')); }
options = value || {}; var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent; return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null; };
// the pop up actions
$(function ($) {
  if ($.cookie('popup_fb') != 'yes') {
    $('#fb-back').delay(100).fadeIn("fast"); // options slow or fast
    $('#fb-close, #fb-exit').click(function () {
      $('#fb-back').stop().fadeOut("fast"); // options slow or fast
    });
 }
//initiate popup function by setting up the cookie expiring time
$.cookie('popup_fb', 'yes', { path: '/', expires: 3 });
});
//]]>
</script>
<!-- facebook like box ends -->
<?php } ?>
            
            <!-- Fixed navbar -->
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand" style="padding-top:5px;" href="<?php echo base_url(); ?>apps/home"> <img                    class="img responsive" style="margin-top:0px;width:190px;height:50px;padding-top:0px;"                    src="<?php echo base_url(); ?>assets/imgs/logo.png"/></a></div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a style='height:65px;padding-top:23px;font-size:16px;' href="<?php echo base_url(); ?>apps/home">Home</a> </li>
                            <?php for ($i = 0; $i < count($menu_names); $i++) { ?>                    <?php if($menu_names[$i]->id!=7 && $menu_names[$i]->id!=6){                        ?>                        <li class="active"><a style='height:65px;padding-top:23px;font-size:16px;'                                              href="<?php echo base_url() ?>apps/<?php echo strtolower($menu_names[$i]->name); ?>/<?php echo $menu_names[$i]->id; ?>/0"><?php echo $menu_names[$i]->name ?></a>                        </li>                        <?php                    }?>                               <?php } ?>	
                            <li class="active"> <a style='height:65px;padding-top:23px;font-size:16px;' href="<?php echo base_url() ?>apps/privacy-policy"> Privacy Policy</a> </li>
                            <li class="active"> <a style='height:65px;padding-top:23px;font-size:16px;' href="<?php echo base_url() ?>contact"> Contact Us</a> </li>
                            <?php if($this->session->userdata("user_id")!=FALSE){ ?>
                                <li class="active"><a style='height:65px;padding-top:23px;font-size:16px;' href="<?php echo base_url(); ?>apps/logout">Logout</a>
                                    <?php } ?>
                                </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </nav>
            <div class="container-fluid content">