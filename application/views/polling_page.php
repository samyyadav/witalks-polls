<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->view('header'); ?>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<style>
    .body { margin-top:20px; }
.panel-body:not(.two-col) { padding:0px }
.glyphicon { margin-right:5px; }
.glyphicon-new-window { margin-left:5px; }
.panel-body .radio,.panel-body .checkbox {margin-top: 0px;margin-bottom: 0px;}
.panel-body .list-group {margin-bottom: 0;}
.margin-bottom-none { margin-bottom: 0; }
.panel-body .radio label,.panel-body .checkbox label { display:block; }
</style>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->view('navbar'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper" style="padding-top: 0px;">
      <!-- partial:partials/_sidebar.html -->
      <?php $this->view('sidebar'); ?>
      
      <!-- partial -->
      <div class="main-panel">
          
        <div class="content-wrapper" style="padding: 0px;margin-top:-20px">
            
          <div class="row">
              
            <div class="col-lg-12 grid-margin">
                
              
              <div class="row">
                  
                  
            <?php for($i=0;$i<count($polls);$i++){?>
                
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                    
                    <h1><?php echo $polls[0]->title ?></h1>
                    <hr>
            <!-- <p style="font-size:13px;margin-right:0px;margin-left:0px;"><b>This polling is closed as per Telangana State Election Commission rule. Your voting will not be considered from now</b></p> -->
             <hr>
            
             <b style="color:red"><?php echo $polls[0]->comment_vote ?>
             </b>            
             <div class="" style="width:110%;margin-left:-10px;padding-right:0px;">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- PollsWitalks -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-9262602609645935"
     data-ad-slot="7991451803"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
            </div>
            <br>
                    
                    <a style="font-size:18px"><?php echo $polls[0]->description ?></a>
                    
                  </div>
                  
                  <?php }?>
                  <hr>
                  <p style="font-size:10px;margin-right:10px;margin-left:10px">Witalks Polls is not endorsed, sponsored, administered or associated with or by Facebook. Facebook is no way associated with Witalks Polls.</p>
                  <hr>
                  
                  <div class="row">
                      
        <div class="col-md-12" style="padding-left:20px;padding-right:20px;">
            <form method="post" action="<?php echo base_url()."poll-redirect/".$polls[0]->id ?>">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-arrow-right"></span><?php echo $polls[0]->title ?>
                    </h3>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <?php for($j=0;$j<count($options);$j++){  ?>
                  

                  
                        <li class="list-group-item">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="option" value="<?php echo $options[$j]->id; ?>">
                                    
                                    <?php echo $options[$j]->option; ?>
                                </label>
                            </div>
                        </li>
                        
                        <?php  } ?>
                    </ul>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        Vote & View Result</button>
                        
                        
                    
                    <!--<a href="<?php echo base_url()."poll-result/".$polls[0]->id?>">View Result</a>-->
                   
                </div>
                 
                
            </div>
            </form>
        </div>
        <div class="" style="padding-right:0px;padding-left:10px;">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- pollswitalks336x280 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-9262602609645935"
     data-ad-slot="4337297779"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
            </div>
        </div>
        </div>
                  
                  
                </div>
                <div class="container">
    
                
                
              </div>
            </div>
              
            
            </div>
          </div>
          
          <!--<div class="template-demo">
            <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" class="btn btn-primary">1</button>
                          <button type="button" class="btn btn-primary">2</button>
                          <button type="button" class="btn btn-primary">3</button>
                        </div>
                        </div>
                        -->
          
        </div>
        
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <?php $this->view('footer'); ?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  
</body>

</html>