<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
  <?php $this->view('header'); ?>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<style>

    .body { margin-top:0px; }
.panel-body:not(.two-col) { padding:0px }
.glyphicon { margin-right:0px; }
.glyphicon-new-window { margin-left:0px; }
.panel-body .radio,.panel-body .checkbox {margin-top: 0px;margin-bottom: 0px;}
.panel-body .list-group {margin-bottom: 0;}
.margin-bottom-none { margin-bottom: 0; }
.panel-body .radio label,.panel-body .checkbox label { display:block; }
.link_copied {
    background: #a94442;
    color: white;
    padding: 5px;
    border-radius: 18px;
}
</style>
<div id="fb-root"></div>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=487651168108939";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php $this->view('navbar'); ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper" style="padding-top: 0px;">
      <!-- partial:partials/_sidebar.html -->
      <?php if($this->agent->is_mobile()){ ?>
          <?php //$this->view('sidebar'); ?>
     <?php }?>
      
      <!-- partial -->
      <div class="main-panel">
          
        <div class="content-wrapper" style="padding: 0px;margin-top:-20px">
            
          <div class="row">
              
            <div class="col-lg-12 grid-margin">
              <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card-body">
            <h3 style="color:red">Thank You for Voting!</h3>
            
             <b style="color:black"> <?php echo $polls[0]->comments ?></b>            
             <br><br>
             <div class="panel panel-primary" style="padding-left:-10px;padding-right:-10px;">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-arrow-right"></span><?php echo $polls[0]->title ?>
                    </h3>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="container">
  
      <?php for($j=0;$j<count($poll_result);$j++){ ?>
      <?php echo $poll_result[$j]->option ?> <b style="color:red"><?php echo round((($poll_result[$j]->attempts_count/$total_count)*100))."%(".($poll_result[$j]->attempts_count).")" ?></b>
      <div class="progress">
        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo (($poll_result[$j]->attempts_count/$total_count)*100)."%" ?>">
      
      </div>
      
    </div>
    
    
    <?php } ?>
    Total Votes : <b style="color:red"><?php echo $total_count?></b><br>
    <b>Share this Poll with your friends</b>
  
</div>
                        </li>
                    </ul>
                </div>
                <br>
                

            </div>
            <div style="margin-top: 9px;"></div>
                    <div class="col-md-6 col-sm-12 col-xs-12 share-btn-col-md btn-mrgn2">
<a href="whatsapp://send?text=<?php echo $polls[0]->title ?> Vote now <?php echo base_url().strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $polls[0]->title)))."/".$poll_id ?>" class="btn btn-default btn-block" style="font-size: large; background: #128c7e; color: white;">
<img src="<?php echo base_url()?>assets/imgs/whatsapp1.png" height=20 width=20>&nbsp;
WhatsApp </a>
</div>
<div class="col-md-6 col-sm-12 col-xs-12 share-btn-col-md btn-mrgn2">
<a href="http://www.facebook.com/sharer/sharer.php?app_id=1174679189321277&sdk=joey&u=<?php echo base_url().strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $polls[0]->title)))."/".$poll_id ?>&display=popup&ref=plugin&src=share_button"><button class="btn btn-default btn-block" id="share_btn_fb" style="font-size: large; background: #3b5998; color: white;">
<img src="<?php echo base_url()?>assets/imgs/facebook1.png" height=20 width=20>&nbsp;
Facebook </button>
</a>
</div>
<div class="col-md-6 col-sm-12 col-xs-12 share-btn-col-md btn-mrgn2">
<a href="fb-messenger://share?link=<?php echo base_url().strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $polls[0]->title)))."/".$poll_id ?>" class="btn btn-default btn-block" style="font-size: large; background: #0084ff; color: #fff;">
<img src="<?php echo base_url()?>assets/imgs/messenger1.png" height=25 width=25>&nbsp;
Messenger </a>
</div>
<div class="col-md-6 col-sm-12 col-xs-12 share-btn-col-md btn-mrgn2">
<a href="https://twitter.com/share?url=<?php echo base_url().strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $polls[0]->title)))."/".$poll_id ?>Vote now " class="btn btn-default btn-block" style="font-size: large; background: #1da1f2; color: white;">
<img src="<?php echo base_url()?>assets/imgs/twitter1.png" height=28 width=28>&nbsp;
Twitter </a>
</div>

<br>

             <div class="row">
                 <br><br>
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- pollswitalks336x280 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-9262602609645935"
     data-ad-slot="4337297779"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
            </div>
            <br>
            
                </div>
                <div class="row">
            <div class="col-lg-12 grid-margin"style="padding: 0px;">
                
              
              
            <?php if(get_cookie('poll_id')==$poll_id){?>
            <div class="row ">
                
            <div class="col-12" style="margin-left:15px">
                
              <span class="align-items-center">
                
                
                
                
                
              </span>
            </div>
            </div>
            <?php }?>
            <br>
            
        </div>
        </div>
        
        
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">

<br>
<div class="row">
<div class="fb-comments" data-href="<?php echo base_url().strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(" ", "-", $polls[0]->title)))."/".$poll_id ?>" data-width="728" data-numposts="10"></div>
            </div>
            <div class="row">
            <?php for($i=0;$i<count($suggestion_polls);$i++){?>
                
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card" style="padding: 0px;" >
              <div class="card card-statistics">
                  
                <div class="card-body">
                    Votes:<b style="color:red"> <?php echo $suggestion_poll_total_count[$i]?></b>
                    <br>
                  <div class="clearfix">
                    <a href="<?php echo base_url()?>polls/<?php echo $suggestion_polls[$i]->slug ?>/<?php echo $suggestion_polls[$i]->id?>">
                    <img class="img-responsive" src="<?php echo base_url()?>container/input-images/<?php echo $suggestion_polls[$i]->id?>/cover_pic.jpg"></img>
                    
                  </div>
                  <p class="mt-3 mb-0">
                     <h4><?php echo $suggestion_polls[$i]->title?></h4>
                  </p>
                </div>
                </a>
              </div>
            </div>
            <?php }?>  
            </div>
          </div>
          
          <!--<div class="template-demo">
            <div class="btn-group" role="group" aria-label="Basic example">
                          <button type="button" class="btn btn-primary">1</button>
                          <button type="button" class="btn btn-primary">2</button>
                          <button type="button" class="btn btn-primary">3</button>
                        </div>
                        </div>
                        -->
          
        </div>
                </div>
              </div>
            </div>
            
        </div>
                  
                  
                </div>
                <div class="container">
    
                
                
              </div>
            </div>
              
            
            </div>
          </div>
          
          
          
        </div>
        
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <?php $this->view('footer'); ?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  
</body>

</html>