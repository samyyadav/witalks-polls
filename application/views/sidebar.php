<nav class="sidebar sidebar-offcanvas" id="sidebar" style="width:220px;">
        <ul class="nav">
          <li class="nav-item nav-profile">
            
          </li>
          
          <li class="nav-item">
            <!-- <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">తెలంగాణ </span>
              <i class="menu-arrow"></i>
            </a> -->
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>view-polls/1">మెదక్</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>view-polls/2">నల్గొండ</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>view-polls/3">వరంగల్</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>view-polls/4">నిజామాబాద్</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>view-polls/5">కరీంనగర్</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>view-polls/6">ఆదిలాబాద్</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>view-polls/7">రంగారెడ్డి</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>view-polls/8">మహబూబ్ నగర్</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>view-polls/9">ఖమ్మం </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>view-polls/10">హైదరాబాద్</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="menu-icon mdi mdi-restart"></i>
              <span class="menu-title"><b>ఆంధ్రప్రదేశ్</b></span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>view-polls/11"> Andhra Pradesh </a>
                </li>
                
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url()?>view-polls/12">
              <i class="menu-icon mdi mdi-backup-restore"></i>
              <span class="menu-title"><b>Other</b></span>
            </a>
          </li>
		  <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url()?>view-polls/13">
              <i class="menu-icon mdi mdi-trophy"></i>
              <span class="menu-title"><b>IPL</b></span>
            </a>
          </li>
        </ul>
      </nav>