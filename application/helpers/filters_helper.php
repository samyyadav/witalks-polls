<?php
if(!function_exists('freshblue'))
{
function freshblue($imagePath,$outpath){

   $image = imagecreatefromjpeg($imagePath);
    imagefilter($image, IMG_FILTER_CONTRAST, -5);
    imagefilter($image, IMG_FILTER_COLORIZE, 20, 0, 80, 60);
    imagejpeg($image, $outpath);
    imagedestroy($image);
}
}
if(!function_exists('gray'))
{
function gray($imagePath,$outpath){
    $image = imagecreatefromjpeg($imagePath);
    imagefilter($image, IMG_FILTER_CONTRAST, -60);
    imagefilter($image, IMG_FILTER_GRAYSCALE);
    imagejpeg($image, $outpath);
    imagedestroy($image);
}
}
if(!function_exists('sepia2'))
{
function sepia2($imagePath,$outpath){
    $image = imagecreatefromjpeg($imagePath);
    imagefilter($image, IMG_FILTER_GRAYSCALE);
    imagefilter($image, IMG_FILTER_BRIGHTNESS, -10);
    imagefilter($image, IMG_FILTER_CONTRAST, -20);
    imagefilter($image, IMG_FILTER_COLORIZE, 60, 30, -15);
    imagejpeg($image, $outpath);
    imagedestroy($image);
}
}
if(!function_exists('aqua'))
{
function aqua($imagePath,$outpath){
    $image = imagecreatefromjpeg($imagePath);
    imagefilter($image, IMG_FILTER_COLORIZE, 0, 70, 0, 30);
    imagejpeg($image, $outpath);
    imagedestroy($image);
}
}
if(!function_exists('merge2'))
{
function merge2($filename_x, $filename_y, $filename_result, $left ,$right ) {
 // Get dimensions for specified images
 list($width_x, $height_x) = getimagesize($filename_x);
 list($width_y, $height_y) = getimagesize($filename_y);
 // Create new image with desired dimensions
 $image = imagecreatetruecolor($width_x, $height_x);
 // Load images and then copy to destination image
 $image_x = imagecreatefromjpeg($filename_x);
 $image_y = imagecreatefromjpeg($filename_y);
 imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);                     //  top, left, border,border
 imagecopy($image, $image_y, $left, $right ,0, 0, $width_y, $height_y);
 // Save the resulting image to disk (as JPEG)
 imagejpeg($image, $filename_result,100);
 // Clean up
 imagedestroy($image);
 imagedestroy($image_x);
 imagedestroy($image_y);

}
}
if(!function_exists('mergeIplSemis'))
{
    function mergeIplSemis($filename_x, $filename_y, $filename_result, $left ,$right,$teams) {
        // Get dimensions for specified images
        list($width_x, $height_x) = getimagesize($filename_x);
        list($width_y, $height_y) = getimagesize($filename_y);
        // Create new image with desired dimensions
        $image = imagecreatetruecolor($width_x, $height_x);
        // Load images and then copy to destination image
        $image_x = imagecreatefromjpeg($filename_x);
        $image_y = imagecreatefromjpeg($filename_y);
        imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);                     //  top, left, border,border
        imagecopy($image, $image_y, $left, $right ,0, 0, $width_y, $height_y);
        // Allocate A Color For The Text
        $white = imagecolorallocate($image, 255, 255, 255);

        $font_path = 'images/FbEmotions/RobotoCondensed-Bold.ttf';

        $keys = array_keys($teams);
        imagettftext($image, 25, 0, 300, 100, $white , $font_path, strtoupper($teams[$keys[0]]));
        imagettftext($image, 25, 0, 300, 140, $white , $font_path, strtoupper($teams[$keys[1]]));
        imagettftext($image, 25, 0, 300, 180, $white , $font_path, strtoupper($teams[$keys[2]]));
        imagettftext($image, 25, 0, 300, 220, $white , $font_path, strtoupper($teams[$keys[3]]));
        // Save the resulting image to disk (as JPEG)
        imagejpeg($image, $filename_result,100);
        // Clean up
        imagedestroy($image);
        imagedestroy($image_x);
        imagedestroy($image_y);

    }
}
?>