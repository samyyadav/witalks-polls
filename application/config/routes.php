<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['apps/home']='Apps/index';
$route['(:num)']='Apps/view_single_app_short/$1';
$route['app']='Apps/after_login';


$route['index'] = 'Apps/index_page/';
$route['static'] = 'Apps/static_page/';
$route['polls/(:any)/(:num)'] = 'Apps/polling_page/$1/$2';
$route['poll-redirect/(:num)'] = 'Apps/poll_redirect/$1';
$route['poll-result/(:num)'] = 'Apps/poll_result/$1';
$route['lang/(:any)/(:num)'] = 'Apps/polls_by_category/$1/$2';
$route['poll-result-copy/(:num)'] = 'Apps/poll_result_copy/$1';
$route['view-polls/(:num)'] = 'Apps/view_polls_by_division/$1';

$route['update-images-db/(:num)/(:num)'] = 'Apps/update_images_db/$1/$2';

$route['upload'] = 'Apps/upload_index/';
$route['do-upload'] = 'Apps/do_upload/';

$route['admin'] = 'Apps/admin_login/';
$route['admin-home'] = 'Apps/admin_index/';
$route['add-polls'] = 'Apps/add_polls/';
$route['add-polls-db'] = 'Apps/add_polls_db/';
$route['add-options'] = 'Apps/add_options/';
$route['add-options-db'] = 'Apps/add_options_db/';
$route['update-polls-index/(:any)'] = 'Apps/update_polls_index/$1';
$route['update-polls/(:any)/(:num)'] = 'Apps/update_polls/$1/$2';



$route['default_controller'] = 'Apps';

$route['contact']  = 'Apps/careers';
$route['apps/privacy-policy'] = 'Apps/privacy_policy';

$route['translate_uri_dashes'] = FALSE;